CREATE PROC dbo.SEGS14630_SPS (
			@proposta_id NUMERIC(11,0),
			@evento_id INT,
			@cobertura_id INT)
			
AS

/*
	Sergio Ricardo (nova consultoria) - 26/12/2019
	Demanda: 00210408-esteira_flash	
	Descri��o: procedure para validar se o aviso de sinistro � passivel da esteira flash.
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*   
  DECLARE @proposta_id NUMERIC(11,0),
		  @evento_id INT,
		  @cobertura_id INT
		  
  SELECT @proposta_id = 50575976,
		 @evento_id = 300,
		 @cobertura_id = 498

  BEGIN TRAN
    IF @@TRANCOUNT > 0 
		 EXEC seguros_db.dbo.SEGS14630_SPS @proposta_id, @evento_id, @cobertura_id
    ELSE 
		SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	BEGIN TRY

		 SELECT COUNT(s.sinistro_parametro_chave_id) 'Flash'
		   FROM seguros_db.dbo.proposta_tb p WITH (NOLOCK)
		  INNER JOIN seguros_db.dbo.sinistro_parametro_chave_tb s WITH (NOLOCK) 
			 ON p.produto_id = s.produto_id
			AND p.ramo_id = s.ramo_id
		  WHERE p.proposta_id = @proposta_id
			AND s.evento_sinistro_id = @evento_id
			AND s.tp_cobertura_id = @cobertura_id   
			AND s.dt_fim_vigencia IS NULL
			
		 RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END	