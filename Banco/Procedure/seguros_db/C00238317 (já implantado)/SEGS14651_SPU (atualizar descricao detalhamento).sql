CREATE PROC dbo.SEGS14651_SPU (
			@evento_id INT)
			
AS

/*
	Sergio Ricardo (nova consultoria) - 30/01/2020
	Demanda: 00210408-esteira_flash	
	Descri��o: procedure para Atualizar ultima linha de detalhamento do aviso de sinistro
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*   
  DECLARE @evento_id INT
		  
  SELECT @evento_id = 63569617

  BEGIN TRAN
    IF @@TRANCOUNT > 0 
		 EXEC seguros_db.dbo.SEGS14651_SPU @evento_id
    ELSE 
		SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	BEGIN TRY

		 DECLARE @seq_detalhe INT
		 
		 SELECT @seq_detalhe = MAX(a.seq_detalhe) 
		   FROM seguros_db.dbo.evento_segbr_sinistro_detalhe_atual_tb a WITH (NOLOCK)
		  WHERE a.evento_id = @evento_id
		  
		 --UPDATE a
		 --   SET a.descricao = 'CLASSIFICA��O DO SINISTRO' 
		 --  FROM seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb a
		 -- WHERE a.evento_id = @evento_id
		 --   AND a.seq_detalhe = @seq_detalhe 

		SELECT @seq_detalhe															   
			
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END	
