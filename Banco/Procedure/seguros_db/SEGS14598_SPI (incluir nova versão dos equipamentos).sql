CREATE PROCEDURE dbo.SEGS14598_SPI ( @usuario as varchar(20)
)
AS
/*
	Bruno schoralick (nova consultoria) - 26/12/2019
	Demanda: 00210408-esteira_flash	
	Descri��o: procedure para incluir nova versao de equipamentos importados.
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
    IF OBJECT_ID('tempdb..##sinistro_equipamento_importado') IS NOT NULL
    BEGIN
	    DROP TABLE ##sinistro_equipamento_importado
    END

    CREATE TABLE ##sinistro_equipamento_importado (
	    tipo_bem VARCHAR(60) NOT NULL
	    ,modelo VARCHAR(60) NOT NULL
	    ,vl_perda_total NUMERIC(15, 2) NULL
	    ,vl_reparo NUMERIC(15, 2) NULL
	    )

    INSERT INTO ##sinistro_equipamento_importado (
	    tipo_bem
	    ,modelo
	    ,vl_perda_total
	    ,vl_reparo
	    )
    VALUES (
	    'teste bem'
	    ,'teste modelo'
	    ,'10'
	    ,'5.35'
	    )

    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14598_SPI @usuario = 'TESTE' ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)
	DECLARE @dt_operacional AS SMALLDATETIME

	SELECT @dt_operacional = dt_operacional
	FROM seguros_db.dbo.parametro_geral_tb WITH (NOLOCK)
	-- (fim) Declara��o e tratamento de variaveis 

	-----------
	BEGIN TRY
	
    DECLARE @proxima_versao AS INT

    SELECT @proxima_versao = ISNULL(MAX(a.num_versao),0) + 1
    FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)

    UPDATE a
    SET a.dt_fim_vigencia = @dt_operacional
	    ,a.dt_alteracao = GETDATE()
    FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)
    WHERE a.dt_fim_vigencia IS NULL

    INSERT INTO seguros_db.dbo.sinistro_equipamento_importacao_tb (
	    tipo_bem
	    ,modelo
	    ,vl_perda_total
	    ,vl_reparo
	    ,dt_inicio_vigencia
	    ,num_versao
	    ,usuario
	    ,dt_inclusao
	    )
    SELECT tipo_bem
	    ,modelo
	    ,vl_perda_total
	    ,vl_reparo
	    ,@dt_operacional --dt_inicio_vigencia 
	    ,@proxima_versao --num_versao
	    ,@usuario
	    ,GETDATE() --dt_inclusao
    FROM ##sinistro_equipamento_importado
	
		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO

			
	