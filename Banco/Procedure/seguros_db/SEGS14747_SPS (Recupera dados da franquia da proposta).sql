CREATE PROCEDURE dbo.SEGS14747_SPS (@proposta_id AS INT
  ,@tp_cobertura_id AS INT
  ,@ramo_id AS INT
  ,@produto_id AS INT
  )
/*
	(nova consultoria) - 08/05/2018
	Demanda: 00210408-esteira_flash-fase2
	Descri��o: procedure para recuperar os dados da franquia da proposta.
	Banco: n/d	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
		IF OBJECT_ID('tempdb..#dados_franquia_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #dados_franquia_tmp
		END
		  
		CREATE TABLE #dados_franquia_tmp (id INT IDENTITY(1,1) NOT NULL PRIMARY KEY
		,texto VARCHAR(100) NULL
		,fat_franquia DECIMAL(10,2) NULL
		,val_min_franquia DECIMAL(10,2) NULL
		)
		  
	  DECLARE @proposta_id AS INT
	  ,@tp_cobertura_id AS INT
	  ,@ramo_id AS INT
	  ,@produto_id AS INT
	  
	  SELECT TOP 1 @proposta_id = proposta_id
		,@tp_cobertura_id = tp_cobertura_id
		,@ramo_id = ramo_id 
		,@produto_id = produto_id
	  FROM seguros_db.dbo.escolha_tp_cob_generico_tb a WITH (NOLOCK)
	  WHERE CHARINDEX('$', texto_franquia) > 0
	  
	  EXEC SEGS14747_SPS
	  @proposta_id 
	  ,@tp_cobertura_id 
	  ,@ramo_id 
	  ,@produto_id
	  
	  SELECT * FROM #dados_franquia_tmp
  ROLLBACK
*/	
AS
BEGIN
	SET NOCOUNT ON
	
	BEGIN TRY

	  DECLARE @perc NVARCHAR(60)
		  ,@valor NVARCHAR(60)
		  ,@texto_franquia as VARCHAR(60)
		  ,@fat_franquia AS NUMERIC(5,4)
		  ,@val_min_franquia AS NUMERIC(15,2)
	    ,@fat_convert as NUMERIC(10,4)
      ,@CharInvalido SMALLINT	
      ,@resultado varchar(60)
		  
    SELECT @texto_franquia = ISNULL(a.texto_franquia, '')
          ,@fat_franquia = ISNULL(a.fat_franquia,0)
          ,@val_min_franquia = ISNULL(a.val_min_franquia,0)
	  FROM seguros_db.dbo.escolha_tp_cob_generico_tb a WITH (NOLOCK)
	  WHERE a.proposta_id = @proposta_id
	  AND a.tp_cobertura_id = @tp_cobertura_id
      AND a.ramo_id = @ramo_id
	  AND a.produto_id = @produto_id
	  AND a.dt_fim_vigencia_esc IS NULL
  		
    IF NOT @texto_franquia = ''
    BEGIN
      --BUSCANDO PERCENTUAL
      IF ISNULL(CHARINDEX('%', @texto_franquia),0) > 0
      BEGIN    
        SELECT @perc = rtrim(ltrim(LEFT(@texto_franquia,CHARINDEX('%', @texto_franquia)-1)))    
        set @resultado = @perc    
        SET @CharInvalido = PATINDEX('%[^0-9].,%', @Resultado)
        WHILE @CharInvalido > 0
        BEGIN
          SET @Resultado = STUFF(@Resultado, @CharInvalido, 1, '')
          SET @CharInvalido = PATINDEX('%[^0-9].,%', @Resultado)
        END
        SET @perc = @Resultado        
        SET @perc = REPLACE(@perc,'.','')
        SET @perc = REPLACE(@perc,',','.')   
        SET @fat_convert = CONVERT(DECIMAL(10,4), @perc)
        SET @fat_franquia = @fat_convert / 100
      END

      --BUSCANDO VALOR  
      IF isnull(CHARINDEX('$', @texto_franquia),0) > 0
      BEGIN
        SELECT @valor = rtrim(ltrim(SUBSTRING (@texto_franquia,CHARINDEX('$', @texto_franquia)+1,LEN(@texto_franquia))))
        set @resultado = @valor    
        SET @CharInvalido = PATINDEX('%[^0-9]%.,', @Resultado)
        WHILE @CharInvalido > 0
        BEGIN
          SET @Resultado = STUFF(@Resultado, @CharInvalido, 1, '')
          SET @CharInvalido = PATINDEX('%[^0-9]%.,', @Resultado)
        END
        SET @valor = @Resultado      
        SET @valor = REPLACE(@valor,'.','')
        SET @valor = REPLACE(@valor,',','.')
        SET @val_min_franquia = CONVERT(DECIMAL(15,2), @valor) 
      END
    END  
        
    --Resultado
    INSERT INTO #dados_franquia_tmp (fat_franquia ,val_min_franquia)
      SELECT @fat_franquia ,@val_min_franquia     
    
		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO



