CREATE PROCEDURE dbo.SEGS14614_SPS (
	@tipo_bem AS VARCHAR(60) = NULL
	)
AS
/*
	ntendencia - 07/01/2020
	Demanda: 00210408-esteira_flash
	Descri��o: procedure para retornar o tipo de bens ou modelo da tabela de equipamneto
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14614_SPS @tipo_bem = NULL ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

  DECLARE @versao_atual AS INT

  SELECT @versao_atual = ISNULL(MAX(a.num_versao),0) 
  FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)
  WHERE dt_fim_vigencia is NULL

	BEGIN TRY
    IF ISNULL(@tipo_bem, '') = ''
    BEGIN
      SELECT DISTINCT a.tipo_bem
      FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)
      WHERE num_versao = @versao_atual      
    END
    ELSE		
    BEGIN
      SELECT RIGHT('000000000' + CAST(a.equipamento_id as VARCHAR),9) + ' - ' + a.modelo
      FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)
      WHERE num_versao = @versao_atual
      AND a.tipo_bem = @tipo_bem
    END		
				
		SET NOCOUNT OFF

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO

			