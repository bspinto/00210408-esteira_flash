CREATE PROCEDURE dbo.SEGS14619_SPI ( @sinistro_id numeric(11,0), @usuario as varchar(20)
)
AS
/*
	Ricardo Morais (nova consultoria) - 16/01/2020
	Demanda: 00210408-esteira_flash	
	Descrição: procedure para incluir equipamentos inseridos na abertura do sinistro na tabela fisica.
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
		IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL BEGIN DROP TABLE #Equipamento END
		CREATE TABLE #Equipamento (Codigo INT  ,TipoBem VARCHAR(60)  ,Modelo VARCHAR(60)  ,Dano INT  ,Orcamento INT  ,Valor NUMERIC(15,2)  ,Laudo INT  ,NomeAssistencia VARCHAR(60)  ,TelAssistencia VARCHAR(15)  ,Descricao VARCHAR(255))

		INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
		SELECT '000000037', 'Ar Condicionado', 'Acima de 19000BTUS', '1', '1', '249.00', '1', 'Ar teC', '(32) 34414-444_', 'dano no ar'
		UNION
		SELECT '000000018', 'Baba Eletronica', 'Sem Camera', '2', '1', '251.00', '1', 'baby tec', '(32) 98888-4444', 'danos na baba'
		UNION
		SELECT '000000023', 'Ferro de Passar', 'Ferro de Passar', '2', '1', '200.00', '1', 'FERRO TEC', '(32) 32131-2313', 'teste novo item'

    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14619_SPI @sinistro_id = 14201922558, @usuario = 'TESTE' ELSE SELECT 'Erro. A transação não foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	BEGIN TRY
    

    INSERT INTO seguros_db.dbo.sinistro_equipamento_tb (
		 sinistro_id
		,equipamento_id
		,tipo_bem 
		,modelo 
		,tipo_dano 
		,possui_laudo
		,nome_assistencia 
		,tel_assistencia 
		,possui_orcamento
		,valor
		,desc_dano 
		,dt_inclusao 
		,usuario
	    )
    SELECT 
	    @sinistro_id
		,codigo
		,tipoBem
		,modelo 
		,dano
		,Laudo
		,NomeAssistencia 
		,TelAssistencia  
		,orcamento
		,valor
		,Descricao 
	    ,GETDATE()
	    ,@usuario
    FROM #Equipamento
		
		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO

			
	