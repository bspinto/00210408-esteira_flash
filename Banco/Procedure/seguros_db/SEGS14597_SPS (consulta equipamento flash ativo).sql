CREATE PROCEDURE dbo.SEGS14597_SPS 
AS
/*
	Bruno schoralick (nova consultoria) - 26/12/2019
	Demanda: 00210408-esteira_flash	
	Descri��o: procedure para listar equipamentos ativos.
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14597_SPS ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

  DECLARE @versao_atual AS INT

  SELECT @versao_atual = ISNULL(MAX(a.num_versao),0) 
  FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)
  WHERE dt_fim_vigencia is NULL

	-----------
	BEGIN TRY
	
	  SELECT tipo_bem
  		,modelo
  		,vl_perda_total 
  		,vl_reparo 
		  ,dt_inicio_vigencia 
		  ,num_versao 
		  ,usuario 
		  ,dt_inclusao 
		  ,dt_alteracao 
		FROM seguros_db.dbo.sinistro_equipamento_importacao_tb WITH (NOLOCK)
		WHERE num_versao = @versao_atual
		SET NOCOUNT OFF

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO

			
	