/*
	Bruno Schoralick Pinto (ntendencia) - 26/12/2019
	Demanda: 00210408-esteira_flash
	Nome do script: script_create_sinistro_equipamento_importacao_tb
	Descri��o: Script para criar tabela de importa��o de equipamentos
	Objetivo: create
	Banco: seguros_db
	Ambiente: AB e ABS
*/
IF OBJECT_ID('dbo.sinistro_equipamento_importacao_tb', 'U') IS NULL
BEGIN
	CREATE TABLE dbo.sinistro_equipamento_importacao_tb (
		equipamento_id INT IDENTITY(1, 1) NOT NULL
		,tipo_bem VARCHAR(60) NOT NULL
		,modelo VARCHAR(60) NOT NULL
		,vl_perda_total NUMERIC(15, 2) NULL
		,vl_reparo NUMERIC(15, 2) NULL
		,dt_inicio_vigencia SMALLDATETIME NOT NULL
		,dt_fim_vigencia SMALLDATETIME NULL
		,num_versao INT NOT NULL
		,dt_inclusao dbo.ud_dt_inclusao NOT NULL
		,dt_alteracao dbo.ud_dt_alteracao NULL
		,usuario dbo.ud_usuario NOT NULL
		,lock TIMESTAMP NOT NULL
		,CONSTRAINT PK_sinistro_equipamento_importacao PRIMARY KEY CLUSTERED (equipamento_id ASC)
		)
END
GO


