/*
	Ricardo Morais (ntendencia) - 15/01/2020
	Demanda: 00210408-esteira_flash
	Nome do script: 08 - script_criar_tabela_sinistro_equipamento_tb
	Descri��o: Script para criar tabela onde ser�o gravados os equipamentos na abertura do sinistro
	Objetivo: create
	Banco: seguros_db
	Ambiente: AB e ABS
*/
IF OBJECT_ID('dbo.sinistro_equipamento_tb', 'U') IS NULL
BEGIN
	CREATE TABLE dbo.sinistro_equipamento_tb (
		sinistro_equipamento_id INT IDENTITY(1, 1) NOT NULL
		,sinistro_id [numeric](11, 0) NULL
		,equipamento_id INT NOT NULL
		,tipo_bem VARCHAR(60) NOT NULL
		,modelo VARCHAR(60) NOT NULL
		,tipo_dano INT NOT NULL
		,possui_laudo INT NOT NULL
		,nome_assistencia VARCHAR(60) NULL
		,tel_assistencia VARCHAR(20) NULL
		,possui_orcamento INT NOT NULL
		,valor NUMERIC(15,2) NULL
		,desc_dano VARCHAR(255) NULL
		,dt_inclusao dbo.ud_dt_inclusao NOT NULL
		,dt_alteracao dbo.ud_dt_alteracao NULL
		,usuario dbo.ud_usuario NOT NULL
		,lock TIMESTAMP NOT NULL
		,CONSTRAINT PK_sinistro_equipamento PRIMARY KEY CLUSTERED (sinistro_equipamento_id ASC)
		)
END
GO

IF (OBJECT_ID('FK_sinistro_equipamento_importacao_x_sinistro_equipamento', 'F') IS NULL)
BEGIN
  ALTER TABLE [dbo].[sinistro_equipamento_tb]
	  WITH CHECK ADD CONSTRAINT [FK_sinistro_equipamento_importacao_x_sinistro_equipamento] FOREIGN KEY ([equipamento_id]) REFERENCES [dbo].[sinistro_equipamento_importacao_tb]([equipamento_id])


  ALTER TABLE [dbo].[sinistro_equipamento_tb] CHECK CONSTRAINT [FK_sinistro_equipamento_importacao_x_sinistro_equipamento]
END  
GO


