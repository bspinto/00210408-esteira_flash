/*-----------------------------------------------------------------------------------------------------            
	AUTOR: SERGIO RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: sinistro_classificacao_parametro_tb
	OBJETIVO: Script para criar tabela que ira gravar o resultado da classificação do sinistro
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  

IF OBJECT_ID('sinistro_classificacao_parametro_tb', 'U') IS NULL
BEGIN
CREATE TABLE dbo.sinistro_classificacao_parametro_tb(
	sinistro_classificacao_parametro_id INT IDENTITY(1,1) NOT NULL,
	tp_sinistro_parametro_id INT NOT NULL,
	sinistro_id NUMERIC(11,0) NOT NULL,
	resultado INT NOT NULL,
	dt_inclusao dbo.UD_dt_inclusao NOT NULL,
	dt_alteracao dbo.UD_dt_alteracao NULL,
	lock TIMESTAMP NOT NULL,
	usuario dbo.UD_usuario NULL,
 CONSTRAINT [PK_sinistro_classificacao_parametro] PRIMARY KEY CLUSTERED 
(
	sinistro_classificacao_parametro_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
