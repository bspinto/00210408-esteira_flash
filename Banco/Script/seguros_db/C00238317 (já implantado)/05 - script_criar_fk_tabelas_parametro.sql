/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: script_criar_fk_tabelas_parametro
	OBJETIVO: Script para criar as chaves estrangeiras entre as tabelas criadas para o projeto
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  



IF (OBJECT_ID('fk_tp_sinistro_parametro_x_sinistro_parametro_chave', 'F') IS NULL)
BEGIN
  ALTER TABLE [dbo].[sinistro_parametro_chave_tb]
	  WITH CHECK ADD CONSTRAINT [FK_tp_sinistro_parametro_X_sinistro_parametro_chave] FOREIGN KEY ([tp_sinistro_parametro_id]) REFERENCES [dbo].[tp_sinistro_parametro_tb]([tp_sinistro_parametro_id])


  ALTER TABLE [dbo].[sinistro_parametro_chave_tb] CHECK CONSTRAINT [FK_tp_sinistro_parametro_X_sinistro_parametro_chave]
END  

IF (OBJECT_ID('FK_produto_X_sinistro_parametro_chave', 'F') IS NULL)
BEGIN
ALTER TABLE [dbo].[sinistro_parametro_chave_tb]  WITH CHECK ADD  CONSTRAINT [FK_produto_X_sinistro_parametro_chave] FOREIGN KEY([produto_id]) REFERENCES [dbo].[produto_tb] ([produto_id])


ALTER TABLE [dbo].[sinistro_parametro_chave_tb] CHECK CONSTRAINT [FK_produto_X_sinistro_parametro_chave]
END


IF (OBJECT_ID('FK_ramo_X_sinistro_parametro_chave', 'F') IS NULL)
BEGIN
ALTER TABLE [dbo].[sinistro_parametro_chave_tb]  WITH CHECK ADD  CONSTRAINT [FK_ramo_X_sinistro_parametro_chave] FOREIGN KEY([ramo_id]) REFERENCES [dbo].[ramo_tb] ([ramo_id])


ALTER TABLE [dbo].[sinistro_parametro_chave_tb] CHECK CONSTRAINT [FK_ramo_X_sinistro_parametro_chave]
END 


IF (OBJECT_ID('FK_evento_sinistro_X_sinistro_parametro_chave', 'F') IS NULL)
BEGIN
ALTER TABLE [dbo].[sinistro_parametro_chave_tb]  WITH CHECK ADD  CONSTRAINT [FK_evento_sinistro_X_sinistro_parametro_chave] FOREIGN KEY([evento_sinistro_id]) REFERENCES [dbo].[evento_sinistro_tb] ([evento_sinistro_id])


ALTER TABLE [dbo].[sinistro_parametro_chave_tb] CHECK CONSTRAINT [FK_evento_sinistro_X_sinistro_parametro_chave]
END

IF (OBJECT_ID('FK_tp_cobertura_X_sinistro_parametro_chave', 'F') IS NULL)
BEGIN
ALTER TABLE [dbo].[sinistro_parametro_chave_tb]  WITH CHECK ADD  CONSTRAINT [FK_tp_cobertura_X_sinistro_parametro_chave] FOREIGN KEY([tp_cobertura_id]) REFERENCES [dbo].[tp_cobertura_tb] ([tp_cobertura_id])


ALTER TABLE [dbo].[sinistro_parametro_chave_tb] CHECK CONSTRAINT [FK_tp_cobertura_X_sinistro_parametro_chave]
END


IF (OBJECT_ID('FK_sinistro_parametro_chave_X_sinistro_parametro_chave_regra', 'F') IS NULL)
BEGIN
  ALTER TABLE [dbo].[sinistro_parametro_chave_regra_tb]  WITH CHECK ADD CONSTRAINT [FK_sinistro_parametro_chave_X_sinistro_parametro_chave_regra] FOREIGN KEY ([sinistro_parametro_chave_id]) REFERENCES [dbo].[sinistro_parametro_chave_tb]([sinistro_parametro_chave_id])


  ALTER TABLE [dbo].[sinistro_parametro_chave_regra_tb] CHECK CONSTRAINT [FK_sinistro_parametro_chave_X_sinistro_parametro_chave_regra]
END

IF (OBJECT_ID('FK_sinistro_parametro_regra_X_sinistro_parametro_chave_regra', 'F') IS NULL)
BEGIN
  ALTER TABLE [dbo].[sinistro_parametro_chave_regra_tb]  WITH CHECK ADD CONSTRAINT [FK_sinistro_parametro_regra_X_sinistro_parametro_chave_regra] FOREIGN KEY ([sinistro_parametro_regra_id]) REFERENCES [dbo].[sinistro_parametro_regra_tb]([sinistro_parametro_regra_id])


  ALTER TABLE [dbo].[sinistro_parametro_chave_regra_tb] CHECK CONSTRAINT [FK_sinistro_parametro_regra_X_sinistro_parametro_chave_regra]
END