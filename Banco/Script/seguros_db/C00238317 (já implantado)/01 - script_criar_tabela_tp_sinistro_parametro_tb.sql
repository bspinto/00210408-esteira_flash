/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: script_criar_tabela_tp_sinistro_parametro_tb
	OBJETIVO: Script para criar tabela que ira gravar os nomes de parametrizacoes definidas
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  

USE [seguros_db]
GO
----------------------------------------------------------------------------------------------------- 

IF Object_id('tp_sinistro_parametro_tb', 'U') IS NULL
BEGIN
CREATE TABLE [dbo].[tp_sinistro_parametro_tb](
	[tp_sinistro_parametro_id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](200) NULL,
	[dt_inclusao] [dbo].[UD_dt_inclusao] NOT NULL,
	[dt_alteracao] [dbo].[UD_dt_alteracao] NULL,
	[lock] [timestamp] NOT NULL,
	[usuario] [varchar](20) NULL,
 CONSTRAINT [PK_tp_sinistro_parametro] PRIMARY KEY CLUSTERED 
(
	[tp_sinistro_parametro_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
