/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: script_insert_tp_parametro_tb
	OBJETIVO: Script para inserir na tp_parametro_tb parametriacao de pgto imediato
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)
DECLARE @usuario VARCHAR(20)
DECLARE @tp_sinistro_parametro_id INT

SELECT @total_registro_afetados = 0

/*qtd registro que serao afetados*/
SELECT @total_registro = 567

SELECT @usuario = 'C00210408'

---========================================
--tp_sinistro_parametro_tb
---========================================


	INSERT INTO seguros_db.dbo.tp_sinistro_parametro_tb ( nome, dt_inclusao, usuario )
	     VALUES ( 'Esteira Pagamento Imediato - Sinistros de Danos Elétricos Residenciais', GETDATE(), @USUARIO )
		    SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
		--01 registro

	SELECT @tp_sinistro_parametro_id  = tp_sinistro_parametro_id 
	  FROM seguros_db.dbo.tp_sinistro_parametro_tb 
	 WHERE NOME = 'Esteira Pagamento Imediato - Sinistros de Danos Elétricos Residenciais'

   INSERT INTO seguros_db.dbo.sinistro_parametro_chave_tb (tp_sinistro_parametro_id,produto_id,ramo_id,evento_sinistro_id,tp_cobertura_id,dt_ini_vigencia,dt_inclusao,usuario)
        VALUES 
		--104
( @tp_sinistro_parametro_id,104,	14,	3,	     498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,104,	14,	34,	     498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,104,	14,	300,     498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,104,	14,	302,	 498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,104,	14,	311,	 498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,104,	14, 313,	 498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,104,	14,	324,	 498,getdate(),getdate(),@usuario),
		--106                                                           
( @tp_sinistro_parametro_id,106,	14,	3,	     498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,106,	14,	34,	     498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,106,	14,	300,     498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,106,	14,	302,	 498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,106,	14,	311,	 498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,106,	14, 313,	 498,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,106,	14,	324,	 498,getdate(),getdate(),@usuario),
		--108                                                          
( @tp_sinistro_parametro_id,108,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,108,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,108,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,108,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,108,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,108,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,108,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--116                                                           
( @tp_sinistro_parametro_id,116,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,116,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,116,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,116,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,116,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,116,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,116,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--809                                                           
( @tp_sinistro_parametro_id,809,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,809,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,809,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,809,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,809,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,809,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,809,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--1176                                                          
( @tp_sinistro_parametro_id,1176,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1176,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1176,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1176,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1176,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1176,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1176,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--1220                                                          
( @tp_sinistro_parametro_id,1220,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1220,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1220,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1220,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1220,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1220,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1220,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--1221                                                         
( @tp_sinistro_parametro_id,1221,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1221,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1221,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1221,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1221,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1221,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1221,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--1222                                                          
( @tp_sinistro_parametro_id,1222,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1222,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1222,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1222,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1222,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1222,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1222,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--1223                                                         
( @tp_sinistro_parametro_id,1223,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1223,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1223,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1223,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1223,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1223,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1223,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--1224                                                          
( @tp_sinistro_parametro_id,1224,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1224,	14,	34,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1224,	14,	300,     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1224,	14,	302,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1224,	14,	311,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1224,	14, 313,	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1224,	14,	324,	 16 ,getdate(),getdate(),@usuario),
		--1241                                                         
( @tp_sinistro_parametro_id,1241,	14,	3,	     16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1241,	14,	34,	 	 16 ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1241,	14,	324,	 16 ,getdate(),getdate(),@usuario)
      -- 80 registros
SET @total_registro_afetados = @total_registro_afetados + @@rowcount

--inserindo regras
INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB (nome,descricao,tipo,dt_inclusao,usuario)
		VALUES
		('valor_estimativa','Valor da estimativa/Orçamento','numeric 15,2',getdate(),@usuario),
		('periodo_relacionamento','Período de relacionamento do cliente com a BrasilSeg em dias','dias',getdate(),@usuario),
		('periodo_sem_sinistro','Período sem sinistro','dias',getdate(),@usuario),
		('verifica_restricao','Campo que define se deve ser verificada restricao Ação Judicial/Indicio de Fraude','s ou n',getdate(),@usuario),
		('verifica_pre_analise','Campo que define se deve ser verificado se o CPF do cliente consta em pre-analise','s ou n',getdate(),@usuario),
		('verifica_pgto_em_dia','Campo que define se deve ser verificado se o pagamento está em dia','s ou n',getdate(),@usuario)
		 SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
		     --6 registros

DECLARE @sinistro_parametro_regra_id_1 int
DECLARE @sinistro_parametro_regra_id_2 int
DECLARE @sinistro_parametro_regra_id_3 int
DECLARE @sinistro_parametro_regra_id_4 int
DECLARE @sinistro_parametro_regra_id_5 int
DECLARE @sinistro_parametro_regra_id_6 int

SELECT @sinistro_parametro_regra_id_1 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'valor_estimativa'
SELECT @sinistro_parametro_regra_id_2 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'periodo_relacionamento'
SELECT @sinistro_parametro_regra_id_3 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'periodo_sem_sinistro'
SELECT @sinistro_parametro_regra_id_4 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_restricao'
SELECT @sinistro_parametro_regra_id_5 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_pre_analise'
SELECT @sinistro_parametro_regra_id_6 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_pgto_em_dia'


-- Cursor para inserir as regras de acordo com os parametros ja cadastrados na sinistro_parametro_tb
DECLARE @SINISTRO_PARAMETRO_CHAVE_ID int

DECLARE CURSOR_INSERE_REGRA CURSOR FOR
    SELECT
        SINISTRO_PARAMETRO_CHAVE_ID
    FROM
        SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
    WHERE
        TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
	ORDER BY 
	    SINISTRO_PARAMETRO_CHAVE_ID ASC
		

OPEN CURSOR_INSERE_REGRA

FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id

WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
		VALUES
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_1,'3500.00','<=',getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_2,'365','>',getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_3,'365','>=',getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_4,'s',null,getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_5,'s',null,getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_6,'s',null,getdate(),@usuario)
		 SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
    -- Lendo a próxima linha
    FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id
END

-- Fechando Cursor para leitura
CLOSE CURSOR_INSERE_REGRA

-- Desalocando o cursor
DEALLOCATE CURSOR_INSERE_REGRA 


--1 registro afetado
SELECT @total_registro '@total_registro'
	,@total_registro_afetados '@total_registro_afetados'

IF (
		(@@servername = 'SISAB003')
		AND @total_registro <> @total_registro_afetados
		)
	OR (
		(@@servername = 'SISAB051')
		AND @total_registro <> @total_registro_afetados
		)
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + convert(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + convert(VARCHAR(10), @total_registro_afetados)

	RAISERROR (
			@mensagem
			,16
			,1
			)
END
GO


