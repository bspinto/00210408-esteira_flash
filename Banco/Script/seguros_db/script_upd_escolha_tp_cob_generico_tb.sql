/*
	Nome_do_desenvolvedor (ntendencia) - 02/05/2020
	Demanda: 00210408-esteira_flash-fase2
	Nome do script: script_upd_escolha_tp_cob_generico_tb
	Descri��o: Script para Atualiza��o dos dados de franquia na proposta
	Objetivo: altera��o
	Banco: seguros_db
	Ambiente: AB e ABS
*/

-- variaveis de controle
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

-- variaveis de desenvolvimento
DECLARE @dt_inclusao SMALLDATETIME
DECLARE @dt_alteracao SMALLDATETIME
DECLARE @usuario varchar(20)

SELECT @dt_alteracao = GETDATE()
  ,@usuario = 'C00210408'

-- totalizador de registro
SELECT @total_registro_afetados = 0
, @total_registro = (SELECT COUNT(proposta_id) FROM seguros_db.dbo.escolha_tp_cob_generico_tb a WHERE proposta_id = 50930129)
                  + (SELECT COUNT(proposta_id) FROM seguros_db.dbo.escolha_tp_cob_generico_tb a WHERE proposta_id = 50977412)

-- bloco de desenvolvimento
---------------------------------------------------- (inicio)
UPDATE a
SET TEXTO_FRANQUIA = '10% DOS PREJU�ZOS COM M�NIMO DE R$ 400,00' 
,VAL_MIN_FRANQUIA = 400
,FAT_FRANQUIA = 0.1
,dt_alteracao = @dt_alteracao
,usuario = @usuario
FROM seguros_db.dbo.escolha_tp_cob_generico_tb a
WHERE proposta_id = 50930129
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT

UPDATE a
SET TEXTO_FRANQUIA = '10% DOS PREJU�ZOS COM M�NIMO DE R$ 800,00' 
,VAL_MIN_FRANQUIA = 800
,FAT_FRANQUIA = 0.1
,dt_alteracao = @dt_alteracao
,usuario = @usuario
FROM seguros_db.dbo.escolha_tp_cob_generico_tb a
WHERE proposta_id = 50977412
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT
---------------------------------------------------- (fim)

-- Verifica��o
SELECT @total_registro_afetados
SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' /* AB */ or /* ABS */ @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) 
	              + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

	RAISERROR (@mensagem, 16, 1)
END
GO	     




