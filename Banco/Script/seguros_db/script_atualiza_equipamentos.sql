/*
	Nome_do_desenvolvedor (ntendencia) - 19/06/2020
	Demanda: 00210408-esteira_flash
	Nome do script: script_atualiza_tabela_equipamentos
	Descri��o: Script para versionar tabela de equipamentos
	Objetivo: versionar
	Banco: interface_dados_db
	Ambiente: AB e ABS
*/

-- variaveis de controle
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

-- variaveis de desenvolvimento
DECLARE @dt_inclusao SMALLDATETIME
DECLARE @dt_alteracao SMALLDATETIME
DECLARE @usuario varchar(20)
DECLARE @proxima_versao AS INT  

SELECT @dt_alteracao = GETDATE()
  ,@usuario = 'FLOWBR_00210408'
  ,@dt_inclusao = GETDATE()
  ,@proxima_versao = ISNULL(MAX(a.num_versao),0) + 1  
FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)    
  
--tabelas temporarias
CREATE TABLE #sinistro_equipamento_importado (
  tipo_bem VARCHAR(60) NOT NULL
  ,modelo VARCHAR(60) NOT NULL
  ,vl_perda_total NUMERIC(15, 2) NULL
  ,vl_reparo NUMERIC(15, 2) NULL
)

INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'TUBO - At� 32 Polegadas', '900.00', '675.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'TUBO - Acima de 32 Polegadas', '1200.00', '900.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'Plasma At� 32 Polegadas', '1500.00', '1125.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'Plasma - Acim de 32 Polegadas', '2500.00', '1875.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'LED At� 32 Polegadas', '2000.00', '1500.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'LED - Entre 32 Polegas e 55 Polegadas', '3500.00', '2625.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'LED - Acima de 55 Polegadas', '4500.00', '3375.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'Smart TV - At� 32 Polegadas', '2500.00', '1875.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'Smart TV - Entre 32 Polegas e 55 Polegadas', '4000.00', '3000.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('TV', 'Smart TV - Acima de 55 Polegadas', '5000.00', '3750.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Geladeira', 'At� 350 Lt', '2000.00', '1500.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Geladeira', 'Entre 350Lt e 550Lt', '3500.00', '2625.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Geladeira', 'Acima de 550Lt', '4000.00', '3000.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('DVR', 'At� 03 Cameras', '1500.00', '1125.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('DVR', 'Entre 04 e 08 Cameras', '2500.00', '1875.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('DVR', 'Acima de 08 Cameras', '3500.00', '2625.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Baba Eletronica', 'Com Camera', '800.00', '600.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Baba Eletronica', 'Sem Camera', '500.00', '375.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Eletronicos de Beleza', 'Chapinha', '350.00', '262.50')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Eletronicos de Beleza', 'Secador de Cabelo', '300.00', '225.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Eletronicos de Beleza', 'Massageador / Barbeador / Depiladores', '200.00', '150.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Ferro de Passar', 'Ferro de Passar', '300.00', '225.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Fog�o a Gas', 'At� 04 Bocas', '300.00', '225.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Fog�o a Gas', 'Acima de 04 Bocas', '450.00', '337.50')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Fog�o de Indu��o', 'At� 04 Bocas', '2500.00', '1875.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Fog�o de Indu��o', 'Acima de 04 Bocas', '3500.00', '2625.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Fog�o Eletrico', 'At� 04 Bocas', '1000.00', '750.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Fog�o Eletrico', 'Acima de 04 Bocas', '1500.00', '1125.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Forno Eletrico', 'Forno Eletrico', '1000.00', '750.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Port�o Automatico', 'Port�o Automatico', '1500.00', '1125.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Porteiro Eletronico', 'Porteiro Eletronico', '800.00', '600.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Roteador', 'Roteador', '500.00', '375.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Telefone Sem Fio', 'Telefone Sem Fio', '700.00', '525.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Ar Condicionado', 'At� 12000 BTUS', '1500.00', '1125.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Ar Condicionado', 'De 12001 at� 19000 BTUS', '2500.00', '1875.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Ar Condicionado', 'Acima de 19000BTUS', '3500.00', '2625.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('M�quina de Lavar', 'At� 08Kg', '800.00', '600.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('M�quina de Lavar', 'De 09Kg at� 12Kg', '1100.00', '825.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('M�quina de Lavar', 'Acima de 12Kg', '1350.00', '1012.50')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Impressora', 'Laser', '1400.00', '1050.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Impressora', 'Jato de Tinta', '400.00', '300.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Freezer', 'At� 350 Lt', '1500.00', '1125.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Freezer', 'Entre 350Lt e 550Lt', '2625.00', '1968.75')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Freezer', 'Acima de 550Lt', '3000.00', '2250.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Microondas', 'At� 20 Lt', '250.00', '187.50')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Microondas', 'Entre 21Lt e 35Lt', '350.00', '262.50')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Microondas', 'Acima de 35Lt', '500.00', '375.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Outros', 'Outros', '500.00', '375.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Secadora de Roupas', 'At� 08Kg', '350.00', '262.50')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Secadora de Roupas', 'De 09Kg at� 11Kg', '1400.00', '1050.00')
INSERT INTO #sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES ('Secadora de Roupas', 'Acima de 11Kg', '2600.00', '1950.00')

-- totalizador de registro
SELECT @total_registro_afetados = 0
, @total_registro = ( select count(*) FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK) WHERE a.dt_fim_vigencia IS NULL )
                  + (SELECT COUNT(*) FROM #sinistro_equipamento_importado)

-- bloco de desenvolvimento
---------------------------------------------------- (inicio)
    UPDATE a  
    SET a.dt_fim_vigencia = @dt_inclusao  
     ,a.dt_alteracao = GETDATE()  
     ,a.usuario = @usuario
    FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)  
      WHERE a.dt_fim_vigencia IS NULL  
    SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT
  
    INSERT INTO seguros_db.dbo.sinistro_equipamento_importacao_tb (  
     tipo_bem  
     ,modelo  
     ,vl_perda_total  
     ,vl_reparo  
     ,dt_inicio_vigencia  
     ,num_versao  
     ,usuario  
     ,dt_inclusao  
     )  
    SELECT tipo_bem  
     ,modelo  
     ,vl_perda_total  
     ,vl_reparo  
     ,@dt_inclusao --dt_inicio_vigencia   
     ,@proxima_versao --num_versao  
     ,@usuario  
     ,GETDATE() --dt_inclusao  
    FROM #sinistro_equipamento_importado  
    SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT
---------------------------------------------------- (fim)

-- Verifica��o
SELECT @total_registro_afetados

SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' /* AB */ or /* ABS */ @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) 
	              + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

	RAISERROR (@mensagem, 16, 1)
END
GO	     
