/*-----------------------------------------------------------------------------------------------------            
	(NTENDENCIA)
	DEMANDA: 00210408-esteira_flash-fase2
	TIPO: Script
	NOME: script_insert_tp_parametro_tb
	OBJETIVO: Script para inserir na tp_parametro_tb parametriacao de pgto imediato
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)
DECLARE @usuario VARCHAR(20)
DECLARE @tp_sinistro_parametro_id INT

SELECT @total_registro_afetados = 0

/*qtd registro que serao afetados*/
SELECT @total_registro = 21

SELECT @usuario = 'C00210408'

---========================================
--tp_sinistro_parametro_tb
---========================================
SET @tp_sinistro_parametro_id = 1

INSERT INTO seguros_db.dbo.sinistro_parametro_chave_tb (tp_sinistro_parametro_id,produto_id,ramo_id,evento_sinistro_id,tp_cobertura_id,dt_ini_vigencia,dt_inclusao,usuario)
VALUES 
--1242        
( @tp_sinistro_parametro_id,1242,	14,	3,	     16,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1242,	14,	34,	     16,getdate(),getdate(),@usuario),
--( @tp_sinistro_parametro_id,1242,	14,	300,     16,getdate(),getdate(),@usuario),
--( @tp_sinistro_parametro_id,1242,	14,	302,	 16,getdate(),getdate(),@usuario),
--( @tp_sinistro_parametro_id,1242,	14,	311,	 16,getdate(),getdate(),@usuario),
--( @tp_sinistro_parametro_id,1242,	14, 313,	 16,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1242,	14,	324,	 16,getdate(),getdate(),@usuario)
SET @total_registro_afetados = @total_registro_afetados + @@rowcount

DECLARE @sinistro_parametro_regra_id_1 int
DECLARE @sinistro_parametro_regra_id_2 int
DECLARE @sinistro_parametro_regra_id_3 int
DECLARE @sinistro_parametro_regra_id_4 int
DECLARE @sinistro_parametro_regra_id_5 int
DECLARE @sinistro_parametro_regra_id_6 int

SELECT @sinistro_parametro_regra_id_1 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'valor_estimativa'
SELECT @sinistro_parametro_regra_id_2 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'periodo_relacionamento'
SELECT @sinistro_parametro_regra_id_3 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'periodo_sem_sinistro'
SELECT @sinistro_parametro_regra_id_4 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_restricao'
SELECT @sinistro_parametro_regra_id_5 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_reanalise_sinistro'
SELECT @sinistro_parametro_regra_id_6 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_pgto_em_dia'

-- Cursor para inserir as regras de acordo com os parametros ja cadastrados na sinistro_parametro_tb
DECLARE @SINISTRO_PARAMETRO_CHAVE_ID int

DECLARE CURSOR_INSERE_REGRA CURSOR LOCAL FAST_FORWARD FOR
    SELECT
        SINISTRO_PARAMETRO_CHAVE_ID
    FROM
        SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
    WHERE
        TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
        AND produto_id = 1242
	ORDER BY 
	    SINISTRO_PARAMETRO_CHAVE_ID ASC
		

OPEN CURSOR_INSERE_REGRA

FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id

WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
		VALUES
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_1,'3500.00','<=',getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_2,'365','>',getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_3,'365','>=',getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_4,'s',null,getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_5,'s',null,getdate(),@usuario),
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_6,'s',null,getdate(),@usuario)
		 SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
    -- Lendo a pr�xima linha
    FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id
END

-- Fechando Cursor para leitura
CLOSE CURSOR_INSERE_REGRA

-- Desalocando o cursor
DEALLOCATE CURSOR_INSERE_REGRA 


--1 registro afetado
SELECT @total_registro as '@total_registro' 
	  ,@total_registro_afetados as '@total_registro_afetados' 

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + convert(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + convert(VARCHAR(10), @total_registro_afetados)

	RAISERROR (
			@mensagem
			,16
			,1
			)
END
