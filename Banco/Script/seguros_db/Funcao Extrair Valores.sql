--DROP PROCEDURE dbo.teste_sergio  
CREATE FUNCTION dbo.teste_sergio (@texto NVARCHAR(MAX))  
RETURNS NVARCHAR(MAX)  
--DROP FUNCTION dbo.teste_sergio  
--CREATE PROC dbo.teste_sergio_2 (@texto NVARCHAR(MAX))  
AS  
  
BEGIN  
  
 DECLARE @perc NVARCHAR(MAX),  
 @valor NVARCHAR(MAX),  
 @ind1 INT,  
 @ind2 INT,  
 @tamanho INT,  
 @resultado NVARCHAR(MAX)  
  
 SET @tamanho = 0  
 SET @ind1 = 0  
 SET @ind2 = 1  
 SET @perc = ''  
 SET @valor = ''  
  
 IF CHARINDEX('%',@texto) = 0  
    BEGIN  
   SET @ind1 = 1  
   SET @ind2 = 0  
   END   
  
 WHILE @tamanho <> LEN(@texto)  
   BEGIN  
   SET @tamanho = @tamanho + 1  
   IF SUBSTRING (@texto ,@tamanho , 1 ) <> ','  
    BEGIN  
    IF SUBSTRING (@texto ,@tamanho , 1 ) = '%'  
       BEGIN  
      SET @ind1 = 1  
      SET @ind2 = 0  
      END   
    IF (SUBSTRING (@texto ,@tamanho , 1 ) <> '$') AND (SUBSTRING (@texto ,@tamanho , 1 ) <> '-')  
       BEGIN   
      IF ISNUMERIC(SUBSTRING (@texto ,@tamanho , 1 )) <> 0  
      --PRINT SUBSTRING (@texto ,@tamanho , 1 )  
         BEGIN    
         IF @ind1 = 0  
         SET @perc = @perc + SUBSTRING(@texto ,@tamanho , 1 )  
         IF @ind2 = 0  
          SET @valor = @valor + SUBSTRING(@texto ,@tamanho , 1 )        
        END    
      END     
      END  
   ELSE  
    SET @tamanho = LEN(@texto)   
  END  
  
 IF (ISNUMERIC(@perc) = 0) Or (@perc = '.') Or (@perc = '')  
    SET @perc = '0.00'  
      
 IF (ISNUMERIC(@valor) = 0) Or (@valor = '.') Or (@valor = '')  
    SET @valor = '0.00'  
  
 --SELECT CONVERT(DECIMAL(10,02),@perc) / 100 'Percentual',  
 --  CONVERT(DECIMAL(10,02),@valor) 'Valor'  
   
 SET @resultado = 'Percentual: ' + CONVERT(NVARCHAR(20), CONVERT(DECIMAL(10,02),@perc) / 100) + ' Valor: R$ ' + CONVERT(NVARCHAR(20), CONVERT(DECIMAL(10,02),@valor))  
   
 RETURN @resultado  
  
 --SELECT @perc 'Percentual',  
 --       @valor 'Valor'  
  END  