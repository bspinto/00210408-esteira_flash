CREATE FUNCTION dbo.fn_Dados_Franquia (@texto NVARCHAR(MAX))
RETURNS NVARCHAR(MAX)
/*
	(nova consultoria) - 08/05/2018
	Demanda: 00210408-esteira_flash-fase2
	Descri��o: fun��o para recuperar os dados da franquia da proposta.
	Banco: n/d	
*/
-- BLOCO DE TESTE 
/*  
  DECLARE @texto NVARCHAR(MAX)
  
  SELECT TOP 1 @texto = texto_franquia
  FROM seguros_db.dbo.escolha_tp_cob_generico_tb a WITH (NOLOCK)
  WHERE CHARINDEX('$', texto_franquia) > 0
  
  SELECT dbo.fn_Dados_Franquia(@texto)
*/	
AS
BEGIN
	DECLARE @perc NVARCHAR(MAX)
		,@valor NVARCHAR(MAX)
		,@ind1 INT
		,@ind2 INT
		,@tamanho INT
		,@resultado NVARCHAR(MAX)

	SET @tamanho = 0
	SET @ind1 = 0
	SET @ind2 = 1
	SET @perc = ''
	SET @valor = ''
	  
	IF CHARINDEX('%', @texto) = 0
	BEGIN
		SET @ind1 = 1
		SET @ind2 = 0
	END

  --extraindo valores
	WHILE @tamanho <> LEN(@texto)
	BEGIN
		SET @tamanho = @tamanho + 1

		IF SUBSTRING(@texto, @tamanho, 1) <> ','
		BEGIN
			IF SUBSTRING(@texto, @tamanho, 1) = '%'
			BEGIN
				SET @ind1 = 1
				SET @ind2 = 0
			END

			IF (SUBSTRING(@texto, @tamanho, 1) <> '$')
				AND (SUBSTRING(@texto, @tamanho, 1) <> '-')
			BEGIN
				IF ISNUMERIC(SUBSTRING(@texto, @tamanho, 1)) <> 0				
				BEGIN
					IF @ind1 = 0
					BEGIN
						SET @perc = @perc + SUBSTRING(@texto, @tamanho, 1)
					END

					IF @ind2 = 0
					BEGIN
						SET @valor = @valor + SUBSTRING(@texto, @tamanho, 1)
					END
				END
			END
		END
		ELSE
			SET @tamanho = LEN(@texto)
	END

  --formatando valores
	IF (ISNUMERIC(@perc) = 0)
		OR (@perc = '.')
		OR (@perc = '')
	BEGIN
		SET @perc = '0.00'
	END

	IF (ISNUMERIC(@valor) = 0)
		OR (@valor = '.')
		OR (@valor = '')
	BEGIN
		SET @valor = '0.00'
	END

  --Resultado
	SET @resultado = 'Percentual: ' + CONVERT(NVARCHAR(20), CONVERT(DECIMAL(10, 02), @perc) / 100) + ' Valor: R$ ' + CONVERT(NVARCHAR(20), CONVERT(DECIMAL(10, 02), @valor))

	RETURN @resultado
END
GO


