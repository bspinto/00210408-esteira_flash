use seguros_db
go

--localizar proposta para o produto 809
		SELECT TOP 100 p.produto_id
			,p.ramo_id
			,p.proposta_id --, e.tp_cobertura_id, e.dt_inicio_vigencia_esc
			,ISNULL(ap.dt_fim_vigencia, ISNULL(ad.dt_fim_vigencia, pf.dt_fim_vig)) AS dt_fim_vigencia
			,s.evento_sinistro_id
			,e.tp_cobertura_id
		--    into #proposta
		FROM SEGUROS_DB.DBO.PROPOSTA_TB p WITH (NOLOCK)
		 inner join escolha_tp_cob_generico_tb e on p.proposta_id = e.proposta_id
		--INNER JOIN escolha_tp_cob_res_tb e WITH (NOLOCK) ON p.proposta_id = e.proposta_id
		--vigencia vou adesao ou na fechada (ap�lice_tb)
		LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB pf WITH (NOLOCK)
			ON p.PROPOSTA_ID = pf.PROPOSTA_ID
		LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ad WITH (NOLOCK)
			ON p.PROPOSTA_ID = ad.PROPOSTA_ID
		LEFT JOIN seguros_db.dbo.apolice_tb ap WITH (NOLOCK)
			ON p.proposta_id = ap.proposta_id
		LEFT JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)
			ON p.proposta_id = s.proposta_id
		WHERE p.produto_id = 1242
			AND p.ramo_id = 14
			--and s.evento_sinistro_id = 324
			AND p.situacao = 'i'
			--AND e.dt_fim_vigencia_esc IS NULL
			AND e.tp_cobertura_id = 16
			AND P.subramo_id = 1424
			and evento_sinistro_id = 1
		ORDER BY dt_inicio_vigencia_esc DESC

--proposta separadas
--50845947
--50845949
--50845952


--verificando se elas tem cobertura 16
exec SEGS7752_SPS '20200606',50845947,0, 0 
exec SEGS7752_SPS '20200606',50845949,0, 0 
exec SEGS7752_SPS '20200606',50845952,0, 0 

-- verificnado se as propostas est�o aptas
select * from seguros_db.dbo.sinistro_tb s WITH (NOLOCK) where s.proposta_id = 50845947
select * from seguros_db.dbo.sinistro_tb s WITH (NOLOCK) where s.proposta_id = 50845949
select * from seguros_db.dbo.sinistro_tb s WITH (NOLOCK) where s.proposta_id = 50845952

--verificando se tem evento para a cobertura 16 ativo
SELECT  tp_cobertura_id,evento_sinistro_id,subramo_id 
FROM seguros_db.dbo.produto_estimativa_sinistro_tb WITH (NOLOCK)
WHERE produto_id = 1242
	AND ramo_id = 14
	AND subramo_id = 1424
	--AND evento_sinistro_id = 34
	AND tp_cobertura_id = 16
	AND situacao = 'A'

-- se n�o tiver, temos que tratar o evento
use desenv_db
go

alter proc dbo.trata_evento_sinistro_bsp @tp_cobertura_id as int
as
begin
  update a 
  set evento_sinistro_id = 34
  from seguros_db.dbo.produto_estimativa_sinistro_tb a with (nolock)
  WHERE produto_id = 1242
	AND ramo_id = 14
	AND subramo_id = 1424
	--AND evento_sinistro_id = 34
	AND tp_cobertura_id = @tp_cobertura_id
	AND situacao = 'A'
end

WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
  if @@trancount > 0 exec dbo.trata_evento_sinistro_bsp 16
--commit

--atualizando a vigencia das proposta para ficarem longas e dentro do prazo pagamento imediato
WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
  if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50845947, @dt_ini = '20180601', @dt_fim = '20200901' else select 'transa��o fechada'
  if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50845949, @dt_ini = '20180601', @dt_fim = '20200901' else select 'transa��o fechada'
  if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50845952, @dt_ini = '20180601', @dt_fim = '20200901' else select 'transa��o fechada'
--commit

WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
  IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50845947, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
  IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50845949, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
  IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50845952, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
  SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50845947
  SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50845949
  SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50845952
--commit

-- CANCELANDO DEBITOS EM ABERTO
USE desenv_db
GO

alter proc DBO.ATUAL_AGENDAMENTO_bsp @proposta_id as int
as
begin
  update a
  set dt_agendamento = GETDATE()+30
  from SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB a WITH (NOLOCK)
  where proposta_id = @proposta_id
 	AND getdate() > a.dt_agendamento -- cobran�as vencidas  
	AND (a.SITUACAO = 'p' AND a.CANC_ENDOSSO_ID IS NULL) -- cobran�as pendente n�o canceladas 
end

WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.ATUAL_AGENDAMENTO_bsp 50845947
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.ATUAL_AGENDAMENTO_bsp 50845949
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.ATUAL_AGENDAMENTO_bsp 50845952
--commit

--verificando e atualizando data do ultimo sinistro avisado
--localizando
SELECT a.*, a.dt_inclusao
FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_tb a WITH (NOLOCK)
WHERE 1 = 1 AND a.cpf_cgc_segurado = ( 		SELECT isnull(isnull(CLIENTE.cpf_cnpj, FISICA.cpf), juridica.cgc) cpf		FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)		INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID		LEFT JOIN SEGUROS_DB.DBO.pessoa_fisica_tb FISICA WITH (NOLOCK) ON FISICA.pf_cliente_id = CLIENTE.CLIENTE_ID		LEFT JOIN SEGUROS_DB.DBO.pessoa_juridica_tb juridica WITH (NOLOCK) ON juridica.pj_cliente_id = CLIENTE.CLIENTE_ID		
	WHERE PROPOSTA.PROPOSTA_ID = 50845947		)
	AND a.evento_bb_id = 1100	AND a.ramo_id = 14	AND a.ind_reanalise <> 'S'
SELECT a.*, a.dt_inclusao
FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_tb a WITH (NOLOCK)
WHERE 1 = 1 AND a.cpf_cgc_segurado = ( 		SELECT isnull(isnull(CLIENTE.cpf_cnpj, FISICA.cpf), juridica.cgc) cpf		FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)		INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID		LEFT JOIN SEGUROS_DB.DBO.pessoa_fisica_tb FISICA WITH (NOLOCK) ON FISICA.pf_cliente_id = CLIENTE.CLIENTE_ID		LEFT JOIN SEGUROS_DB.DBO.pessoa_juridica_tb juridica WITH (NOLOCK) ON juridica.pj_cliente_id = CLIENTE.CLIENTE_ID		
	WHERE PROPOSTA.PROPOSTA_ID = 50845949		)
	AND a.evento_bb_id = 1100	AND a.ramo_id = 14	AND a.ind_reanalise <> 'S'
SELECT a.*, a.dt_inclusao
FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_tb a WITH (NOLOCK)
WHERE 1 = 1 AND a.cpf_cgc_segurado = ( 		SELECT isnull(isnull(CLIENTE.cpf_cnpj, FISICA.cpf), juridica.cgc) cpf		FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)		INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID		LEFT JOIN SEGUROS_DB.DBO.pessoa_fisica_tb FISICA WITH (NOLOCK) ON FISICA.pf_cliente_id = CLIENTE.CLIENTE_ID		LEFT JOIN SEGUROS_DB.DBO.pessoa_juridica_tb juridica WITH (NOLOCK) ON juridica.pj_cliente_id = CLIENTE.CLIENTE_ID		
	WHERE PROPOSTA.PROPOSTA_ID = 50845952		)
	AND a.evento_bb_id = 1100	AND a.ramo_id = 14	AND a.ind_reanalise <> 'S'

USE desenv_db
GO

alter proc DBO.ATUAL_AGENDAMENTO_bsp (@proposta_id as int, @evento_id as int, @sinistro_id as numeric(15))
as
begin
  update a
  set dt_inclusao = GETDATE()- 400
  FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_atual_tb a WITH (NOLOCK)
  where proposta_id = @proposta_id
  and evento_id = @evento_id
  and sinistro_id = @sinistro_id
  
end

WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.ATUAL_AGENDAMENTO_bsp 50845947, 63569558, 14201922551
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.ATUAL_AGENDAMENTO_bsp 50845949, 63569560, 14201922553
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.ATUAL_AGENDAMENTO_bsp 50845949, 63570411, 14202000093
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.ATUAL_AGENDAMENTO_bsp 50845952, 63569597, 14201922558
--commit

	

		

--verificando a saida		
WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
  IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL BEGIN DROP TABLE #Equipamento END
  CREATE TABLE #Equipamento (Codigo INT  ,TipoBem VARCHAR(60)  ,Modelo VARCHAR(60)  ,Dano INT  ,Orcamento INT  ,Valor NUMERIC(15,2)  ,Laudo INT  ,NomeAssistencia VARCHAR(60)  ,TelAssistencia VARCHAR(15)  ,Descricao VARCHAR(255))

  INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
  SELECT '000000037', 'Ar Condicionado', 'Acima de 19000BTUS', '1', '1', '600.00', '1', 'Ar teC', '(32) 34414-444_', 'dano no ar'

exec seguros_db.dbo.segs14618_sps 1222, 14, 3, 16 ,50845947, 1500.00 ,'2020-06-07' 
--exec seguros_db.dbo.segs14618_sps 1222, 14, 3, 16 ,50845949, 1500.00 ,'2020-06-07' 
--exec seguros_db.dbo.segs14618_sps 1222, 14, 3, 16 ,50845952, 1500.00 ,'2020-06-07' 
*/


--REFERENCES seguros_db.dbo.seguro_generico_tb 