VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDocumento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private proposta_id     As String
Private documento_id    As Integer
Private documento       As String
Public Property Let Proposta(ByVal vData As String)
    proposta_id = vData
End Property
Public Property Get Proposta() As String
    Proposta = proposta_id
End Property
Public Property Let idDocumento(ByVal vData As String)
    documento_id = vData
End Property
Public Property Get idDocumento() As String
    idDocumento = documento_id
End Property
Public Property Let nomeDocumento(ByVal vData As String)
    documento = vData
End Property
Public Property Get nomeDocumento() As String
    nomeDocumento = documento
End Property

