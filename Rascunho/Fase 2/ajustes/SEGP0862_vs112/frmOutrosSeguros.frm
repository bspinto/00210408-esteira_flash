VERSION 5.00
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GRIDALIANCA.OCX"
Begin VB.Form frmOutrosSeguros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   3795
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8415
   Icon            =   "frmOutrosSeguros.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3795
   ScaleWidth      =   8415
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   5595
      TabIndex        =   6
      Top             =   3240
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   6990
      TabIndex        =   7
      Top             =   3240
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   4200
      TabIndex        =   5
      Top             =   3240
      Width           =   1275
   End
   Begin VB.Frame Frame2 
      Caption         =   "Outros Seguros para o mesmo bem"
      Height          =   2895
      Left            =   0
      TabIndex        =   8
      Top             =   120
      Width           =   8415
      Begin VB.TextBox txtNomeSeguradora 
         Height          =   330
         Left            =   1320
         TabIndex        =   1
         Top             =   600
         Width           =   5535
      End
      Begin VB.TextBox txtApolice 
         Height          =   330
         Left            =   120
         TabIndex        =   0
         Top             =   600
         Width           =   1140
      End
      Begin VB.CommandButton cmdExcluir 
         Caption         =   "Excluir"
         Height          =   420
         Left            =   6960
         TabIndex        =   4
         Top             =   2280
         Width           =   1275
      End
      Begin VB.CommandButton cmdIncluir 
         Caption         =   "Incluir"
         Height          =   420
         Left            =   6960
         TabIndex        =   2
         Top             =   570
         Width           =   1275
      End
      Begin GridAlianca.grdAlianca grdOutrosSeg 
         Height          =   1695
         Left            =   120
         TabIndex        =   3
         Top             =   1080
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   2990
         BorderStyle     =   1
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "Nome da Seguradora"
         Height          =   195
         Left            =   1320
         TabIndex        =   10
         Top             =   360
         Width           =   1515
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Ap�lice"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   525
      End
   End
End
Attribute VB_Name = "frmOutrosSeguros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdContinuar_Click()
    Dim wCounter As Integer
    
    With frmAviso
        .grdOutrosSeg.Clear
        .grdOutrosSeg.Rows = 2
        .grdOutrosSeg.Cols = 3
        .grdOutrosSeg.AutoFit H�brido
        cod_objeto = 0
        For wCounter = 1 To grdOutrosSeg.Rows - 1
            .grdOutrosSeg.AdicionarLinha vbTab & grdOutrosSeg.TextMatrix(CInt(wCounter), 1) & vbTab & grdOutrosSeg.TextMatrix(CInt(wCounter), 2)
        Next wCounter
        '.grdOutrosSeg.AutoFit H�brido
        If Trim(.grdOutrosSeg.TextMatrix(1, 1)) = "" Then .grdOutrosSeg.RemoveItem 1
        
        
    End With
        
    Dim wAddLinha As String
    Dim wAddLinha2 As String
    
    For Each Aviso In Avisos
        
        wAddLinha = "" & _
        vbTab & Str(Aviso.Proposta) & _
        vbTab & Aviso.Proposta_bb & _
        vbTab & Str(Aviso.Produto) & _
        vbTab & Aviso.nome_produto & _
        vbTab & Aviso.apolice & _
        vbTab & Str(Aviso.Ramo) & _
        vbTab & frmEvento.cboEvento.Text & _
        vbTab & FormataValorParaPadraoBrasil(Aviso.valor_prejuizo)
        
        frmAviso.grdAvisos.AdicionarLinha (wAddLinha)
        If frmAviso.grdAvisos.TextMatrix(1, 1) = "" Then
            frmAviso.grdAvisos.RemoveItem (1)
        End If
        
        
        
    
    
        
    Next Aviso
    frmAviso.grdAvisos.AutoFit H�brido
    
    If frmAvisoPropostas.grdResultadoPesquisa.Rows = 1 Then
        wAddLinha2 = "" & _
            vbTab & "AVSR" & _
            vbTab & 0 & _
            vbTab & 0 & _
            vbTab & "Sem Proposta" & _
            vbTab & 0 & _
            vbTab & "Sem Produto" & _
            vbTab & 0
            
        frmAviso.GridPropostaAfetada.AdicionarLinha (wAddLinha2)
        If frmAviso.GridPropostaAfetada.TextMatrix(1, 1) = "" Then
            frmAviso.GridPropostaAfetada.RemoveItem (1)
        End If
    Else
    
        For i = 1 To frmAvisoPropostas.grdResultadoPesquisa.Rows - 1
        
            If frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 1) = "AVSR" Or frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 1) = "RNLS" Then
                wAddLinha2 = "" & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 1) & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 2) & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 3) & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 4) & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 5) & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 6) & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 7) & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 8) & _
                vbTab & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(i), 9)
                
                frmAviso.GridPropostaAfetada.AdicionarLinha (wAddLinha2)
                If frmAviso.GridPropostaAfetada.TextMatrix(1, 1) = "" Then
                    frmAviso.GridPropostaAfetada.RemoveItem (1)
                End If
            
            End If
            
        Next i
    End If
    MsgPgtoImediato = 0 '(ntedencia) 00210408-esteira_flash 
    frmAviso.Show
    Me.Hide
End Sub
Private Sub cmdExcluir_Click()
    If grdOutrosSeg.RowSel = 1 Then
        grdOutrosSeg.TextMatrix(grdOutrosSeg.RowSel, 1) = ""
        grdOutrosSeg.TextMatrix(grdOutrosSeg.RowSel, 2) = ""
    Else
        grdOutrosSeg.RemoveItem (grdOutrosSeg.RowSel)
    End If
End Sub
Private Sub cmdIncluir_Click()
    Dim wAddLinha As String
    If grdOutrosSeg.Rows = 5 Then
        MsgBox "J� foi atingido o n�mero m�ximo de ap�lices.", vbInformation + vbOKOnly
    Else
        If Trim(txtApolice.Text) = "" Then
            MsgBox "Insira o n�mero da ap�lice.", vbInformation + vbOKOnly
            Exit Sub
        ElseIf Trim(txtNomeSeguradora.Text) = "" Then
            MsgBox "Insira o nome da seguradora.", vbInformation + vbOKOnly
            Exit Sub
        End If
        
        If Not IsNumeric(txtApolice.Text) Then
            MsgBox "O campo da ap�lice deve ser num�rico.", vbInformation + vbOKOnly
            txtApolice.SetFocus
            Exit Sub
        End If
        
        wAddLinha = vbTab & txtApolice.Text & vbTab & txtNomeSeguradora.Text
        grdOutrosSeg.AdicionarLinha wAddLinha
        txtApolice.Text = ""
        txtNomeSeguradora.Text = ""
        If Trim(grdOutrosSeg.TextMatrix(1, 1)) = "" Then grdOutrosSeg.RemoveItem (1)
        txtApolice.SetFocus
    End If
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call Sair(Me)
End Sub

Private Sub cmdVoltar_Click()
    For i = 1 To Avisos.Count
        Avisos.Remove (1)
    Next i
    
  '14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
   If frmAviso.grdEquipamentos.Rows > 1 Then 'C00210408 - Esteira Flash
       Call frmEquipamentos.limparGridFrmAviso
   End If
   '14/01/2019 (ntedencia) 00210408-esteira_flash (fim)
    
    frmObjSegurado.Show
    Me.Hide
End Sub

Private Sub Form_Load()
Dim SQL As String
Dim bRetornoPesquisa As Long
Dim rsPesquisa As ADODB.Recordset
    
'AKIO.OKUNO - INICIO - 17860335 - 26/06/2013
'    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2, "Produ��o", "Qualidade")
    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2 Or glAmbiente_id = 6, "Produ��o", "Qualidade")
'AKIO.OKUNO - FIM - 17860335 - 26/06/2013

    CentraFrm Me
    
    With grdOutrosSeg
        .Cols = 3
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "Ap�lice"
        .TextMatrix(0, 2) = "Nome da Seguradora"

        .AutoFit H�brido
    End With
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub
Private Sub txtNomeSeguradora_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cmdIncluir_Click
    End If
End Sub
