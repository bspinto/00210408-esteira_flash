Attribute VB_Name = "AvisoSinistroRE"
''Option Explicit
Public Const situacao_aviso = "N"

Public cProdutoBBProtecao As String

Public Type estimativa
 val_inicial As Double
 perc_estimativa As Double
 utiliza_percentual_subevento As String
 Descricao As String
 atinge As Boolean
 cobertura_bb As Integer
End Type

Public Type CoberturaAfetada
    tp_cobertura_id As String
    cobertura_bb As Integer
    Descricao As String
    val_estimado As String
End Type

Public Type Aviso_Sinistro
    sinistro As Object
    cobertura_afetada() As CoberturaAfetada
    cobertura_atingida As Boolean
End Type

Public propostasAvisadas As String 'para as propostas avisadas
Public propostasReanalise As String 'para as propostas preanalise
Public clientesPropostas As String 'para o cliente da proposta

Public Avisos_Sinistros() As Aviso_Sinistro
Public Propostas As New Collection
Public Questionario As New Collection
Public Avisos As New Collection
Public QuestionarioObjetos As New Collection

Public EventoId As Integer  'Demanda 18723625

'14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
Public Const cCapReparo As String = "Pass�vel de Reparo"
Public Const cCapPerdaTotal As String = "Perda Total"
Public Const cCapSim As String = "Sim"
Public Const cCapNao As String = "N�o"
Public MsgPgtoImediato As Integer
Public dValorIS As Double
Public dValorEquipamentos As Double
'14/01/2019 (ntedencia) 00210408-esteira_flash (fim)


Public Function getEstimativa(Proposta As Long, _
                                tp_cobertura_id As Long, _
                                evento_sinistro_id As Long, _
                                SubEvento_sinistro_id As Long, _
                                tp_componente_id As Integer, _
                                dtOcorrenciaSinistro As String) As estimativa
Dim SQL As String
Dim Rs  As ADODB.Recordset
Dim rs1 As ADODB.Recordset
Dim ramo_id As Integer
Dim subramo_id As Long
Dim produto_id As Integer
Dim perc_estimativa_subevento As Double

    'Seleciona as informa��es da proposta
    SQL = ""
    SQL = SQL & " select a.ramo_id, b.subramo_id, b.produto_id "
    SQL = SQL & " from apolice_tb a with (nolock), proposta_tb b with (nolock)"
    SQL = SQL & " where a.proposta_id = b.proposta_id "
    SQL = SQL & " and b.proposta_id = " & Proposta
    SQL = SQL & " union "
    SQL = SQL & " select a.ramo_id, b.subramo_id, b.produto_id"
    SQL = SQL & " from proposta_adesao_tb a with (nolock), proposta_tb b with (nolock)"
    SQL = SQL & " where a.proposta_id = b.proposta_id"
    SQL = SQL & " and b.proposta_id = " & Proposta
                                  
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not Rs.EOF Then
        ramo_id = Rs!ramo_id
        subramo_id = IIf(IsNull(Rs!subramo_id), 0, Rs!subramo_id)
        produto_id = Rs!produto_id
    End If
    
    Rs.Close
    
     SQL = "select val_inicial, perc_estimativa, isnull(utiliza_percentual_subevento,'N') utiliza_percentual_subevento, nome, a.cobertura_bb"
     SQL = SQL & " from produto_estimativa_sinistro_tb a with (nolock),"
     SQL = SQL & " tp_cobertura_tb b with (nolock)"
     SQL = SQL & " where produto_id = " & produto_id
     SQL = SQL & " and a.tp_cobertura_id = b.tp_cobertura_id"
     SQL = SQL & " and ramo_id = " & ramo_id
     SQL = SQL & " and subramo_id = " & subramo_id
     SQL = SQL & " and tp_componente_id = " & tp_componente_id
     SQL = SQL & " and evento_sinistro_id = " & evento_sinistro_id
     SQL = SQL & " and a.tp_cobertura_id = " & tp_cobertura_id
     SQL = SQL & " and convert(char(8),dt_inicio_vigencia, 112) <= '" & dtOcorrenciaSinistro & "'"
     SQL = SQL & " and (convert(char(8), dt_fim_vigencia, 112) >= '" & dtOcorrenciaSinistro & "'"
     SQL = SQL & "     or dt_fim_vigencia is null)"

    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
                         
    
     If Not Rs.EOF Then
         If Rs!utiliza_percentual_subevento = "S" Then
            SQL = "SELECT isnull(perc_estimativa,0) perc_estimativa "
            SQL = SQL & " FROM  evento_subevento_sinistro_tb with (nolock)"
            SQL = SQL & " WHERE evento_sinistro_id = " & evento_sinistro_id & " and "
            SQL = SQL & "       subevento_sinistro_id = " & SubEvento_sinistro_id

            Set rs1 = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  SQL, _
                                  True)
                                 
            If Not rs1.EOF Then
               perc_estimativa_subevento = rs1!perc_estimativa
            Else
               perc_estimativa_subevento = 0
            End If
            rs1.Close
         End If
               
         If Not IsNull(Rs!val_inicial) Then
             getEstimativa.val_inicial = CDbl(Rs!val_inicial)
         End If
         
         If Rs!utiliza_percentual_subevento = "S" Then
            getEstimativa.perc_estimativa = perc_estimativa_subevento
         Else
            If Not IsNull(Rs!perc_estimativa) Then
               getEstimativa.perc_estimativa = CDbl(Rs!perc_estimativa)
            End If
         End If
         
         getEstimativa.Descricao = Rs!Nome
         getEstimativa.atinge = True
         If Not IsNull(Rs!cobertura_bb) Then
            getEstimativa.cobertura_bb = Rs!cobertura_bb
         End If
         getEstimativa.utiliza_percentual_subevento = Rs!utiliza_percentual_subevento
    Else
        getEstimativa.atinge = False
    
    End If
    
    Rs.Close
    

End Function

Public Sub Main()
 Dim Rs As ADODB.Recordset
  Dim SQL As String
  'Monta_Parametros ("SEGP0862")
    If Not Trata_Parametros(Command) Then
       Call FinalizarAplicacao
    End If
    
   '  cUserName = "fesantos"
   ' glAmbiente_id = 7
  
    SQL = "Select dt_operacional, "
    SQL = SQL & "status_sistema,  "
    SQL = SQL & "dt_contabilizacao "
    SQL = SQL & "from parametro_geral_tb with (nolock) "
    
     Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not Rs.EOF Then
        Data_Sistema = Format(Rs(0), "dd/mm/yyyy")
    End If
    
    frmConsulta.Show
  
End Sub

Public Function getIndiceProposta(Proposta_id As Long) As Integer
getIndiceProposta = 0

    For Each Proposta In Propostas
        getIndiceProposta = getIndiceProposta + 1
        If CDbl(Proposta.Proposta) = Proposta_id Then
            Exit For
        End If
        
    Next Proposta
    

End Function

Public Function getIndiceAviso(Proposta_id As Long) As Integer
    getIndiceAviso = 0
    
    For Each Aviso In Avisos
        getIndiceAviso = getIndiceAviso + 1
        If CDbl(Aviso.Proposta) = Proposta_id Then
            Exit For
        End If
        
    Next Aviso


End Function

'14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
Public Function RetornaQueryPagtoImediato(ByVal bValidaFluxo As Boolean, _
                                          Optional ByVal iProduto_id As Integer, _
                                          Optional ByVal iRamo_id As Integer, _
                                          Optional ByVal ievento_id As Integer, _
                                          Optional ByVal icobertura_id As Integer, _
                                          Optional ByVal sProposta_id As String, _
                                          Optional ByVal sDt_Ocorrencia As String, _
                                          Optional ByVal iAvisoRural As Integer) As String
    Dim sSQL As String
    Dim iOrcamento As Integer
    Dim iDano As Integer
    Dim iLaudo As Integer

    sSQL = "SET NOCOUNT ON "
    sSQL = sSQL & "IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL "
    sSQL = sSQL & "BEGIN DROP TABLE #Equipamento END"
    sSQL = sSQL & vbNewLine
    
    sSQL = sSQL & "CREATE TABLE #Equipamento (Codigo INT"
    sSQL = sSQL & "  ,TipoBem VARCHAR(60)"
    sSQL = sSQL & "  ,Modelo VARCHAR(60)"
    sSQL = sSQL & "  ,Dano INT"
    sSQL = sSQL & "  ,Orcamento INT"
    sSQL = sSQL & "  ,Valor NUMERIC(15,2)"
    sSQL = sSQL & "  ,Laudo INT"
    sSQL = sSQL & "  ,NomeAssistencia VARCHAR(60)"
    sSQL = sSQL & "  ,TelAssistencia VARCHAR(15)"
    sSQL = sSQL & "  ,Descricao VARCHAR(255)"
    sSQL = sSQL & ")" & vbNewLine & vbNewLine
    
    For i = 1 To frmAviso.grdEquipamentos.Rows - 1
        sCodigo = frmAviso.grdEquipamentos.TextMatrix(i, 0)
        sTipoBem = frmAviso.grdEquipamentos.TextMatrix(i, 1)
        sModelo = frmAviso.grdEquipamentos.TextMatrix(i, 2)
        sDano = frmAviso.grdEquipamentos.TextMatrix(i, 3)
        sOrcamento = frmAviso.grdEquipamentos.TextMatrix(i, 4)
        sValor = frmAviso.grdEquipamentos.TextMatrix(i, 5)
        sLaudo = frmAviso.grdEquipamentos.TextMatrix(i, 6)
        sNome = frmAviso.grdEquipamentos.TextMatrix(i, 7)
        sTele = frmAviso.grdEquipamentos.TextMatrix(i, 8)
        sDescricao = frmAviso.grdEquipamentos.TextMatrix(i, 9)
        
        iOrcamento = 0
        If sOrcamento = cCapSim Then
          iOrcamento = 1
        End If
                    
        iLaudo = 0
        If sLaudo = cCapSim Then
          iLaudo = 1
        End If
        
        iDano = 0
        If sDano = cCapReparo Then
          iDano = 1
        End If
        If sDano = cCapPerdaTotal Then
          iDano = 2
        End If
                        
        If i = 1 Then
            sSQL = sSQL & "INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao) " & vbNewLine
        Else
            sSQL = sSQL & "UNION" & vbNewLine
        End If
        sSQL = sSQL & "SELECT " & CInt(sCodigo) & ", '" & sTipoBem & "', '" & sModelo & "', " & iDano & ", " & iOrcamento & ", " & Replace(sValor, ",", ".") & ", " & iLaudo & ", '" & sNome & "', '" & sTele & "', '" & sDescricao & "' " & vbNewLine
    Next i
    
   sSQL = sSQL + vbNewLine
   
   If bValidaFluxo Then
      sSQL = sSQL & "EXEC seguros_db.dbo.SEGS14618_SPS "
      sSQL = sSQL & iProduto_id & ", "
      sSQL = sSQL & iRamo_id & ", "
      sSQL = sSQL & ievento_id & ", "
      sSQL = sSQL & icobertura_id & ", "
      sSQL = sSQL & sProposta_id & ", "
      sSQL = sSQL & Replace(dValorIS, ",", ".") & ", "
      sSQL = sSQL & "'" & sDt_Ocorrencia & "', "
      sSQL = sSQL & iAvisoRural & " "
   End If
    
   RetornaQueryPagtoImediato = sSQL
End Function
'14/01/2019 (ntedencia) 00210408-esteira_flash (fim)

