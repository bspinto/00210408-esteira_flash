VERSION 5.00
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GRIDALIANCA.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDocumentos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aviso de Sinistro - Documentos"
   ClientHeight    =   8025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11865
   Icon            =   "frmDocumentos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8025
   ScaleWidth      =   11865
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   495
      Left            =   8160
      TabIndex        =   5
      Top             =   6960
      Width           =   1455
   End
   Begin GridAlianca.grdAlianca GridDinamico1 
      Height          =   5775
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   10186
      BorderStyle     =   1
      AllowUserResizing=   3
      EditData        =   0   'False
      Highlight       =   1
      ShowTip         =   0   'False
      SortOnHeader    =   0   'False
      BackColor       =   -2147483643
      BackColorBkg    =   -2147483633
      BackColorFixed  =   -2147483633
      BackColorSel    =   -2147483635
      FixedCols       =   1
      FixedRows       =   1
      FocusRect       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      ForeColorFixed  =   -2147483630
      ForeColorSel    =   -2147483634
      GridColor       =   -2147483630
      GridColorFixed  =   12632256
      GridLine        =   1
      GridLinesFixed  =   2
      MousePointer    =   0
      Redraw          =   -1  'True
      Rows            =   2
      TextStyle       =   0
      TextStyleFixed  =   0
      Cols            =   2
      RowHeightMin    =   0
   End
   Begin VB.CheckBox chkEmail 
      Caption         =   "Enviar lista de documentos para Solicitante"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   480
      TabIndex        =   1
      Top             =   7080
      Width           =   4095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Novo Aviso"
      Height          =   465
      Left            =   10200
      TabIndex        =   0
      Top             =   6960
      Width           =   1455
   End
   Begin MSComctlLib.StatusBar stbQtd 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   3
      Top             =   7680
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   609
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   720
      Left            =   120
      TabIndex        =   2
      Top             =   6000
      Width           =   11625
   End
End
Attribute VB_Name = "frmDocumentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public produto_id As String
Public ramo_id As String
Public subramo_id As String
Public evento_sinistro_id As Integer
Public sCoberturas As String
Public produtoResidencial As Boolean
Public produtoOutros As Boolean
Dim Conex As Long


Private Sub CarregaCabecalho()
    With GridDinamico1
        
        .Rows = 1
        .Cols = 3
        .TextMatrix(0, 0) = "Tipo de C�pia        "
        .TextMatrix(0, 1) = "Documento" & Space(130)
        .TextMatrix(0, 2) = "Local para obter a documenta��o" & Space(27)
        '.TextMatrix(0, 3) = "C�digo do documento"
        
        .RowHeight(0) = 480
        .AutoFit H�brido
    
    End With
End Sub

Private Sub cmdNovoAviso_Click()
    Me.Hide
    frmConsulta.Show
End Sub
Private Sub chkEmail_Click()
    
    EnviaMensagem (frmSolicitante.txtEmailSolicitante.Text)
    
End Sub

Private Sub cmdOK_Click()
    
    If chkEmail.value = 1 Then
        EnviaMensagem (frmSolicitante.txtEmailSolicitante.Text)
    End If
    
    LimpaTudo
    Set AvisoSinistro = Nothing
    Shell "segp0794.exe " & Command
    End
    
End Sub


Private Function NovoTextoEmailPlainText() As String

    Dim msg As String
    Dim i As Long
    

    With AvisoSinistro
        msg = ""
        msg = msg & "Prezado(a) " & Trim(.SolicitanteNome) & "," & vbCrLf & vbCrLf
        msg = msg & "Solicitamos que seja providenciada e encaminhada a seguinte documenta��o: " & vbCrLf & vbCrLf
        
        msg = msg & "REF.: Sinistro: " & .sinistro_id & vbCrLf & vbCrLf
        
        For i = 1 To GridDinamico1.Rows - 1
           'msg = msg & "� " & GridDinamico1.TextMatrix(i, 0) & " " & Replace(LCase(GridDinamico1.TextMatrix(i, 2)), Chr(13), "") & " (" & Replace(UCase(GridDinamico1.TextMatrix(i, 1)), Chr(13), "") & ") " & vbCrLf & vbCrLf
            msg = msg & "� " & UCase(GridDinamico1.TextMatrix(i, 1)) & vbCrLf & vbCrLf
        Next i
        
        msg = msg & vbCrLf & "Esta documenta��o dever� ser entregue preferencialmente no site do BB Seguros "
        
        If (produtoResidencial) Then
           msg = msg & "(https://www.bbseguros.com.br/seguradora/servicos/sinistro/residencial/), "
        ElseIf (produtoOutros) Then
            msg = msg & "(https://www.bbseguros.com.br/seguradora/servicos/sinistro/), "
        End If
        
        msg = msg & "onde inserindo os dados solicitados ter� acesso ao campo de anexo de documentos e visualiza��o do processo, ou atrav�s ag�ncia de contato, informada no momento do aviso. " & vbCrLf & vbCrLf
        msg = msg & "Informa��es importantes ao segurado: " & vbCrLf
        msg = msg & "� � Indispens�vel a apresenta��o de todos os documentos solicitados para regula��o do sinistro." & vbTab & vbCrLf
        msg = msg & "� Salientamos que outros documentos poder�o ser solicitados no decorrer do processo." & vbTab & vbCrLf
        msg = msg & "� Em caso de necessidade de vistoria no local, esta ser� realizada pela seguradora." & vbTab & vbCrLf
        msg = msg & "� Documentos encaminhados atrav�s deste e-mail n�o ser�o recepcionados em virtude do bloqueio do nosso servidor." & vbTab & vbCrLf & vbCrLf
        
        msg = msg & "Qualquer d�vida ou informa��o entrar em contato no telefone 0800 729 7000. " & vbCrLf
        msg = msg & "Atenciosamente, " & vbCrLf
        msg = msg & "Alian�a do Brasil " & vbCrLf
        msg = msg & "Central de Atendimento aos Clientes " & vbCrLf
        msg = msg & "Tel.: 0800 729 70000 " & vbCrLf
        msg = msg & "Comunica��o Corporativa Alian�a do Brasil"
    End With
    
     msg = Replace(msg, vbLf, "")
    
    NovoTextoEmailPlainText = msg


End Function


'In�cio - Fernanda Nunes Dutra - 119443889 - Ajustar email de comunica��o de Sinistro - 04/12/2016
Private Function NovoTextoEmail() As String
    
    
    Dim msg As String
    Dim i As Long
    
    

    With AvisoSinistro
        msg = ""
        msg = msg & "<html><body>Prezado (a) <b>" & .SolicitanteNome & " </b>, <br/><br/>"
        msg = msg & "Solicitamos que seja providenciada e encaminhada a seguinte documenta��o: <br/><br/>"
        
        msg = msg & "REF.: Sinistro: " & .sinistro_id & "<br/><br/>"
            
        For i = 1 To GridDinamico1.Rows - 1
           ' msg = msg & GridDinamico1.TextMatrix(i, 0) & " " & LCase(GridDinamico1.TextMatrix(i, 2)) & " (" & UCase(GridDinamico1.TextMatrix(i, 1)) & ") <br/><br/>"
            msg = msg & UCase(GridDinamico1.TextMatrix(i, 1)) & "<br/><br/>"
        Next i
        
        
        msg = msg & "Esta documenta��o dever� ser entregue preferencialmente no site do BB Seguros "
        
        If (produtoResidencial) Then
            msg = msg & "(<href>https://www.bbseguros.com.br/seguradora/servicos/sinistro/residencial/</href>), "
        ElseIf (produtoOutros) Then
            msg = msg & "(<href>https://www.bbseguros.com.br/seguradora/servicos/sinistro/</href>), "
        End If
        
        msg = msg & "onde inserindo os dados solicitados ter� acesso ao campo de anexo de documentos e visualiza��o do processo, ou atrav�s ag�ncia de contato, informada no momento do aviso. <br/><br/>"
        msg = msg & "Informa��es importantes ao segurado: <br/><br/>"
        msg = msg & "�  � Indispens�vel a apresenta��o de todos os documentos solicitados para regula��o do sinistro; <br/>"
        msg = msg & "�  Salientamos que outros documentos poder�o ser solicitados no decorrer do processo; <br/>"
        msg = msg & "�  Em caso de necessidade de vistoria no local, esta ser� realizada pela seguradora; <br/>"
        msg = msg & "�  Documentos encaminhados atrav�s deste e-mail n�o ser�o recepcionados em virtude do bloqueio do nosso servidor. <br/> <br/>"
        msg = msg & "Qualquer d�vida ou informa��o entrar em contato no telefone 0800 729 7000. <br/>"
        msg = msg & "Atenciosamente, <br/>"
        msg = msg & "Alian�a do Brasil <br/>"
        msg = msg & "Central de Atendimento aos Clientes <br/>"
        msg = msg & "Tel.: 0800 729 70000 <br/>"
        msg = msg & "Comunica��o Corporativa Alian�a do Brasil</body></html>"
    End With
    
    NovoTextoEmail = msg

     
End Function

Private Function textoEmail() As String
    
  
    Dim msg As String
    Dim i As Long


    With AvisoSinistro
        msg = ""
        msg = msg & "Prezado(a) " & .SolicitanteNome & "," & vbCrLf & vbCrLf
        msg = msg & "Segue a lista de documentos necess�ria para a an�lise do sinistro comunicado. " & vbCrLf
        msg = msg & vbCrLf
        msg = msg & "Estes documentos dever�o ser entregues � ag�ncia de contato, informada no momento do aviso:" & vbCrLf & vbCrLf
        msg = msg & "Ag�ncia de contato: " & .AgenciaAviso & "-" & .AgenciaDvAviso & vbCrLf
        msg = msg & "Nome: " & .AgenciaNome & vbCrLf
        msg = msg & "Endere�o: " & .AgenciaEndereco & vbCrLf
        msg = msg & vbCrLf
        msg = msg & "---------------------------------------------- " & vbCrLf
        msg = msg & "Documentos: " & vbCrLf
        msg = msg & vbCrLf
        
        For i = 1 To GridDinamico1.Rows - 1
            msg = msg & GridDinamico1.TextMatrix(i, 0) & " " & LCase(GridDinamico1.TextMatrix(i, 2)) & " (" & UCase(GridDinamico1.TextMatrix(i, 1)) & ") " & vbCrLf
            msg = msg & "   Local para obter a documenta��o: " & GridDinamico1.TextMatrix(i, 2) & vbCrLf
            msg = msg & vbCrLf
        Next i
        
        msg = msg & "---------------------------------------------- " & vbCrLf
        msg = msg & "A SEGURADORA RESERVA-SE O DIREITO DE SOLICITAR QUALQUER DOCUMENTO QUE JULGAR " & vbCrLf
        msg = msg & "NECESS�RIO PARA A EFETIVA COMPROVA��O DA COBERTURA DO SINISTRO." & vbCrLf
        msg = msg & vbCrLf
        msg = msg & "Atenciosamente,"
        msg = msg & vbCrLf
        msg = msg & vbCrLf
        msg = msg & "Alian�a do Brasil" & vbCrLf
        msg = msg & "Central de Atendimento aos Clientes" & vbCrLf
        msg = msg & "Tel.: 0800 729 70000 " & vbCrLf
        msg = msg & "----------------------------------------------" & vbCrLf
        msg = msg & "Comunica��o Corporativa Alian�a do Brasil"
    
    End With
        
        
    textoEmail = msg

End Function


Sub ObtemProduto()

    Dim Produto As Integer
    On Error GoTo Erro
    
    produtoResidencial = False
    produtoOutros = False
    
    
    For Each a In Avisos
        
        Produto = a.Produto
        
        If (Produto = 109 Or Produto = 1188 Or Produto = 1220 Or Produto = 1221 Or Produto = 1222 Or Produto = 1223 Or Produto = 1224) Then
            
            produtoResidencial = True
            chkEmail.Enabled = True
        
        ElseIf (Produto = 111 Or Produto = 112 Or Produto = 1207 Or Produto = 1228 Or Produto = 1229) Then
                  
            produtoOutros = True
            chkEmail.Enabled = True
            
        Else
            produtoOutros = False
            produtoResidencial = False
            
            Exit For
            
        End If
    
    Next a
        

Exit Sub
   
Erro:
   Me.MousePointer = vbDefault
   MsgBox "N�o foi possivel verificar o produto da proposta.", vbCritical, "Obten��o de Produto"
   Exit Sub
             
End Sub

Function BuscaDestinatario() As String

    Dim dadosSolicitante  As String
    Dim SQL As String
    
    On Error GoTo Erro
    
    BuscaDestinatario = ""
            
    SQL = "SELECT e_mail , nome FROM seguros_db.dbo.cliente_tb WHERE cliente_id = " & AvisoSinistro.SeguradoCliente_id
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    If Not Rs.EOF Then
        BuscaDestinatario = Trim(Rs("nome")) & ";" & Trim(Rs("e_mail"))
        Exit Function
    End If
    
    Exit Function
   
Erro:
   Me.MousePointer = vbDefault
   MsgBox "N�o foi poss�vel obter o email do solicitante.", vbCritical, "Busca de dados do solicitante"
   Exit Function
             
End Function



'Fim - Fernanda Nunes Dutra - 119443889 - Ajustar email de comunica��o de Sinistro - 04/12/2016

Private Sub EnviaMensagem(destinatario As String)
Dim msg As String
Dim SQL As String
Dim i As Long

On Error GoTo Erro

    Me.MousePointer = vbHourglass
    
    
    'In�cio - Fernanda Nunes Dutra - 119443889 - Ajustar email de comunica��o de Sinistro - 04/12/2016
    
    If (produtoResidencial Or produtoOutros) Then
        msg = NovoTextoEmailPlainText()
    Else
        msg = textoEmail()
    End If
    
    
    If (LenB(destinatario) > 0) Then
        
        'RETORNAR A  PROCEDURE ANTIGA
        SQL = "set nocount on exec envia_email_sp '" & destinatario & _
              "', '" & "Documenta��o B�sica para o aviso de sinistro " & "', '" & msg & "'"


        ' RETIRAR - APENAS PARAR TESTAR O ENVIO DE EMAIL POIS A PROCEDURE LEGADA N�O FUNCIONA EM QUALIDADE
'        sSQL = ""
'        sSQL = "exec email_db..SGSS0782_SPI "
'        sSQL = sSQL & " 'SEGP0044', "                                       'SIGLA_RECURSO
'        sSQL = sSQL & " 'SEGBR', "                                          'SIGLA_SISTEMA
'        sSQL = sSQL & " 'SEGP0044', "                                       'CHAVE
'        sSQL = sSQL & " 'SEGP0044@aliancadobrasil.com.br', "                'REMETENTE
'        sSQL = sSQL & " '" & destinatario & "', "                           'DESTINAT�RIO
'        sSQL = sSQL & " 'Documenta��o B�sica para o aviso de sinistro', "   'ASSUNTO DO EMAIL
'        sSQL = sSQL & " '" & msg & "', "                                    'CORPO DO EMAIL
'        sSQL = sSQL & " '', "                                               'COPIA
'        sSQL = sSQL & " '', "                                               'ANEXO
'        sSQL = sSQL & " '1', "                                              '1 - TEXTO ou 2 - HTML'
'        sSQL = sSQL & " '" & cUserName & "', "                              'USUARIO DA APLICACAO
'        sSQL = sSQL & " 0 "                                                 'RETORNO
'
'        'RETIRAR
'        glAmbiente_id = 3

        Call ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, False)
        Me.MousePointer = vbDefault
        
        'RETIRAR
'        glAmbiente_id = 7
    
    Else
    
        MsgBox "N�o foi poss�vel enviar os documentos por falta de cadastro de email.", vbCritical, "Envio de Email"
        chkEmail.Enabled = True
        chkEmail.value = 0
        Me.MousePointer = vbDefault
        
        Exit Sub
    End If
    
    MsgBox "A lista de documentos foi enviada por e-mail.", vbInformation, "Aviso de Sinistro"
    chkEmail.Enabled = False
    'Fim - Fernanda Nunes Dutra - 119443889 - Ajustar email de comunica��o de Sinistro - 04/12/2016
    
   Exit Sub
   
Erro:
   Me.MousePointer = vbDefault
   MsgBox "N�o foi poss�vel enviar o email.", vbCritical, "Envio de Email"
   Exit Sub
         
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call FinalizarAplicacao
End Sub

Private Sub Form_Load()
Dim i As Long
    
    If frmSolicitante.txtEmailSolicitante.Text <> "" Then
        chkEmail.Enabled = True
    Else
        chkEmail.Enabled = False
    End If
   
    CarregaCabecalho
    CarregaGrid
End Sub


Private Sub CarregaGrid()
    On Error GoTo TrataErro
    Dim Rs As ADODB.Recordset
    Dim SQL As String
    Dim bBol As Boolean
    Dim Aviso As Object
    
   
    'In�cio - Fernanda Nunes Dutra - 119443889 - Ajustar email de comunica��o de Sinistro - 04/12/2016
    ObtemProduto
    'Fim - Fernanda Nunes Dutra - 119443889 - Ajustar email de comunica��o de Sinistro - 04/12/2016
   
    Set Rs = BuscaDocumentos()
        
    While Not Rs.EOF
        
        GravaLinha Rs("tipo_copia"), Rs("documento"), IIf(IsNull(Rs("local")), "-", Rs("local"))
        Rs.MoveNext
    Wend
    
    If GridDinamico1.Rows > 1 Then
        GridDinamico1.FixedRows = 1
    End If
        
 
Exit Sub
    Resume
TrataErro:
     TrataErroGeral "Carrega Grid de Documentos", "Aviso de Sinistro"
     Exit Sub
End Sub

Private Function VerificaDocumentos() As ADODB.Recordset
Dim SQL As String

    SQL = ""
    SQL = SQL & " set nocount on select  " & vbCrLf
    SQL = SQL & " 'C�pia' = tp_copia, " & vbCrLf
    SQL = SQL & " 'Documento' = descricao," & vbCrLf
    SQL = SQL & " 'Local' = local_origem," & vbCrLf
    SQL = SQL & " documento_id " & vbCrLf
    SQL = SQL & " from #temp_documento " & vbCrLf
    SQL = SQL & " group by tp_copia, descricao, local_origem, documento_id" & vbCrLf
    SQL = SQL & " order by descricao" & vbCrLf
    
    Set VerificaDocumentos = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, Conex, True)
    
End Function



Private Sub GravaLinha(tipo_documento As String, Descricao As String, sLocal As String)
Dim LinhaL, Linha As String
Dim alturaLocal As Integer
Dim j, i As Integer

For i = 1 To Len(Descricao) Step 90
    If i + 90 > Len(Descricao) Then
        Linha = Linha & Mid(Descricao, i, Len(Descricao) - i + 1)
    Else
        Linha = Linha & Mid(Descricao, i, 90) & Chr(13)
    End If
    
Next i

For j = 1 To Len(sLocal) Step 40
    If j + 40 > Len(sLocal) Then
        LinhaL = LinhaL & Mid(sLocal, j, Len(sLocal) - j + 1)
    Else
        LinhaL = LinhaL & Mid(sLocal, j, 40) & Chr(13)
    End If
    
Next j

    With GridDinamico1
    .Rows = .Rows + 1
    .TextMatrix(.Rows - 1, 0) = tipo_documento
    .TextMatrix(.Rows - 1, 1) = Linha
    .RowHeight(.Rows - 1) = (i / 91) * 240
    
    .TextMatrix(.Rows - 1, 2) = LinhaL
    If .RowHeight(.Rows - 1) < (j / 41) * 240 Then
         .RowHeight(.Rows - 1) = (j / 41) * 240
    End If
        
    End With
    


End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub

'sergio.so - 15/10/2010
'Private Function BuscaDocumentos() As ADODB.Recordset
'
'
'    SQL = ""
'    SQL = SQL & "select distinct isnull(tp.nome, '-') as 'Tipo_Copia',"
'    SQL = SQL & "                          d.descricao as 'Documento',"
'    SQL = SQL & "                   isnull(ods.nome, '-') as 'Local' ,"
'    SQL = SQL & "                                       d.documento_id  from evento_segbr_sinistro_documento_tb essd"
'    SQL = SQL & " inner join evento_segbr_sinistro_atual_tb essa"
'    SQL = SQL & "    on essa.evento_id = essd.evento_id"
'    SQL = SQL & " inner join documento_aviso_sinistro_tb pds"
'    SQL = SQL & "    on essd.documento_id = pds.documento_id"
'    SQL = SQL & "        and pds.ramo_id = essa.ramo_id"
'    SQL = SQL & "        and pds.evento_sinistro_id = essa.evento_sinistro_id"
'    SQL = SQL & "        and pds.Produto_Id = essa.Produto_Id"
'    SQL = SQL & " INNER JOIN documento_tb d ON pds.documento_id = d.documento_id"
'    SQL = SQL & "   LEFT JOIN tp_copia_tb tp ON pds.tp_copia = tp.tp_copia"
'    SQL = SQL & "   LEFT JOIN origem_documento_sinistro_tb ods ON d.origem_documento_sinistro_id = ods.origem_documento_sinistro_id"
'    If lista_avisos <> "" Then
'        SQL = SQL & " where essd.evento_id in (" & lista_avisos & ")"
'    Else
'        SQL = SQL & " where essd.evento_id in ('')"
'    End If
'Set BuscaDocumentos = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
'
'
'End Function
'sergio.eo

Private Function BuscaDocumentos() As ADODB.Recordset


    Dim blnTemDocumento As Boolean
    blnTemDocumento = False
    SQL = ""
    
    
    'In�cio - Fernanda Nunes Dutra - 119443889 - Ajustar email de comunica��o de Sinistro - 04/12/2016
    If (produtoResidencial Or produtoOutros) Then
    
        SQL = "SELECT distinct documento_tb.documento_id," & vbNewLine
        SQL = SQL & " ISNULL(tp_copia_tb.nome, '-') as 'Tipo_Copia', " & vbNewLine
        SQL = SQL & " documento_tb.descricao as 'Documento' , " & vbNewLine
        SQL = SQL & " ISNULL(origem_documento_sinistro_tb.nome, '') as 'Local' , " & vbNewLine
        SQL = SQL & " documento_tb.documento_id , " & vbNewLine
        SQL = SQL & " ISNULL(tp_copia_tb.nome, '') tp_copia_nome, " & vbNewLine
        SQL = SQL & " ISNULL(documento_tb.origem_documento_sinistro_id, 0) origem_documento_sinistro_id, " & vbNewLine
        SQL = SQL & " CONVERT(VARCHAR, documento_tb.documento_id) + ' - ' + documento_tb.descricao documento_descricao " & vbNewLine
        SQL = SQL & " FROM seguros_db..documento_tb documento_tb " & vbNewLine
        SQL = SQL & " LEFT JOIN seguros_db..origem_documento_sinistro_tb origem_documento_sinistro_tb " & vbNewLine
        SQL = SQL & " ON documento_tb.origem_documento_sinistro_id = origem_documento_sinistro_tb.origem_documento_sinistro_id " & vbNewLine
        SQL = SQL & " join seguros_db..documento_aviso_sinistro_tb documento_aviso_sinistro_tb " & vbNewLine
        SQL = SQL & " on documento_aviso_sinistro_tb.documento_id = documento_tb.documento_id " & vbNewLine
        SQL = SQL & " join evento_segbr_sinistro_documento_tb essd on essd.documento_id = documento_aviso_sinistro_tb.documento_id " & vbNewLine
        SQL = SQL & " LEFT JOIN seguros_db..tp_copia_tb tp_copia_tb " & vbNewLine
        SQL = SQL & " ON documento_tb.tp_copia = tp_copia_tb.tp_copia " & vbNewLine
            
    Else
        SQL = SQL & "select distinct isnull(tp.nome, '-') as 'Tipo_Copia'," & vbNewLine
        SQL = SQL & "                          d.descricao as 'Documento'," & vbNewLine
        SQL = SQL & "                   isnull(ods.nome, '-') as 'Local' ," & vbNewLine
        SQL = SQL & "                   d.documento_id  " & vbNewLine
        SQL = SQL & " from evento_segbr_sinistro_documento_tb essd with (nolock) " & vbNewLine
        SQL = SQL & " inner join evento_segbr_sinistro_atual_tb essa with (nolock) " & vbNewLine
        SQL = SQL & "    on essa.evento_id = essd.evento_id" & vbNewLine
        SQL = SQL & " inner join documento_aviso_sinistro_tb pds with (nolock) " & vbNewLine
        SQL = SQL & "    on essd.documento_id = pds.documento_id" & vbNewLine
        SQL = SQL & "        and pds.ramo_id = essa.ramo_id" & vbNewLine
        SQL = SQL & "        and pds.evento_sinistro_id = essa.evento_sinistro_id" & vbNewLine
        SQL = SQL & "        and pds.Produto_Id = essa.Produto_Id" & vbNewLine
        SQL = SQL & " INNER JOIN documento_tb d with (nolock)  ON pds.documento_id = d.documento_id" & vbNewLine
        SQL = SQL & "   LEFT JOIN tp_copia_tb tp with (nolock)  ON pds.tp_copia = tp.tp_copia" & vbNewLine
        SQL = SQL & "   LEFT JOIN origem_documento_sinistro_tb ods with (nolock)  ON d.origem_documento_sinistro_id = ods.origem_documento_sinistro_id" & vbNewLine
  
    
    End If
    'Fim - Fernanda Nunes Dutra - 119443889 - Ajustar email de comunica��o de Sinistro - 04/12/2016
    
    
    If lista_avisos <> "" Then
        SQL = SQL & " where essd.evento_id in (" & lista_avisos & ")" & vbNewLine
    Else
        SQL = SQL & " where essd.evento_id in ('')" & vbNewLine
    End If
   
    
    Set BuscaDocumentos = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

'sergio.sn - 13/10/2010
' ALTERA��O PARA FOR�AR A GRAVA��O DE DOCUMENTO Formul�rio "aviso de sinistro" assinado

If BuscaDocumentos.EOF Then

    Set BuscaDocumentos = Nothing
    
    Dim arrEvento() As String
    Dim strEventoSinistroId As String

    
    arrEvento = Split(lista_avisos, ",")
    
    Dim C As Long
    Dim i As Long
    Dim rsPesquisa As ADODB.Recordset
    
    For C = 0 To UBound(arrEvento)
    
        Set BuscaDocumentos = Nothing
        
        SQL = " SELECT * FROM evento_segbr_sinistro_documento_tb with (nolock)  " & vbNewLine
        SQL = SQL & "   WHERE evento_id = " & Trim(arrEvento(C))
        
        Set BuscaDocumentos = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        
        If BuscaDocumentos.EOF Then
        
            Set rsPesquisa = Nothing
            
            'causa_id
            
            SQL = " SELECT evento_sinistro_id FROM evento_segbr_sinistro_atual_tb with (nolock) " & vbNewLine
            SQL = SQL & "   WHERE evento_id = " & Trim(arrEvento(C))
            
            Set rsPesquisa = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
            
            If Not rsPesquisa.EOF Then
            
                 strEventoSinistroId = CStr(rsPesquisa("evento_sinistro_id"))
                 
                 Set rsPesquisa = Nothing
            End If
               
            Set AvisoRegras = CreateObject("SEGL0144.cls00326")
        
            AvisoRegras.mvarAmbienteId = glAmbiente_id
            AvisoRegras.mvarSiglaSistema = gsSIGLASISTEMA
            AvisoRegras.mvarSiglaRecurso = App.Title
            AvisoRegras.mvarDescricaoRecurso = App.FileDescription
            
            Set rsPesquisa = AvisoRegras.ObterDocumentos(Avisos(C + 1).Produto, _
                                                        Avisos(C + 1).Ramo, _
                                                        Avisos(C + 1).SubRamo, _
                                                        "2", _
                                                        CLng(strEventoSinistroId), _
                                                        Avisos(C + 1).Cobertura, True)
                                                        
            Set AvisoRegras = Nothing
            
            If Not rsPesquisa.EOF Then
                   
                Do While Not rsPesquisa.EOF
                
                    If Not AvisoSinistro.IncluirDocumento(rsPesquisa("documento_id"), rsPesquisa("documento")) Then GoTo ProximoRegistro
                    rsPesquisa.MoveNext
                    
                Loop
                
                'Inserir documentos
                Call AvisoSinistro.InserirDocumento(Trim(arrEvento(C)))
                
                blnTemDocumento = True
                
                AvisoSinistro.mvarExisteDocumento = False
                
            End If
        
        End If
        
ProximoRegistro:
    
    Next C
    
       
    If blnTemDocumento = False Then
    
        Set BuscaDocumentos = Nothing

        SQL = ""
        SQL = SQL & "select distinct isnull(tp.nome, '-') as 'Tipo_Copia'," & vbNewLine
        SQL = SQL & "                          d.descricao as 'Documento'," & vbNewLine
        SQL = SQL & "                   isnull(ods.nome, '-') as 'Local' ," & vbNewLine
        SQL = SQL & "                                       d.documento_id " & vbNewLine
        SQL = SQL & "from evento_segbr_sinistro_documento_tb essd with (nolock) " & vbNewLine
        SQL = SQL & " inner join evento_segbr_sinistro_atual_tb essa with (nolock) " & vbNewLine
'        SQL = SQL & "    on essa.evento_id = essd.evento_id with (nolock) " & vbNewLine        'CONFORME ORIENTA��O DO CVAO (CONFITEC-RJ) - 25/06/2013
        SQL = SQL & "    on essa.evento_id = essd.evento_id  " & vbNewLine
        SQL = SQL & " inner join documento_aviso_sinistro_tb pds with (nolock) " & vbNewLine
        SQL = SQL & "    on essd.documento_id = pds.documento_id" & vbNewLine
        SQL = SQL & "        and pds.ramo_id = essa.ramo_id" & vbNewLine
        SQL = SQL & "        and pds.evento_sinistro_id = essa.evento_sinistro_id" & vbNewLine
        SQL = SQL & "        and pds.Produto_Id = ISNULL(essa.Produto_Id, 0)" & vbNewLine
        SQL = SQL & " INNER JOIN documento_tb d with (nolock) ON pds.documento_id = d.documento_id" & vbNewLine
        SQL = SQL & "   LEFT JOIN tp_copia_tb tp with (nolock) ON pds.tp_copia = tp.tp_copia" & vbNewLine
        SQL = SQL & "   LEFT JOIN origem_documento_sinistro_tb ods with (nolock) ON d.origem_documento_sinistro_id = ods.origem_documento_sinistro_id" & vbNewLine
        SQL = SQL & " WHERE d.documento_id = 1" & vbNewLine
        SQL = SQL & "   AND pds.produto_id  = 0 " & vbNewLine
        SQL = SQL & "   AND pds.ramo_id  = 0 " & vbNewLine
        
        
        Set BuscaDocumentos = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
        'TIPO DE C�PIA: Original
        'FORMULARIO: Formul�rio "aviso de sinistro" assinado
        'NOME: Documento encaminhado pela Seguradora no kit de sinistro, que dever� ser datado e assinado
        '      pelo solicitante (na aus�ncia deste, o documento pode ser encontrado no Banco do Brasil)
        
    Else
    
        SQL = ""
        SQL = SQL & "select distinct isnull(tp.nome, '-') as 'Tipo_Copia'," & vbNewLine
        SQL = SQL & "                          d.descricao as 'Documento'," & vbNewLine
        SQL = SQL & "                   isnull(ods.nome, '-') as 'Local' ," & vbNewLine
        SQL = SQL & "                                       d.documento_id" & vbNewLine
        SQL = SQL & "from evento_segbr_sinistro_documento_tb essd with (nolock) " & vbNewLine
        SQL = SQL & " inner join evento_segbr_sinistro_atual_tb essa with (nolock) " & vbNewLine
        SQL = SQL & "    on essa.evento_id = essd.evento_id" & vbNewLine
        SQL = SQL & " inner join documento_aviso_sinistro_tb pds with (nolock) " & vbNewLine
        SQL = SQL & "    on essd.documento_id = pds.documento_id" & vbNewLine
        SQL = SQL & "        and pds.ramo_id = essa.ramo_id" & vbNewLine
        SQL = SQL & "        and pds.evento_sinistro_id = essa.evento_sinistro_id" & vbNewLine
        SQL = SQL & "        and pds.Produto_Id = essa.Produto_Id" & vbNewLine
        SQL = SQL & " INNER JOIN documento_tb d with (nolock) ON pds.documento_id = d.documento_id" & vbNewLine
        SQL = SQL & "   LEFT JOIN tp_copia_tb tp with (nolock) ON pds.tp_copia = tp.tp_copia" & vbNewLine
        SQL = SQL & "   LEFT JOIN origem_documento_sinistro_tb ods with (nolock) ON d.origem_documento_sinistro_id = ods.origem_documento_sinistro_id" & vbNewLine
        If lista_avisos <> "" Then
            SQL = SQL & " where essd.evento_id in (" & lista_avisos & ")" & vbNewLine
        Else
            SQL = SQL & " where essd.evento_id in ('')" & vbNewLine
        End If
        
        Set BuscaDocumentos = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
        
    End If

End If

'sergio.en - 13/10/2010

End Function

