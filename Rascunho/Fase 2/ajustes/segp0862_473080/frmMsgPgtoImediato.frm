VERSION 5.00
Begin VB.Form frmMsgPgtoImediato 
   Caption         =   "Resultado da Classificação"
   ClientHeight    =   2280
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2280
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   1710
      TabIndex        =   1
      Top             =   1305
      Width           =   1200
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H0000FFFF&
      Caption         =   "Aviso foi classificado como Pagamento Imediato!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   315
      TabIndex        =   0
      Top             =   360
      Width           =   4065
   End
End
Attribute VB_Name = "frmMsgPgtoImediato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
Private Sub btnOK_Click()
    Unload Me
    MsgPgtoImediato = 1
End Sub
'14/01/2019 (ntedencia) 00210408-esteira_flash (Fim)
