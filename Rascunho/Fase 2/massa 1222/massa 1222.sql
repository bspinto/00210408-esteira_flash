--localizar proposta para o produto 809
		SELECT TOP 100 p.produto_id
			,p.ramo_id
			,p.proposta_id --, e.tp_cobertura_id, e.dt_inicio_vigencia_esc
			,ISNULL(ap.dt_fim_vigencia, ISNULL(ad.dt_fim_vigencia, pf.dt_fim_vig)) AS dt_fim_vigencia
		--    into #proposta
		FROM SEGUROS_DB.DBO.PROPOSTA_TB p WITH (NOLOCK)
		-- inner join escolha_tp_cob_generico_tb e on p.proposta_id = e.proposta_id
		INNER JOIN escolha_tp_cob_res_tb e WITH (NOLOCK)
			ON p.proposta_id = e.proposta_id
		--vigencia vou adesao ou na fechada (ap�lice_tb)
		LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB pf WITH (NOLOCK)
			ON p.PROPOSTA_ID = pf.PROPOSTA_ID
		LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ad WITH (NOLOCK)
			ON p.PROPOSTA_ID = ad.PROPOSTA_ID
		LEFT JOIN seguros_db.dbo.apolice_tb ap WITH (NOLOCK)
			ON p.proposta_id = ap.proposta_id
		LEFT JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)
			ON p.proposta_id = s.proposta_id
		WHERE p.produto_id = 1222
			AND p.ramo_id = 14
			--and s.evento_sinistro_id = 324
			AND p.situacao = 'i'
			AND e.dt_fim_vigencia_esc IS NULL
			AND e.tp_cobertura_id = 16
			AND P.subramo_id = 1402
		--ORDER BY dt_inicio_vigencia_esc DESC
		
begin tran

if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 40249790, @dt_ini = '20180601', @dt_fim = '20200901' else select 'transa��o fechada'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 40249864, @dt_ini = '20180601', @dt_fim = '20200901' else select 'transa��o fechada'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 40249910, @dt_ini = '20180601', @dt_fim = '20200901' else select 'transa��o fechada'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 40249913, @dt_ini = '20180601', @dt_fim = '20200901' else select 'transa��o fechada'
--commit

WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 40249790, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 40249864, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 40249910, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 40249913, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1

SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 40249790
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 40249864
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 40249910
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 40249913
--commit
		
		
  WHILE @@TRANCOUNT > 0 ROLLBACK
  IF @@TRANCOUNT = 0 BEGIN TRAN
  IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL BEGIN DROP TABLE #Equipamento END
  CREATE TABLE #Equipamento (Codigo INT  ,TipoBem VARCHAR(60)  ,Modelo VARCHAR(60)  ,Dano INT  ,Orcamento INT  ,Valor NUMERIC(15,2)  ,Laudo INT  ,NomeAssistencia VARCHAR(60)  ,TelAssistencia VARCHAR(15)  ,Descricao VARCHAR(255))

  INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
  SELECT '000000037', 'Ar Condicionado', 'Acima de 19000BTUS', '1', '1', '600.00', '1', 'Ar teC', '(32) 34414-444_', 'dano no ar'




exec seguros_db.dbo.segs14618_sps 1222, 14, 3, 16 ,40249790, 1500.00 ,'2020-06-06' 
exec seguros_db.dbo.segs14618_sps 1222, 14, 3, 16 ,40249864, 1500.00 ,'2020-06-06' 
exec seguros_db.dbo.segs14618_sps 1222, 14, 3, 16 ,40249910, 1500.00 ,'2020-06-06' 
exec seguros_db.dbo.segs14618_sps 1222, 14, 3, 16 ,40249913, 1500.00 ,'2020-06-06' 


---40249790
O SINISTRO N�O FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO:    Informa��es:    Todos os equipamentos informados com  perda total est�o com laudo.     Todos os equipamentos informados como  pass�veis de reparo, est�o com laudo ou or�amento.     Todos os equipamentos est�o com valor de estimativa  dentro do limite permitido para o pagamento imediato.     Estimativa/Or�amento R$600.00 est� dentro do limite  para o pagamento imediato R$3500.00.     Per�odo de relacionamento do cliente com a BrasilSeg 737 dias  atingiu o valor m�nimo de 365 dias para o pagamento imediato.     Pagamento imediato negado:  Contagem de dias do �ltimo sinistro 1 dias  n�o atingiu o valor m�nimo de 365 dias.     O CPF/CNPJ 01929405898  n�o possui restri��o (A��o Judicial/Ind�cio de Fraude).     O CPF/CNPJ 01929405898  n�o possui sinistro com pedido de rean�lise.     A proposta 40249790 est� sem d�bitos em aberto  na data da abertura do sinistro 07/06/2020.     A data de ocorr�ncia do aviso  est� dentro do per�odo de vig�ncia da ap�lice.     A data de ocorr�ncia do aviso 06/06/2020  est� dentro do per�odo de vig�ncia da cobertura.     O Valor total do preju�zo l�quido de franquia  foi menor que o valor da importancia segurada     Valor da Importancia Segurada:  1500.00  Valor do Preju�zo Reclamado:  600.00  Valor M�nimo da Franquia:  500.00  Valor da Franquia:  0.00  Valor Total do Preju�zo L�quido de Franquia:  600.00  Valor a Indenizar:  600.00