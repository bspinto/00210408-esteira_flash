  WHILE @@TRANCOUNT > 0 ROLLBACK
  IF @@TRANCOUNT = 0 BEGIN TRAN
  IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL BEGIN DROP TABLE #Equipamento END
  CREATE TABLE #Equipamento (Codigo INT  ,TipoBem VARCHAR(60)  ,Modelo VARCHAR(60)  ,Dano INT  ,Orcamento INT  ,Valor NUMERIC(15,2)  ,Laudo INT  ,NomeAssistencia VARCHAR(60)  ,TelAssistencia VARCHAR(15)  ,Descricao VARCHAR(255))

  INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
  SELECT '000000127', 'Ar Condicionado', 'Acima de 19000BTUS', '1', '1', '5000.00', '1', 'Ar teC', '(32) 34414-444_', 'dano no ar'

--exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,50806754, 505.00 ,'2020-06-25'

--CREATE PROCEDURE SEGS14618_SPS 
declare 
@produto_id INT  = 809
 ,@ramo_id INT   = 14
 ,@evento_sinistro_id INT   = 3
 ,@tp_cobertura_id INT  =16
 ,@proposta_id INT  = 50806754
 ,@valor_is NUMERIC(15, 2)  = 505
 ,@dt_ocorrencia SMALLDATETIME  = '20200625'
 ,@aviso_rural INT = 0  
  
 -- Declara��o e tratamento de variaveis (inicio)    
 DECLARE @PgtoImediato AS SMALLINT = 1 -- (1-Sim / N�o)    
 DECLARE @Detalhamento AS VARCHAR(3000) = ''  
 DECLARE @ValorTotal AS NUMERIC(15, 2)  
 DECLARE @QuebraLinha AS NVARCHAR(20) = ''  
 DECLARE @Dt_ini AS SMALLDATETIME  
 DECLARE @Dt_fim AS SMALLDATETIME  
 DECLARE @ValorApurar AS NUMERIC(15, 2)  
 DECLARE @tp_sinistro_parametro_id AS INT   
   
 -- (fim) Declara��o e tratamento de variaveis     
 --constantes    
 -- Laudo INT    
 --DECLARE @LaudoSim AS SMALLINT = 1  
 DECLARE @LaudoNao AS SMALLINT = 0  
 --DECLARE @OrcamentoSim AS SMALLINT = 1    
 DECLARE @orcamentoNao AS SMALLINT = 0  
 --Dano INT    
 DECLARE @Reparo AS SMALLINT = 1  
 DECLARE @PerdaTotal AS SMALLINT = 2  
  
 -----------    
 
  --SOMATORIO DA ESTIMATIVA DOS EQUIPAMENTOS    
  SELECT @ValorTotal = SUM(Valor)  
  FROM #Equipamento  
  
  --Valida��o totas dos equipamentos com a IS    
  SELECT @ValorApurar = CASE   
    WHEN @ValorTotal > @valor_is  
     THEN @valor_is  
    ELSE @ValorTotal  
    END  
  
  SET @QuebraLinha = CONCAT (  
    CHAR(13)  
    ,CHAR(10)  
    )  
  
  --VALIDA��O TELA: PERDA TOTAL             
  --LAUDO       
  IF EXISTS (  
    SELECT TOP (1) 1  
    FROM #Equipamento a  
    WHERE a.dano = @PerdaTotal  
     AND a.laudo = @LaudoNao  
    )  
  BEGIN  
   --RECUSA (SEM LAUDO)    
   SET @PgtoImediato = 0  
   SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Pagamento imediato negado:'  
     ,@QuebraLinha  
     ,'foram informados equipamentos com'  
     ,@QuebraLinha  
     ,'perda total e sem o laudo.'  
     )  
  END  
  ELSE  
  BEGIN  
   SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Todos os equipamentos informados com'  
     ,@QuebraLinha  
     ,'perda total est�o com laudo.'  
     )  
  END  
    
  SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,' ')  
  
  --VALIDA��O TELA: PASS�VEL DE REPARO    
  --LAUDO    
  IF EXISTS (  
    SELECT TOP (1) 1  
    FROM #Equipamento a  
    WHERE a.dano = @Reparo  
     AND a.laudo = @LaudoNao  
     AND a.orcamento = @orcamentoNao  
    )  
  BEGIN  
   --RECUSA (SEM LAUDO)    
   SET @PgtoImediato = 0  
   SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Pagamento imediato negado:'  
     ,@QuebraLinha  
     ,'foram informados equipamentos como'  
     ,@QuebraLinha  
     ,'pass�veis de reparo, sem o laudo e sem or�amento.'  
     )  
  END  
  ELSE    BEGIN  
   SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Todos os equipamentos informados como'  
     ,@QuebraLinha  
     ,'pass�veis de reparo, est�o com laudo ou or�amento.'  
     )  
  END  
  
  SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,' ')  
  
  --VALIDA��O DE TELA: EQUIPAMENTOS COM OR�AMENTO SUPERIOR AO PARAMETRIZADO    
  IF EXISTS (  
    SELECT TOP (1) 1  
    FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)  
    INNER JOIN #equipamento b ON a.equipamento_id = b.codigo  
    WHERE b.dano = @PerdaTotal  
     AND b.Valor > a.vl_perda_total  
     AND a.dt_fim_vigencia IS NULL  
    )  
   OR EXISTS (  
    SELECT TOP (1) 1  
    FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)  
    INNER JOIN #equipamento b WITH (NOLOCK) ON a.equipamento_id = b.codigo  
    WHERE b.dano = @reparo  
     AND b.Valor > a.vl_reparo  
     AND a.dt_fim_vigencia IS NULL  
    )  
  BEGIN  
   --RECUSA    
   SET @PgtoImediato = 0  
   SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Pagamento imediato negado: Foram informados equipamentos'  
     ,@QuebraLinha  
     ,'com valor de estimativa superior ao limite permitido.'  
     )  
  END  
  ELSE  
  BEGIN  
   SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Todos os equipamentos est�o com valor de estimativa'  
     ,@QuebraLinha  
     ,'dentro do limite permitido para o pagamento imediato.'  
     )  
  END  
  
  SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,' ')  
  
  --VALIDA��O DOS PAR�MENTROS    
  DECLARE @sinistro_parametro_chave_id INT  
   ,@CPF_CNPJ VARCHAR(30)  
   ,@SQL AS NVARCHAR(1000)  
   ,@ParmDefinition AS NVARCHAR(300)  
  
  --PEGANDO A CHAVE DOS PARAMETROS    
  --ENCONTRANDO A CHAVE VIGENTE DA PROPOSTA    
  SELECT DISTINCT @sinistro_parametro_chave_id = B.sinistro_parametro_chave_id  
  FROM SEGUROS_DB.dbo.tp_sinistro_parametro_tb a WITH (NOLOCK)  
  INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_chave_tb b WITH (NOLOCK) ON a.tp_sinistro_parametro_id = b.tp_sinistro_parametro_id  
  INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb c WITH (NOLOCK) ON b.sinistro_parametro_chave_id = c.sinistro_parametro_chave_id  
  INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb d WITH (NOLOCK) ON c.sinistro_parametro_regra_id = d.sinistro_parametro_regra_id  
  WHERE 1 = 1  
   AND b.produto_id = @produto_id -- 1241    
   AND b.ramo_id = @ramo_id -- 14     
   AND b.evento_sinistro_id = @evento_sinistro_id -- 3 --    
   AND b.tp_cobertura_id = @tp_cobertura_id -- 16 --    
   AND b.dt_fim_vigencia IS NULL  
  
  IF @sinistro_parametro_chave_id IS NULL  
  BEGIN  
   SET @PgtoImediato = 0  
   SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     --,'O produto ' + convert(VARCHAR(60), @produto_id) + ' ramo ' + convert(VARCHAR(60), @ramo_id) + ' evento ' + convert(VARCHAR(60), @evento_sinistro_id) + ' cobertura ' + convert(VARCHAR(60), @tp_cobertura_id)    
     ,'O produto,ramo,evento e cobertura selecionados no aviso'  
     ,@QuebraLinha  
     ,'n�o est�o habilitados para o fluxo do pagamento imediato.'  
     ,@QuebraLinha  
     ,' '  
     )  
            
  END  
  
  --PEGANDO O CPF/CNPJ DO CLIENTE    
  SELECT @CPF_CNPJ = isnull(isnull(CLIENTE.cpf_cnpj, FISICA.cpf), juridica.cgc)  
  FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)  
  INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID  
  LEFT JOIN SEGUROS_DB.DBO.pessoa_fisica_tb FISICA WITH (NOLOCK) ON FISICA.pf_cliente_id = CLIENTE.CLIENTE_ID  
  LEFT JOIN SEGUROS_DB.DBO.pessoa_juridica_tb juridica WITH (NOLOCK) ON juridica.pj_cliente_id = CLIENTE.CLIENTE_ID  
  WHERE PROPOSTA.PROPOSTA_ID = @proposta_id --42901994 --104 --    
  
  -----------------------------------------------------------------------------------------------------------------------------------------------------    
  --###################################################################################################################################################    
  --1� VALIDA��O DA ESTIMATIVA 'valor_estimativa'    
  --Regra: O valor do somatorio da estimativas dos equipamentos informados tem que ser "menor/maior/menor ou igual / maior ou igual/diferente" que o valor parametrizado.    
  --BUSCANDO A REGRA PARAMETRIZADA    
  /*    
   SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo  ,* FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE    
    INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA    
   ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id    
   WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id    
     AND REGRA.NOME = 'valor_estimativa'    
  */  
  DECLARE @VALIDA_REGRAS INT = 1  
  
  SELECT @sql = 'IF( ' + convert(VARCHAR(20), convert(NUMERIC(15, 2), @ValorApurar)) + ' ' + CHAVE.OPERADOR + ' ' + CHAVE.VALOR + ')     
         SELECT @retornoOUT = 1    
        ELSE     
        SELECT @retornoOUT = 0    
        '  
  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE  
  INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
  WHERE 1 = 1  
   AND CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079     
   AND REGRA.NOME = 'valor_estimativa' --PARAMETRO    
  
  SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';  
  
  EXECUTE sp_executesql @SQL  
   ,@ParmDefinition  
   ,@retornoOUT = @VALIDA_REGRAS OUTPUT  
  
  IF @VALIDA_REGRAS = 1  
  BEGIN  
   SELECT @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Estimativa/Or�amento R$'  
     ,convert(VARCHAR(20), @ValorApurar)  
     ,' est� dentro do limite'  
     ,@QuebraLinha  
     ,'para o pagamento imediato R$'  
     ,convert(VARCHAR(20), CHAVE.valor)  
     ,'.'  
     )  
   FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)  
   INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
   WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --    
    AND REGRA.NOME = 'valor_estimativa'  
  END  
  ELSE  
  BEGIN  
   --RECUSA    
   SET @PgtoImediato = 0  
  
   SELECT @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Pagamento imediato negado: Estimativa/Or�amento R$'  
     ,convert(VARCHAR(20), @ValorApurar)  
     ,@QuebraLinha  
     ,'est� acima do limite para o pagamento imediato  R$ '  
     ,convert(VARCHAR(20), CHAVE.valor)  
     ,'.'  
     )  
   FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)  
   INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
   WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --    
    AND REGRA.NOME = 'valor_estimativa'  
  END  
  
  SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,' ')  
  -----------------------------------------------------------------------------------------------------------------------------------------------------    
  --###################################################################################################################################################    
  --2� VALIDA��O DO PERIODO DE RELACIONAMENTO: 'periodo_relacionamento'    
  -- Regra: O cliente tem que ter uma ou mais apolices vigentes, onde a soma dos dias das apolices vigentes � maior que o periodo parametrizado     
  -- e que neste periodo o cliente n�o tenha ficado nenhum dia sem ao menos uma apolice vigente.    
  --BUSCANDO A REGRA PARAMETRIZADA    
  /*    
   SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE    
    INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA    
   ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id    
   WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id    
     AND REGRA.NOME = 'periodo_relacionamento'    
   */  
  IF (object_id('tempdb..#propostas') IS NOT NULL)  
  BEGIN  
   DROP TABLE #propostas  
  END  
  
  IF (object_id('tempdb..#propostas_aux') IS NOT NULL)  
  BEGIN  
   DROP TABLE #propostas_aux  
  END  
  
  IF (object_id('tempdb..#propostas_aux_ESPELHO') IS NOT NULL)  
  BEGIN  
   DROP TABLE #propostas_aux_ESPELHO  
  END  
  
  CREATE TABLE #propostas_aux_ESPELHO (  
   ID INT IDENTITY(1, 1) NOT NULL  
   ,produto_id INT NULL  
   ,cpf_cnpj VARCHAR(14) NULL  
   ,proposta_id INT NULL  
   ,dt_inicio_vigencia SMALLDATETIME NULL  
   ,dt_fim_vigencia SMALLDATETIME NULL  
   ,data_limite SMALLDATETIME NULL  
   ,data_atual SMALLDATETIME NULL  
   ,DIAS INT NULL -- TOTAL DE DIAS VIGENTE DA PROPOSTA    
   ,FLAG BIT NULL -- TEVE INTERRUP�AO SIM OU NAO    
   )  
    
  CREATE TABLE #propostas_aux (  
   ID INT IDENTITY(1, 1) NOT NULL  
   ,produto_id INT NULL  
   ,cpf_cnpj VARCHAR(14) NULL  
   ,proposta_id INT NULL  
   ,dt_inicio_vigencia SMALLDATETIME NULL  
   ,dt_fim_vigencia SMALLDATETIME NULL  
   ,data_limite SMALLDATETIME NULL  
   ,data_atual SMALLDATETIME NULL  
   ,DIAS INT NULL -- TOTAL DE DIAS VIGENTE DA PROPOSTA    
   ,FLAG BIT NULL -- TEVE INTERRUP�AO SIM OU NAO    
   )  
  
  CREATE TABLE #propostas (  
   ID INT PRIMARY KEY IDENTITY(1, 1) NOT NULL  
   ,produto_id INT NULL  
   ,cpf_cnpj VARCHAR(14) NULL  
   ,proposta_id INT NULL  
   ,dt_inicio_vigencia SMALLDATETIME NULL  
   ,dt_fim_vigencia SMALLDATETIME NULL  
   ,data_limite SMALLDATETIME NULL  
   ,data_atual SMALLDATETIME NULL  
   ,DIAS INT NULL -- TOTAL DE DIAS VIGENTE DA PROPOSTA    
   ,FLAG BIT NULL -- TEVE INTERRUP�AO SIM OU NAO    
   )  
  
  DECLARE @PERIODO_MIN_DE_RELACIONAMENTO INT  
  
  SELECT @PERIODO_MIN_DE_RELACIONAMENTO = CHAVE.valor  
  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)  
  INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
  WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id  
   AND REGRA.NOME = 'periodo_relacionamento'  
  
  --ENCONTRANDO AS PROPOSTAS DO CLIENTE     
  INSERT INTO #propostas_aux (  
   produto_id  
   ,cpf_cnpj  
   ,proposta_id  
   ,dt_inicio_vigencia  
   ,dt_fim_vigencia  
   ,data_limite  
   ,data_atual  
   )  
  SELECT DISTINCT PROPOSTA.PRODUTO_ID  
   ,CLIENTE.cpf_cnpj AS CPF_CNPJ  
   ,PROPOSTA.proposta_id  
   ,FORMAT(ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig)), 'yyyy-MM-dd') AS dt_inicio_vigencia  
   ,FORMAT(ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig)), 'yyyy-MM-dd') AS dt_fim_vigencia  
   ,FORMAT(GETDATE() - @PERIODO_MIN_DE_RELACIONAMENTO, 'yyyy-MM-dd') AS data_limite  
   ,FORMAT(GETDATE(), 'yyyy-MM-dd') AS data_atual  
  --INTO #propostas    
  FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)  
  INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID  
  LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB FECHADA WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = FECHADA.PROPOSTA_ID  
  LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ADESAO WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = ADESAO.PROPOSTA_ID  
  LEFT JOIN seguros_db.dbo.apolice_tb APOLICE WITH (NOLOCK) ON PROPOSTA.proposta_id = APOLICE.proposta_id  
  WHERE CLIENTE.cpf_cnpj = @CPF_CNPJ  
   AND PROPOSTA.ramo_id = 14 --@RAMO_ID    
   AND PROPOSTA.situacao = 'i'  
    
  UNION ALL  
    
  SELECT DISTINCT PROPOSTA.PRODUTO_ID  
   ,CLIENTE.cpf_cnpj AS CPF_CNPJ  
   ,PROPOSTA.proposta_id  
   ,FORMAT(ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig)), 'yyyy-MM-dd') AS dt_inicio_vigencia  
   ,FORMAT(ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig)), 'yyyy-MM-dd') AS dt_fim_vigencia  
   ,FORMAT(GETDATE() - @PERIODO_MIN_DE_RELACIONAMENTO, 'yyyy-MM-dd') AS data_limite  
   ,FORMAT(GETDATE(), 'yyyy-MM-dd') AS data_atual  
  FROM [ABSS].SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)  
  INNER JOIN [ABSS].SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID  
  LEFT JOIN [ABSS].SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB FECHADA WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = FECHADA.PROPOSTA_ID  
  LEFT JOIN [ABSS].SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ADESAO WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = ADESAO.PROPOSTA_ID  
  LEFT JOIN [ABSS].seguros_db.dbo.apolice_tb APOLICE WITH (NOLOCK) ON PROPOSTA.proposta_id = APOLICE.proposta_id  
  WHERE CLIENTE.cpf_cnpj = @CPF_CNPJ  
   AND PROPOSTA.ramo_id = 14 --@RAMO_ID    
   AND PROPOSTA.situacao = 'i'  
  --ORDER BY 4  
  
  --REMOVENDO AS PROPOSTAS QUE TIVERAM vig�ncia FINDADAS ANTES DA DATA LIMITE    
  DELETE PROP  
  FROM #propostas_aux PROP  
  WHERE dt_fim_vigencia < data_limite - 1  
  
  --REMOVENDO TODOS EM QUE A DATA DE INICIO DE vig�ncia INICIA DEPOIS DA DATA ATUAL    
  DELETE PROP  
  FROM #propostas_aux PROP  
  WHERE dt_inicio_vigencia > data_atual  
  
  --DELETANDO AS PROPOSTA QUE ESTAO COM vig�ncia DENTRO DE OUTRAS     
  DELETE A  
  FROM #propostas_aux A  
  INNER JOIN #propostas_aux B 
    ON A.ID = B.ID + 1  
  WHERE A.dt_inicio_vigencia BETWEEN B.DT_INICIO_vigencia  
    AND B.DT_FIM_vigencia  
   AND A.DT_FIM_vigencia BETWEEN B.DT_INICIO_vigencia  
    AND B.DT_FIM_vigencia  
  
   ---
   DECLARE @total_registro_afetados INT = 1
   WHILE @total_registro_afetados > 0
   BEGIN
     INSERT INTO #propostas_aux_ESPELHO(produto_id 
     ,cpf_cnpj 
     ,proposta_id 
     ,dt_inicio_vigencia 
     ,dt_fim_vigencia 
     ,data_limite
     ,data_atual 
     ,DIAS 
     ,FLAG) 
     SELECT produto_id 
     ,cpf_cnpj 
     ,proposta_id 
     ,dt_inicio_vigencia 
     ,dt_fim_vigencia 
     ,data_limite
     ,data_atual 
     ,DIAS 
     ,FLAG 
     FROM #propostas_aux
     ORDER BY dt_inicio_vigencia
     
    DELETE A  
    FROM #propostas_aux_ESPELHO A  
    INNER JOIN #propostas_aux_ESPELHO B 
      ON A.ID = B.ID + 1  
    WHERE A.dt_inicio_vigencia BETWEEN B.DT_INICIO_vigencia  
      AND B.DT_FIM_vigencia  
     AND A.DT_FIM_vigencia BETWEEN B.DT_INICIO_vigencia  
      AND B.DT_FIM_vigencia     
    SET @total_registro_afetados = @@ROWCOUNT
        
    IF @total_registro_afetados > 0
    BEGIN
     DELETE FROM #propostas_aux
     
     INSERT INTO #propostas_aux (produto_id 
     ,cpf_cnpj 
     ,proposta_id 
     ,dt_inicio_vigencia 
     ,dt_fim_vigencia 
     ,data_limite
     ,data_atual 
     ,DIAS 
     ,FLAG) 
     SELECT produto_id 
     ,cpf_cnpj 
     ,proposta_id 
     ,dt_inicio_vigencia 
     ,dt_fim_vigencia 
     ,data_limite
     ,data_atual 
     ,DIAS 
     ,FLAG 
     FROM #propostas_aux_ESPELHO
     ORDER BY dt_inicio_vigencia  
     
     DELETE FROM #propostas_aux_ESPELHO
   END  
  END
  
  --INICIALIZADO OS ID    
  INSERT INTO #propostas (  
   produto_id  
   ,cpf_cnpj  
   ,proposta_id  
   ,dt_inicio_vigencia  
   ,dt_fim_vigencia  
   ,data_limite  
   ,data_atual  
   )  
  SELECT produto_id  
   ,cpf_cnpj  
   ,proposta_id  
   ,dt_inicio_vigencia  
   ,dt_fim_vigencia  
   ,data_limite  
   ,data_atual  
  FROM #propostas_aux WITH (NOLOCK)  
  ORDER BY 4  
  
--DELETANDO AS PROPOSTA QUE ESTAO COM vig�ncia DENTRO DE OUTRAS     
  DELETE A  
  FROM #propostas A  
  INNER JOIN #propostas B 
    ON A.ID = B.ID + 1  
  WHERE A.dt_inicio_vigencia BETWEEN B.DT_INICIO_vigencia  
    AND B.DT_FIM_vigencia  
   AND A.DT_FIM_vigencia BETWEEN B.DT_INICIO_vigencia  
    AND B.DT_FIM_vigencia    
  
  --AJUSTANDO O FIM DE vig�ncia COM O INICIO DE vig�ncia DA PROXIMA    
  UPDATE A  
  SET A.DT_INICIO_vigencia = B.DT_FIM_vigencia  
   ,A.FLAG = 1  
  FROM #propostas A  
  INNER JOIN #propostas B ON A.ID = B.ID + 1  
  WHERE B.DT_FIM_vigencia + 1 BETWEEN A.DT_INICIO_vigencia  
    AND A.DT_FIM_vigencia  
  
  --COLOCANDO A FLAG NOS PERIODOS INETERRUPTOS    
  UPDATE B  
  SET B.FLAG = 1  
  FROM #propostas A  
  INNER JOIN #propostas B ON A.ID = B.ID + 1  
  WHERE B.FLAG IS NULL  
   AND a.flag = 1  
  
  
  --GUARDANDO A DATA DE INICIO DE vig�ncia DA MENOR PROPOSTA    
  --OBS CASO A DATA SEJA NULA O CLIENTE N�O ESTA ELIGIVEL PARA O PAGAMENTO IMEDIATO    
  DECLARE @INICIO_vigencia_INICIAL SMALLDATETIME  
  
  SELECT @INICIO_vigencia_INICIAL = A.DT_INICIO_vigencia  
  FROM #propostas A  
  WHERE A.dt_inicio_vigencia < a.data_limite  
   AND ID = (  
    SELECT TOP (1) id  
    FROM #propostas  
    ORDER BY id ASC  
    )  
    
    select @INICIO_vigencia_INICIAL
  
  --ajustar in�cio =  data_limite (do primeiro registro caso esteja anteda da data limite)    
  UPDATE A  
  SET A.DT_INICIO_vigencia = data_limite  
  FROM #propostas A  
  WHERE A.dt_inicio_vigencia < a.data_limite  
   AND ID = (  
    SELECT TOP (1) id  
    FROM #propostas  
    ORDER BY id ASC  
    )  
  
  --SELECT * FROM #propostas order by 4    
  -- ajustar data final = data atual (do �ltimo registro)    
  UPDATE A  
  SET A.dt_fim_vigencia = data_atual  
  FROM #propostas A  
  WHERE A.dt_fim_vigencia > A.data_atual  
   AND ID = (  
    SELECT TOP (1) id  
    FROM #propostas  
    ORDER BY id DESC  
    )  
  
  DECLARE @TOTAL_DIAS_RELACIONAMENTO INT  
  
  UPDATE A  
  SET DIAS = DATEDIFF(day, DT_INICIO_vigencia, DT_FIM_vigencia)  
  FROM #propostas A  
  
  --CASO EXISTA APENAS UMA PROPOSTA E A MESMA N�O TENHA INTERRUPCAO NO PERIODO COLOCA A FLAG 1 NA MESMA PARA VALIDAR O PERIODO DE RELACIONAMENTO    
  UPDATE A  
  SET FLAG = 1  
  FROM #propostas A  
  WHERE DIAS = @PERIODO_MIN_DE_RELACIONAMENTO  
   AND (  
    SELECT COUNT(1)  
    FROM #propostas  
    ) = 1  
  
  SELECT @TOTAL_DIAS_RELACIONAMENTO = ISNULL(SUM(dias), 0)  
  FROM #PROPOSTAS  
  WHERE flag = 1  
  
  --VALIDANDO SE O CLIETE TEVE ALGUMA INTERRUP��O NO PERIODO PARAMETRIZADO    
  select * from #propostas
  IF EXISTS (  
    SELECT TOP (1) 1  
    FROM #propostas  
    WHERE FLAG IS NULL  
    )  
  BEGIN  
   SELECT @TOTAL_DIAS_RELACIONAMENTO = SUM(DIAS)  
   FROM #PROPOSTAS  
   WHERE FLAG IS NULL     
   SET @VALIDA_REGRAS = 0  
   
   SELECT SUM(DIAS)  
   FROM #PROPOSTAS  
   WHERE FLAG IS NULL  
   
  END  
  ELSE  
   -- VALIDANDO A QUANTIDADE DE DIAS SEM INTERRUP��O    
  BEGIN  
   --PEGANDO O RESTANTE DA vig�ncia DA PROPOSTA QUE ANTECEDE A DATA LIMITE    
   SELECT @TOTAL_DIAS_RELACIONAMENTO = @TOTAL_DIAS_RELACIONAMENTO + ISNULL(DATEDIFF(day, @INICIO_vigencia_INICIAL, GETDATE() - @PERIODO_MIN_DE_RELACIONAMENTO), 0)  
  
   SET @sql = ''  
   SET @ParmDefinition = ''  
  
   SELECT @sql = 'IF( ' + CONVERT(VARCHAR(20), @TOTAL_DIAS_RELACIONAMENTO) + ' ' + CHAVE.OPERADOR + ' ' + CHAVE.VALOR + ')     
         SELECT @retornoOUT = 1    
        ELSE     
        SELECT @retornoOUT = 0    
        '  
   FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)  
   INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
   WHERE 1 = 1  
    AND CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id  
    AND REGRA.NOME = 'periodo_relacionamento' --PARAMETRO    
  
   SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';  
  
   EXECUTE sp_executesql @SQL  
    ,@ParmDefinition  
    ,@retornoOUT = @VALIDA_REGRAS OUTPUT  
    
  END  
  
  IF @VALIDA_REGRAS = 1  
  BEGIN  
   SELECT @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Per�odo de relacionamento do cliente com a BrasilSeg '  
     ,convert(VARCHAR(20), @TOTAL_DIAS_RELACIONAMENTO)  
     ,' dias'  
     ,@QuebraLinha  
     ,'atingiu o valor m�nimo de '  
     ,convert(VARCHAR(20), CHAVE.valor)  
     ,' dias para o pagamento imediato.'  
     )  
   FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)  
   INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
   WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id  
    AND REGRA.NOME = 'periodo_relacionamento'  
  END  
  ELSE  
  BEGIN  
   --RECUSA    
   SET @PgtoImediato = 0  
  
   SELECT @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,'Pagamento imediato negado:'  
     ,@QuebraLinha  
     ,'Per�odo de relacionamento do cliente com a BrasilSeg '  
     ,convert(VARCHAR(20), @TOTAL_DIAS_RELACIONAMENTO)  
     ,' dias'  
     ,@QuebraLinha  
     ,'n�o atingiu o valor m�nimo de '  
     ,convert(VARCHAR(20), CHAVE.valor)  
     ,' dias.'  
     )  
   FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)  
   INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
   WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id  
    AND REGRA.NOME = 'periodo_relacionamento'  
  END  
  
  SET @Detalhamento = CONCAT (  
     @Detalhamento  
     ,@QuebraLinha  
     ,' ')  
  
select @Detalhamento