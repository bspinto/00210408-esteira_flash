--SELECT TEXTO_FRANQUIA, val_min_franquia, fat_franquia, *
--       FROM SEGUROS_DB..ESCOLHA_TP_COB_GENERICO_TB E WITH (NOLOCK)
--               JOIN SEGUROS_DB..PROPOSTA_TB P WITH (NOLOCK) ON E.PROPOSTA_ID = P.PROPOSTA_ID
--       WHERE P.PRODUTO_ID = 1220 
--       AND P.PROPOSTA_ID = 49905371
--       AND E.TP_COBERTURA_ID = 16
       
WHILE NOT @@TRANCOUNT = 0
	ROLLBACK

WHILE NOT @@TRANCOUNT = 1
	BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#dados_franquia_tmp') IS NOT NULL
BEGIN
	DROP TABLE #dados_franquia_tmp
END

CREATE TABLE #dados_franquia_tmp (
	id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
	,texto VARCHAR(100) NULL
	,fat_franquia DECIMAL(10, 2) NULL
	,val_min_franquia DECIMAL(10, 2) NULL
	)

DECLARE @proposta_id AS INT = 10844985
	,@tp_cobertura_id AS INT = 43
	,@ramo_id AS INT = 18
	,@produto_id AS INT = 111
DECLARE @perc NVARCHAR(60)
	,@valor NVARCHAR(60)
	,@ind1 INT
	,@ind2 INT
	,@tamanho INT
	,@texto_franquia AS VARCHAR(60)
	,@fat_franquia AS NUMERIC(5, 4)
	,@val_min_franquia AS NUMERIC(15, 2)
	,@fat_convert as NUMERIC(10,4)
DECLARE @CharInvalido SMALLINT	
DECLARE @resultado varchar(60)

SET @tamanho = 0
SET @ind1 = 0
SET @ind2 = 1
SET @perc = ''
SET @valor = ''

SELECT @texto_franquia = ISNULL(a.texto_franquia, '')
	,@fat_franquia = ISNULL(a.fat_franquia, 0)
	,@val_min_franquia = ISNULL(a.val_min_franquia, 0)
FROM seguros_db.dbo.escolha_tp_cob_generico_tb a WITH (NOLOCK)
WHERE a.proposta_id = @proposta_id
	AND a.tp_cobertura_id = @tp_cobertura_id
	AND a.ramo_id = @ramo_id
	AND a.produto_id = @produto_id
	AND a.dt_fim_vigencia_esc IS NULL

SELECT @texto_franquia
	,@fat_franquia
	,@val_min_franquia
	
SET @texto_franquia	= ''

IF NOT @texto_franquia = ''
BEGIN
  --BUSCANDO PERCENTUAL
  IF ISNULL(CHARINDEX('%', @texto_franquia),0) > 0
  BEGIN    
    SELECT @perc = rtrim(ltrim(LEFT(@texto_franquia,CHARINDEX('%', @texto_franquia)-1)))    
    set @resultado = @perc    
    SET @CharInvalido = PATINDEX('%[^0-9].,%', @Resultado)
    WHILE @CharInvalido > 0
    BEGIN
      SET @Resultado = STUFF(@Resultado, @CharInvalido, 1, '')
      SET @CharInvalido = PATINDEX('%[^0-9].,%', @Resultado)
    END
    SET @perc = @Resultado        
    SET @perc = REPLACE(@perc,'.','')
    SET @perc = REPLACE(@perc,',','.')   
    SET @fat_convert = CONVERT(DECIMAL(10,4), @perc)
    SET @fat_franquia = @fat_convert / 100
  END

  --BUSCANDO VALOR  
  IF isnull(CHARINDEX('$', @texto_franquia),0) > 0
  BEGIN
    SELECT @valor = rtrim(ltrim(SUBSTRING (@texto_franquia,CHARINDEX('$', @texto_franquia)+1,LEN(@texto_franquia))))
    set @resultado = @valor    
    SET @CharInvalido = PATINDEX('%[^0-9]%.,', @Resultado)
    WHILE @CharInvalido > 0
    BEGIN
      SET @Resultado = STUFF(@Resultado, @CharInvalido, 1, '')
      SET @CharInvalido = PATINDEX('%[^0-9]%.,', @Resultado)
    END
    SET @valor = @Resultado      
    SET @valor = REPLACE(@valor,'.','')
    SET @valor = REPLACE(@valor,',','.')
    SET @val_min_franquia = CONVERT(DECIMAL(15,2), @valor) 
  END
END  

SELECT @fat_franquia, @val_min_franquia
  

--	--formatando valores  
--	IF (ISNUMERIC(@perc) = 0)
--		OR (@perc = '.')
--		OR (@perc = '')
--	BEGIN
--		SET @perc = '0.00'
--	END

--	IF (ISNUMERIC(@valor) = 0)
--		OR (@valor = '.')
--		OR (@valor = '')
--	BEGIN
--		SET @valor = '0.00'
--	END
--END



--select @perc, @valor

----Resultado  
--INSERT INTO #dados_franquia_tmp (
--	fat_franquia
--	,val_min_franquia
--	)
--VALUES (
--	CONVERT(DECIMAL(10, 02), @perc)
--	,CONVERT(DECIMAL(10, 02), @valor)
--	)

--select * from #dados_franquia_tmp