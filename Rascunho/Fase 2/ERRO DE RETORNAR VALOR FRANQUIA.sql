SELECT TEXTO_FRANQUIA, val_min_franquia, fat_franquia, *
       FROM SEGUROS_DB..ESCOLHA_TP_COB_GENERICO_TB E WITH (NOLOCK)
               JOIN SEGUROS_DB..PROPOSTA_TB P WITH (NOLOCK) ON E.PROPOSTA_ID = P.PROPOSTA_ID
       WHERE P.PRODUTO_ID = 1220 
       AND P.PROPOSTA_ID = 49905371
       AND E.TP_COBERTURA_ID = 16
       
WHILE NOT @@TRANCOUNT = 0
	ROLLBACK

WHILE NOT @@TRANCOUNT = 1
	BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#dados_franquia_tmp') IS NOT NULL
BEGIN
	DROP TABLE #dados_franquia_tmp
END

CREATE TABLE #dados_franquia_tmp (
	id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
	,texto VARCHAR(100) NULL
	,fat_franquia DECIMAL(10, 2) NULL
	,val_min_franquia DECIMAL(10, 2) NULL
	)

DECLARE @proposta_id AS INT = 49905371
	,@tp_cobertura_id AS INT = 16
	,@ramo_id AS INT = 14
	,@produto_id AS INT = 1220
DECLARE @perc NVARCHAR(MAX)
	,@valor NVARCHAR(MAX)
	,@ind1 INT
	,@ind2 INT
	,@tamanho INT
	,@texto_franquia AS VARCHAR(60)
	,@fat_franquia AS NUMERIC(5, 4)
	,@val_min_franquia AS NUMERIC(15, 2)
	,@texto VARCHAR(MAX)

SET @tamanho = 0
SET @ind1 = 0
SET @ind2 = 1
SET @perc = ''
SET @valor = ''

SELECT @texto_franquia = ISNULL(a.texto_franquia, '')
	,@fat_franquia = ISNULL(a.fat_franquia, 0)
	,@val_min_franquia = ISNULL(a.val_min_franquia, 0)
FROM seguros_db.dbo.escolha_tp_cob_generico_tb a WITH (NOLOCK)
WHERE a.proposta_id = @proposta_id
	AND a.tp_cobertura_id = @tp_cobertura_id
	AND a.ramo_id = @ramo_id
	AND a.produto_id = @produto_id
	AND a.dt_fim_vigencia_esc IS NULL

SELECT @texto_franquia
	,@fat_franquia
	,@val_min_franquia

IF @texto_franquia = ''
BEGIN
	SET @perc = '0.00'
	SET @valor = '0.00'
END
ELSE
BEGIN
	IF CHARINDEX('%', @texto) = 0
	BEGIN
		SET @ind1 = 1
		SET @ind2 = 0
	END

	--extraindo valores  
	WHILE @tamanho <> LEN(@texto)
	BEGIN
		SET @tamanho = @tamanho + 1

		IF SUBSTRING(@texto, @tamanho, 1) <> ','
		BEGIN
			IF SUBSTRING(@texto, @tamanho, 1) = '%'
			BEGIN
				SET @ind1 = 1
				SET @ind2 = 0
			END

			IF (SUBSTRING(@texto, @tamanho, 1) <> '$')
				AND (SUBSTRING(@texto, @tamanho, 1) <> '-')
			BEGIN
				IF ISNUMERIC(SUBSTRING(@texto, @tamanho, 1)) <> 0
				BEGIN
					IF @ind1 = 0
					BEGIN
						SET @perc = @perc + SUBSTRING(@texto, @tamanho, 1)
					END

					IF @ind2 = 0
					BEGIN
						SET @valor = @valor + SUBSTRING(@texto, @tamanho, 1)
					END
				END
			END
		END
		ELSE
		BEGIN
			SET @tamanho = LEN(@texto)
		END
	END


SELECT @texto_franquia
	,@fat_franquia
	,@val_min_franquia
	,@texto
	,@perc
	,@valor
	
END	
	
	--formatando valores  
	IF (ISNUMERIC(@perc) = 0)
		OR (@perc = '.')
		OR (@perc = '')
	BEGIN
		SET @perc = '0.00'
	END

	IF (ISNUMERIC(@valor) = 0)
		OR (@valor = '.')
		OR (@valor = '')
	BEGIN
		SET @valor = '0.00'
	END
END

IF @perc = '0.00'
BEGIN
	IF @fat_franquia > 0
	BEGIN
		SET @perc = CONVERT(VARCHAR(20), @fat_franquia)
	END
END

IF @valor = '0.00'
BEGIN
	IF @val_min_franquia > 0
	BEGIN
		SET @valor = CONVERT(VARCHAR(20), @val_min_franquia)
	END
END

--Resultado  
INSERT INTO #dados_franquia_tmp (
	fat_franquia
	,val_min_franquia
	)
VALUES (
	CONVERT(DECIMAL(10, 02), @perc)
	,CONVERT(DECIMAL(10, 02), @valor)
	)
