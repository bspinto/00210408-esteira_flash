--localizar proposta para o produto 809
		SELECT TOP 100 p.produto_id
			,p.ramo_id
			,p.proposta_id --, e.tp_cobertura_id, e.dt_inicio_vigencia_esc
			,ISNULL(ap.dt_fim_vigencia, ISNULL(ad.dt_fim_vigencia, pf.dt_fim_vig)) AS dt_fim_vigencia
		--    into #proposta
		FROM SEGUROS_DB.DBO.PROPOSTA_TB p WITH (NOLOCK)
		-- inner join escolha_tp_cob_generico_tb e on p.proposta_id = e.proposta_id
		INNER JOIN escolha_tp_cob_res_tb e WITH (NOLOCK)
			ON p.proposta_id = e.proposta_id
		--vigencia vou adesao ou na fechada (ap�lice_tb)
		LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB pf WITH (NOLOCK)
			ON p.PROPOSTA_ID = pf.PROPOSTA_ID
		LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ad WITH (NOLOCK)
			ON p.PROPOSTA_ID = ad.PROPOSTA_ID
		LEFT JOIN seguros_db.dbo.apolice_tb ap WITH (NOLOCK)
			ON p.proposta_id = ap.proposta_id
		LEFT JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)
			ON p.proposta_id = s.proposta_id
		WHERE p.produto_id = 809
			AND p.ramo_id = 14
			--and s.evento_sinistro_id = 324
			AND p.situacao = 'i'
			AND e.dt_fim_vigencia_esc IS NULL
			AND e.tp_cobertura_id = 16
			AND P.subramo_id = 1402
			AND p.proposta_id NOT IN (
				50752690
				,50754050
				,50754049
				,50711966
				,50806754
				,50745739
				,50737815
				)
		--ORDER BY dt_inicio_vigencia_esc DESC
		
--separados	
809, 14, 3, 16 ,50568906
809, 14, 3, 16 ,50524669
809, 14, 3, 16 ,50282440
809, 14, 3, 16 ,50282439
809, 14, 3, 16 ,50262719
809, 14, 3, 16 ,50098446
809, 14, 3, 16 ,50163043
809, 14, 3, 16 ,49887805
809, 14, 3, 16 ,49975688
809, 14, 3, 16 ,49975689
809, 14, 3, 16 ,49887804
809, 14, 3, 16 ,49830491
809, 14, 3, 16 ,49806314
809, 14, 3, 16 ,49868403
809, 14, 3, 16 ,49686166
809, 14, 3, 16 ,49830492
809, 14, 3, 16 ,49718585
809, 14, 3, 16 ,49655280
809, 14, 3, 16 ,49642077
809, 14, 3, 16 ,49596234
809, 14, 3, 16 ,49501908
809, 14, 3, 16 ,49586884
809, 14, 3, 16 ,49482401
809, 14, 3, 16 ,49451352
	
--ATUALIZADOS		
WHILE @@TRANCOUNT > 0 ROLLBACK
if @@TRANCOUNT = 0 BEGIN TRAN
SELECT @@TRANCOUNT

--if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50568906, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50524669, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50282440, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50282439, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50262719, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50098446, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 50163043, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49887805, @dt_ini = '20180601', @dt_fim = '20200901'

if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49975688, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49975689, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49887804, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49830491, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49806314, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49868403, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49686166, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49830492, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49718585, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49655280, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49642077, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49596234, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49501908, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49586884, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49482401, @dt_ini = '20180601', @dt_fim = '20200901'
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 49451352, @dt_ini = '20180601', @dt_fim = '20200901'
--commit

--ATUALIZANDO TEXTO FRANQUIA

USE desenv_db
GO
 
CrEAtE PROC DBO.atualiza_texto_franquia_bsp(
@proposta_id  INT, @TEXTO VARCHAR(60), @VAL NUMERIC(15,2), @FAT NUMERIC(5,4))
AS
BEGIN
  DECLARE @USUARIO VARCHAR(20) = 'C00210408'
  DECLARE @DT AS SMALLDATETIME = GETDATE()
 
  IF EXISTS(SELECT 1 FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = @PROPOSTA_ID)
  BEGIN
    UPDATE a
    SET TEXTO_FRANQUIA = @TEXTO
    ,VAL_MIN_FRANQUIA = @VAL
    ,FAT_FRANQUIA = @FAT
    ,dt_alteracao = @DT
    ,usuario = @USUARIO
    FROM seguros_db.dbo.escolha_tp_cob_generico_tb a
    WHERE proposta_id = @PROPOSTA_ID
  END
  ELSE
  BEGIN
		IF OBJECT_ID('tempdb..#IS') IS NOT NULL
		BEGIN
			DROP TABLE #IS
		END

		CREATE TABLE #IS (
			tp_cobertura_id INT NULL
			,nome varchar(60) NULL
			,val_is numeric(15,2) NULL
			,dt_inicio_vigencia_esc smalldatetime NULL
			,dt_fim_vigencia_esc smalldatetime NULL
			,text_franquia varchar(60) NULL
			)

    INSERT INTO #IS
		EXEC SEGS7752_SPS '20200528', @proposta_id

  
    SELECT TOP 1 * 
    INTO #TMP
    FROM seguros_db.dbo.escolha_tp_cob_generico_tb a     
    WHERE RAMO_ID = 14
    AND TP_COBERTURA_ID = 16    
    
    UPDATE A
      SET USUARIO = @USUARIO
      ,DT_INCLUSAO = @DT
      ,PRODUTO_ID = 809
      ,PROPOSTA_ID = @proposta_id
      ,NUM_ENDOSSO = 1 
      ,VAL_IS = (SELECT ISNULL(VAL_IS,0) FROM #IS WHERE tp_cobertura_id = 16)
      ,fat_taxa = @FAT
      ,val_min_franquia = @VAL
      ,texto_franquia = @texto      
    FROM #TMP A
    
    UPDATE A set VAL_IS = 5000 
    FROM #TMP A 
    where VAL_IS = 0
    
    insert seguros_db.dbo.escolha_tp_cob_generico_tb (ramo_id
,seq_canc_endosso_seg
,produto_id
,tp_cobertura_id
,proposta_id
,cod_objeto_segurado
,dt_inicio_vigencia_seg
,dt_inicio_vigencia_esc
,num_endosso
,seq_canc_endosso_esc
,dt_fim_vigencia_esc
,val_is
,fat_taxa
,fat_desconto_tecnico
,val_min_franquia
,fat_franquia
,texto_franquia
,val_premio
,dt_inclusao
,dt_alteracao
,usuario
,acumula_is
,acumula_is_re_seguro)
    select ramo_id
,seq_canc_endosso_seg
,produto_id
,tp_cobertura_id
,proposta_id
,cod_objeto_segurado
,dt_inicio_vigencia_seg
,dt_inicio_vigencia_esc
,num_endosso
,seq_canc_endosso_esc
,dt_fim_vigencia_esc
,val_is
,fat_taxa
,fat_desconto_tecnico
,val_min_franquia
,fat_franquia
,texto_franquia
,val_premio
,dt_inclusao
,dt_alteracao
,usuario
,acumula_is
,acumula_is_re_seguro from #TMP
     
  
  END
END

WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50524669, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50282440, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50282439, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50262719, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50098446, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 50163043, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1
IF @@TRANCOUNT = 1 EXEC DESENV_DB.DBO.atualiza_texto_franquia_bsp 49887805, '10% DOS PREJU�ZOS COM M�NIMO DE R$ 500,00', 500, 0.1

SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50568906
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50524669
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50282440
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50282439
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50262719
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50098446
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 50163043
SELECT * FROM seguros_db.dbo.escolha_tp_cob_generico_tb a (NOLOCK) WHERE proposta_id = 49887805

--commit
 
seguros_db.dbo.escolha_tp_cob_generico_tb a
  WHERE proposta_id = 50568906 
 
--CONFERIDOS
  WHILE @@TRANCOUNT > 0 ROLLBACK
  IF @@TRANCOUNT = 0 BEGIN TRAN
  IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL BEGIN DROP TABLE #Equipamento END
  CREATE TABLE #Equipamento (Codigo INT  ,TipoBem VARCHAR(60)  ,Modelo VARCHAR(60)  ,Dano INT  ,Orcamento INT  ,Valor NUMERIC(15,2)  ,Laudo INT  ,NomeAssistencia VARCHAR(60)  ,TelAssistencia VARCHAR(15)  ,Descricao VARCHAR(255))

  INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
  SELECT '000000037', 'Ar Condicionado', 'Acima de 19000BTUS', '1', '1', '249.00', '1', 'Ar teC', '(32) 34414-444_', 'dano no ar'


--exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,50568906, 1500.00 ,'2020-06-06' 
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,50524669, 1500.00 ,'2020-06-06'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,50282440, 1500.00 ,'2020-06-06'
--exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,50282439, 1500.00 ,'2020-06-06'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,50262719, 1500.00 ,'2020-06-06'
--exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,50098446, 1500.00 ,'2020-06-06'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,50163043, 1500.00 ,'2020-06-06'
--exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49887805, 1500.00 ,'2020-06-06'

exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49975688, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49975689, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49887804, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49830491, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49806314, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49868403, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49686166, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49830492, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49718585, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49655280, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49642077, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49596234, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49501908, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49586884, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49482401, 1500.00 ,'2020-05-28'
exec seguros_db.dbo.segs14618_sps 809, 14, 3, 16 ,49451352, 1500.00 ,'2020-05-28'