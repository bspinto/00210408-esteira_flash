-- ajuste de massa ap�s o smqp0059
-- ajusete adicionando o t�cnico
use desenv_db
go

alter proc dbo.ajusta_smqp0059_bsp 
  @sinistro_id as numeric(15)
as 
begin
  select a.*, b.produto_id
  into #sinistro_tmp
  FROM seguros_db.dbo.Sinistro_Tb a WITH (NOLOCK) 
  inner join seguros_db.dbo.proposta_tb b WITH (NOLOCK) 
  on b.proposta_id = a.proposta_id  
  where Sinistro_ID = @sinistro_id
  
  if exists(select 1 from #sinistro_tmp) 
  begin
    if Not exists(select 1 FROM seguros_db.dbo.Sinistro_Tecnico_Tb WITH (NOLOCK) where Sinistro_ID = @sinistro_id)
    begin
      select top 1 a.*  
      into #Sinistro_Tecnico_tmp
		  FROM seguros_db.dbo.Sinistro_Tecnico_Tb a WITH (NOLOCK)
		  INNER JOIN seguros_db.dbo.Tecnico_Tb b WITH (NOLOCK)
			  ON a.Tecnico_ID = b.Tecnico_ID
		  INNER JOIN seguros_db.dbo.Sinistro_Tb c WITH (NOLOCK)
			  ON c.Sinistro_ID = a.Sinistro_ID
			inner join #sinistro_tmp d with (nolock)
			  on d.ramo_id = c.ramo_id					  	  
			  and d.seguradora_cod_susep = c.seguradora_cod_susep			 
			inner join seguros_db.dbo.proposta_tb e with (nolock)
			  on e.proposta_id = c.proposta_id			  
			  and e.produto_id = d.produto_id
		  WHERE a.Dt_fim_vigencia IS NULL   
		  
		  insert into seguros_db.dbo.Sinistro_Tecnico_Tb (
		    sinistro_id
        ,apolice_id
        ,sucursal_seguradora_id
        ,seguradora_cod_susep
        ,ramo_id
        ,tecnico_id
        ,dt_inicio_vigencia
        ,dt_inclusao
        ,dt_fim_vigencia
        ,dt_alteracao
        ,usuario
        --,lock
        ,cod_atividade
        ,dt_atividade
        ,observacao_atividade
        )
        select 		    
          b.sinistro_id
          ,b.apolice_id
          ,b.sucursal_seguradora_id
          ,b.seguradora_cod_susep
          ,b.ramo_id
          ,a.tecnico_id
          ,b.dt_aviso_sinistro --dt_inicio_vigencia
          ,getdate()
          ,NULL
          ,NULL
          ,'NT_00210408'
          --,lock
          ,a.cod_atividade
          ,b.dt_aviso_sinistro --dt_atividade
          ,a.observacao_atividade
        from #Sinistro_Tecnico_tmp a
        inner join #sinistro_tmp b
        on a.ramo_id = b.ramo_id		           
    end
    else 
    begin
      select 'Aparentemente o SMQP0059 rodou com sucesso!'
    end
  end
  else
  begin
    select 'Sinistro n�o encontrado. SMQP0059 n�o rodou ainda!'
  end
end

WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
  if @@trancount > 0 exec desenv_db.dbo.ajusta_smqp0059_bsp 14202000282
  if @@trancount > 0 exec desenv_db.dbo.ajusta_smqp0059_bsp 14202000281
  if @@trancount > 0 exec desenv_db.dbo.ajusta_smqp0059_bsp 14202000278
--commit  

--ajuste destravar usuario para o sinistro
use desenv_db
go

create proc dbo.destrava_usuario_sinistro_bsp @sinistro_id as numeric(15)
as
begin
    update a
    set lock_aviso = 'n'
    from seguros_db.dbo.sinistro_tb a 
    WHERE sinistro_id = @sinistro_id   
end

WHILE @@TRANCOUNT > 0 ROLLBACK
IF @@TRANCOUNT = 0 BEGIN TRAN
  if @@trancount > 0 exec dbo.destrava_usuario_sinistro_bsp 14202000198
  if @@trancount > 0 exec dbo.destrava_usuario_sinistro_bsp 14202000199
  if @@trancount > 0 exec dbo.destrava_usuario_sinistro_bsp 14202000200
--commit  


--Exec SEGS13114_SPS  14202000198
