Imports Util
Imports System
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Data
Imports System.Data.OleDb
Imports System.Security

Public Class frmImportacao
    Public CPFUsuario As String
    Private extensaoDoArquivo As String
    Private Shared _Usuario As cUsuario

    'Private oParamCon As New cParamCon
    Private oConect As New cConect
    Private sMsg01 As String = "Este n�o � um arquivo valido para executar a importa��o."
    Private sMsg02 As String = "Ocorreu um erro ao tentar importar a planilha do excel." & vbCrLf & "Possiveis causas do problema:" & vbCrLf & "* Voc� n�o tem permiss�o para acessar o arquivo." & vbCrLf & "* O arquivo n�o pode ser aberto pelo programa Excel." & vbCrLf & "* O nome da planilha informado n�o esta correto."
    Private sMsg03 As String = "Ocorreu um erro ao tentar gravar os importados."
    Private sMsg04 As String = "As altera��es foram gravadas com sucesso!"
    Private sMsg05 As String = "N�o existem resultados que satisfa�am a pesquisa!" & vbCrLf & "Altere os filtros e tente novamente!"


    Private Sub frmImportacao_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        setUsuarioAplicacao()

        'Dim ConnectionHelper As ConnectionHelper = New Util.ConnectionHelper
        'txtUsuario.Text = IIf(ConnectionHelper.GetUsuario <> String.Empty, ConnectionHelper.GetUsuario, "SEGXPT1")
        'ConnectionHelper = Nothing

        txtUsuario.Text = _Usuario.pLOGIN_REDE
        oConect.oNegocios.oParamCon.Usuario = _Usuario.pLOGIN_REDE

        ConfigurarComponentes()
    End Sub

#Region "Fun��es de acesso ao sistema"
    Private Sub setUsuarioAplicacao()
        Dim usuario As New cUsuario

        usuario.pLOGIN_REDE = GetLoginRede()
        _Usuario = usuario
    End Sub
    Public Shared Function getUsuarioAplicacao() As cUsuario
        Return _Usuario
    End Function
    Public ReadOnly Property GetLoginRede() As String
        Get
            Dim oControl = New cNegociosDAO
            Dim login_rede As String = oControl.getLoginRede()

            If Not IsNothing(login_rede) AndAlso Trim(login_rede) <> "" Then
                Return login_rede
            Else
                Return "SEGP1519"
            End If
        End Get
    End Property
#End Region


#Region "fun��es do sistema"
    Private Function StringToBoolean(ByVal s As String) As Boolean
        Return s = "1" Or s = "S"
    End Function

    Private Function BooleanToString(ByVal b As Boolean) As String
        Dim sn As String = "0"

        If b = True Then
            sn = "1"
        End If

        Return sn
    End Function

    Private Function BooleanToInt(ByVal b As Boolean) As Integer
        Dim i As Integer = 0

        If b = True Then
            i = 1
        End If

        Return i
    End Function
    Private Function ConverteDataToSql(ByVal sData As String) As String
        Dim sDia As String = String.Empty
        Dim sMes As String = String.Empty
        Dim sAno As String = String.Empty
        Dim iSeparaDia As Integer
        Dim iSeparaMes As Integer

        iSeparaDia = IIf(sData.Substring(2, 1) = "/", 2, 1)
        iSeparaMes = IIf(sData.Substring(iSeparaDia + 3, 1) = "/", iSeparaDia + 3, iSeparaDia + 2)

        sDia = sData.Substring(0, iSeparaDia)
        sMes = sData.Substring(iSeparaDia + 1, iSeparaMes - iSeparaDia - 1)
        sAno = sData.Substring(iSeparaMes + 1, 4)

        Return sAno + "-" + sMes + "-" + sDia

    End Function

    Private Function ValidaNomePlanilha() As Boolean
        Dim bRet As Boolean = (Trim(txtNomePlanilha.Text) <> "")

        If bRet Then
            ErrorProvider1.SetError(txtNomePlanilha, "")
        Else
            ErrorProvider1.SetError(txtNomePlanilha, "Nome da planilha n�o informado ou inv�lido.")
        End If

        Return bRet
    End Function
#End Region

#Region "validated"
    Private Sub txtNomePlanilha_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNomePlanilha.Validated
        ValidaNomePlanilha()
    End Sub
#End Region

    Private Sub btnLocalizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLocalizar.Click
        Dim result As DialogResult = OpenFileDialog1.ShowDialog()

        ' Test result.
        If result = Windows.Forms.DialogResult.OK Then
            Dim fi As New FileInfo(OpenFileDialog1.FileName)
            txtExcelCaminho.Text = fi.Directory.ToString
            txtExcelArquivo.Text = fi.Name.ToString
            extensaoDoArquivo = fi.Extension.ToString

            txtNomePlanilha.Enabled = True
            txtNomePlanilha.Focus()
            txtNomePlanilha.Text = "Tabela de Bens"

            btnImportar.Enabled = True
        End If
    End Sub

    Private Sub ConfigurarComponentes()
        'Configurando FileDialog
        OpenFileDialog1.Multiselect = False
        OpenFileDialog1.Title = "Selecionar Arquivos"
        OpenFileDialog1.InitialDirectory = "C:\"
        'filtra para exibir somente arquivos de Excel
        OpenFileDialog1.Filter = "Excel (*.xls;*.xlsx)|*.xls;*.xlsx|" & "Todos (*.*)|*.*"
        OpenFileDialog1.CheckFileExists = True
        OpenFileDialog1.CheckPathExists = True
        OpenFileDialog1.FilterIndex = 1
        OpenFileDialog1.RestoreDirectory = True
        OpenFileDialog1.ReadOnlyChecked = True
        OpenFileDialog1.ShowReadOnly = True

        'configurando textbox
        txtExcelArquivo.ReadOnly = True
        txtExcelCaminho.ReadOnly = True
        txtNomePlanilha.Enabled = False
        txtExcelArquivo.Clear()
        txtExcelCaminho.Clear()
        txtNomePlanilha.Clear()

        'configurando button
        btnImportar.Enabled = False
        btnGravar.Enabled = False
        btnEquipamentos.Enabled = True
        btnLocalizar.Enabled = True

        'configurando label
        lblMsgSemDetalhe.Text = ""

        'configurando grid
        grdEquipamento.DataSource = New DataSet

    End Sub

    Private Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click
        Dim CaminhoEArquivoExcel As String = txtExcelCaminho.Text + "\" + txtExcelArquivo.Text
        Dim stringConexao As String = ""

        If ValidaNomePlanilha() Then
            If extensaoDoArquivo = ".xls" Then
                stringConexao = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source =" + CaminhoEArquivoExcel + "; Extended Properties = 'Excel 8.0;HDR=YES'"
            ElseIf extensaoDoArquivo = ".xlsx" Then
                stringConexao = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source =" + CaminhoEArquivoExcel + "; Extended Properties = 'Excel 12.0;HDR=YES'"
            Else
                lblMsgSemDetalhe.Text = sMsg01
                Return
            End If

            importar1(txtNomePlanilha.Text, stringConexao)
        End If
    End Sub


    Private Sub importar1(ByVal sNomePlanilha As String, ByVal stringConexao As String)
        Dim conexaoOleDb As OleDbConnection = Nothing
        Dim ds As DataSet
        Dim cmd As OleDbDataAdapter
        Try
            conexaoOleDb = New OleDbConnection(stringConexao)
            conexaoOleDb.Open()

            cmd = New OleDbDataAdapter("Select * from [" + sNomePlanilha + "$]", conexaoOleDb)
            cmd.TableMappings.Add("Table", "tabelaExcel")
            ds = New DataSet
            cmd.Fill(ds)
            grdEquipamento.DataSource = ds.Tables(0)

            txtNomePlanilha.Enabled = False

            btnGravar.Enabled = True
            btnImportar.Enabled = False
            btnEquipamentos.Enabled = False
            btnLocalizar.Enabled = False

            For i As Integer = 0 To grdEquipamento.Columns.Count - 1
                Select Case grdEquipamento.Columns(i).Name.ToUpper
                    Case "tipo de bem".ToUpper
                        grdEquipamento.Columns(i).Width = 200
                    Case "Modelo".ToUpper
                        grdEquipamento.Columns(i).Width = 200
                    Case "Vlr Perda Total".ToUpper
                        grdEquipamento.Columns(i).Width = 100
                    Case "Vlr Reparo".ToUpper
                        grdEquipamento.Columns(i).Width = 100
                    Case "Inicio Vig�ncia".ToUpper
                        grdEquipamento.Columns(i).Width = 100
                    Case "Termino Vig�ncia".ToUpper
                        grdEquipamento.Columns(i).Width = 100
                    Case "Usu�rio que Cadastrou".ToUpper
                        grdEquipamento.Columns(i).Width = 100
                    Case Else
                        grdEquipamento.Columns(i).Visible = False
                End Select
            next
        Catch ex As Exception
            MessageBox.Show(sMsg02 & vbCrLf & vbCrLf & "Erro original: " & ex.ToString, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtNomePlanilha.Focus()
        Finally
            conexaoOleDb.Close()
        End Try
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ConfigurarComponentes()
    End Sub

    Private Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click
        Try
            If GravarGrid() Then
                MessageBox.Show(sMsg04, "Dados Gravados", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ConfigurarComponentes()
            End If
        Catch ex As Exception
            MessageBox.Show(sMsg03 & vbCrLf & vbCrLf & "Erro original: " & ex.ToString, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Function GravarGrid() As Boolean
        Dim sTipoBem As String
        Dim sModelo As String
        Dim sVlPerdaTotal As Double
        Dim sVlReparo As Double
        Dim sSql As String = ""
        Dim sInsert As String = ""
        Dim sValorInsert As String = ""
        Dim iGrdTipoBem As Integer = 0
        Dim iGrdModelo As Integer = 1
        Dim iGrdVlPerdaTotal As Integer = 2
        Dim iGrdVlReparo As Integer = 3

        sSql = sSql & "IF OBJECT_ID('tempdb..##sinistro_equipamento_importado') IS NOT NULL" & vbNewLine
        sSql = sSql & "BEGIN" & vbNewLine
        sSql = sSql & "  DROP TABLE ##sinistro_equipamento_importado" & vbNewLine
        sSql = sSql & "END" & vbNewLine
        sSql = sSql & vbNewLine
        sSql = sSql & "CREATE TABLE ##sinistro_equipamento_importado (" & vbNewLine
        sSql = sSql & "  tipo_bem VARCHAR(60) NOT NULL" & vbNewLine
        sSql = sSql & "  ,modelo VARCHAR(60) NOT NULL" & vbNewLine
        sSql = sSql & "  ,vl_perda_total NUMERIC(15, 2) NULL" & vbNewLine
        sSql = sSql & "  ,vl_reparo NUMERIC(15, 2) NULL" & vbNewLine
        sSql = sSql & ")"
        sSql = sSql & vbNewLine & vbNewLine

        iGrdTipoBem = grdEquipamento.Columns("tipo de bem").Index
        iGrdModelo = grdEquipamento.Columns("Modelo").Index
        iGrdVlPerdaTotal = grdEquipamento.Columns("Vlr Perda Total").Index
        iGrdVlReparo = grdEquipamento.Columns("Vlr Reparo").Index

        For Each colunas As DataGridViewRow In grdEquipamento.Rows
            sInsert = "INSERT INTO ##sinistro_equipamento_importado (tipo_bem, modelo, vl_perda_total, vl_reparo) VALUES "

            sTipoBem = IIf(colunas.Cells(iGrdTipoBem).Value Is DBNull.Value, "N/D", colunas.Cells(iGrdTipoBem).Value)
            sModelo = IIf(colunas.Cells(iGrdModelo).Value Is DBNull.Value, "N/D", colunas.Cells(iGrdModelo).Value)
            sVlPerdaTotal = IIf(colunas.Cells(iGrdVlPerdaTotal).Value Is DBNull.Value, 0, colunas.Cells(iGrdVlPerdaTotal).Value)
            sVlReparo = IIf(colunas.Cells(iGrdVlReparo).Value Is DBNull.Value, 0, colunas.Cells(iGrdVlReparo).Value)

            sValorInsert = "("
            sValorInsert = sValorInsert & "'" & sTipoBem & "', "
            sValorInsert = sValorInsert & "'" & sModelo & "', "
            sValorInsert = sValorInsert & "'" & sVlPerdaTotal.ToString("F").Replace(",", ".") & "', "
            sValorInsert = sValorInsert & "'" & sVlReparo.ToString("F").Replace(",", ".") & "'"
            sValorInsert = sValorInsert & ")"

            If sValorInsert <> "('', '', '0.00', '0.00')" And sValorInsert <> "('N/D', 'N/D', '0.00', '0.00')" Then
                If sTipoBem = "N/D" Or sModelo = "N/D" Then
                    MessageBox.Show("Planilha preenchida incorretamente." & vbNewLine & "A planilha importada cont�m um ou mais campos vazios para: (Tipo de bem) e/ou (Modelo)", "Aten��o", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return False
                End If
                sSql = sSql & sInsert & sValorInsert & vbNewLine
            End If
        Next

        Return oConect.GravarNovaVersao(sSql)
    End Function

    Private Sub btnEquipamentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEquipamentos.Click
        Dim ds As New DataTable
        ds = oConect.GetEquipamentoAtivo
        Dim i As Integer
        If ds.Rows.Count > 0 Then
            grdEquipamento.DataSource = ds
            With grdEquipamento
                .Columns("tipo_bem").Width = 200
                .Columns("tipo_bem").HeaderText = "Tipo de bem"

                .Columns("modelo").Width = 200
                .Columns("modelo").HeaderText = "Modelo"

                .Columns("vl_perda_total").Width = 75
                .Columns("vl_perda_total").HeaderText = "Perda total"

                .Columns("vl_reparo").Width = 75
                .Columns("vl_reparo").HeaderText = "Reparo"

                .Columns("dt_inicio_vigencia").DefaultCellStyle.Format = "d"
                .Columns("dt_inicio_vigencia").HeaderText = "Inicio vig�ncia"

                .Columns("num_versao").Width = 75
                .Columns("num_versao").HeaderText = "Vers�o"

                .Columns("usuario").Width = 75
                .Columns("usuario").HeaderText = "Usu�rio"

                .Columns("dt_inclusao").DefaultCellStyle.Format = "d"
                .Columns("dt_inclusao").HeaderText = "Data inclus�o"

                .Columns("dt_alteracao").DefaultCellStyle.Format = "d"
                .Columns("dt_alteracao").HeaderText = "Data altera��o"

                For i = 0 To .ColumnCount - 1
                    .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                Next
            End With
        End If

    End Sub

End Class
