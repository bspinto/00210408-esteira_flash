<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportacao
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportacao))
        Me.StatusBar = New System.Windows.Forms.StatusStrip
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblUsuario = New System.Windows.Forms.ToolStripStatusLabel
        Me.txtUsuario = New System.Windows.Forms.ToolStripStatusLabel
        Me.pnlGeral = New System.Windows.Forms.Panel
        Me.pnlGrid = New System.Windows.Forms.Panel
        Me.grdEquipamento = New System.Windows.Forms.DataGridView
        Me.pnlGravar = New System.Windows.Forms.Panel
        Me.btnEquipamentos = New System.Windows.Forms.Button
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.btnGravar = New System.Windows.Forms.Button
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.txtNomePlanilha = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtExcelArquivo = New System.Windows.Forms.TextBox
        Me.btnImportar = New System.Windows.Forms.Button
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnLocalizar = New System.Windows.Forms.Button
        Me.txtExcelCaminho = New System.Windows.Forms.TextBox
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblMsgSemDetalhe = New System.Windows.Forms.Label
        Me.tmrExibeAlerta = New System.Windows.Forms.Timer(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.StatusBar.SuspendLayout()
        Me.pnlGeral.SuspendLayout()
        Me.pnlGrid.SuspendLayout()
        CType(Me.grdEquipamento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGravar.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusBar
        '
        Me.StatusBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus, Me.lblUsuario, Me.txtUsuario})
        Me.StatusBar.Location = New System.Drawing.Point(0, 615)
        Me.StatusBar.Name = "StatusBar"
        Me.StatusBar.Size = New System.Drawing.Size(1081, 22)
        Me.StatusBar.SizingGrip = False
        Me.StatusBar.TabIndex = 0
        Me.StatusBar.Text = "StatusStrip1"
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 17)
        '
        'lblUsuario
        '
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(47, 17)
        Me.lblUsuario.Text = "Usu�rio:"
        '
        'txtUsuario
        '
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(0, 17)
        '
        'pnlGeral
        '
        Me.pnlGeral.Controls.Add(Me.pnlGrid)
        Me.pnlGeral.Controls.Add(Me.pnlGravar)
        Me.pnlGeral.Controls.Add(Me.txtNomePlanilha)
        Me.pnlGeral.Controls.Add(Me.Label3)
        Me.pnlGeral.Controls.Add(Me.Label2)
        Me.pnlGeral.Controls.Add(Me.txtExcelArquivo)
        Me.pnlGeral.Controls.Add(Me.btnImportar)
        Me.pnlGeral.Controls.Add(Me.Label1)
        Me.pnlGeral.Controls.Add(Me.btnLocalizar)
        Me.pnlGeral.Controls.Add(Me.txtExcelCaminho)
        Me.pnlGeral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlGeral.Location = New System.Drawing.Point(0, 0)
        Me.pnlGeral.Name = "pnlGeral"
        Me.pnlGeral.Size = New System.Drawing.Size(1081, 615)
        Me.pnlGeral.TabIndex = 3
        '
        'pnlGrid
        '
        Me.pnlGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlGrid.Controls.Add(Me.grdEquipamento)
        Me.pnlGrid.Location = New System.Drawing.Point(12, 137)
        Me.pnlGrid.Name = "pnlGrid"
        Me.pnlGrid.Size = New System.Drawing.Size(1057, 436)
        Me.pnlGrid.TabIndex = 11
        '
        'grdEquipamento
        '
        Me.grdEquipamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdEquipamento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdEquipamento.Location = New System.Drawing.Point(0, 0)
        Me.grdEquipamento.MultiSelect = False
        Me.grdEquipamento.Name = "grdEquipamento"
        Me.grdEquipamento.ReadOnly = True
        Me.grdEquipamento.Size = New System.Drawing.Size(1057, 436)
        Me.grdEquipamento.TabIndex = 4
        '
        'pnlGravar
        '
        Me.pnlGravar.Controls.Add(Me.btnEquipamentos)
        Me.pnlGravar.Controls.Add(Me.btnGravar)
        Me.pnlGravar.Controls.Add(Me.btnCancelar)
        Me.pnlGravar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlGravar.Location = New System.Drawing.Point(0, 573)
        Me.pnlGravar.Name = "pnlGravar"
        Me.pnlGravar.Size = New System.Drawing.Size(1081, 42)
        Me.pnlGravar.TabIndex = 10
        '
        'btnEquipamentos
        '
        Me.btnEquipamentos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEquipamentos.ImageKey = "iconfinder_46_List_106199.png"
        Me.btnEquipamentos.ImageList = Me.ImageList1
        Me.btnEquipamentos.Location = New System.Drawing.Point(12, 9)
        Me.btnEquipamentos.Name = "btnEquipamentos"
        Me.btnEquipamentos.Size = New System.Drawing.Size(183, 24)
        Me.btnEquipamentos.TabIndex = 2
        Me.btnEquipamentos.Text = "Listar equipamentos ativos"
        Me.btnEquipamentos.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "if_save_2639912.png")
        Me.ImageList1.Images.SetKeyName(1, "if_063_Undo_183192.png")
        Me.ImageList1.Images.SetKeyName(2, "if_icons_search_1564527.png")
        Me.ImageList1.Images.SetKeyName(3, "iconfinder_46_List_106199.png")
        '
        'btnGravar
        '
        Me.btnGravar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGravar.ImageKey = "if_save_2639912.png"
        Me.btnGravar.ImageList = Me.ImageList1
        Me.btnGravar.Location = New System.Drawing.Point(886, 9)
        Me.btnGravar.Name = "btnGravar"
        Me.btnGravar.Size = New System.Drawing.Size(183, 24)
        Me.btnGravar.TabIndex = 1
        Me.btnGravar.Text = "&Gravar Nova Vers�o"
        Me.btnGravar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.ImageKey = "if_063_Undo_183192.png"
        Me.btnCancelar.ImageList = Me.ImageList1
        Me.btnCancelar.Location = New System.Drawing.Point(697, 9)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(183, 24)
        Me.btnCancelar.TabIndex = 0
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'txtNomePlanilha
        '
        Me.txtNomePlanilha.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNomePlanilha.Enabled = False
        Me.txtNomePlanilha.Location = New System.Drawing.Point(120, 66)
        Me.txtNomePlanilha.Name = "txtNomePlanilha"
        Me.txtNomePlanilha.Size = New System.Drawing.Size(885, 20)
        Me.txtNomePlanilha.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Nome do Arquivo:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Nome da planilha:"
        '
        'txtExcelArquivo
        '
        Me.txtExcelArquivo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtExcelArquivo.Location = New System.Drawing.Point(120, 37)
        Me.txtExcelArquivo.Name = "txtExcelArquivo"
        Me.txtExcelArquivo.ReadOnly = True
        Me.txtExcelArquivo.Size = New System.Drawing.Size(885, 20)
        Me.txtExcelArquivo.TabIndex = 5
        '
        'btnImportar
        '
        Me.btnImportar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImportar.ImageKey = "Excel 3.png"
        Me.btnImportar.ImageList = Me.ImageList2
        Me.btnImportar.Location = New System.Drawing.Point(120, 92)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(885, 39)
        Me.btnImportar.TabIndex = 3
        Me.btnImportar.Text = "               &Acessar e Importar Planilha do Excel"
        Me.btnImportar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImportar.UseVisualStyleBackColor = True
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "excel.png")
        Me.ImageList2.Images.SetKeyName(1, "excel 2.png")
        Me.ImageList2.Images.SetKeyName(2, "Excel 3.png")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(105, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Caminho do Arquivo:"
        '
        'btnLocalizar
        '
        Me.btnLocalizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ErrorProvider1.SetIconAlignment(Me.btnLocalizar, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.btnLocalizar.ImageKey = "if_icons_search_1564527.png"
        Me.btnLocalizar.ImageList = Me.ImageList1
        Me.btnLocalizar.Location = New System.Drawing.Point(1011, 7)
        Me.btnLocalizar.Name = "btnLocalizar"
        Me.btnLocalizar.Size = New System.Drawing.Size(58, 50)
        Me.btnLocalizar.TabIndex = 1
        Me.btnLocalizar.Text = "&Localizar"
        Me.btnLocalizar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnLocalizar.UseVisualStyleBackColor = True
        '
        'txtExcelCaminho
        '
        Me.txtExcelCaminho.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtExcelCaminho.Location = New System.Drawing.Point(120, 9)
        Me.txtExcelCaminho.Name = "txtExcelCaminho"
        Me.txtExcelCaminho.ReadOnly = True
        Me.txtExcelCaminho.Size = New System.Drawing.Size(885, 20)
        Me.txtExcelCaminho.TabIndex = 0
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'lblMsgSemDetalhe
        '
        Me.lblMsgSemDetalhe.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMsgSemDetalhe.AutoSize = True
        Me.lblMsgSemDetalhe.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgSemDetalhe.ForeColor = System.Drawing.Color.Red
        Me.lblMsgSemDetalhe.Location = New System.Drawing.Point(823, 618)
        Me.lblMsgSemDetalhe.Name = "lblMsgSemDetalhe"
        Me.lblMsgSemDetalhe.Size = New System.Drawing.Size(253, 13)
        Me.lblMsgSemDetalhe.TabIndex = 4
        Me.lblMsgSemDetalhe.Text = "N�o existem detalhes para essa importa��o"
        Me.lblMsgSemDetalhe.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'tmrExibeAlerta
        '
        Me.tmrExibeAlerta.Interval = 2000
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Excel (*.xls;*.xlsx)|*.xls;*.xlsx|"" & ""Todos (*.*)|*.*"
        '
        'frmImportacao
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1081, 637)
        Me.Controls.Add(Me.lblMsgSemDetalhe)
        Me.Controls.Add(Me.pnlGeral)
        Me.Controls.Add(Me.StatusBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmImportacao"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SEGP1519 -  Importa��o de Equipamentos"
        Me.StatusBar.ResumeLayout(False)
        Me.StatusBar.PerformLayout()
        Me.pnlGeral.ResumeLayout(False)
        Me.pnlGeral.PerformLayout()
        Me.pnlGrid.ResumeLayout(False)
        CType(Me.grdEquipamento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGravar.ResumeLayout(False)
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusBar As System.Windows.Forms.StatusStrip
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents pnlGeral As System.Windows.Forms.Panel
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblMsgSemDetalhe As System.Windows.Forms.Label
    Friend WithEvents tmrExibeAlerta As System.Windows.Forms.Timer
    Friend WithEvents btnImportar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnLocalizar As System.Windows.Forms.Button
    Friend WithEvents txtExcelCaminho As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtExcelArquivo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNomePlanilha As System.Windows.Forms.TextBox
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents pnlGrid As System.Windows.Forms.Panel
    Friend WithEvents grdEquipamento As System.Windows.Forms.DataGridView
    Friend WithEvents pnlGravar As System.Windows.Forms.Panel
    Friend WithEvents btnGravar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEquipamentos As System.Windows.Forms.Button

End Class
