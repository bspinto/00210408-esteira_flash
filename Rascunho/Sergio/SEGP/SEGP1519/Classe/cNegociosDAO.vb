Imports System.Data.SqlClient
Imports Util

Public Class cNegociosDAO

    Const banco As String = "seguros_db.dbo."
    Public oErro As New cErro
    Public oParamCon As New cParamCon
    Private ccon As ConnectionHelper

#Region "Configura��o"

    Public Sub New()
        Dim configuraConexao = New cMSSQLDAO
        ccon = configuraConexao.pConexao
    End Sub

    Public Function GetEquipamentoAtivo() As DataTable
        Dim ds As DataSet = Nothing
        Dim sProc As String = banco & "SEGS14597_SPS"
        Try
            ds = ccon.ExecuteSQL(CommandType.StoredProcedure, sProc)
            Return ds.Tables(0)
        Catch ex As Exception
            oErro.PopularErro(oErro.GetCurrentMethodName, sProc, ex.Message.ToString & " (" & ex.InnerException.InnerException.Message.ToString & ")", "N�o foi poss�vel retornar a consulta dos equipamentos ativos.")
            Throw New System.Exception()
        End Try
    End Function

    Public Function GravarNovaVersao(ByVal pSql As String) As Boolean
        Dim ds As DataSet = Nothing
        pSql = pSql & "EXEC " & banco & "SEGS14598_SPI" & " @usuario = '" & oParamCon.Usuario & "'" & vbNewLine        

        Try
            ds = ccon.ExecuteSQL(CommandType.Text, pSql)
            Return True
        Catch ex As Exception
            oErro.PopularErro(oErro.GetCurrentMethodName, pSql, ex.Message.ToString & " (" & ex.InnerException.InnerException.Message.ToString & ")", "N�o foi poss�vel gravar uma nova vers�o dos equipamentos importados.")
            Throw New System.Exception()
        End Try

    End Function

    Public Function getLoginRede() As String
        Dim ds As New DataSet
        Dim login As String = ""
        Dim SQL As String = ""
        Dim cpf As String

        cpf = Util.ConnectionHelper.GetUsuarioCPF
        Try
            If IsNumeric(Replace(Replace(cpf, ".", ""), "-", "")) Then
                SQL = "EXEC segab_db.dbo.retorna_login_cpf_sps "
                SQL &= "'" & cpf & "'"

                ds = ccon.ExecuteSQL(CommandType.Text, SQL)
                login = ds.Tables(0).Rows(0).Item("login_rede")
            End If
        Catch ex As Exception
        End Try

        Return login
    End Function

#End Region
End Class
