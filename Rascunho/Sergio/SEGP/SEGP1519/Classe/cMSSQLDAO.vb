Imports System.Configuration
Imports System.Security
Imports Util

Public Class cMSSQLDAO
    Private Shared _iConexao As Util.ConnectionHelper
    Public Sub New()
        Dim glAmbienteID As String

        Try
            Dim mensagem As New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)

            glAmbienteID = mensagem.RetornaMenssage()
            pConexao = mensagem
        Catch ex As Exception
            Try
                EventLog.WriteEntry("SEGP1455", "Erro de conex�o ao banco de dados. " & ex.Message.ToString, System.Diagnostics.EventLogEntryType.Error)
            Catch ex2 As Exception
            End Try

            Environment.Exit(0)
        End Try
    End Sub
    Public Property pConexao() As Util.ConnectionHelper
        Get
            Return _iConexao
        End Get
        Set(ByVal value As Util.ConnectionHelper)
            _iConexao = value
        End Set
    End Property
End Class