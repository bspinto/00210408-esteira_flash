VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GridAlianca.ocx"
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.ocx"
Begin VB.Form frmObjSegurado 
   Caption         =   "Form1"
   ClientHeight    =   9720
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12705
   Icon            =   "frmObjSegurado.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9720
   ScaleWidth      =   12705
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      Caption         =   "Quantidade de animais mortos:"
      Height          =   735
      Left            =   240
      TabIndex        =   28
      Top             =   5280
      Visible         =   0   'False
      Width           =   10455
      Begin MSMask.MaskEdBox Txt_Qtd_Machos 
         Height          =   330
         Index           =   0
         Left            =   2880
         TabIndex        =   29
         Top             =   240
         Width           =   810
         _ExtentX        =   1429
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   4
         Mask            =   "9####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox Txt_Qtd_Femeas 
         Height          =   330
         Index           =   0
         Left            =   4560
         TabIndex        =   30
         Top             =   240
         Width           =   810
         _ExtentX        =   1429
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   4
         Mask            =   "9####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox Txt_Qtd_Total 
         Height          =   330
         Index           =   0
         Left            =   6000
         TabIndex        =   33
         Top             =   240
         Width           =   810
         _ExtentX        =   1429
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   4
         Mask            =   "9####"
         PromptChar      =   "_"
      End
      Begin Mask2S.ConMask2S txtValor 
         Height          =   375
         Index           =   1
         Left            =   8520
         TabIndex        =   35
         Top             =   240
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         mask            =   ""
         text            =   "0,00"
         locked          =   0   'False
         enabled         =   -1  'True
      End
      Begin VB.Label Label1 
         Caption         =   "Valor do Preju�zo"
         Height          =   255
         Index           =   1
         Left            =   7080
         TabIndex        =   36
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Total:"
         Height          =   195
         Index           =   1
         Left            =   5520
         TabIndex        =   34
         Top             =   360
         Width           =   405
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Machos:"
         Height          =   195
         Left            =   2160
         TabIndex        =   32
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "F�meas:"
         Height          =   195
         Index           =   0
         Left            =   3840
         TabIndex        =   31
         Top             =   360
         Width           =   600
      End
   End
   Begin VB.ComboBox cboMutuarios 
      Height          =   315
      Left            =   240
      TabIndex        =   26
      Text            =   "Combo1"
      Top             =   6240
      Visible         =   0   'False
      Width           =   10335
   End
   Begin MSFlexGridLib.MSFlexGrid grdPropostasAviso 
      Height          =   1335
      Left            =   240
      TabIndex        =   23
      Top             =   7560
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   2355
      _Version        =   393216
   End
   Begin MSFlexGridLib.MSFlexGrid grdPropostas 
      Height          =   1695
      Left            =   240
      TabIndex        =   22
      Top             =   360
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   2990
      _Version        =   393216
   End
   Begin Mask2S.ConMask2S mskIS 
      Height          =   255
      Left            =   8880
      TabIndex        =   20
      Top             =   6840
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   450
      mask            =   ""
      text            =   "0,00"
      locked          =   0   'False
      enabled         =   0   'False
   End
   Begin Mask2S.ConMask2S txtValor 
      Height          =   375
      Index           =   0
      Left            =   8880
      TabIndex        =   19
      Top             =   4920
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      mask            =   ""
      text            =   "0,00"
      locked          =   0   'False
      enabled         =   -1  'True
   End
   Begin VB.Frame frmTextoObjeto 
      Caption         =   "Objeto Segurado"
      Height          =   975
      Left            =   120
      TabIndex        =   16
      Top             =   2280
      Visible         =   0   'False
      Width           =   10575
      Begin VB.TextBox txtObjetoSegurado 
         Height          =   360
         Left            =   90
         TabIndex        =   17
         Top             =   375
         Width           =   10335
      End
   End
   Begin VB.OptionButton optReintegracaoIS 
      Caption         =   "N�o"
      Height          =   255
      Index           =   1
      Left            =   11400
      TabIndex        =   15
      Top             =   6840
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.OptionButton optReintegracaoIS 
      Caption         =   "Sim"
      Height          =   255
      Index           =   0
      Left            =   10680
      TabIndex        =   14
      Top             =   6840
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txtFranquia 
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   12
      Top             =   6840
      Width           =   6615
   End
   Begin VB.ComboBox cboCobAtingida 
      Height          =   315
      Left            =   240
      TabIndex        =   9
      Text            =   "Escolha a cobertura"
      Top             =   4920
      Width           =   6615
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   495
      Left            =   10920
      TabIndex        =   6
      Top             =   9120
      Width           =   1575
   End
   Begin VB.CommandButton btnAvancar 
      Caption         =   "Avan�ar"
      Height          =   495
      Left            =   9120
      TabIndex        =   5
      Top             =   9120
      Width           =   1575
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<<  Voltar"
      Height          =   495
      Left            =   7320
      TabIndex        =   4
      Top             =   9120
      Width           =   1575
   End
   Begin VB.CommandButton btnAlterarProposta 
      Caption         =   "Alterar Proposta"
      Height          =   495
      Left            =   10920
      TabIndex        =   3
      Top             =   8520
      Width           =   1575
   End
   Begin VB.CommandButton cmdAdicionar 
      Caption         =   "Adicionar"
      Height          =   495
      Left            =   10920
      TabIndex        =   2
      Top             =   7920
      Width           =   1575
   End
   Begin VB.Frame fraPropostasAviso 
      Caption         =   "Propostas do Aviso"
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   7320
      Width           =   10575
   End
   Begin VB.Frame fraPropostas 
      Caption         =   "Propostas"
      Height          =   2055
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   10575
   End
   Begin VB.Frame fraDescricao 
      Caption         =   "Descri��o do Objeto Segurado"
      Height          =   2295
      Left            =   120
      TabIndex        =   7
      Top             =   2280
      Width           =   10575
      Begin GridAlianca.grdAlianca grdDescrObjSegurado 
         Height          =   1815
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   3201
         BorderStyle     =   0
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
   End
   Begin VB.Label lblAvisoObjSeg 
      Caption         =   "� necess�rio selecionar o objeto segurado!"
      Height          =   240
      Left            =   1890
      TabIndex        =   27
      Top             =   4695
      Visible         =   0   'False
      Width           =   4875
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Mutu�rios"
      Height          =   195
      Left            =   240
      TabIndex        =   25
      Top             =   6000
      Visible         =   0   'False
      Width           =   690
   End
   Begin VB.Label Label5 
      Caption         =   "Label5"
      Height          =   15
      Left            =   240
      TabIndex        =   24
      Top             =   6000
      Width           =   495
   End
   Begin VB.Label Label4 
      Caption         =   "Franquia"
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   6600
      Width           =   1695
   End
   Begin VB.Label Label3 
      Caption         =   "Cobertura Afetada"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   4680
      Width           =   1485
   End
   Begin VB.Label lblReintegIS 
      Caption         =   "Reintegra��o da IS?"
      Height          =   255
      Left            =   10680
      TabIndex        =   13
      Top             =   6240
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "Valor da IS"
      Height          =   255
      Left            =   7440
      TabIndex        =   11
      Top             =   6840
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Valor do Preju�zo"
      Height          =   255
      Index           =   0
      Left            =   7440
      TabIndex        =   10
      Top             =   5040
      Width           =   1335
   End
End
Attribute VB_Name = "frmObjSegurado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Projeto 1113817
'Diego Galizoni Caversan
'Atribui��o da cobertura e valor do preju�zo para cada proposta selecionada na tela anterior

Dim texto_franquia() As Franquia
Public tQuestionario As cQuestionario
Const EVENTO_SINISTRO_PONTUAL = 1
Const EVENTO_SINISTRO_PERIODICO = 2
Public tp_cobertura_id As Integer
Public flagCredito As String
Public ValorIS As Double

'14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
Const cCapReparo As String = "Pass�vel de Reparo"
Const cCapPerdaTotal As String = "Perda Total"
Const cCapSim As String = "Sim"
Const cCapNao As String = "N�o"
'14/01/2019 (ntedencia) 00210408-esteira_flash (fim)

'Novo produto pecu�ro
Dim reanalise As Boolean

'In�cio - Rafael Bispo - 19381771 - Aviso de Sinistro por Risco Rural SEGP0862 21/07/2017
Dim flagProdutoPossuiBem As Boolean
'Fim - Rafael Bispo - 19381771 - Aviso de Sinistro por Risco Rural SEGP0862 21/07/2017
Public Evento_Cobertura_Basica_Faturamento_Pecuario As Boolean '' Laurent Silva - 18/07/2018 - Demanda de faturamento pecu�rio

'Elimina as escolhas e deixa a proposta disponivel novamente para escolha da cobertura e valor do preju�zo
Private Sub btnAlterarProposta_Click()
Dim indice As Integer

If grdPropostasAviso.Rows > 1 And grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 1) <> "" Then
    If grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 1) <> 0 Then
        wAddLinha = "" & _
                vbTab & grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 1) & _
                vbTab & grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 2) & _
                vbTab & grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 3) & _
                vbTab & grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 6) & _
                vbTab & grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 5) & _
                vbTab & grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 4) & _
                vbTab & Propostas(getIndiceProposta(grdPropostasAviso.TextMatrix(grdPropostasAviso.RowSel, 1))).SubRamo
                
        grdPropostas.AddItem (wAddLinha)
    Else
        'Aviso sem proposta
        grdPropostas.TextMatrix(1, 1) = "0"
        grdPropostas.TextMatrix(1, 2) = "0"
        grdPropostas.TextMatrix(1, 3) = "0"
        grdPropostas.TextMatrix(1, 4) = "0"
        grdPropostas.TextMatrix(1, 5) = "0"
        grdPropostas.TextMatrix(1, 6) = "Aviso Sem Proposta"
        grdPropostas.TextMatrix(1, 7) = "0"
    End If
        'Se a primeira coluna da primeira linha est� vazia, elimina a linha
        If grdPropostas.TextMatrix(1, 1) = "" Then
            grdPropostas.RemoveItem (1)
        End If
        
        'Se existe apenas o cabe�alho e um item, adiciona uma linha em branco antes de remover
        If grdPropostasAviso.Rows = 2 Then
            grdPropostasAviso.AddItem (" ")
        End If
        'Remove a linha selecionada
        grdPropostasAviso.RemoveItem (grdPropostasAviso.RowSel)
        txtObjetoSegurado = ""
    
End If

If produto_id = 1240 And tp_cobertura = 1169 Then LimparFramePecuario

End Sub

Private Sub btnAvancar_Click()
    Dim Rs As ADODB.Recordset '14/01/2019 (ntedencia) 00210408-esteira_flash

'sergio.sn - corre��o para evitar duplicidade na gera��o de avisos de sinistros
' 04/11/2010
Set Avisos = Nothing
'sergio.en

Dim indice As Integer
If grdPropostas.Rows > 1 Then
    If grdPropostas.Rows > 1 And grdPropostas.TextMatrix(1, 1) <> "" Then
        For i = 1 To grdPropostasAviso.Rows - 1
            'If InStr(1, cProdutoBBProtecao, grdPropostasAviso.TextMatrix(CInt(i), 3), vbTextCompare) Then
            If frmAvisoPropostas.VerificarProdutoBB(cProdutoBBProtecao, grdPropostasAviso.TextMatrix(CInt(i), 3)) Then
                For j = 1 To grdPropostas.Rows - 1
                    'If InStr(1, cProdutoBBProtecao, grdPropostas.TextMatrix(CInt(j), 3), vbTextCompare) Then
                    If frmAvisoPropostas.VerificarProdutoBB(cProdutoBBProtecao, grdPropostasAviso.TextMatrix(CInt(i), 3)) Then
                        MsgBox "Devem ser selecionadas todas as propostas do BB Prote��o.", vbInformation, "Aviso de Sinistro"
                        Exit Sub
                    End If
                Next j
            End If
    
        Next i
        
    End If
End If
    For i = 1 To grdPropostasAviso.Rows - 1
        Set tAviso = New cAviso
         If grdPropostasAviso.TextMatrix(CInt(i), 1) <> "" And grdPropostasAviso.TextMatrix(CInt(i), 1) <> "Proposta" Then
                             
            If grdPropostasAviso.TextMatrix(CInt(i), 1) <> 0 Then
                indice = getIndiceProposta(grdPropostasAviso.TextMatrix(CInt(i), 1))
                With tAviso
                    .Proposta = grdPropostasAviso.TextMatrix(CInt(i), 1)
                    .Proposta_bb = grdPropostasAviso.TextMatrix(CInt(i), 2)
                    .Produto = grdPropostasAviso.TextMatrix(CInt(i), 3)
                    .Ramo = grdPropostasAviso.TextMatrix(CInt(i), 6)
                    .apolice = grdPropostasAviso.TextMatrix(CInt(i), 5)
                    .evento = iEventoId
                    .valor_prejuizo = grdPropostasAviso.TextMatrix(CInt(i), 9)
                    .nome_produto = grdPropostasAviso.TextMatrix(CInt(i), 4)
                    .SubRamo = Propostas(indice).SubRamo
                    .SituacaoProposta = Propostas(indice).SituacaoProposta
                    If sProdutoAgricola = "REANALISE" Then
                        .Cobertura = tp_cobertura_id
                    Else
                        .Cobertura = grdPropostasAviso.TextMatrix(CInt(i), 12)
                    End If
                    
                    '' Laurent Silva - 18/07/2018 - Demanda de faturamento pecu�rio
                    '' N�o chamar a tela de questionario caso os eventos sejam da cobertura b�sica - faturamento.
'                    If Evento_Cobertura_Basica_Faturamento_Pecuario = True Then
'                        .ObjetoSegurado = 0 '' Rever esta l�gica
'                    Else
'                        .ObjetoSegurado = grdPropostasAviso.TextMatrix(CInt(i), 11)
'                    End If

                    .ObjetoSegurado = grdPropostasAviso.TextMatrix(CInt(i), 11)
                    ''.ObjetoSegurado = grdPropostasAviso.TextMatrix(CInt(i), 11)
                    .DescricaoCobertura = grdPropostasAviso.TextMatrix(CInt(i), 10)
                    .ValorEstimativa = IIf(grdPropostasAviso.TextMatrix(CInt(i), 13) = "", 0, grdPropostasAviso.TextMatrix(CInt(i), 13))
                    .ProcessaReintegracaoIs = grdPropostasAviso.TextMatrix(CInt(i), 14)
                     
                End With
                
            Else 'Sem proposta
                With tAviso
                    .Proposta = grdPropostasAviso.TextMatrix(CInt(i), 1)
                    .Proposta_bb = IIf(frmDadosCliente.txtPropostaBB = "", 0, frmDadosCliente.txtPropostaBB)
                    .Produto = grdPropostasAviso.TextMatrix(CInt(i), 3)
                    .Ramo = grdPropostasAviso.TextMatrix(CInt(i), 6)
                    .apolice = grdPropostasAviso.TextMatrix(CInt(i), 5)
                    .evento = iEventoId
                    .valor_prejuizo = grdPropostasAviso.TextMatrix(CInt(i), 9)
                    .nome_produto = grdPropostasAviso.TextMatrix(CInt(i), 4)
                    .SubRamo = 0
                    .SituacaoProposta = "Sem proposta"
                    .Cobertura = 1 'gen�rica
                    .ObjetoSegurado = grdPropostasAviso.TextMatrix(CInt(i), 11)
                    .DescricaoCobertura = cboCobAtingida.Text
                    .ValorEstimativa = CDbl(IIf(txtFranquia.Text = "", 0, txtFranquia.Text))
                    .ProcessaReintegracaoIs = processa_reintegracao_is
                End With
            End If
            Avisos.Add tAviso
            'Rs.Close
        Else
            MsgBox "Adicione uma proposta antes de prosseguir.", vbCritical
            Exit Sub
        End If
        
    Next i
    
'Gerson - CWI - Mensagem vinda de frmEvento.cmdContinuar_Click

    If MsgBox("Solicitante autoriza cr�dito em conta? ", vbQuestion + vbYesNo) = vbYes Then
        flagCredito = "S"
    Else
        flagCredito = "N"
    End If
        
    Me.Hide
    
    '14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
    'Se existir equipamentos cadastrados validar pagamento imediato
    If frmAviso.grdEquipamentos.Rows > 1 Then
        frmAviso.txtEquipamentoDetalhamento = ""
        
        SQL = RetornaQueryPagtoImediato()
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        frmAviso.txtEquipamentoValor.Text = Rs(0)
        frmAviso.txtEquipamentoDetalhamento.Text = Rs(1)
        frmAviso.cboPgtoImediato.ListIndex = Rs(2)
    
        If frmAviso.cboPgtoImediato.ListIndex = 1 Then
            'EXIBIR TELA DE BENEFICIARIO DO PAGAMENTO IMEDIATO
        Else
            frmOutrosSeguros.Show
        End If
    Else
    '14/01/2019 (ntedencia) 00210408-esteira_flash (fim)
        frmOutrosSeguros.Show
    End If '14/01/2019 (ntedencia) 00210408-esteira_flash
End Sub

'14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
Public Function RetornaQueryPagtoImediato() As String
    Dim sSQL As String
    Dim iOrcamento As Integer
    Dim iDano As Integer
    Dim iLaudo As Integer

    sSQL = ""
    sSQL = sSQL & "CREATE TABLE #Equipamento (Codigo INT"
    sSQL = sSQL & "  ,TipoBem VARCHAR(60)"
    sSQL = sSQL & "  ,Modelo VARCHAR(60)"
    sSQL = sSQL & "  ,Dano INT"
    sSQL = sSQL & "  ,Orcamento INT"
    sSQL = sSQL & "  ,Valor NUMERIC(15,2)"
    sSQL = sSQL & "  ,Laudo INT"
    sSQL = sSQL & "  ,NomeAssistencia VARCHAR(60)"
    sSQL = sSQL & "  ,TelAssistencia VARCHAR(15)"
    sSQL = sSQL & "  ,Descricao VARCHAR(255)"
    sSQL = sSQL & ")" & vbNewLine & vbNewLine

    For i = 1 To frmAviso.grdEquipamentos.Rows - 1
        sCodigo = frmAviso.grdEquipamentos.TextMatrix(i, 0)
        sTipoBem = frmAviso.grdEquipamentos.TextMatrix(i, 1)
        sModelo = frmAviso.grdEquipamentos.TextMatrix(i, 2)
        sDano = frmAviso.grdEquipamentos.TextMatrix(i, 3)
        sOrcamento = frmAviso.grdEquipamentos.TextMatrix(i, 4)
        sValor = frmAviso.grdEquipamentos.TextMatrix(i, 5)
        sLaudo = frmAviso.grdEquipamentos.TextMatrix(i, 6)
        sNome = frmAviso.grdEquipamentos.TextMatrix(i, 7)
        sTele = frmAviso.grdEquipamentos.TextMatrix(i, 8)
        sDescricao = frmAviso.grdEquipamentos.TextMatrix(i, 9)
        
        iOrcamento = 0
        If sOrcamento = cCapSim Then
          iOrcamento = 1
        End If
                    
        iLaudo = 0
        If sLaudo = cCapSim Then
          iLaudo = 1
        End If
        
        iDano = 0
        If sDano = cCapReparo Then
          iDano = 1
        End If
        If sDano = cCapPerdaTotal Then
          iDano = 2
        End If
                        
        If i > 0 Then
            sSQL = sSQL & "INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)" & vbNewLine
            sSQL = sSQL & "VALUES ('" & sCodigo & "', '" & sTipoBem & "', '" & sModelo & "', '" & iDano & "', '" & iOrcamento & "', '" & Replace(sValor, ",", ".") & "', '" & iLaudo & "', '" & sNome & "', '" & sTele & "', '" & sDescricao & "')" & vbNewLine
         End If
    Next i
    
    sSQL = sSQL + vbNewLine
    sSQL = sSQL & "EXEC seguros_db.dbo.SEGS14618_SPS"
    
    RetornaQueryPagtoImediato = sSQL
End Function
'14/01/2019 (ntedencia) 00210408-esteira_flash (fim)

Private Sub cboCobAtingida_Click()
    Dim Aviso As Object
    Dim cValPrejuizo As Currency
    Dim oPrejuizo As Object
    Dim RSaux As ADODB.Recordset
    Dim SQLaux As String

    'Demanda 18723625 - IN�CIO
    Dim subramo_id As Integer
    Dim tp_cobertura As Integer
    'Demanda 18723625 - FIM

    Dim valDesconto As Double  'Demanda MU-2017-043439

    If cboCobAtingida.ListIndex <> -1 Then
        Me.MousePointer = vbHourglass
        ''Me.txtValor(0).SetFocus

        'ObtemISRE da DLL
        Set Aviso = CreateObject("SEGL0144.cls00385")
        Set oPrejuizo = CreateObject("SEGL0144.cls00326")

        Aviso.mvarAmbienteId = glAmbiente_id
        Aviso.mvarSiglaSistema = gsSIGLASISTEMA
        Aviso.mvarSiglaRecurso = App.Title
        Aviso.mvarDescricaoRecurso = App.FileDescription


        oPrejuizo.mvarAmbienteId = glAmbiente_id
        oPrejuizo.mvarSiglaSistema = gsSIGLASISTEMA
        oPrejuizo.mvarSiglaRecurso = App.Title
        oPrejuizo.mvarDescricaoRecurso = App.FileDescription
        oPrejuizo.mvarAmbiente = ConvAmbiente(glAmbiente_id)

'' C�lculo de estimativa do aviso de produto 1240


        'RAFAEL martins
        '19381771 - Aviso de Sinistro por Risco Rural SEGP0862
        'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
        'ADICIONADO PRODUTOS 230
        If ProdutoProposta <> 156 And ProdutoProposta <> 226 _
           And ProdutoProposta <> 227 And ProdutoProposta <> 228 _
           And ProdutoProposta <> 229 And ProdutoProposta <> 300 _
           And ProdutoProposta <> 230 _
           And ProdutoProposta <> 701 Then

            SQL = ""
            SQL = SQL & "exec SEGS7752_SPS '" & Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd") & "'"
            If grdPropostas.TextMatrix(grdPropostas.RowSel, 1) = "Aviso sem proposta" Then
                SQL = SQL & ", 0"
            Else
                SQL = SQL & ", " & grdPropostas.TextMatrix(grdPropostas.RowSel, 1)
            End If

        Else
            SQL = ""
            SQL = SQL & "exec SEGS7752_SPS '" & Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd") & "'"
            If grdPropostas.TextMatrix(grdPropostas.RowSel, 1) = "Aviso sem proposta" Then
                SQL = SQL & ", 0"
            Else
                SQL = SQL & ", " & grdPropostas.TextMatrix(grdPropostas.RowSel, 1) & ", 1, " & grdDescrObjSegurado.TextMatrix(grdDescrObjSegurado.RowSel, 1)
            End If

        End If


        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
        While Not Rs.EOF
            If Rs("tp_cobertura_id") = cboCobAtingida.ItemData(cboCobAtingida.ListIndex) Then
                ValorIS = Rs("val_is")
                mskIS.Text = Format(ValorIS, "0.00")
            End If
            Rs.MoveNext
        Wend
        Rs.Close

        Set Aviso = Nothing

        txtFranquia.Text = ObtemTexto_franquia(CInt(cboCobAtingida.ItemData(cboCobAtingida.ListIndex)))
        Me.MousePointer = vbDefault

        'vbarbosa - 15/12/2006
        If produtoQuestionario <> 0 Then
            cValPrejuizo = oPrejuizo.ObtemValPrejuizoCalculado(grdPropostas.TextMatrix(grdPropostas.RowSel, 1), CCur(mskIS.Text), cUserName, "#aviso_sinistro_tb", oSABL0100)
        End If

        'asouza 16.03.06
        If cValPrejuizo <> 0 Then
            '  txtValor.Locked = True
        Else
            txtValor(0).Locked = False
        End If

        'fazer o calculo automatico do prejuizo
        'adicionado por Afonso Filho - GPTI
        'DATA: 13/11/2008
        If Propostas.Count > 0 Then
            produtoId = grdPropostas.TextMatrix(grdPropostas.RowSel, 3)
        Else
            produto_id = 0
        End If

        If produtoId = 1152 Then

            SQLaux = "SET NOCOUNT ON EXEC SEGS7264_SPS"

            Set RSaux = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, glConexao, "0", True)

            If Not RSaux.EOF Then
                If RSaux(0) = 0 Then
                    cValPrejuizo = 50
                Else
                    cValPrejuizo = (mskIS.Text * RSaux(0)) / 100

                    'Demanda 18723625 - IN�CIO
                    If Propostas.Count > 0 Then
                        subramo_id = grdPropostas.TextMatrix(grdPropostas.RowSel, 7)
                    Else
                        subramo_id = 0
                    End If

                    tp_cobertura = cboCobAtingida.ItemData(cboCobAtingida.ListIndex)

                    SQLaux = "Select IsNull(perc_estimativa,0) " & vbNewLine
                    SQLaux = SQLaux & " From produto_estimativa_sinistro_tb " & vbNewLine
                    SQLaux = SQLaux & " Where produto_id = " & produtoId & vbNewLine
                    If subramo_id > 0 Then
                        SQLaux = SQLaux & " And subramo_id = " & subramo_id & vbNewLine
                    End If
                    SQLaux = SQLaux & " And tp_cobertura_id = " & tp_cobertura & vbNewLine
                    SQLaux = SQLaux & " And evento_sinistro_id = " & EventoId & vbNewLine
                    SQLaux = SQLaux & " And utiliza_percentual_subevento = 'S' " & vbNewLine
                    SQLaux = SQLaux & " And dt_fim_vigencia Is Null " & vbNewLine

                    Set RSaux = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, glConexao, "0", True)

                    If Not RSaux.EOF Then
                        cValPrejuizo = cValPrejuizo - (cValPrejuizo * (RSaux(0) / 100))
                    End If

                    RSaux.Close
                    'Demanda 18723625 - FIM
                End If
            End If

        End If

        'Demanda MU-2017-043439 - Parametrizar Estimativa dos Sinistros - Paulo Pelegrini - 11/05/2018 - IN�CIO
        'add 1240 -> 00421597-seguro-faturamento-pecuario
        If produtoId = 1204 Or produtoId = 1240 Then

            If Propostas.Count > 0 Then
                subramo_id = grdPropostas.TextMatrix(grdPropostas.RowSel, 7)
            Else
                subramo_id = 0
            End If

            tp_cobertura = cboCobAtingida.ItemData(cboCobAtingida.ListIndex)

            SQLaux = "Select IsNull(perc_estimativa,0) " & vbNewLine
            SQLaux = SQLaux & " From produto_estimativa_sinistro_tb " & vbNewLine
            SQLaux = SQLaux & " Where produto_id = " & produtoId & vbNewLine
            If subramo_id > 0 Then
                SQLaux = SQLaux & " And subramo_id = " & subramo_id & vbNewLine
            End If
            SQLaux = SQLaux & " And tp_cobertura_id = " & tp_cobertura & vbNewLine
            SQLaux = SQLaux & " And evento_sinistro_id = " & EventoId & vbNewLine
            SQLaux = SQLaux & " And utiliza_percentual_subevento = 'S' " & vbNewLine
            SQLaux = SQLaux & " And dt_fim_vigencia Is Null " & vbNewLine

            Set RSaux = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, True)

            If Not RSaux.EOF Then
                valDesconto = (RSaux(0) / 100)
            End If

            RSaux.Close


        End If
        'Demanda MU-2017-043439 - Parametrizar Estimativa dos Sinistros - Paulo Pelegrini - 11/05/2018 - FIM

        '####################################################################################################################
        '#                                RPORTO - JMCONFITEC 17/02/2014 Demanda 18037006
        '####################################################################################################################


        If (produtoId = 1201 Or produtoId = 1210) And cboCobAtingida.ItemData(cboCobAtingida.ListIndex) = "527" Then

            Dim sqlTemp As String
            Dim strDtOcorrencia As String
            Dim rsTemp As Recordset
            strDtOcorrencia = "'" & Format(frmAviso.txtDtOcorrencia.Text, "yyyymmdd") & "'"

            sqlTemp = ""
            adSQL sqlTemp, "SELECT escolha_tp_cob_generico_tb.val_is"
            adSQL sqlTemp, "  FROM seguros_db..escolha_tp_cob_generico_tb escolha_tp_cob_generico_tb WITH(NOLOCK)"
            adSQL sqlTemp, " WHERE escolha_tp_cob_generico_tb.proposta_id =  " & grdPropostas.TextMatrix(grdPropostas.RowSel, 1)
            adSQL sqlTemp, "   AND escolha_tp_cob_generico_tb.tp_cobertura_id = 527"
            adSQL sqlTemp, "   AND escolha_tp_cob_generico_tb.dt_inicio_vigencia_esc <=" & strDtOcorrencia
            adSQL sqlTemp, "   AND ISNULL(escolha_tp_cob_generico_tb.dt_fim_vigencia_esc,DATEADD(year,1,escolha_tp_cob_generico_tb.dt_inicio_vigencia_esc)) >=" & strDtOcorrencia

            Set rsTemp = ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sqlTemp, True)

            If Not rsTemp.EOF Then
                cValPrejuizo = rsTemp("val_is")
                txtValor(0).Enabled = False
                optReintegracaoIS(0).Enabled = False
                optReintegracaoIS(1).Enabled = False
                optReintegracaoIS(0).value = False
                optReintegracaoIS(1).value = True
            End If
            rsTemp.Close
            Set rsTemp = Nothing
        End If

        '####################################################################################################################

        'rm - add 1240 -> 00421597-seguro-faturamento-pecuario
        If produtoId = 1240 And tp_cobertura <> 1169 Then
            SQLaux = "EXEC seguros_db.dbo.SEGS13785_SPS " & produtoId & ", " & tp_cobertura & ", " & EventoId & ", " & subramo_id

            Set RSaux = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, True)
            If RSaux.EOF Then
                cValPrejuizo = 0
            Else
                cValPrejuizo = CDbl(RSaux("val_inicial"))
            End If
            RSaux.Close

          'Novo produto pecu�rio in�cio
          If tp_cobertura = 1169 Then '1240
            produtoQuestionario = 994 '1240
          ElseIf tp_cobertura = 1170 Then
            produtoQuestionario = 995 '1240
          ElseIf tp_cobertura = 1171 Then
            produtoQuestionario = 995 '1240
          Else
            produtoQuestionario = 0
          End If

         If produtoQuestionario > 0 And Not reanalise Then
           If Not MontarQuestionarioProduto() Then
              Set Aviso = Nothing
              Me.MousePointer = vbDefault
              Me.Show
           End If
         End If
      End If
    'Novo produto pecu�rio Fim

        'DEMANDA: 7979370 - FELIPPE MENDES/ THIAGO ERUSTES - 20111020
        'IMPLEMENTANDO A VERIFICA��O PARA O PRODUTO 1204

        If produtoId = 1204 Then

            Dim proposta_id As String
            Dim AreaSinistrada As Double
            Dim AreaSegurada As Double
            Dim LmiCobReplantio As Double
            Dim LmiCobBasica As Double
            Dim ProdutividadeObtidaTotal As Double
            Dim ProdutividadeObtida As Double
            Dim ProdutividadeEsperada As Double
            Dim PrecoBase As Double

            proposta_id = grdPropostas.TextMatrix(grdPropostas.RowSel, 1)

            SQLaux = " select texto_resposta from #questionario_aviso_sinistro_tb (nolock) WHERE PERGUNTA_ID = 2849"
            Set RSaux = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, glConexao, "0", True)
            If RSaux.EOF Then
                AreaSinistrada = 0
            Else
                AreaSinistrada = CDbl(RSaux(0))
            End If
            RSaux.Close

            SQLaux = " select area_segurada, produtividade_esperada from seguro_esp_agricola_tb (nolock) where proposta_id = " & proposta_id
            Set RSaux = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, glConexao, "0", True)
            If RSaux.EOF Then
                AreaSegurada = 0
                ProdutividadeEsperada = 0
            Else
                AreaSegurada = CDbl(RSaux("area_segurada"))
                ProdutividadeEsperada = CDbl(RSaux("produtividade_esperada"))
            End If
            RSaux.Close

            SQLaux = " select texto_resposta from questionario_objeto_tb (nolock) where pergunta_id = 7740 and proposta_id =  " & proposta_id
            'FELIPPE MENDES (CONFITEC) - 02/04/2012
            'BUSCAR A �LTIMA VERS�O DO ENDOSSO PARA RETORNAR OS DADOS.
            SQLaux = SQLaux & " and endosso_id = 0"
            Set RSaux = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, glConexao, "0", True)
            If RSaux.EOF Then
                PrecoBase = 0
            Else
                PrecoBase = CDbl(RSaux("texto_resposta"))
            End If
            RSaux.Close

            SQLaux = " select 'lmi_cob_repl' =  val_is from escolha_tp_cob_generico_tb (nolock) where tp_cobertura_id = 834 and proposta_id = " & proposta_id
            Set RSaux = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, glConexao, "0", True)
            If RSaux.EOF Then
                LmiCobReplantio = 0
            Else
                LmiCobReplantio = RSaux("lmi_cob_repl")
            End If
            RSaux.Close

            SQLaux = " select 'lmi_cob_fat' =  val_is from escolha_tp_cob_generico_tb (nolock) where tp_cobertura_id = 835 and proposta_id = " & proposta_id
            Set RSaux = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, glConexao, "0", True)
            If RSaux.EOF Then
                LmiCobBasica = 0
            Else
                LmiCobBasica = RSaux("lmi_cob_fat")
            End If
            RSaux.Close

            SQLaux = " select texto_resposta from #questionario_aviso_sinistro_tb (nolock) WHERE PERGUNTA_ID = 3271"
            Set RSaux = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, glConexao, "0", True)
            If RSaux.EOF Then
                ProdutividadeObtida = 0
            Else
                ProdutividadeObtida = CDbl(RSaux(0))
            End If
            RSaux.Close

            If cboCobAtingida.ItemData(cboCobAtingida.ListIndex) = 834 Then    'Replantio
                If AreaSinistrada > (0.2 * AreaSegurada) Then
                    cValPrejuizo = (AreaSinistrada * LmiCobReplantio) / AreaSegurada
                Else
                    cValPrejuizo = 50
                End If
                If cValPrejuizo > LmiCobReplantio Then
                    cValPrejuizo = LmiCobReplantio
                End If
            Else
                If cboCobAtingida.ItemData(cboCobAtingida.ListIndex) = 835 Then    'Basica
                    ProdutividadeObtidaTotal = ((AreaSinistrada * ProdutividadeObtida) + ((AreaSegurada - AreaSinistrada) * ProdutividadeEsperada)) / AreaSegurada
                    If ProdutividadeObtidaTotal < (0.2 * ProdutividadeEsperada) Then
                        ProdutividadeObtidaTotal = (0.2 * ProdutividadeEsperada)
                    End If

                    cValPrejuizo = LmiCobBasica - (AreaSegurada * (ProdutividadeObtidaTotal / 60) * PrecoBase)
                    If cValPrejuizo <= 0 Then
                        cValPrejuizo = 50
                    End If
                    If cValPrejuizo > LmiCobBasica Then
                        cValPrejuizo = LmiCobBasica
                    End If
                End If
            End If
            
            'Demanda MU-2017-043439 - Parametrizar Estimativa dos Sinistros - Paulo Pelegrini - 11/05/2018 (INI)
            If valDesconto > 0 Then
                cValPrejuizo = cValPrejuizo - (cValPrejuizo * valDesconto)
            End If
            'Demanda MU-2017-043439 - Parametrizar Estimativa dos Sinistros - Paulo Pelegrini - 11/05/2018 (FIM)

        End If

    '' Laurent Silva - 13/08/2018
    
    produto_id = Val(grdPropostas.TextMatrix(grdPropostas.RowSel, 3))
    tp_cobertura = Val(cboCobAtingida.ItemData(cboCobAtingida.ListIndex))

    'Thiago - Confitec - 2019/05 - Demanda N�o pedir numero de animais para evento 430 > inclus�o eventoID
    If produto_id = 1240 And tp_cobertura = 1169 And sProdutoAgricola <> "REANALISE" Then
        
        Me.Evento_Cobertura_Basica_Faturamento_Pecuario = True
        txtValor(0).Visible = False
        Label1(0).Visible = False
        LimparFramePecuario
        txtValor(1).Text = Format(cValPrejuizo, "0.00")
           If EventoId = 430 Then
                Me.Frame4.Visible = False
           End If
         Exit Sub
    Else
        
        Me.Evento_Cobertura_Basica_Faturamento_Pecuario = False
        txtValor(0).Visible = True
        Label1(0).Visible = True
        Me.Frame4.Visible = False
        
    End If

        txtValor(0).Text = Format(cValPrejuizo, "0.00")

    End If

End Sub

Private Sub LimparFramePecuario()

    Me.Frame4.Visible = True
    Me.Txt_Qtd_Machos(0).Text = "0____"
    Me.Txt_Qtd_Femeas(0).Text = "0____"
    Me.Txt_Qtd_Total(0).Text = "0____"

End Sub

Private Sub cmdAdicionar_Click()

    Dim wAddLinha As String
    Dim indice As Integer
    Dim Proposta As cProposta

    'Demanda MU-2017-043439 - Parametrizar Estimativa dos Sinistros - Paulo Pelegrini - 11/05/2018 - IN�CIO
    Dim RSaux As ADODB.Recordset
    Dim SQLaux As String
    Dim subramo_id As Integer
    Dim tp_cobertura As Integer
    'Demanda MU-2017-043439 - Parametrizar Estimativa dos Sinistros - Paulo Pelegrini - 11/05/2018 - FIM



    'Racras 20/10/2010
    'Verifica se o txtvalor tem valor.
    'DEMANDAS - 4766934 - � poss�vel inserir valor 0,00
    'If (Trim(Replace(txtValor.Text, "0", "")) = "," Or Trim(txtValor.Text) = "") Then
    '       MsgBox "Preencha o valor do preju�zo (Estimado)", vbCritical, App.Title
    '      txtValor.SetFocus
    '     Exit Sub
    ' End If


    If cboCobAtingida.Text = "" Then
        MsgBox "� preciso especificar a cobertura afetada!", vbInformation, "Aviso de Sinistro"
        Exit Sub
    End If

    If lblReintegIS.Visible Then
        If Not Me.optReintegracaoIS(0).value And Not Me.optReintegracaoIS(1).value Then
            Call MsgBox("� necess�rio indicar se h� reintegra��o de IS.", vbCritical, "Aviso RE - Objeto Sinistrado")
            Exit Sub
        End If
    End If


If Evento_Cobertura_Basica_Faturamento_Pecuario Then
    'DEMANDAS - 4766934
    If Val(txtValor(1).Text) = 0 Then
        If MsgBox("Confirma que o solicitante n�o sabe informar uma estimativa do valor do prejuizo?", vbYesNo, "Aviso de Sinistro") = vbNo Then
            txtValor(1).SetFocus
            Exit Sub
        Else
        End If
    End If
Else

    'DEMANDAS - 4766934
    If Val(txtValor(0).Text) = 0 Then
        If MsgBox("Confirma que o solicitante n�o sabe informar uma estimativa do valor do prejuizo?", vbYesNo, "Aviso de Sinistro") = vbNo Then
            txtValor(0).SetFocus
            Exit Sub
        Else
        End If
    End If

End If
    
    'Demanda MU-2017-043439 - Parametrizar Estimativa dos Sinistros - Paulo Pelegrini - 11/05/2018 - IN�CIO
    If Propostas.Count > 0 Then
        produtoId = grdPropostas.TextMatrix(grdPropostas.RowSel, 3)
        indice = 0
        For Each Proposta In Propostas
            indice = indice + 1
            If CDbl(Proposta.Proposta) = grdPropostas.TextMatrix(grdPropostas.RowSel, 1) Then
                Exit For
            End If
        Next Proposta
    Else
        produtoId = 0
    End If

    
        'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
        'ADICIONADO PRODUTOS 230
    If (produtoId = 8 Or produtoId = 14 Or produtoId = 155 Or produtoId = 156 _
        Or produtoId = 227 Or produtoId = 228 Or produtoId = 229 Or produtoId = 300 _
        Or produtoId = 230 _
        Or produtoId = 701 Or produtoId = 722) And Propostas(indice).TipoAviso = "AVSR" Then

        If Propostas.Count > 0 Then
            subramo_id = grdPropostas.TextMatrix(grdPropostas.RowSel, 7)
        Else
            subramo_id = 0
        End If

        tp_cobertura = cboCobAtingida.ItemData(cboCobAtingida.ListIndex)

        SQLaux = "Select IsNull(perc_estimativa,0) " & vbNewLine
        SQLaux = SQLaux & " From produto_estimativa_sinistro_tb " & vbNewLine
        SQLaux = SQLaux & " Where produto_id = " & produtoId & vbNewLine
        If subramo_id > 0 Then
            SQLaux = SQLaux & " And subramo_id = " & subramo_id & vbNewLine
        End If
        SQLaux = SQLaux & " And tp_cobertura_id = " & tp_cobertura & vbNewLine
        SQLaux = SQLaux & " And evento_sinistro_id = " & EventoId & vbNewLine
        SQLaux = SQLaux & " And utiliza_percentual_subevento = 'S' " & vbNewLine
        SQLaux = SQLaux & " And dt_fim_vigencia Is Null " & vbNewLine

        Set RSaux = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLaux, True)

        If Not RSaux.EOF Then
            cValPrejuizo = txtValor(0).Text
            cValPrejuizo = cValPrejuizo - (cValPrejuizo * (RSaux(0) / 100))
            txtValor(0).Text = Format(cValPrejuizo, "0.00")
        End If
        RSaux.Close
    End If
    'Demanda MU-2017-043439 - Parametrizar Estimativa dos Sinistros - Paulo Pelegrini - 11/05/2018 - FIM

    cboCobAtingida.Enabled = False

    If grdPropostas.TextMatrix(grdPropostas.RowSel, 1) <> "" Then
        indice = 0


        If grdPropostas.Rows > 1 Then
            For Each Proposta In Propostas
                indice = indice + 1
                If CDbl(Proposta.Proposta) = grdPropostas.TextMatrix(grdPropostas.RowSel, 1) Then
                    Exit For
                End If

            Next Proposta

            If grdPropostas.TextMatrix(grdPropostas.RowSel, 1) <> "0" Then
                wAddLinha = "" & _
                            vbTab & CDbl(Propostas.Item(indice).Proposta) & _
                            vbTab & grdPropostas.TextMatrix(grdPropostas.RowSel, 2) & _
                            vbTab & Propostas.Item(indice).Produto & _
                            vbTab & grdPropostas.TextMatrix(grdPropostas.RowSel, 6) & _
                            vbTab & grdPropostas.TextMatrix(grdPropostas.RowSel, 5) & _
                            vbTab & Propostas.Item(indice).Ramo
                If grdDescrObjSegurado.Rows > 1 Then
                    'If Propostas.Item(indice).Produto = 1152 Then

                    'DEMANDA: 7979370 - FELIPPE MENDES - 20111020
                    'IMPLEMENTANDO VERIFICA��O PARA O PRODUTO 1204
                    'add produto 1240 -> 00421597-seguro-faturamento-pecuario
                    If Propostas.Item(indice).Produto = 1152 Or Propostas.Item(indice).Produto = 1204 Or Propostas.Item(indice).Produto = 1240 Then
                        wAddLinha = wAddLinha & vbTab & "Cultura: " & grdDescrObjSegurado.TextMatrix(6, 2)
                        wAddLinha = wAddLinha & ", Endere�o: " & Trim(grdDescrObjSegurado.TextMatrix(8, 2))
                        wAddLinha = wAddLinha & ", Munic�pio: " & Trim(grdDescrObjSegurado.TextMatrix(10, 2))
                        wAddLinha = wAddLinha & ", UF: " & grdDescrObjSegurado.TextMatrix(9, 2)
                    Else
                        wAddLinha = wAddLinha & vbTab & grdDescrObjSegurado.TextMatrix(1, 2)
                    End If
                Else
                    wAddLinha = wAddLinha & vbTab & ""
                End If
                wAddLinha = wAddLinha & vbTab & frmEvento.cboEvento.Text & _
                            vbTab & IIf(Evento_Cobertura_Basica_Faturamento_Pecuario, txtValor(1).Text, txtValor(0).Text) & _
                            vbTab & cboCobAtingida.Text
                'If Propostas.Item(indice).Produto = 1152 Or Propostas.Item(indice).Produto = 1108 Then

'' Laurent Silva - Adicionando o produto 1240 no if do produto 1204
                'DEMANDA: 7979370 - FELIPPE MENDES - 20111020
                'IMPLEMENTANDO VERIFICA��O PARA O PRODUTO 1204
                'add produto 1240 -> 00421597-seguro-faturamento-pecuario
                If Propostas.Item(indice).Produto = 1152 Or Propostas.Item(indice).Produto = 1108 Or Propostas.Item(indice).Produto = 1204 Or Propostas.Item(indice).Produto = 1240 Then
                    wAddLinha = wAddLinha & vbTab & "1"
                Else
                    wAddLinha = wAddLinha & vbTab & grdDescrObjSegurado.TextMatrix(grdDescrObjSegurado.RowSel, 1)
                End If
                If cboCobAtingida.ListIndex <> -1 Then
                    wAddLinha = wAddLinha & vbTab & cboCobAtingida.ItemData(cboCobAtingida.ListIndex)
                Else
                    wAddLinha = wAddLinha & vbTab & tp_cobertura_id
                End If
                wAddLinha = wAddLinha & vbTab & mskIS.Text & _
                            vbTab & processa_reintegracao_is
            Else    'Sem proposta
                wAddLinha = "" & _
                            vbTab & "0" & _
                            vbTab & "0" & _
                            vbTab & "0" & _
                            vbTab & "Sem produto" & _
                            vbTab & "0" & _
                            vbTab & "0" & _
                            vbTab & txtObjetoSegurado & _
                            vbTab & frmEvento.cboEvento.Text & _
                            vbTab & txtValor(0).Text & _
                            vbTab & cboCobAtingida.Text & _
                            vbTab & "0" & _
                            vbTab & cboCobAtingida.ItemData(cboCobAtingida.ListIndex) & _
                            vbTab & mskIS.Text & _
                            vbTab & processa_reintegracao_is
            End If

            grdPropostasAviso.AddItem (wAddLinha)
            If grdPropostasAviso.TextMatrix(1, 1) = "" Then
                grdPropostasAviso.RemoveItem (1)
            End If
            If grdPropostas.Rows = 2 Then
                grdPropostas.AddItem (" ")
            End If

            grdPropostas.RemoveItem (grdPropostas.RowSel)
            cboCobAtingida.Text = ""
            txtFranquia.Text = ""
            ''txtValor.Text = ""
            If Evento_Cobertura_Basica_Faturamento_Pecuario Then
                txtValor(1).Text = ""
            Else
                txtValor(0).Text = ""
            End If
            
            mskIS.Text = ""
            cmdAdicionar.Enabled = False

            If Propostas.Count > 0 Then

                'If Propostas.Item(indice).Produto = 1152 Then

                'DEMANDA: 7979370 - FELIPPE MENDES - 20111020
                'IMPLEMENTANDO VERIFICA��O PARA O PRODUTO 1204
                'add produto 1240 -> 00421597-seguro-faturamento-pecuario
                If Propostas.Item(indice).Produto = 1152 Or Propostas.Item(indice).Produto = 1204 Or Propostas.Item(indice).Produto = 1240 Then
                    grdPropostasAviso.ColWidth(7) = (Len(grdDescrObjSegurado.TextMatrix(6, 2)) _
                                                   + Len(Trim(grdDescrObjSegurado.TextMatrix(8, 2))) _
                                                   + Len(Trim(grdDescrObjSegurado.TextMatrix(10, 2))) _
                                                   + Len(grdDescrObjSegurado.TextMatrix(9, 2)) _
                                                   + 20) * 100
                End If
            End If
            Call limpaGrid

            lblReintegIS.Visible = False
            optReintegracaoIS(0).Visible = False
            optReintegracaoIS(1).Visible = False
            processa_reintegracao_is = ""



            Me.Show
        End If
    End If
End Sub
Private Sub limpaGrid()
    grdDescrObjSegurado.Clear
    While grdDescrObjSegurado.Rows > 2
        grdDescrObjSegurado.RemoveItem (1)
    Wend
End Sub


Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub
Private Sub cmdVoltar_Click()
    Me.Hide
    'If bSemProposta = True And (frmAvisoPropostas.grdResultadoPesquisa.Rows = 1 Or (frmAvisoPropostas.grdResultadoPesquisa.Rows = 2 And frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(1, 2) = "")) Then
    If bSemProposta = True Then
        If frmAvisoPropostas.grdResultadoPesquisa.Rows > 1 Then
            If frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(1, 2) = "" Then
                frmAgencia.Show
            Else
                '14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
                If frmAviso.grdEquipamentos.Rows > 1 Then 'C00210408 - Esteira Flash
                    frmEquipamentos.Show
                Else
                '14/01/2019 (ntedencia) 00210408-esteira_flash (fim)
                    frmAvisoPropostas.Show
                End If '14/01/2019 (ntedencia) 00210408-esteira_flash
            End If
        Else
            frmAgencia.Show
        End If
    Else
        
        '14/01/2019 (ntedencia) 00210408-esteira_flash (inicio)
        If frmAviso.grdEquipamentos.Rows > 1 Then 'C00210408 - Esteira Flash
            frmEquipamentos.Show
        Else
        '14/01/2019 (ntedencia) 00210408-esteira_flash (fim)
            frmAvisoPropostas.Show
        End If '14/01/2019 (ntedencia) 00210408-esteira_flash
    End If
    
    While Propostas.Count <> 0
        Propostas.Remove (1)
    Wend
    
    bSemProposta = False
End Sub


Private Sub Form_Activate()
    If frmAvisoPropostas.grdResultadoPesquisa.Rows > 1 And bSemProposta = False Then
        frmTextoObjeto.Visible = False
        fraDescricao.Visible = True
    End If
     Call CarregaPropostas
End Sub

Private Sub Form_Load()
    CentraFrm Me
'AKIO.OKUNO - INICIO - 17860335 - 26/06/2013
'    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2, "Produ��o", "Qualidade")
    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2 Or glAmbiente_id = 6, "Produ��o", "Qualidade")
'AKIO.OKUNO - FIM - 17860335 - 26/06/2013
    
    txtValor(0).Mask = "13V2"
    mskIS.Mask = "13V2"
    With grdPropostas
        .Cols = 8
      
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "Proposta"
        .TextMatrix(0, 2) = "Proposta BB"
        .TextMatrix(0, 3) = "Produto"
        .TextMatrix(0, 4) = "Ramo"
        .TextMatrix(0, 5) = "Ap�lice"
        .TextMatrix(0, 6) = "Nome Produto"
        .TextMatrix(0, 7) = "Sub Ramo"

        .ColWidth(0) = 200
        .ColWidth(1) = 900
        .ColWidth(2) = 1000
        .ColWidth(3) = 700
        .ColWidth(4) = 600
        .ColWidth(5) = 800
        .ColWidth(6) = 5070
        
        For i = 0 To 7
            .ColAlignment(i) = 4
            
        Next
        '.AutoFit H�brido
    End With
    
    With grdPropostasAviso
        .Cols = 15
              
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "Proposta"
        .TextMatrix(0, 2) = "Proposta BB"
        .TextMatrix(0, 3) = "Produto"
        .TextMatrix(0, 4) = "Nome do Produto"
        .TextMatrix(0, 5) = "Ap�lice"
        .TextMatrix(0, 6) = "Ramo"
        .TextMatrix(0, 7) = "Objeto Segurado"
        .TextMatrix(0, 8) = "Evento do Sinistro"
        .TextMatrix(0, 9) = "Valor do Prezu�zo"
        .TextMatrix(0, 10) = "Cobertura"
        .TextMatrix(0, 11) = "Codigo do objeto segurado"
        .TextMatrix(0, 12) = "C�digo da Cobertura"
        .TextMatrix(0, 13) = "Valor da IS"
        .TextMatrix(0, 14) = "Processa Reintegra��o de IS"
        '.AutoFit H�brido
        
        .ColWidth(0) = 200
        .ColWidth(1) = 900
        .ColWidth(2) = 1000
        .ColWidth(3) = 700
        .ColWidth(4) = 3000
        .ColWidth(5) = 700
        .ColWidth(6) = 700
        .ColWidth(7) = 5000
        .ColWidth(8) = 4000
        .ColWidth(9) = 2000
        .ColWidth(10) = 9000
        .ColWidth(11) = 2500
        .ColWidth(12) = 1600
        .ColWidth(13) = 1500
        .ColWidth(14) = 2200
        For i = 0 To 14
            .ColAlignment(i) = 4
            
        Next
    
    End With
    
    click_tela = False
End Sub
Sub CarregaPropostas()

Dim i As Integer
Dim listaPropostas As String
Dim listaPropostasExcluir As String
Dim excluiDoGrid As String
Dim lproposta() As String

lblAvisoObjSeg.Visible = False

'MONTA A LISTA DE PROPOSTAS SELECIONADAS NA TELA ANTERIOR COMO "AVSR"
For i = 1 To Propostas.Count
    If i = 1 Then
        listaPropostas = Propostas.Item(i).Proposta
    Else
        listaPropostas = listaPropostas & ", " + Propostas.Item(i).Proposta
    End If
Next i

'MONTA A LISTA DE PROPOSTAS JA ADICIONADAS AO SEGUNDO GRID E QUE N�O DEVER�O SER NOVAMENTE EXIBIDAS NO PRIMEIRO GRID
For i = 1 To grdPropostasAviso.Rows - 1
    If grdPropostasAviso.TextMatrix(CDbl(i), 1) <> "" Then
        If existeProposta(grdPropostasAviso.TextMatrix(CDbl(i), 1)) Then
            If listaPropostasExcluir = "" Then
                listaPropostasExcluir = grdPropostasAviso.TextMatrix(CDbl(i), 1)
            Else
                listaPropostasExcluir = listaPropostasExcluir & ", " + grdPropostasAviso.TextMatrix(CDbl(i), 1)
            End If
        Else
            If excluiDoGrid = "" Then
                excluiDoGrid = grdPropostasAviso.TextMatrix(CDbl(i), 1)
            Else
                excluiDoGrid = excluiDoGrid & ", " + grdPropostasAviso.TextMatrix(CDbl(i), 1)
            End If
        End If
    End If
Next i
'SELECIONA OS REGISTROS QUE DEVEM SER EXCLUIDOS DO GRID DE AVISO CASO SEJAM MARCADAS COMO "NAVS" NA TELA ANTERIOR
If excluiDoGrid <> "" Then
    lproposta = Split(excluiDoGrid, ",")
    'PARA CADA PROPOSTA DA LISTA, REMOVE O ITEM DO GRID RELACIONADO COM A MESMA
    For nIndex = LBound(lproposta) To UBound(lproposta)
        For i = 1 To grdPropostasAviso.Rows - 1
            If lproposta(nIndex) = grdPropostasAviso.TextMatrix(CDbl(i), 1) Then
                If grdPropostasAviso.Rows = 2 Then
                    grdPropostasAviso.AddItem ("")
                End If
                grdPropostasAviso.RemoveItem (i)
                Exit For
            End If
        Next i
    Next nIndex
End If


If Propostas.Count > 0 Then

    SQL = ""
    SQL = SQL & "select p.proposta_id, pa.proposta_bb,pd.produto_id, p.ramo_id,pa.apolice_id, pd.nome, p.subramo_id " & vbNewLine
    SQL = SQL & "  from proposta_tb p with (nolock) " & vbNewLine
    SQL = SQL & " inner join produto_tb pd with (nolock) " & vbNewLine
    SQL = SQL & "    on p.produto_id = pd.produto_id" & vbNewLine
    SQL = SQL & " inner join proposta_adesao_tb pa with (nolock) " & vbNewLine
    SQL = SQL & "    on pa.proposta_id = p.proposta_id" & vbNewLine
    SQL = SQL & " Where p.proposta_id in (" & listaPropostas & ")" & vbNewLine
    If listaPropostasExcluir <> "" Then
        SQL = SQL & " And p.proposta_id not in (" & listaPropostasExcluir & ")"
    End If
    SQL = SQL & "Union" & vbNewLine
    SQL = SQL & "select p.proposta_id, pa.proposta_bb,pd.produto_id, p.ramo_id,a.apolice_id, pd.nome, p.subramo_id " & vbNewLine
    SQL = SQL & "from proposta_tb p with (nolock)" & vbNewLine
    SQL = SQL & "inner join produto_tb pd with (nolock)" & vbNewLine
    SQL = SQL & "on p.produto_id = pd.produto_id" & vbNewLine
    SQL = SQL & "inner join proposta_fechada_tb pa with (nolock) " & vbNewLine
    SQL = SQL & "on pa.proposta_id = p.proposta_id" & vbNewLine
    SQL = SQL & "inner join apolice_tb a with (nolock)" & vbNewLine
    SQL = SQL & "on p.proposta_id = a.proposta_id" & vbNewLine
    SQL = SQL & "Where p.proposta_id in (" & listaPropostas & ")"
    If listaPropostasExcluir <> "" Then
        SQL = SQL & " And p.proposta_id not in (" & listaPropostasExcluir & ")"
    End If
'************************* Inclu�do/Alterado/Comentado por Jos� Edson - Demanda: 12682475 - 19/12/2011
    SQL = SQL & "Union" & vbNewLine
    SQL = SQL & "select p.proposta_id, isnull(pa.proposta_bb,0) as proposta_bb ,pd.produto_id, p.ramo_id,isnull(pa.apolice_id,0) as apolice_id, pd.nome, p.subramo_id from proposta_tb p" & vbNewLine
    SQL = SQL & "inner join produto_tb pd with (nolock)" & vbNewLine
    SQL = SQL & "on p.produto_id = pd.produto_id" & vbNewLine
    SQL = SQL & "left join proposta_adesao_tb pa with (nolock)" & vbNewLine
    SQL = SQL & "on pa.proposta_id = p.proposta_id" & vbNewLine
    'add produto 1240 -> 00421597-seguro-faturamento-pecuario
    SQL = SQL & "Where p.proposta_id in (" & listaPropostas & ") AND pd.produto_id IN (1204, 1152, 1240) and p.situacao IN ('E', 'A') " & vbNewLine
    If listaPropostasExcluir <> "" Then
        SQL = SQL & " And p.proposta_id not in (" & listaPropostasExcluir & ")"
    End If
'******************************************************************************************************

Else
    SQL = "select Aviso ='Aviso sem proposta'"
    fraDescricao.Visible = False
    frmTextoObjeto.Visible = True
        
    Call grdPropostas_Click
        
End If

        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
                             
        grdPropostas.AddItem ("")
        For i = 1 To grdPropostas.Rows - 1
            If grdPropostas.Rows > 2 Then
                grdPropostas.RemoveItem (1)
            End If
        Next

        Do While Not Rs.EOF
            If Rs(0) <> "Aviso sem proposta" Then
                grdPropostas.AddItem ("" & vbTab & Rs(0) & vbTab & Rs(1) & vbTab & Rs(2) & vbTab & Rs(3) & vbTab & Rs(4) & vbTab & Rs(5) & vbTab & Rs(6))
            Else
                grdPropostas.AddItem ("" & vbTab & "0" & vbTab & "0" & vbTab & "0" & vbTab & "0" & vbTab & "0" & vbTab & "Aviso Sem proposta" & vbTab & "0")
            End If
            Rs.MoveNext
        Loop
        
        If grdPropostas.Rows > 2 Then
            If grdPropostas.TextMatrix(1, 2) = "" Then
                grdPropostas.RemoveItem (1)
            End If
        End If
        Rs.Close
        
End Sub

Private Sub grdObjSegurado_Click()

 'rafael bispo inicio
    'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
    'ADICIONADO PRODUTOS 230
    If ProdutoProposta <> 156 Or ProdutoProposta <> 226 _
    Or ProdutoProposta <> 227 Or ProdutoProposta <> 228 _
    Or ProdutoProposta <> 229 Or ProdutoProposta <> 300 _
    Or ProdutoProposta <> 230 _
    Or ProdutoProposta <> 701 Then
    
    SQL = ""
    SQL = SQL & " exec SEGS7786_SPS " & grdPropostas.TextMatrix(grdPropostas.RowSel, 1) & ", 0"
    
    Else
  
    SQL = ""
    SQL = SQL & " exec SEGS7786_SPS " & grdPropostas.TextMatrix(grdPropostas.RowSel, 0) & ", 2"
    
    End If
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
    
    grdDescrObjSegurado.PopulaGrid (Rs)
    Rs.Close
End Sub
Function MontarQuestionarioProduto() As Boolean

Dim oTemporariaAviso As Object
Dim oQuestionario As Object
Dim sResposta As String
Dim sSQL As String
Dim sLerQuestionario As String

'Dim oSABL0100 As Object

'asouza 19.12.05
If oSABL0100 Is Nothing Then
    glConexao = -1
    Set oSABL0100 = CreateObject("SABL0100.conexao")
End If
  
If glConexao < 0 Then
  glConexao = oSABL0100.AbrirConexao("SEGBR", ConvAmbiente(glAmbiente_id), _
                                    App.Title, 0, App.FileDescription)
End If

Set oTemporariaAviso = CreateObject("SEGL0144.cls00435")

    Set oQuestionario = CreateObject("SEGL0197.cls00436")


oTemporariaAviso.mvarSiglaSistema = "SEGBR"
oTemporariaAviso.mvarAmbienteId = glAmbiente_id
oTemporariaAviso.mvarSiglaRecurso = App.Title
oTemporariaAviso.mvarDescricaoRecurso = App.FileDescription
oTemporariaAviso.mvarAmbiente = ConvAmbiente(glAmbiente_id)


'Cria a Tempor�ria para ser usada no c�lculo de campos de question�rio
If bTemporariaAviso = False Then
    Call oTemporariaAviso.CriarTemporariaAvisoSinistro(glConexao, oSABL0100, TEMPORARIAAVISO)
    bTemporariaAviso = True
End If

'psouza - 24/11/2006

Call oTemporariaAviso.IncluirRegistroTemporariaAviso(Format(frmEvento.mskDataOcorrencia, "yyyymmdd"), _
                                                     Format(Date, "yyyymmdd"), _
                                                     IIf(frmEvento.mskDataOcorrenciaFim.Visible, EVENTO_SINISTRO_PERIODICO, EVENTO_SINISTRO_PONTUAL), _
                                                     cUserName, _
                                                     grdPropostas.TextMatrix(grdPropostas.RowSel, 1), _
                                                     0, _
                                                     TEMPORARIAAVISO, _
                                                     oSABL0100, _
                                                     glConexao)
                                                     

'Criando tempor�ria para respostas do question�rio
If bTemporariaQuestionario = False Then
    Call oTemporariaAviso.CriarTemporariaQuestionario(glConexao, oSABL0100, TEMPORARIAQUESTIONARIO)
    bTemporariaQuestionario = True
    sLerQuestionario = "N"
Else
    sLerQuestionario = "S"
End If
SQL = ""
'sql = sql & "SET NOCOUNT OFF Truncate Table #questionario_aviso_sinistro_tb " & vbNewLine
SQL = SQL & "delete from #questionario_aviso_sinistro_tb " & vbNewLine

Call oSABL0100.ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, glConexao, "0", False)



    sResposta = oQuestionario.PegarRespostas("SEGBR", _
                                            ConvAmbiente(glAmbiente_id), _
                                            App.Title, _
                                            App.FileDescription, _
                                            glConexao, _
                                            oSABL0100, _
                                            CLng(produtoQuestionario), _
                                            TEMPORARIAQUESTIONARIO, _
                                            TEMPORARIAAVISO, _
                                            "Question�rio de Aviso de Sinistro", _
                                            sLerQuestionario, grdPropostas.TextMatrix(grdPropostas.RowSel, 2))
    
    Set oQuestionario = Nothing
    
    
    If sResposta = "S" Then
        SairAplicacao
    Else
       MontarQuestionarioProduto = sResposta = "C"
    End If
    
    SQL = ""
    SQL = SQL & "obtem_segbr_sinistro_questionario_sps " & vbNewLine
    
 Set Rs = oSABL0100.ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, _
                                glConexao, _
                                "0", _
                                True)
                                
       While Not Rs.EOF
        
            Set tQuestionario = New cQuestionario
            With tQuestionario
                .Proposta = grdPropostas.TextMatrix(grdPropostas.RowSel, 2)
                .texto = Rs("pergunta") & " " & Rs("resposta")
            End With
        
        Questionario.Add tQuestionario
        Rs.MoveNext
       Wend
       Rs.Close


End Function

Private Sub grdDescrObjSegurado_Click()

    Call CarregaCoberturas(grdPropostas.TextMatrix(grdPropostas.RowSel, 1), Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd"))

End Sub

Private Sub grdPropostas_Click()

Evento_Cobertura_Basica_Faturamento_Pecuario = True '' LPS

 Dim Proposta As Long
 Dim controleBenef As Boolean
 Dim ProdutoProposta As Integer

    'DEMANDA: 7979370 - FELIPPE MENDES - 20111020
    'RN01.07 - N�O PERMITIR ABERTURA DE MAIS DE UM AVISO DE SINISTRO DE EVENTO DE PRE�O PARA UMA PROPOSTA.

    If frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex) = 430 Then
        If mVerificaEventoPreco(grdPropostas.TextMatrix(grdPropostas.RowSel, 1), frmEvento.cboEvento.ListIndex) Then
            MsgBox "J� existe um aviso de sinistro de evento de pre�o para esta proposta!", vbCritical, "Aviso de Sinistro"
            Exit Sub
        End If
    End If

 If grdPropostas.TextMatrix(grdPropostas.RowSel, 1) <> "" Then
 
   If grdPropostas.TextMatrix(grdPropostas.RowSel, 1) = "0" Then
        
    lblReintegIS.Visible = False
    optReintegracaoIS(0).Visible = False
    optReintegracaoIS(1).Visible = False
    processa_reintegracao_is = ""
    Proposta = "0"
    
    Call CarregaCoberturas(0, Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd"))
    cboCobAtingida.Enabled = True
    cmdAdicionar.Enabled = True
   Else
 
    If VerificaReintegracaoIS(grdPropostas.TextMatrix(grdPropostas.RowSel, 3), grdPropostas.TextMatrix(grdPropostas.RowSel, 4)) Then
        lblReintegIS.Visible = True
        optReintegracaoIS(0).Visible = True
        optReintegracaoIS(1).Visible = True
        
        If processa_reintegracao_is = "" Then
            processa_reintegracao_is = "S"
        End If
    End If
    
     'rafael bispo inicio
     'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
     'ADICIONADO PRODUTOS 230
    If ProdutoProposta <> 156 Or ProdutoProposta <> 226 _
        Or ProdutoProposta <> 227 Or ProdutoProposta <> 228 _
        Or ProdutoProposta <> 229 Or ProdutoProposta <> 300 _
        Or ProdutoProposta <> 230 _
        Or ProdutoProposta <> 701 Then
    
     
        SQL = ""
        SQL = SQL & " exec SEGS7786_SPS " & grdPropostas.TextMatrix(grdPropostas.RowSel, 1) & ", 0"
            
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQL, True)
                                
    Else
                                
        SQL = ""
        SQL = SQL & " exec SEGS7786_SPS " & grdPropostas.TextMatrix(grdPropostas.RowSel, 1) & ", 1"
            
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQL, True)
                                
    End If
    
    grdDescrObjSegurado.PopulaGrid (Rs)
    Rs.Close
    
    
     ProdutoProposta = grdPropostas.TextMatrix(grdPropostas.RowSel, 3)
     lblAvisoObjSeg.Visible = False
    
    'rafael bispo inicio
    'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
    'ADICIONADO PRODUTOS 230
    If ProdutoProposta = 156 Or ProdutoProposta = 226 _
    Or ProdutoProposta = 227 Or ProdutoProposta = 228 _
    Or ProdutoProposta = 229 Or ProdutoProposta = 300 _
    Or ProdutoProposta = 230 _
    Or ProdutoProposta = 701 Then
        lblAvisoObjSeg.Visible = True
        cboCobAtingida.Clear
        mskIS.Text = ""
        txtFranquia.Text = ""
        txtValor(0).Text = ""
    
    End If
     
    
    'rafael bispo fim

    
    
    Set Aviso = CreateObject("SEGL0144.cls00385")
        Aviso.mvarAmbienteId = glAmbiente_id
        Aviso.mvarSiglaSistema = gsSIGLASISTEMA
        Aviso.mvarSiglaRecurso = App.Title
        Aviso.mvarDescricaoRecurso = App.FileDescription
    
    indice = getIndiceProposta(grdPropostas.TextMatrix(grdPropostas.RowSel, 1))
    
    'DEMANDA: 7979370 - THIAGO ERUSTES/ANDREA B PONTES - 20111104
    'IMPLEMENTANDO VERIFICA��O PARA O PRODUTO 1204
    'add produto 1240 -> 00421597-seguro-faturamento-pecuario
    If grdPropostas.TextMatrix(grdPropostas.RowSel, 3) = "1152" Or grdPropostas.TextMatrix(grdPropostas.RowSel, 3) = "1204" Or grdPropostas.TextMatrix(grdPropostas.RowSel, 3) = "1240" Then
        sProdutoAgricola = Propostas(indice).TipoAviso
    End If
    'FIM - DEMANDA: 7979370 - THIAGO ERUSTES/ANDREA B PONTES - 20111104
    
    'Novo produto pecu�rio --in�cio
    reanalise = True
    If Propostas(indice).TipoAviso <> "REANALISE" Then
        reanalise = False
        If ProdutoProposta <> 1240 Then '1240
    
        produtoQuestionario = Aviso.ObterQuestionarioSinistro(grdPropostas.TextMatrix(grdPropostas.RowSel, 3))
'o if <> 1240 j� faz o que esse trecho faria
'' Laurent Silva - 18/07/2018 - Demanda de faturamento pecu�rio
'' N�o chamar a tela de questionario caso os eventos sejam da cobertura b�sica - faturamento.
'If Evento_Cobertura_Basica_Faturamento_Pecuario = True Then
'    produtoQuestionario = 0
'End If
       
        If produtoQuestionario > 0 Then
    
            If Not MontarQuestionarioProduto() Then
                Set Aviso = Nothing
                Me.MousePointer = vbDefault
                Me.Show
                Exit Sub
            End If
        End If
        End If
       'Novo produto pecu�rio --fim
        
         
    'rafael bispo inicio
    'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
    'ADICIONADO PRODUTOS 230
    If ProdutoProposta <> 156 And ProdutoProposta <> 226 _
    And ProdutoProposta <> 227 And ProdutoProposta <> 228 _
    And ProdutoProposta <> 229 And ProdutoProposta <> 300 _
    And ProdutoProposta <> 230 _
    And ProdutoProposta <> 701 Then
        
    Call CarregaCoberturas(grdPropostas.TextMatrix(grdPropostas.RowSel, 1), Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd"))
    
    End If
    'rafael bispo fim
    
    'Proposta = grdPropostas.TextMatrix(grdPropostas.RowSel, 2)
    cboCobAtingida.Enabled = True
    cmdAdicionar.Enabled = True
    Me.Show
    Else 'Reanalise
    
    Call CarregaCoberturas(grdPropostas.TextMatrix(grdPropostas.RowSel, 1), Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd"))
    
        SQL = " set nocount on exec consultar_aviso_sinistro_re_sps " & sSinistro_id
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL)
                        
    cboCobAtingida.Text = Rs("nome_cobertura")
    cboCobAtingida.Enabled = False
    txtValor(0).Text = Rs("val_estimativa")
    txtValor(0).Enabled = True
    tp_cobertura_id = Rs("tp_cobertura_id")
    Rs.Close
    
    If txtValor(0).Text = 0 Then
        SQL = ""
        SQL = SQL & "exec SEGS8039_SPS " & sSinistro_id
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                glAmbiente_id, _
                App.Title, _
                App.FileDescription, _
                SQL)
                
        txtValor(0).Text = Format(Rs("val_estimado"), "0.00")
        
        Rs.Close
    End If
    
    SQL = ""
    SQL = SQL & "exec SEGS7752_SPS '" & Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd") & "'"
    If grdPropostas.TextMatrix(grdPropostas.RowSel, 1) = "0" Then
       SQL = SQL & ", 0"
    Else
        'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
        'ADICIONADO PRODUTOS 230
         If ProdutoProposta <> 156 And ProdutoProposta <> 226 _
        And ProdutoProposta <> 227 And ProdutoProposta <> 228 _
        And ProdutoProposta <> 229 And ProdutoProposta <> 300 _
        And ProdutoProposta <> 230 _
        And ProdutoProposta <> 701 Then
            SQL = SQL & ", " & grdPropostas.TextMatrix(grdPropostas.RowSel, 1) & ", 0, 0 "
        Else
            SQL = SQL & ", " & grdPropostas.TextMatrix(grdPropostas.RowSel, 1) & ", 1, " & grdDescrObjSegurado.TextMatrix(grdDescrObjSegurado.RowSel, 1)
        End If
        
    End If
    
    
     Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          True)
    While Not Rs.EOF
     If Rs("tp_cobertura_id") = tp_cobertura_id Then
             mskIS.Text = Format(Rs("val_is"), "0.00")
     End If
     Rs.MoveNext
    Wend
    Rs.Close
    
    
    txtFranquia.Text = ObtemTexto_franquia(CInt(tp_cobertura_id))

    Proposta = grdPropostas.TextMatrix(grdPropostas.RowSel, 1)
    
    Me.Hide
    frmMotivoReanalise.Show
    
    'INICIO - MU00416975 - Reabertura de Sinistro
    
        frmMotivoReanalise.cboMotivoReanalise.Text = ""
        
    'FIM - MU00416975 - Reabertura de Sinistro

    End If
 
  End If
 End If
End Sub
Public Sub CarregaCoberturas(proposta_id As String, dtOcorrencia As String, Optional Cod_Objeto_Segurado As Integer)
Dim SQL As String
Dim rsPesquisa As ADODB.Recordset
Dim iCounter As Integer
Dim ProdutoProposta As Integer 'rafael bispo - Nova

Dim GridResposta As Long


 'rafael bispo inicio
 'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
 'ADICIONADO PRODUTOS 230
 ProdutoProposta = grdPropostas.TextMatrix(grdPropostas.RowSel, 3)
 
    If ProdutoProposta <> 156 And ProdutoProposta <> 226 _
    And ProdutoProposta <> 227 And ProdutoProposta <> 228 _
    And ProdutoProposta <> 229 And ProdutoProposta <> 300 _
    And ProdutoProposta <> 230 _
    And ProdutoProposta <> 701 Then

    SQL = ""
    SQL = SQL & " exec SEGS7752_SPS '" & dtOcorrencia & "',"
    SQL = SQL & proposta_id & ","
    SQL = SQL & "0, "
    SQL = SQL & "0"

    Set rsPesquisa = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
                                
   Else
   
   If IsNumeric(grdDescrObjSegurado.TextMatrix(grdDescrObjSegurado.RowSel, 1)) Then
    
        SQL = ""
        SQL = SQL & " exec SEGS7752_SPS '" & dtOcorrencia & "',"
        SQL = SQL & proposta_id & ","
        SQL = SQL & "1, "
        SQL = SQL & grdDescrObjSegurado.TextMatrix(grdDescrObjSegurado.RowSel, 1)
            
        Set rsPesquisa = ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQL, True)
   Else
        Exit Sub
   End If
                                
  End If

    With cboCobAtingida
        .Clear
        'GridSetup True
        ReDim texto_franquia(0) As Franquia
        iCounter = 0
        
        'S� preenche o combo das coberturas se o endere�o de risco for da p�lice
        'Se o endere�o de risco n�o for da ap�lice, apenas abrir a cobertura gen�rica para avisar.
        
            'Caso o cod_objeto_segurado seja <> 0, s� insere as coberturas atingidas
            ' poss�veis do objeto segurado escolhido
            
            'DEMANDA: 7979370 - FELIPPE MENDES - 20100923
            'VERIFICA��O ZOAGRO PARA O PRODUTO 1204
            
            If grdPropostas.TextMatrix(grdPropostas.RowSel, 3) = "1204" Then
                If PeriodoZoagro(grdPropostas.TextMatrix(grdPropostas.RowSel, 1), Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd")) Then
                    .AddItem "Cobertura Adicional de Replantio"
                    .ItemData(.NewIndex) = 834
                Else
                    .AddItem "Cobertura B�sica de Faturamento"
                    .ItemData(.NewIndex) = 835
                End If
            Else
                If Not rsPesquisa.EOF Then
                        While Not rsPesquisa.EOF
                            If ExisteEstimativa(grdPropostas.TextMatrix(grdPropostas.RowSel, 3), _
                                                frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex), _
                                                rsPesquisa("tp_cobertura_id"), _
                                                grdPropostas.TextMatrix(grdPropostas.RowSel, 4), _
                                                grdPropostas.TextMatrix(grdPropostas.RowSel, 7)) Then
                                    .AddItem rsPesquisa("nome")
                                    .ItemData(.NewIndex) = rsPesquisa("tp_cobertura_id")
                                    
                                    'Contador de franquia
                                    iCounter = iCounter + 1
                                    'Grava no array  o texto das franquias das coberturas encontradas para a apolice
                                    ReDim Preserve texto_franquia(0 To iCounter) As Franquia
                                    texto_franquia(UBound(texto_franquia)).tp_cobertura_id = rsPesquisa("tp_cobertura_id")
                                    texto_franquia(UBound(texto_franquia)).texto_franquia = IIf(IsNull(rsPesquisa("texto_franquia")), "", rsPesquisa("texto_franquia"))
    
                            End If
                            rsPesquisa.MoveNext
                        Wend
    
                End If
            End If
        'Caso n�o tenha cobertura ou o endere�o de risco n�o for da ap�lice,
        'insere a gen�rica
        
        'DEMANDA: 7979370 - FELIPPE MENDES - 20110923
        'VERIFICA��O PARA O PRODUTO 1204 DE ZOAGRO
        'add produto 1240 -> 00421597-seguro-faturamento-pecuario
        If grdPropostas.TextMatrix(grdPropostas.RowSel, 1) <> "1204" And grdPropostas.TextMatrix(grdPropostas.RowSel, 1) <> "1240" Then
            If cboCobAtingida.ListCount = 0 Then
                 .AddItem "COBERTURA GEN�RICA"
                 .ItemData(.NewIndex) = "1"
                 rsPesquisa.Close
                 Exit Sub
            End If
        End If
        
        txtValor(0).Enabled = True
        .ListIndex = -1
        txtFranquia = ""
        mskIS.Text = "0,00"
        txtValor(0).Text = "0,00"
    End With
    
    rsPesquisa.Close
    
End Sub

Private Function ExisteEstimativa(produto_id As String, _
                                  evento_sinistro_id As Integer, _
                                  tp_cobertura_id As Integer, _
                                  ramo_id As Integer, _
                                  subramo_id As Long) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset
    
    ExisteEstimativa = False
    
    SQL = ""
    SQL = SQL & " select 1 from produto_estimativa_sinistro_tb with (nolock) "
    SQL = SQL & " where produto_id = " & produto_id
    SQL = SQL & " and ramo_id = " & ramo_id
    SQL = SQL & " and subramo_id = " & subramo_id
    SQL = SQL & " and evento_sinistro_id = " & evento_sinistro_id
    SQL = SQL & " and tp_cobertura_id = " & tp_cobertura_id
    SQL = SQL & " and situacao = 'A'"
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
    If Not Rs.EOF Then
        ExisteEstimativa = True
    End If
    Rs.Close
End Function
Private Function PeriodoZoagro(proposta_id As String, _
                                  dataaviso As String) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset
    
    PeriodoZoagro = False
    
    SQL = ""
    SQL = SQL & "EXEC SEGS9772_SPS "
    SQL = SQL & proposta_id
    SQL = SQL & ",'" & dataaviso & "'"
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
    If Not Rs.EOF Then
        PeriodoZoagro = True
    End If
    Rs.Close
End Function


Private Function ObtemTexto_franquia(tp_cobertura_id As Long) As String
Dim i As Integer

ObtemTexto_franquia = ""
For i = 1 To UBound(texto_franquia)
    If texto_franquia(i).tp_cobertura_id = tp_cobertura_id Then
        ObtemTexto_franquia = texto_franquia(i).texto_franquia
        Exit Function
    End If
Next i

End Function
Private Sub optReintegracaoIS_Click(Index As Integer)
    If Index = 0 Then
        processa_reintegracao_is = "S"
    Else
        processa_reintegracao_is = "N"
    End If
End Sub

'DEMANDA: 7979370 - FELIPPE MENDES - 20111020
'RN01.07 - N�O PERMITINDO ABERTURA DE MAIS DE UM AVISO DE SINISTRO DE EVENTO DE PRE�O PARA A PROPOSTA.

Private Function mVerificaEventoPreco(proposta_id As String, Evento_id As String) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset
    
    mVerificaEventoPreco = False
    
    SQL = ""
    SQL = SQL & "EXEC SEGS9806_SPS "
    SQL = SQL & proposta_id & ","
    SQL = SQL & Evento_id
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
    If Rs(0) > 0 Then
        mVerificaEventoPreco = True
    End If
    Rs.Close
    
End Function

Private Sub cboCobAtingida_LostFocus()
 'Cl�udio Gomes   -   Confitec   -   17/02/2012 ***************************************************************************************
    '12921844   -   Habilita combo para sele��o do mutu�rio
    If cboCobAtingida.Text = "COBERTURA ESPECIFICA PARA RISCOS DE MORTE OU INVALIDEZ           PERMANENTE (MIP)" _
       And (grdPropostas.TextMatrix(grdPropostas.RowSel, 4) = "61" Or _
       grdPropostas.TextMatrix(grdPropostas.RowSel, 4) = "68") Then
       
       cboMutuarios.Visible = True
       Label6.Visible = True
       SQL = "EXEC SEGS10092_SPS '" & grdPropostas.TextMatrix(grdPropostas.RowSel, 1) & "'"
       Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
                             
        cboMutuarios.Clear
        cboMutuarios.AddItem ("100,00%  -  Todos")
       If Not Rs.EOF Then
            While Not Rs.EOF
                '------------------------------------------------------------------------------------------------------------------------------------
                ' Ricardo Toledo : 20/05/2012 : INC000003513402 : INICIO
                'formata��o do valor no campo mutu�rios
               
                'cboMutuarios.AddItem (Rs.Fields("percentual_particip_renda") & "%  -  " & Rs.Fields("Nome"))
                cboMutuarios.AddItem Format(Rs.Fields("percentual_particip_renda"), "#,##0.00") & "%  -  " & Rs.Fields("Nome")
                ' Ricardo Toledo : 20/05/2012 : INC000003513402 : FIM
                '------------------------------------------------------------------------------------------------------------------------------------
                                
                Rs.MoveNext
            Wend
       End If
                             
    Else
       cboMutuarios.Visible = False
       Label6.Visible = False
    End If
End Sub

Private Sub cboMutuarios_LostFocus()
'Cl�udio Gomes   -   Confitec   -   17/02/2012 ***************************************************************************************
'12921844   -   C�lculo do valor da IS

    '------------------------------------------------------------------------------------------------------------------------------------
    ' Ricardo Toledo (Confitec) : 25/05/2012 : INC000003530368 : INICIO
    'formata��o do valor no campo mutu�rios
               
    'mskIS.Text = ValorIS * CDbl(Mid(cboMutuarios.List(cboMutuarios.ListIndex), 1, 5))
    'ERICK OLIVEIRA (Confitec) : 17/01/2018 : IM00194962 : INICIO - TRATAMENTO DO CAMPO
    If cboMutuarios.ListIndex <> -1 Then
    mskIS.Text = Format(ValorIS * (CDbl(Mid(cboMutuarios.List(cboMutuarios.ListIndex), 1, 5)) / 100), "#,##0.00")
    End If
    'ERICK OLIVEIRA (Confitec) : 17/01/2018 : IM00194962 : FIM
    ' Ricardo Toledo (Confitec) : 25/05/2012 : INC000003530368 : FIM
    '------------------------------------------------------------------------------------------------------------------------------------

End Sub


Private Sub Txt_Qtd_Femeas_Change(Index As Integer)
    Me.Txt_Qtd_Total(0).Text = Left$((Val(Me.Txt_Qtd_Machos(0).Text) + Val(Me.Txt_Qtd_Femeas(0).Text)) & "_____", 5)
End Sub


Private Sub Txt_Qtd_Femeas_LostFocus(Index As Integer)
    txtValor(1).SetFocus
End Sub

Private Sub Txt_Qtd_Machos_Change(Index As Integer)
    Me.Txt_Qtd_Total(0).Text = Left$((Val(Me.Txt_Qtd_Machos(0).Text) + Val(Me.Txt_Qtd_Femeas(0).Text)) & "_____", 5)
End Sub

Private Sub Txt_Qtd_Machos_LostFocus(Index As Integer)
    Txt_Qtd_Femeas(0).SetFocus
End Sub

Private Sub txtValor_GotFocus(Index As Integer)

    'wilder
    If Index = 1 Then ' valor prejuizo cobertura b�sica produto 1240
        
        Dim Rs As ADODB.Recordset
        Dim SQL As String
        
        SQL = "EXEC SEGUROS_DB.DBO.SEGS13918_SPS " & Replace(Txt_Qtd_Total(0).Text, "_", "") & "," & grdPropostas.TextMatrix(grdPropostas.RowSel, 1) & "," & cboCobAtingida.ItemData(cboCobAtingida.ListIndex) & ",'" & Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd") & "'"
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
            
        If Not Rs.EOF Then
            txtValor(1).Text = Format(Rs(0), "#,##0.00")
            'cboMutuarios.AddItem Format(Rs.Fields("percentual_particip_renda"), "#,##0.00") & "%  -  " & Rs.Fields("Nome")
        End If
      
    End If
       
End Sub

Private Sub txtValor_LostFocus(Index As Integer)

 If Index = 0 Then 'Ajuste de Estimativa com Autoriza��o do Gestor - Rafael Martins
    
    If CDbl(txtValor(0).Text) > ValorIS Then  'Quando o Valor do Preju�zo for maior que o Valor da IS, o sistema dever� definir como valor de preju�zo o valor da IS da cobertura afetada
        MsgBox "O valor informado como preju�zo � maior que a IS da cobertura sinistrada"
        txtValor(0).Text = Format(ValorIS, "0.00")
    End If
    
End If


End Sub
