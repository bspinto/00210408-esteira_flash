VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cQuestionarioObjeto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private proposta_id     As String
Private texto_pergunta  As String
Private texto_resposta  As String
Public Property Let Proposta(ByVal vData As String)
    proposta_id = vData
End Property
Public Property Get Proposta() As String
    Proposta = proposta_id
End Property
Public Property Let Pergunta(ByVal vData As String)
    texto_pergunta = vData
End Property
Public Property Get Pergunta() As String
    Pergunta = texto_pergunta
End Property
Public Property Let Resposta(ByVal vData As String)
    texto_resposta = vData
End Property
Public Property Get Resposta() As String
    Resposta = texto_resposta
End Property
