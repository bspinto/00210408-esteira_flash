USE desenv_db
GO

CREATE PROC dbo.atualiza_vigencia_bsp  ( 
  @proposta INT
  ,@dt_ini SMALLDATETIME
  ,@dt_fim SMALLDATETIME
)
AS
BEGIN
  DECLARE @MSG VARCHAR(200) = 'Proposta: '
  DECLARE @SEP VARCHAR(3) = ' \ '
  
  IF EXISTS(SELECT 1 FROM seguros_db.dbo.proposta_fechada_tb a WHERE a.proposta_id = @proposta)
  BEGIN
    UPDATE a
    SET a.dt_inicio_vig = @dt_ini
      ,a.dt_fim_vig = @dt_fim
    FROM seguros_db.dbo.proposta_fechada_tb a
    WHERE a.proposta_id = @proposta
    
    SET @MSG = @msg + 'FECHADA atualizada'
    
    IF EXISTS(SELECT 1 FROM seguros_db.dbo.apolice_tb a WHERE a.proposta_id = @proposta)
    BEGIN
      UPDATE b
      SET b.dt_inicio_vigencia = @dt_ini
        ,b.dt_fim_vigencia = @dt_fim
      FROM seguros_db.dbo.apolice_tb b
      WHERE b.proposta_id = @proposta
      
      SET @MSG = @MSG + @SEP + 'APOLICE atualizada'
    END        
  END
  ELSE
  BEGIN
      IF EXISTS(SELECT 1 FROM seguros_db.dbo.proposta_adesao_tb b WHERE b.proposta_id = @proposta)
      BEGIN
        UPDATE b
        SET b.dt_inicio_vig = @dt_ini
          ,b.dt_fim_vig = @dt_fim
        FROM seguros_db.dbo.proposta_fechada_tb b
        WHERE b.proposta_id = @proposta
        
        SET @MSG = @msg + 'ADESAO atualizada'
        
        IF EXISTS(SELECT 1 FROM seguros_db.dbo.apolice_tb a WHERE a.proposta_id = @proposta)
        BEGIN
          UPDATE b
          SET b.dt_inicio_vigencia = @dt_ini
            ,b.dt_fim_vigencia = @dt_fim
          FROM seguros_db.dbo.apolice_tb b
          WHERE b.proposta_id = @proposta
          
          SET @MSG = @MSG + @SEP + 'APOLICE atualizada'
        END        
      END
      ELSE
      BEGIN
        SET @MSG = 'proposta N�O ATALIZADA'      
      END
  END    
  
  SELECT @MSG
END


if @@TRANCOUNT > 0 ROLLBACK
if @@TRANCOUNT = 0 BEGIN TRAN
SELECT @@TRANCOUNT
if @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_vigencia_bsp @proposta = 6162578, @dt_ini = '20180601', @dt_fim = '20200601'
-- COMMIT


exec seguros_db.dbo.SEGS7758_SPS 12889829, 3, '20200130'



