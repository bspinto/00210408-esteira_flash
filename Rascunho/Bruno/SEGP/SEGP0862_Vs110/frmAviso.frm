VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX" '14/01/2019 (ntedencia) 00210408-esteira_flash
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GridAlianca.ocx"
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GridDinamico.ocx"
Begin VB.Form frmAviso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   8445
   ClientLeft      =   5190
   ClientTop       =   855
   ClientWidth     =   11835
   Icon            =   "frmAviso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8445
   ScaleWidth      =   11835
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   7440
      TabIndex        =   0
      Top             =   7980
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   10230
      TabIndex        =   2
      Top             =   7980
      Width           =   1275
   End
   Begin VB.CommandButton cmdAvisar 
      Caption         =   "Avisar"
      Height          =   420
      Left            =   8840
      TabIndex        =   1
      Top             =   7980
      Width           =   1275
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7875
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11925
      _ExtentX        =   21034
      _ExtentY        =   13891
      _Version        =   393216
      Tabs            =   8
      Tab             =   6
      TabHeight       =   617
      TabCaption(0)   =   "Ag�ncia"
      TabPicture(0)   =   "frmAviso.frx":0442
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraAgencia"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Ocorr�ncia"
      TabPicture(1)   =   "frmAviso.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "frmObs"
      Tab(1).Control(1)=   "Frame5"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Solicitante"
      TabPicture(2)   =   "frmAviso.frx":047A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "txtTelefone(0)"
      Tab(2).Control(1)=   "txtRamal(4)"
      Tab(2).Control(2)=   "txtRamal(3)"
      Tab(2).Control(3)=   "txtRamal(2)"
      Tab(2).Control(4)=   "txtRamal(1)"
      Tab(2).Control(5)=   "txtRamal(0)"
      Tab(2).Control(6)=   "txtCEPSolicitante"
      Tab(2).Control(7)=   "txtUFSolicitante"
      Tab(2).Control(8)=   "txtMunicipioSolicitante"
      Tab(2).Control(9)=   "txtBairroSolicitante"
      Tab(2).Control(10)=   "txtEnderecoSolicitante"
      Tab(2).Control(11)=   "txtEmailSolicitante"
      Tab(2).Control(12)=   "txtNomeSolicitante"
      Tab(2).Control(13)=   "txtTipoTelefone(0)"
      Tab(2).Control(14)=   "txtTipoTelefone(1)"
      Tab(2).Control(15)=   "txtTipoTelefone(2)"
      Tab(2).Control(16)=   "txtTipoTelefone(3)"
      Tab(2).Control(17)=   "txtTipoTelefone(4)"
      Tab(2).Control(18)=   "txtDDD(0)"
      Tab(2).Control(19)=   "txtDDD(1)"
      Tab(2).Control(20)=   "txtDDD(2)"
      Tab(2).Control(21)=   "txtDDD(4)"
      Tab(2).Control(22)=   "txtTelefone(1)"
      Tab(2).Control(23)=   "txtTelefone(2)"
      Tab(2).Control(24)=   "txtTelefone(3)"
      Tab(2).Control(25)=   "txtTelefone(4)"
      Tab(2).Control(26)=   "txtDDD(3)"
      Tab(2).Control(27)=   "Label15"
      Tab(2).Control(28)=   "Label14(0)"
      Tab(2).Control(29)=   "Label12(0)"
      Tab(2).Control(30)=   "Label11(0)"
      Tab(2).Control(31)=   "Label9"
      Tab(2).Control(32)=   "Label8"
      Tab(2).Control(33)=   "Label6"
      Tab(2).Control(34)=   "Label5"
      Tab(2).Control(35)=   "Label3(2)"
      Tab(2).Control(36)=   "Label2(2)"
      Tab(2).Control(37)=   "Label4"
      Tab(2).ControlCount=   38
      TabCaption(3)   =   "Local de Risco/Vistoria"
      TabPicture(3)   =   "frmAviso.frx":0496
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame3"
      Tab(3).Control(1)=   "Frame2"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Avisos"
      TabPicture(4)   =   "frmAviso.frx":04B2
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame6"
      Tab(4).Control(1)=   "Frame7"
      Tab(4).ControlCount=   2
      TabCaption(5)   =   "Propostas Afetadas"
      TabPicture(5)   =   "frmAviso.frx":04CE
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Frame4"
      Tab(5).Control(1)=   "GridPropostaAfetada"
      Tab(5).ControlCount=   2
      TabCaption(6)   =   "Equipamentos (Pagto Imediato)"
      TabPicture(6)   =   "frmAviso.frx":04EA
      Tab(6).ControlEnabled=   -1  'True
      Tab(6).Control(0)=   "lblPagtoImediato"
      Tab(6).Control(0).Enabled=   0   'False
      Tab(6).Control(1)=   "lblTotEstimativaPrejuizo"
      Tab(6).Control(1).Enabled=   0   'False
      Tab(6).Control(2)=   "grdEquipamentos"
      Tab(6).Control(2).Enabled=   0   'False
      Tab(6).Control(3)=   "cboPgtoImediato"
      Tab(6).Control(3).Enabled=   0   'False
      Tab(6).Control(4)=   "txtEquipamentoValor"
      Tab(6).Control(4).Enabled=   0   'False
      Tab(6).Control(5)=   "txtEquipamentoDetalhamento"
      Tab(6).Control(5).Enabled=   0   'False
      Tab(6).ControlCount=   6
      TabCaption(7)   =   "Beneficiario (Pagto Imediato)"
      TabPicture(7)   =   "frmAviso.frx":0506
      Tab(7).ControlEnabled=   0   'False
      Tab(7).ControlCount=   0
      Begin VB.TextBox txtEquipamentoDetalhamento 
         Height          =   2055
         Left            =   1800
         MultiLine       =   -1  'True
         TabIndex        =   129
         Top             =   3240
         Visible         =   0   'False
         Width           =   8055
      End
      Begin VB.TextBox txtEquipamentoValor 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   9120
         MaxLength       =   12
         TabIndex        =   127
         Text            =   "0,00"
         Top             =   7420
         Width           =   2175
      End
      Begin VB.ComboBox cboPgtoImediato 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmAviso.frx":0522
         Left            =   4440
         List            =   "frmAviso.frx":0524
         TabIndex        =   126
         Text            =   "N�o"
         Top             =   1260
         Width           =   855
      End
      Begin GridAlianca.grdAlianca GridPropostaAfetada 
         Height          =   4935
         Left            =   -74760
         TabIndex        =   124
         Top             =   1425
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   8705
         BorderStyle     =   0
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
      Begin VB.Frame Frame4 
         Enabled         =   0   'False
         Height          =   1545
         Left            =   -74820
         TabIndex        =   120
         Top             =   6585
         Width           =   11445
         Begin GridFrancisco.GridDinamico GridTipo 
            Height          =   1365
            Left            =   60
            TabIndex        =   121
            Top             =   120
            Width           =   11355
            _ExtentX        =   20029
            _ExtentY        =   2408
            BorderStyle     =   1
            AllowUserResizing=   3
            EditData        =   0   'False
            Highlight       =   1
            ShowTip         =   0   'False
            SortOnHeader    =   0   'False
            BackColor       =   -2147483643
            BackColorBkg    =   -2147483633
            BackColorFixed  =   -2147483633
            BackColorSel    =   -2147483635
            FixedCols       =   1
            FixedRows       =   1
            FocusRect       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   -2147483640
            ForeColorFixed  =   -2147483630
            ForeColorSel    =   -2147483634
            GridColor       =   -2147483630
            GridColorFixed  =   12632256
            GridLine        =   1
            GridLinesFixed  =   2
            MousePointer    =   0
            Redraw          =   -1  'True
            Rows            =   2
            TextStyle       =   0
            TextStyleFixed  =   0
            Cols            =   2
            RowHeightMin    =   0
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Avisos"
         Height          =   3735
         Left            =   -74880
         TabIndex        =   113
         Top             =   1185
         Width           =   11535
         Begin GridAlianca.grdAlianca grdAvisos 
            Height          =   3255
            Left            =   120
            TabIndex        =   114
            Top             =   360
            Width           =   11295
            _ExtentX        =   19923
            _ExtentY        =   5741
            BorderStyle     =   1
            AllowUserResizing=   3
            EditData        =   0   'False
            Highlight       =   1
            ShowTip         =   0   'False
            SortOnHeader    =   0   'False
            BackColor       =   -2147483643
            BackColorBkg    =   -2147483633
            BackColorFixed  =   -2147483633
            BackColorSel    =   -2147483635
            FixedCols       =   1
            FixedRows       =   1
            FocusRect       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   -2147483640
            ForeColorFixed  =   -2147483630
            ForeColorSel    =   -2147483634
            GridColor       =   -2147483630
            GridColorFixed  =   12632256
            GridLine        =   1
            GridLinesFixed  =   2
            MousePointer    =   0
            Redraw          =   -1  'True
            Rows            =   2
            TextStyle       =   0
            TextStyleFixed  =   0
            Cols            =   5
            RowHeightMin    =   0
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Outros Seguros sobre o Objeto Sinistrado"
         Height          =   2655
         Left            =   -70800
         TabIndex        =   111
         Top             =   5025
         Width           =   3615
         Begin GridAlianca.grdAlianca grdOutrosSeg 
            Height          =   2175
            Left            =   120
            TabIndex        =   112
            Top             =   360
            Width           =   3375
            _ExtentX        =   5953
            _ExtentY        =   3836
            BorderStyle     =   1
            AllowUserResizing=   3
            EditData        =   0   'False
            Highlight       =   1
            ShowTip         =   0   'False
            SortOnHeader    =   0   'False
            BackColor       =   -2147483643
            BackColorBkg    =   -2147483633
            BackColorFixed  =   -2147483633
            BackColorSel    =   -2147483635
            FixedCols       =   1
            FixedRows       =   1
            FocusRect       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   -2147483640
            ForeColorFixed  =   -2147483630
            ForeColorSel    =   -2147483634
            GridColor       =   -2147483630
            GridColorFixed  =   12632256
            GridLine        =   1
            GridLinesFixed  =   2
            MousePointer    =   0
            Redraw          =   -1  'True
            Rows            =   2
            TextStyle       =   0
            TextStyleFixed  =   0
            Cols            =   3
            RowHeightMin    =   0
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Local de Risco"
         Height          =   2175
         Left            =   -73680
         TabIndex        =   100
         Top             =   1065
         Width           =   9015
         Begin VB.CheckBox ChkLocalNApolice 
            Caption         =   "Local de Risco n�o identificado na ap�lice"
            Enabled         =   0   'False
            Height          =   255
            Left            =   240
            TabIndex        =   116
            Top             =   360
            Width           =   3495
         End
         Begin VB.TextBox txtEndRisco 
            Enabled         =   0   'False
            Height          =   330
            Left            =   120
            MaxLength       =   60
            TabIndex        =   105
            Top             =   960
            Width           =   5175
         End
         Begin VB.TextBox txtBairroRisco 
            Enabled         =   0   'False
            Height          =   330
            Left            =   5400
            MaxLength       =   30
            TabIndex        =   104
            Top             =   960
            Width           =   3485
         End
         Begin VB.ComboBox cboUFRisco 
            Enabled         =   0   'False
            Height          =   315
            Left            =   6240
            TabIndex        =   103
            Text            =   "cboUFRisco"
            Top             =   1680
            Width           =   930
         End
         Begin VB.TextBox txtMunicipioRisco 
            Enabled         =   0   'False
            Height          =   330
            Left            =   120
            MaxLength       =   30
            TabIndex        =   102
            Top             =   1680
            Width           =   5985
         End
         Begin VB.TextBox txtCEPRisco 
            Enabled         =   0   'False
            Height          =   285
            Left            =   7320
            TabIndex        =   101
            Top             =   1680
            Width           =   1575
         End
         Begin VB.Label Label27 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o:"
            Height          =   195
            Left            =   120
            TabIndex        =   110
            Top             =   720
            Width           =   735
         End
         Begin VB.Label Label26 
            AutoSize        =   -1  'True
            Caption         =   "Bairro:"
            Height          =   195
            Left            =   5400
            TabIndex        =   109
            Top             =   720
            Width           =   450
         End
         Begin VB.Label Label25 
            AutoSize        =   -1  'True
            Caption         =   "Munic�pio:"
            Height          =   195
            Left            =   135
            TabIndex        =   108
            Top             =   1440
            Width           =   750
         End
         Begin VB.Label Label24 
            AutoSize        =   -1  'True
            Caption         =   "U.F.:"
            Height          =   195
            Left            =   6255
            TabIndex        =   107
            Top             =   1440
            Width           =   345
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "CEP:"
            Height          =   195
            Index           =   2
            Left            =   7320
            TabIndex        =   106
            Top             =   1440
            Width           =   360
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Dados da Vistoria"
         Height          =   3615
         Left            =   -73680
         TabIndex        =   78
         Top             =   3465
         Width           =   9015
         Begin VB.TextBox txtCEPVistoria 
            Enabled         =   0   'False
            Height          =   285
            Left            =   7320
            TabIndex        =   86
            Top             =   2040
            Width           =   1575
         End
         Begin VB.TextBox txtMunicipioVistoria 
            Enabled         =   0   'False
            Height          =   330
            Left            =   120
            MaxLength       =   30
            TabIndex        =   85
            Top             =   2010
            Width           =   5985
         End
         Begin VB.ComboBox cmbUFVistoria 
            Enabled         =   0   'False
            Height          =   315
            Left            =   6255
            Style           =   2  'Dropdown List
            TabIndex        =   84
            Top             =   2025
            Width           =   930
         End
         Begin VB.TextBox txtBairroVistoria 
            Enabled         =   0   'False
            Height          =   330
            Left            =   5400
            MaxLength       =   30
            TabIndex        =   83
            Top             =   1320
            Width           =   3465
         End
         Begin VB.TextBox txtEndVistoria 
            Enabled         =   0   'False
            Height          =   330
            Left            =   120
            MaxLength       =   60
            TabIndex        =   82
            Top             =   1335
            Width           =   5175
         End
         Begin VB.TextBox txtContatoVistoria 
            Enabled         =   0   'False
            Height          =   330
            Left            =   120
            MaxLength       =   60
            TabIndex        =   81
            Top             =   615
            Width           =   8775
         End
         Begin VB.ComboBox cmbTpTelefoneVistoria 
            Enabled         =   0   'False
            Height          =   315
            Index           =   0
            ItemData        =   "frmAviso.frx":0526
            Left            =   2595
            List            =   "frmAviso.frx":052D
            Style           =   2  'Dropdown List
            TabIndex        =   80
            Top             =   2745
            Width           =   2670
         End
         Begin VB.ComboBox cmbTpTelefoneVistoria 
            Enabled         =   0   'False
            Height          =   315
            Index           =   1
            ItemData        =   "frmAviso.frx":0548
            Left            =   2595
            List            =   "frmAviso.frx":054F
            Style           =   2  'Dropdown List
            TabIndex        =   79
            Top             =   3105
            Width           =   2670
         End
         Begin MSMask.MaskEdBox mskDDDVistoria 
            Height          =   330
            Index           =   0
            Left            =   135
            TabIndex        =   87
            Top             =   2745
            Width           =   555
            _ExtentX        =   979
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskTelefoneVistoria 
            Height          =   330
            Index           =   0
            Left            =   960
            TabIndex        =   88
            Top             =   2760
            Width           =   1410
            _ExtentX        =   2487
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            MaxLength       =   10
            Mask            =   "9####-####"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskDDDVistoria 
            Height          =   330
            Index           =   1
            Left            =   135
            TabIndex        =   89
            Top             =   3105
            Width           =   555
            _ExtentX        =   979
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskTelefoneVistoria 
            Height          =   330
            Index           =   1
            Left            =   960
            TabIndex        =   90
            Top             =   3105
            Width           =   1410
            _ExtentX        =   2487
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            MaxLength       =   10
            Mask            =   "9####-####"
            PromptChar      =   "_"
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "CEP:"
            Height          =   195
            Index           =   0
            Left            =   7320
            TabIndex        =   99
            Top             =   1800
            Width           =   360
         End
         Begin VB.Label Label23 
            AutoSize        =   -1  'True
            Caption         =   "U.F.:"
            Height          =   195
            Left            =   6255
            TabIndex        =   98
            Top             =   1800
            Width           =   345
         End
         Begin VB.Label Label22 
            AutoSize        =   -1  'True
            Caption         =   "Munic�pio:"
            Height          =   195
            Left            =   135
            TabIndex        =   97
            Top             =   1740
            Width           =   750
         End
         Begin VB.Label Label19 
            AutoSize        =   -1  'True
            Caption         =   "Bairro:"
            Height          =   195
            Left            =   5400
            TabIndex        =   96
            Top             =   1080
            Width           =   450
         End
         Begin VB.Label Label18 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o:"
            Height          =   195
            Left            =   120
            TabIndex        =   95
            Top             =   1080
            Width           =   735
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Nome de Contato:"
            Height          =   195
            Left            =   120
            TabIndex        =   94
            Top             =   360
            Width           =   1290
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "D.D.D.:"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   93
            Top             =   2520
            Width           =   555
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Telefone:"
            Height          =   195
            Index           =   1
            Left            =   1005
            TabIndex        =   92
            Top             =   2520
            Width           =   675
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Telefone:"
            Height          =   195
            Index           =   2
            Left            =   2595
            TabIndex        =   91
            Top             =   2520
            Width           =   1035
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Ocorr�ncia"
         Height          =   2775
         Left            =   -73770
         TabIndex        =   57
         Top             =   1260
         Width           =   9240
         Begin VB.TextBox txtSubEvento 
            Height          =   375
            Left            =   360
            TabIndex        =   123
            Top             =   2280
            Width           =   8295
         End
         Begin VB.TextBox txtEvento 
            Height          =   375
            Left            =   360
            TabIndex        =   122
            Top             =   1560
            Width           =   8295
         End
         Begin VB.TextBox txtDtOcorrenciaFim 
            Enabled         =   0   'False
            Height          =   330
            Left            =   1845
            TabIndex        =   118
            Top             =   885
            Width           =   1185
         End
         Begin VB.TextBox txtSinistro 
            Enabled         =   0   'False
            Height          =   330
            Left            =   3975
            TabIndex        =   76
            Top             =   885
            Width           =   2340
         End
         Begin VB.TextBox txtReanalise 
            Enabled         =   0   'False
            Height          =   330
            Left            =   3990
            TabIndex        =   74
            Top             =   885
            Width           =   780
         End
         Begin VB.TextBox txtDtOcorrencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   390
            TabIndex        =   58
            Top             =   885
            Width           =   1185
         End
         Begin MSMask.MaskEdBox mskHora 
            Height          =   330
            Left            =   3165
            TabIndex        =   59
            Top             =   885
            Width           =   660
            _ExtentX        =   1164
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "_"
         End
         Begin VB.Label lblTpAviso 
            AutoSize        =   -1  'True
            Caption         =   "ABERTURA DE SINISTRO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   375
            TabIndex        =   119
            Top             =   300
            Width           =   2280
         End
         Begin VB.Label lblPeriodico 
            AutoSize        =   -1  'True
            Caption         =   "a"
            Height          =   195
            Left            =   1650
            TabIndex        =   117
            Top             =   945
            Width           =   90
         End
         Begin VB.Label Label28 
            Caption         =   "SubEvento"
            Height          =   255
            Left            =   390
            TabIndex        =   115
            Top             =   2070
            Width           =   3585
         End
         Begin VB.Label lblSinistro 
            AutoSize        =   -1  'True
            Caption         =   "Sinistro AB"
            Height          =   195
            Left            =   3975
            TabIndex        =   77
            Top             =   660
            Width           =   765
         End
         Begin VB.Label lblReanalise 
            AutoSize        =   -1  'True
            Caption         =   "Rean�lise"
            Height          =   195
            Index           =   2
            Left            =   3990
            TabIndex        =   75
            Top             =   660
            Width           =   705
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Evento Causador da Ocorr�ncia"
            Height          =   195
            Index           =   0
            Left            =   390
            TabIndex        =   62
            Top             =   1320
            Width           =   2280
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Data da Ocorr�ncia"
            Height          =   195
            Index           =   0
            Left            =   390
            TabIndex        =   61
            Top             =   660
            Width           =   1395
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Hora"
            Height          =   195
            Index           =   3
            Left            =   3165
            TabIndex        =   60
            Top             =   660
            Width           =   345
         End
      End
      Begin VB.Frame frmObs 
         Caption         =   "Observa��es"
         Height          =   3315
         Left            =   -73770
         TabIndex        =   46
         Top             =   4125
         Width           =   9240
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   9
            Left            =   285
            MaxLength       =   70
            TabIndex        =   56
            Top             =   2820
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   8
            Left            =   285
            MaxLength       =   70
            TabIndex        =   55
            Top             =   2550
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   7
            Left            =   285
            MaxLength       =   70
            TabIndex        =   54
            Top             =   2280
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   285
            MaxLength       =   70
            TabIndex        =   53
            Top             =   2010
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   5
            Left            =   285
            MaxLength       =   70
            TabIndex        =   52
            Top             =   1740
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   285
            MaxLength       =   70
            TabIndex        =   51
            Top             =   1470
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   285
            MaxLength       =   70
            TabIndex        =   50
            Top             =   1200
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   285
            MaxLength       =   70
            TabIndex        =   49
            Top             =   915
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   285
            MaxLength       =   70
            TabIndex        =   48
            Top             =   630
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   285
            MaxLength       =   70
            TabIndex        =   47
            Top             =   360
            Width           =   8775
         End
      End
      Begin VB.Frame fraAgencia 
         Caption         =   "Ag�ncia de Contato"
         Height          =   2355
         Left            =   -73785
         TabIndex        =   31
         Top             =   2505
         Width           =   9285
         Begin VB.TextBox txtBairroAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   5280
            TabIndex        =   38
            Top             =   1170
            Width           =   3765
         End
         Begin VB.TextBox txtUFAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   8475
            TabIndex        =   37
            Top             =   1845
            Width           =   555
         End
         Begin VB.TextBox txtMunicipioAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   210
            TabIndex        =   36
            Top             =   1845
            Width           =   7620
         End
         Begin VB.TextBox txtEnderecoAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   210
            TabIndex        =   35
            Top             =   1170
            Width           =   4965
         End
         Begin VB.TextBox txtNomeAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   2655
            TabIndex        =   34
            Top             =   495
            Width           =   6360
         End
         Begin VB.TextBox txtDV 
            Enabled         =   0   'False
            Height          =   330
            Left            =   1620
            TabIndex        =   33
            Top             =   495
            Width           =   465
         End
         Begin VB.TextBox txtCodigoAgencia 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Left            =   240
            TabIndex        =   32
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "Bairro:"
            Height          =   195
            Left            =   5250
            TabIndex        =   45
            Top             =   945
            Width           =   450
         End
         Begin VB.Label Label17 
            AutoSize        =   -1  'True
            Caption         =   "UF:"
            Height          =   195
            Left            =   8475
            TabIndex        =   44
            Top             =   1575
            Width           =   255
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Munic�pio"
            Height          =   195
            Index           =   1
            Left            =   210
            TabIndex        =   43
            Top             =   1620
            Width           =   705
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o:"
            Height          =   195
            Index           =   1
            Left            =   210
            TabIndex        =   42
            Top             =   945
            Width           =   735
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Nome da ag�ncia"
            Height          =   195
            Index           =   1
            Left            =   2640
            TabIndex        =   41
            Top             =   270
            Width           =   1260
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "DV:"
            Height          =   195
            Index           =   1
            Left            =   1620
            TabIndex        =   40
            Top             =   270
            Width           =   270
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "C�d da ag�ncia:"
            Height          =   195
            Index           =   1
            Left            =   240
            TabIndex        =   39
            Top             =   240
            Width           =   1170
         End
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   -71820
         TabIndex        =   30
         Top             =   2100
         Width           =   1410
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   -69975
         TabIndex        =   29
         Top             =   3540
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   -69975
         TabIndex        =   28
         Top             =   3180
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   -69975
         TabIndex        =   27
         Top             =   2820
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   -69975
         TabIndex        =   26
         Top             =   2460
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   -69975
         TabIndex        =   25
         Top             =   2100
         Width           =   915
      End
      Begin VB.TextBox txtCEPSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72765
         MaxLength       =   30
         TabIndex        =   24
         Top             =   6105
         Width           =   1635
      End
      Begin VB.TextBox txtUFSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -70020
         MaxLength       =   2
         TabIndex        =   23
         Top             =   6105
         Width           =   555
      End
      Begin VB.TextBox txtMunicipioSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -70020
         MaxLength       =   60
         TabIndex        =   22
         Top             =   5475
         Width           =   4065
      End
      Begin VB.TextBox txtBairroSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72765
         MaxLength       =   30
         TabIndex        =   21
         Top             =   5475
         Width           =   2625
      End
      Begin VB.TextBox txtEnderecoSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72765
         MaxLength       =   60
         TabIndex        =   20
         Top             =   4845
         Width           =   6810
      End
      Begin VB.TextBox txtEmailSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72765
         MaxLength       =   60
         TabIndex        =   19
         Top             =   4215
         Width           =   6810
      End
      Begin VB.TextBox txtNomeSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72765
         MaxLength       =   60
         TabIndex        =   18
         Top             =   1410
         Width           =   6810
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   -68625
         TabIndex        =   17
         Top             =   2100
         Width           =   2625
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   -68625
         TabIndex        =   16
         Top             =   2460
         Width           =   2625
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   -68625
         TabIndex        =   15
         Top             =   2820
         Width           =   2625
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   -68625
         TabIndex        =   14
         Top             =   3180
         Width           =   2625
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   -68625
         TabIndex        =   13
         Top             =   3540
         Width           =   2625
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   -72765
         TabIndex        =   12
         Top             =   2100
         Width           =   555
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   -72765
         TabIndex        =   11
         Top             =   2460
         Width           =   555
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   -72765
         TabIndex        =   10
         Top             =   2820
         Width           =   555
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   -72765
         TabIndex        =   9
         Top             =   3540
         Width           =   555
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   -71820
         TabIndex        =   8
         Top             =   2460
         Width           =   1410
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   -71820
         TabIndex        =   7
         Top             =   2820
         Width           =   1410
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   -71820
         TabIndex        =   6
         Top             =   3180
         Width           =   1410
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   -71820
         TabIndex        =   5
         Top             =   3540
         Width           =   1410
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   -72765
         TabIndex        =   4
         Top             =   3180
         Width           =   555
      End
      Begin MSFlexGridLib.MSFlexGrid grdEquipamentos 
         Height          =   5625
         Left            =   120
         TabIndex        =   130
         Top             =   1680
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   9922
         _Version        =   393216
         Rows            =   1
         Cols            =   10
         Appearance      =   0
         FormatString    =   "                                                "
      End
      Begin VB.Label lblTotEstimativaPrejuizo 
         Caption         =   "Total da Estimativa do Preju�zo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6240
         TabIndex        =   128
         Top             =   7440
         Width           =   2775
      End
      Begin VB.Label lblPagtoImediato 
         Caption         =   "Sinistro aceito no fluxo de pagamento imediato:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   125
         Top             =   1320
         Width           =   4095
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Telefone:"
         Height          =   195
         Left            =   -68625
         TabIndex        =   73
         Top             =   1875
         Width           =   1035
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Ramal:"
         Height          =   195
         Index           =   0
         Left            =   -69975
         TabIndex        =   72
         Top             =   1875
         Width           =   495
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "CEP:"
         Height          =   195
         Index           =   0
         Left            =   -72765
         TabIndex        =   71
         Top             =   5880
         Width           =   360
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "U.F.:"
         Height          =   195
         Index           =   0
         Left            =   -70020
         TabIndex        =   70
         Top             =   5880
         Width           =   345
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio:"
         Height          =   195
         Left            =   -70020
         TabIndex        =   69
         Top             =   5250
         Width           =   750
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         Height          =   195
         Left            =   -72765
         TabIndex        =   68
         Top             =   5250
         Width           =   450
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         Height          =   195
         Left            =   -72765
         TabIndex        =   67
         Top             =   4620
         Width           =   735
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "E-mail:"
         Height          =   195
         Left            =   -72765
         TabIndex        =   66
         Top             =   3990
         Width           =   465
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Telefone:"
         Height          =   195
         Index           =   2
         Left            =   -71775
         TabIndex        =   65
         Top             =   1875
         Width           =   675
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "D.D.D.:"
         Height          =   195
         Index           =   2
         Left            =   -72765
         TabIndex        =   64
         Top             =   1905
         Width           =   540
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Nome:"
         Height          =   195
         Left            =   -72765
         TabIndex        =   63
         Top             =   1185
         Width           =   465
      End
   End
End
Attribute VB_Name = "frmAviso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'Public sinistro_id As String
Public Proposta_id As String
Public produto_nome As String

Dim Apolice_id As String
Dim sucursal As String
Dim seguradora As String

Dim AProposta() As String
Dim bEnvioEmail As Boolean
Public bAviso As Boolean

'INICIO - MU00416975 - Reabertura de Sinistro - inc 02/04/2018
Public Ramo_reanalise As String
Public Produto_reanalise As String
Public tipo_ramo_vida As Integer
Public Sexo_vida As String
Public Nasciemnto_vida As String
Public cpf_cgc_vida As String
Public Cliente_id_vida As String
Public produtosreanaliseok As Boolean
Public LinhasArray As Integer
'FIM  - MU00416975 - Reabertura de Sinistro
'



Sub FormataTelaOcorrencia()

If Trim(txtDtOcorrenciaFim.Text) = "__/__/____" Then
    lblPeriodico.Visible = False
    txtDtOcorrenciaFim.Visible = False
    mskHora.Left = 1980
    Label2(3).Left = 1980
    'lblReanalise(2).Left = 2790
    'txtReanalise.Left = 2790
    lblSinistro.Left = 2790 'asouza novo
    txtSinistro.Left = 2790
    
'    lblSinistro.Left = 3750
'    txtSinistro.Left = 3750
Else
    lblPeriodico.Visible = True
    txtDtOcorrenciaFim.Visible = True
    mskHora.Left = 3165
    Label2(3).Left = 3165
    'lblReanalise(2).Left = 3990
    'txtReanalise.Left = 3990
    lblSinistro.Left = 3990
    txtSinistro.Left = 3990
    'lblSinistro.Left = 4950
    'txtSinistro.Left = 4950
End If

End Sub

Private Function GravaSinistros(indice As Integer)
   Dim nIndex As Integer
   Dim bValido As Boolean
   Dim nSinistro As String
   
   AProposta = Split(propostasAvisadas, " , ")
   bValido = True
   bSinistro = True
   bEnvioEmail = True
   
   For nIndex = LBound(AProposta) To UBound(AProposta)
      
      Call ObtemDadosSinistro(AProposta(nIndex), nSinistro, indice)
      bSinistro = False
      
      'Concatenar para tratar posteriormente e exibi-los na tela do n�mero de sinistro
      AProposta(nIndex) = AProposta(nIndex) & "|" & nSinistro & "|" & AvisoSinistro.NomeProduto
      
      If produtoQuestionario <> 0 Then '2403
          If AvisoSinistro.IncluirAvisoSinistro(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cUserName, oSABL0100) Then
              bAviso = True
          Else
              bValido = False
          End If
      Else
          If AvisoSinistro.IncluirAvisoSinistro(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cUserName) Then
              bAviso = True
          Else
              bValido = False
          End If
      End If
      
      If Not Envia_email_Sem_Doc Then
         bEnvioEmail = False
      End If
   Next
   
   GravaSinistros = bValido
   
End Function

Private Sub cmdAvisar_Click()
Dim bAviso As Boolean
Dim aProp() As String
Dim indice As Integer
Dim SQL As String
Dim rsConsultarQuestionario As ADODB.Recordset

Debug.Print "frmAviso.cmdAvisar_Click"
Debug.Print "I" & Format(Now(), "hh:mm:ss,ms")
ssituacao_aviso = "N"
cmdAvisar.Enabled = False
If bSemProposta Then
    Call MsgBox("Voc� est� avisando um sinistro sem proposta espec�fica.", vbInformation, "Aviso de Sinistro")

    If MsgBox("Tem certeza de que deseja continuar?", vbYesNo, "Sinistro sem proposta") = vbNo Then
        cmdAvisar.Enabled = True
        Exit Sub
    
    End If
Else
    If MsgBox("Tem certeza que deseja avisar este sinistro?", vbYesNo, "Aviso de Sinistro") = vbNo Then
        cmdAvisar.Enabled = True
        Exit Sub
    End If
End If

indice = 1


Dim indiceProposta
For Each Aviso In Avisos

    bAviso = False
    
    Me.MousePointer = vbHourglass
    Me.SSTab1.MousePointer = ssHourglass
    ObtemDadosSinistros (indice)
    DoEvents
    frmAvisosRealizados.Frame1.Visible = True
    
    ' ODIL RINCON JUNIOR || GPTI  - 23/03/2010 - FLOW: 1113817
    indiceProposta = getIndiceProposta(Aviso.Proposta)
    If Propostas.Count > 0 Then
        If Propostas(indiceProposta).TipoAviso = "INC_OCORRENCIA" Then
        'If sProdutoAgricola = "INC_OCORRENCIA" Then
        'FIM ODIL RINCON JUNIOR || GPTI - 23/03/2010 - FLOW: 1113817
            
            If AvisoSinistro.IncluirOcorrenciaSinistro(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cUserName, oSABL0100) Then '08.02
                
                bAviso = True
                
                lEvento_Id = AvisoSinistro.Evento_id
                sinistro_id = AvisoSinistro.sinistro_id
                If Not Envia_email_Sem_Doc Then
                    MsgBox "N�o foi poss�vel enviar o e-mail para a DITEC, �rea respons�vel pelo cadastro dos documentos. Favor verificar com o respons�vel do sistema.", vbCritical, "SEGP0862 - Envio de e-mail"
                End If
                         
            End If
        Else
        
       
          If produtoQuestionario <> 0 Then '2403
              If AvisoSinistro.IncluirAvisoSinistro(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cUserName, oSABL0100, LinhasArray) Then
                  bAviso = True
              End If
          Else
              If AvisoSinistro.IncluirAvisoSinistro(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cUserName, , LinhasArray) Then
                  bAviso = True
              End If
          End If
      
          If bAviso = True Then
             lEvento_Id = AvisoSinistro.Evento_id
             sinistro_id = AvisoSinistro.sinistro_id
             If Not Envia_email_Sem_Doc Then
                MsgBox "N�o foi poss�vel enviar o e-mail para a DITEC, �rea respons�vel pelo cadastro dos documentos. Favor verificar com o respons�vel do sistema.", vbCritical, "SEGP0862 - Envio de e-mail"
             End If
          End If
        End If
    Else
        
       
          If produtoQuestionario <> 0 Then '2403
              If AvisoSinistro.IncluirAvisoSinistro(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cUserName, oSABL0100, LinhasArray) Then
                  bAviso = True
              End If
          Else
              If AvisoSinistro.IncluirAvisoSinistro(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cUserName, , LinhasArray) Then
                  bAviso = True
              End If
          End If
      
          If bAviso = True Then
             lEvento_Id = AvisoSinistro.Evento_id
             sinistro_id = AvisoSinistro.sinistro_id
              If Not Envia_email_Sem_Doc Then
                MsgBox "N�o foi poss�vel enviar o e-mail para a DITEC, �rea respons�vel pelo cadastro dos documentos. Favor verificar com o respons�vel do sistema.", vbCritical, "SEGP0862 - Envio de e-mail"
             End If
          End If
        End If
    
    If bAviso Then
        'Set AvisoSinistro = Nothing
        Me.MousePointer = vbDefault
        Me.SSTab1.MousePointer = ssDefault
        
        If lista_avisos = "" Then
            lista_avisos = lEvento_Id
        Else
            lista_avisos = lista_avisos & ", " & lEvento_Id
        End If
        
        frmAvisosRealizados.grdPropAvis.AddItem (sinistro_id & vbTab & Aviso.Proposta & vbTab & Aviso.nome_produto)
        If frmAvisosRealizados.grdPropAvis.TextMatrix(1, 1) = "" Then
            frmAvisosRealizados.grdPropAvis.RemoveItem (1)
        End If
    Else
        Me.MousePointer = vbDefault
        Me.SSTab1.MousePointer = ssDefault
        MsgBox "Ocorreu um erro no aviso deste sinistro. N�o foi poss�vel completar esta opera��o.", vbCritical
    End If
    indice = indice + 1
Next Aviso
Me.Hide


           Dim SQLQUEST As String
                
              
            SQLQUEST = "exec SEGS9411_SPS " & frmDocumentos.produto_id & ", " & frmDocumentos.evento_sinistro_id
        
            Set rsConsultarQuestionario = ExecutarSQL(gsSIGLASISTEMA, _
                                                        glAmbiente_id, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        SQLQUEST, _
                                                        True)
                                                   
                
            If rsConsultarQuestionario.EOF Then
                Me.Hide
                frmAvisosRealizados.Show
            Else
                Me.Hide
                frmQuestionarioSinistro.Show
            End If

Debug.Print "F" & Format(Now(), "hh:mm:ss,ms")

End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call Sair(Me)
End Sub

Private Sub cmdVoltar_Click()
    grdAvisos.Clear
    GridPropostaAfetada.Clear
    While GridPropostaAfetada.Rows > 2
        GridPropostaAfetada.RemoveItem (1)
    Wend
    
    Me.Hide
    frmOutrosSeguros.Show
    
End Sub

Private Sub Form_Activate()

    Call GridsSetup
    PreencheGridTipo
    
        With GridPropostaAfetada
        .Cols = 10
        .TextMatrix(0, 1) = "Aviso"
        .TextMatrix(0, 2) = "Proposta AB"
        .TextMatrix(0, 3) = "Proposta BB"
        .TextMatrix(0, 4) = "Situacao"
        .TextMatrix(0, 5) = "Produto"
        .TextMatrix(0, 6) = "Nome do Produto"
        .TextMatrix(0, 7) = "Tipo Ramo"
        .TextMatrix(0, 8) = "Inicio de Vig�ncia"
        .TextMatrix(0, 9) = "Fim de Vig�ncia"
        .AutoFit H�brido
        
    End With
    
        With grdAvisos
        .Cols = 9
              
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "Proposta"
        .TextMatrix(0, 2) = "Proposta BB"
        .TextMatrix(0, 3) = "Produto"
        .TextMatrix(0, 4) = "Nome do Produto"
        .TextMatrix(0, 5) = "Ap�lice"
        .TextMatrix(0, 6) = "Ramo"
        .TextMatrix(0, 7) = "Evento do Sinistro"
        .TextMatrix(0, 8) = "Valor do Prezu�zo"
        .AutoFit H�brido
        
    End With
        
        '--------------------------------------------------------------
    'Ricardo Toledo (Confitec) : 20/01/2017 : INC000005302388 : INI
    txtEvento.Text = frmEvento.cboEvento.Text
    txtSubEvento.Text = frmEvento.cboSubEvento.Text
    txtDtOcorrencia.Text = frmEvento.mskDataOcorrencia.Text
    txtDtOcorrenciaFim.Text = frmEvento.mskDataOcorrenciaFim.Text
    mskHora.Text = frmEvento.mskHora.Text
    'Ricardo Toledo (Confitec) : 20/01/2017 : INC000005302388 : FIM
    '--------------------------------------------------------------
        
End Sub

Private Sub PreencheGridTipo()

    With GridTipo
        .Cols = 5
        
        '(IN�CIO) -- RSouza -- 26/07/2010 --------
        '.Rows = 3
        .Rows = 4
        '(FIM)    -- RSouza -- 26/07/2010 --------
      
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "C�digo"
        .TextMatrix(0, 2) = "Descri��o"
        .TextMatrix(1, 1) = "AVSR"
        .TextMatrix(1, 2) = "A Avisar"
        .TextMatrix(2, 1) = "RNLS"
        .TextMatrix(2, 2) = "Solicita��o de Reanalise"

        '(IN�CIO) -- RSouza -- 26/07/2010 --------
        .TextMatrix(3, 1) = "SRGL"
        .TextMatrix(3, 2) = "Sinistro em regula��o"
        '(FIM)    -- RSouza -- 26/07/2010 --------

        .TextMatrix(0, 3) = "C�digo"
        .TextMatrix(0, 4) = "Descri��o"
        .TextMatrix(1, 3) = "PSCB"
        .TextMatrix(1, 4) = "N�o Avisar - Proposta Sem Cobertura"
        .TextMatrix(2, 3) = "NAVS"
        .TextMatrix(2, 4) = "N�o Avisar"
        .AutoFit H�brido
    End With
    
End Sub

Private Sub Form_Load()

'AKIO.OKUNO - INICIO - 17860335 - 26/06/2013
'    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2, "Produ��o", "Qualidade")
    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2 Or glAmbiente_id = 6, "Produ��o", "Qualidade")
'AKIO.OKUNO - FIM - 17860335 - 26/06/2013

    CentraFrm Me
    Call GridsSetup
    PreencheGridTipo
    
    If bSemProposta Then
        txtEndRisco.Text = frmDadosCliente.txtEndRisco.Text
        txtCEPRisco.Text = frmDadosCliente.txtCEP.Text
        txtMunicipioRisco.Text = frmDadosCliente.cmbMunicipio.Text
        txtBairroRisco.Text = frmDadosCliente.txtBairroRisco.Text
        cboUFRisco.Text = frmDadosCliente.cboUFRisco
    End If

    txtEvento.Text = frmEvento.cboEvento.Text
    txtSubEvento.Text = frmEvento.cboSubEvento.Text
    txtDtOcorrencia.Text = frmEvento.mskDataOcorrencia.Text
    txtDtOcorrenciaFim.Text = frmEvento.mskDataOcorrenciaFim.Text
    mskHora.Text = frmEvento.mskHora.Text
    

    With GridPropostaAfetada
        .Cols = 10
        
        .TextMatrix(0, 1) = "Aviso"
        .TextMatrix(0, 2) = "Proposta AB"
        .TextMatrix(0, 3) = "Proposta BB"
        .TextMatrix(0, 4) = "Situacao"
        .TextMatrix(0, 5) = "Produto"
        .TextMatrix(0, 6) = "Nome do Produto"
        .TextMatrix(0, 7) = "Tipo Ramo"
        .TextMatrix(0, 8) = "Inicio de Vig�ncia"
        .TextMatrix(0, 9) = "Fim de Vig�ncia"
        .AutoFit H�brido
        
    End With

    With grdAvisos
        .Cols = 9
              
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "Proposta"
        .TextMatrix(0, 2) = "Proposta BB"
        .TextMatrix(0, 3) = "Produto"
        .TextMatrix(0, 4) = "Nome do Produto"
        .TextMatrix(0, 5) = "Ap�lice"
        .TextMatrix(0, 6) = "Ramo"
        .TextMatrix(0, 7) = "Evento do Sinistro"
        .TextMatrix(0, 8) = "Valor do Prezu�zo"
        .AutoFit H�brido
        
    End With
    
End Sub

Public Sub GridsSetup(Optional Clear As Boolean)

    With grdOutrosSeg
        If Clear = True Then
            .Clear
        End If
        .Cols = 3
        If .Rows = 1 Then
            .AdicionarLinha vbTab & ""
        End If
        .FixedCols = 1
        .FixedRows = 1
        .TextMatrix(0, 1) = "Ap�lice"
        .TextMatrix(0, 2) = "Nome da Seguradora"

        .AutoFit H�brido
    End With
    
End Sub



Public Sub ObtemDadosSinistros(indice As Integer)
Dim i As Long
Dim Apolice_id As String

Dim sucursal As String
Dim seguradora As String
Dim Moeda As Integer
Dim Motivo(5) As String
Dim Obs(10) As String

'para reanalise
Dim sinistro_id_ant As String
Dim sinistro_bb_id As String
Dim SQL As String
Dim Rs As ADODB.Recordset
Dim rsPesquisa As ADODB.Recordset
Dim dVlrEstimativa As Double
Dim sCoberturasJaInseridas As String

Dim cod_sinistrado_atual As Integer
Dim cod As Integer

Dim str_Coberturas As String

Dim AvisoRegras As Object

Dim estim_cobertura As estimativa

ReDim Preserve iTp_Log_Sinistro(0)

bExisteTpLogSinistro = False

Dim indiceProposta

'Demanda 17905311 - 15/04/2014 - In�cio
Dim Proposta_bb As Long
'Demanda 17905311 - 15/04/2014 - Fim

''Const Evento_bb_id_pecuario_agravamento = 54332
Const Evento_bb_id_pecuario_agravamento = "Null"
Const Evento_segbr_id_pecuario_agravamento = "54332"

On Error GoTo TrataErro

    Set AvisoSinistro = CreateObject("SEGL0144.cls00175")
        
    With AvisoSinistro
    
        .mvarUsuario = cUserName
        .mvarAmbienteId = glAmbiente_id
        .mvarSiglaSistema = gsSIGLASISTEMA
        .mvarSiglaRecurso = App.Title
        .mvarDescricaoRecurso = App.FileDescription
        .mvarAmbiente = ConvAmbiente(glAmbiente_id)
    
        Call VerificaPropostaBB(Avisos.Item(indice).Proposta_bb, "'" & Format(txtDtOcorrencia.Text, "yyyymmdd") & "'")
        bCorretorBB = AvisoSinistro.VerificaSeCorretorBB(CLng(Avisos(indice).Proposta))
        
        .TipoRamo = tipo_ramo
        .NumProcRemessa = "0001"
        
        If sProdutoAgricola = "INC_OCORRENCIA" Then 'ASOUZA
            .Evento_Segbr_id = 10156
            .Recurso = App
        Else
            Obtem_Evento_SEGBR
            .Localizacao = Localizacao
            'wilder
            If valida_pecuario_agravamento(Avisos(indice).Proposta) Then
                .Evento_bb_id = Evento_bb_id_pecuario_agravamento
                Evento_Segbr_id = Evento_segbr_id_pecuario_agravamento
                .Evento_Segbr_id = Evento_segbr_id_pecuario_agravamento
            Else
                .Evento_Segbr_id = Evento_Segbr_id
                .Evento_bb_id = Evento_bb_id
            End If
        End If
        .dt_Aviso = CStr(Date)
        .Proposta_id = Avisos(indice).Proposta
        If frmConsulta.grdResultadoPesquisa.Rows = 1 Then
            .PropostaBB = frmDadosCliente.txtPropostaBB
        Else
            .PropostaBB = Avisos(indice).Proposta_bb
        End If
        .subramo_id = Avisos(indice).SubRamo
        .produto_id = Avisos(indice).Produto
        'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : inicio
        'Call ExisteProdutoBB(Avisos(indice).Produto)
        'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : fim
        .CD_PRD = IIf(iCD_PRD = 0, "", iCD_PRD)
        .CD_MDLD = IIf(iCD_MDLD = 0, "", iCD_MDLD)
        .CD_ITEM_MDLD = IIf(lCD_ITEM_MDLD = 0, "", lCD_ITEM_MDLD)
        .NR_CTR_SGRO = IIf(lNR_CTR_SGRO = 0, "", lNR_CTR_SGRO)
        .NR_VRS_EDS = IIf(lNR_VRS_EDS = 0, "", lNR_VRS_EDS)
        .NomeProduto = Avisos(indice).nome_produto
        If frmConsulta.grdResultadoPesquisa.Rows = 1 Then
            '-----------------------------------------------------------------
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
            '.SeguradoNomeCliente = frmDadosCliente.txtNome
            .SeguradoNomeCliente = MudaAspaSimples(frmDadosCliente.txtNome)
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
            '-----------------------------------------------------------------
            .SeguradoCliente_id = 0
            If Len(frmDadosCliente.mskCPF) > 14 Then
                .SeguradoCPF_CNPJ = DatCGC(frmDadosCliente.mskCPF)
            Else
                .SeguradoCPF_CNPJ = DatCPF(frmDadosCliente.mskCPF)
            End If
            '-----------------------------------------------------------------
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
            '.SinistradoNomeCliente = frmDadosCliente.txtNome
            .SinistradoNomeCliente = MudaAspaSimples(frmDadosCliente.txtNome)
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
            '-----------------------------------------------------------------
        Else
            '-----------------------------------------------------------------
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
            '.SeguradoNomeCliente = frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 1)
            .SeguradoNomeCliente = MudaAspaSimples(frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 1))
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
            '-----------------------------------------------------------------
            .SeguradoCliente_id = frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 3)
            .SeguradoCPF_CNPJ = frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 2)
            '-----------------------------------------------------------------
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
            '.SinistradoNomeCliente = frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 1)
            .SinistradoNomeCliente = MudaAspaSimples(frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 1))
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
            '-----------------------------------------------------------------
        End If
        .IndTpSinistrado = "T" ' Titular
        .AgenciaAviso = txtCodigoAgencia.Text
        .AgenciaDvAviso = txtDV.Text
        If bSemProposta Then 'Segundo orienta��o do Marcio Yoshimura manter o Banco 1 para avisos sem proposta
            .Banco = 1
        Else
            .Banco = IIf(bCorretorBB, 1, 0)
        End If
        .AgenciaNome = txtNomeAgencia
        .AgenciaEndereco = txtEnderecoAgencia.Text & " - " & _
                           txtBairroAgencia.Text & " " & vbCrLf & _
                           Trim(txtMunicipioAgencia.Text) & "-"
        .AgenciaEndereco = .AgenciaEndereco & txtUFAgencia.Text & ""
       
            
        'Local Vistoria
        '-----------------------------------------------------------------
        'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
        '.ContatoVistoria = txtContatoVistoria.Text
        .ContatoVistoria = MudaAspaSimples(txtContatoVistoria.Text)
        'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
        '-----------------------------------------------------------------
        .EnderecoVistoria = txtEndVistoria
        .BairroVistoria = txtBairroVistoria
        .MunicipioVistoria = txtMunicipioVistoria
        .UFVistoria = cmbUFVistoria.Text
        .CEPVistoria = txtCEPVistoria
        
        .DDD1Vistoria = mskDDDVistoria(0).Text
        .Telefone1Vistoria = mskTelefoneVistoria(0).ClipText
        '.Tipo_telefone1Vistoria = IIf(cmbTpTelefoneVistoria(0).ListIndex <> -1, cmbTpTelefoneVistoria(0).ItemData(cmbTpTelefoneVistoria(0).ListIndex), "")
        If cmbTpTelefoneVistoria(0).ListIndex <> -1 Then
            .Tipo_telefone1Vistoria = cmbTpTelefoneVistoria(0).ItemData(cmbTpTelefoneVistoria(0).ListIndex)
        Else
            .Tipo_telefone1Vistoria = ""
        End If
        
        .DDD2Vistoria = mskDDDVistoria(1).Text
        .Telefone2Vistoria = mskTelefoneVistoria(1).ClipText
        If cmbTpTelefoneVistoria(1).ListIndex <> -1 Then
            .Tipo_telefone2Vistoria = cmbTpTelefoneVistoria(1).ItemData(cmbTpTelefoneVistoria(1).ListIndex)
        Else
            .Tipo_telefone2Vistoria = ""
        End If
          
        'Demanda 17905311 - 15/04/2014 - In�cio
        'Call ConsultarDadosApolice(Avisos(indice).Proposta, Apolice_id, iRamo_id, sucursal, seguradora, Moeda)
        Call ConsultarDadosApolice(Avisos(indice).Proposta, Apolice_id, iRamo_id, sucursal, seguradora, Moeda, Proposta_bb)
        'Demanda 17905311 - 15/04/2014 - Fim
        .Apolice_id = Apolice_id
        .ramo_id = iRamo_id
        .Sucursal_id = sucursal
        .Seguradora_id = IIf(seguradora = "", "6785", seguradora)
        .Moeda = Moeda

        'Demanda 17905311 - 15/04/2014 - In�cio
        If (.PropostaBB = "" Or .PropostaBB = 0) Then
            .PropostaBB = Proposta_bb
        End If
        'Demanda 17905311 - 15/04/2014 - Fim
        
        'Dados ocorr�ncia / Risco
        'Inclu�do por asouza 07.12.05 ---
        
        If Trim(txtDtOcorrenciaFim.Text) = "__/__/____" Then
            .tipo_aviso = 1
        Else
            .tipo_aviso = 2
        End If
        
        
        If produtoQuestionario <> 0 Then
            .questionario_produto_id = produtoQuestionario
        End If
        '-------
        .dt_Ocorrencia = Format(txtDtOcorrencia.Text, "yyyymmdd")
        .HoraOcorrencia = Format(mskHora, "0000")
        
        If Trim(txtDtOcorrenciaFim.Text) <> "__/__/____" Then
            .dt_ocorrencia_fim = Format(txtDtOcorrenciaFim.Text, "yyyymmdd")
        End If
        
        .LocalRiscoApolice = IIf(ChkLocalNApolice.value = 0, "S", "N")
        
        .LocalOcorrencia = txtEndRisco.Text
        .BairroOcorrencia = Me.txtBairroRisco.Text
        .MunicipioOcorrencia = Me.txtMunicipioRisco.Text
        .MunicipioIdOcorrencia = frmSolicitante.VerificaMunicipio(frmEvento.cmbMunicipio.Text, frmEvento.cmbUF.Text)
        .CEPOcorrencia = Replace(Me.txtCEPRisco.Text, "-", "")
        .UFOcorrencia = Me.cboUFRisco.Text
        .Causa_id = frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex)
        .Causa = frmEvento.cboEvento.Text
        If frmEvento.cboSubEvento.ListIndex = -1 Then
           .subcausa_id = 0
           .subCausa = ""
        Else
           .subcausa_id = frmEvento.cboSubEvento.ItemData(frmEvento.cboSubEvento.ListIndex)
           .subCausa = frmEvento.cboSubEvento.Text
        End If
        '-----------------------------------------------------------------
        'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
        'Dados Solicitante
        '.SolicitanteNome = txtNomeSolicitante.Text
        .SolicitanteNome = MudaAspaSimples(txtNomeSolicitante.Text)
        'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
        '-----------------------------------------------------------------
        
        .SolicitanteEndereco = Trim(txtEnderecoSolicitante.Text)
        .SolicitanteMunicipio = Trim(txtMunicipioSolicitante.Text)
        '.SolicitanteMunicipio_id = frmSolicitante.cmbMunicipio.ItemData(frmSolicitante.cmbMunicipio.ListIndex)
        .SolicitanteMunicipio_id = frmSolicitante.VerificaMunicipio(Trim(txtMunicipioSolicitante.Text), txtUFSolicitante.Text)
        
        .SolicitanteBairro = Trim(txtBairroSolicitante.Text)
        .SolicitanteEstado = txtUFSolicitante.Text
        .SolicitanteCEP = Replace(txtCEPSolicitante.Text, "-", "")
        .SolicitanteEmail = txtEmailSolicitante.Text
        .DDD1 = txtDDD(0).Text
        .DDD2 = txtDDD(1).Text
        .DDD3 = txtDDD(2).Text
        .DDD4 = txtDDD(3).Text
        .ddd5 = txtDDD(4).Text
        .Telefone1 = IIf(txtTelefone(0).Text = "-", "", Replace(txtTelefone(0).Text, "-", ""))
        .Telefone2 = IIf(txtTelefone(1).Text = "-", "", Replace(txtTelefone(1).Text, "-", ""))
        .Telefone3 = IIf(txtTelefone(2).Text = "-", "", Replace(txtTelefone(2).Text, "-", ""))
        .Telefone4 = IIf(txtTelefone(3).Text = "-", "", Replace(txtTelefone(3).Text, "-", ""))
        .telefone5 = IIf(txtTelefone(4).Text = "-", "", Replace(txtTelefone(4).Text, "-", ""))
        .Ramal1 = Trim(IIf(txtRamal(0).Text = "", "", txtRamal(0).Text))
        .Ramal2 = Trim(IIf(txtRamal(1).Text = "", "", txtRamal(1).Text))
        .Ramal3 = Trim(IIf(txtRamal(2).Text = "", "", txtRamal(2).Text))
        .Ramal4 = Trim(IIf(txtRamal(3).Text = "", "", txtRamal(3).Text))
        .ramal5 = Trim(IIf(txtRamal(4).Text = "", "", txtRamal(4).Text))
        
        If frmSolicitante.cmbTipoTelefone(0).Enabled And frmSolicitante.cmbTipoTelefone(0).ListIndex <> -1 Then
             .tp_Telefone1 = IIf(frmSolicitante.cmbTipoTelefone(0).ListIndex <> -1, frmSolicitante.cmbTipoTelefone(0).ItemData(frmSolicitante.cmbTipoTelefone(0).ListIndex), "")
        Else
             .tp_Telefone1 = ""
        End If
        If frmSolicitante.cmbTipoTelefone(1).Enabled Then
             .tp_Telefone2 = IIf(frmSolicitante.cmbTipoTelefone(1).ListIndex <> -1, frmSolicitante.cmbTipoTelefone(1).ItemData(frmSolicitante.cmbTipoTelefone(1).ListIndex), "")
        Else
             .tp_Telefone2 = ""
        End If
        If frmSolicitante.cmbTipoTelefone(2).Enabled Then
             .tp_Telefone3 = IIf(frmSolicitante.cmbTipoTelefone(2).ListIndex <> -1, frmSolicitante.cmbTipoTelefone(2).ItemData(frmSolicitante.cmbTipoTelefone(2).ListIndex), "")
        Else
             .tp_Telefone3 = ""
        End If
        If frmSolicitante.cmbTipoTelefone(3).Enabled Then
             .tp_Telefone4 = IIf(frmSolicitante.cmbTipoTelefone(3).ListIndex <> -1, frmSolicitante.cmbTipoTelefone(3).ItemData(frmSolicitante.cmbTipoTelefone(3).ListIndex), "")
        Else
             .tp_Telefone4 = ""
        End If
        If frmSolicitante.cmbTipoTelefone(4).Enabled Then
             .tp_telefone5 = IIf(frmSolicitante.cmbTipoTelefone(4).ListIndex <> -1, frmSolicitante.cmbTipoTelefone(4).ItemData(frmSolicitante.cmbTipoTelefone(4).ListIndex), "")
        Else
             .tp_telefone5 = ""
        End If

        .SolicitanteGrauParentesco = ""
        .entidade_id = entidade_id
        .canal_comunicacao = canal_comunicacao
        'Caso o produto n�o faz a reintegra��o (igual a ""), indicar 'N' para o sinistro
        .processa_reintegracao_is = IIf(Avisos(indice).ProcessaReintegracaoIs = "", "N", Avisos(indice).ProcessaReintegracaoIs)
        If bSemProposta Then
            'Alterado por Cleber da Stefanini - data: 24/05/2005
            'sSituacao_Aviso = "L"
            ssituacao_aviso = "N"
            .tp_log_sinistro_id = 2
            '***************************************************
            'Add_Tp_Log_Sinistro 2
        End If
        
        If Avisos(indice).SituacaoProposta = "Em Estudo" Then
            ssituacao_aviso = "L"
            'Alterado por Cleber da Stefanini - data: 24/05/2005
            .tp_log_sinistro_id = 1
            '***************************************************
            'Add_Tp_Log_Sinistro 1
        End If

        .NumRemessa = .Busca_Numero_Remessa
        indiceProposta = getIndiceProposta(Avisos(indice).Proposta)
        If indiceProposta <> 0 Then
            .Ind_reanalise = IIf(Propostas(indiceProposta).TipoAviso = "REANALISE", "S", "N")
        Else
            .Ind_reanalise = "N"
        End If
        If Propostas.Count > 0 Then
        ' Odil Rincon Junior || GPTI - 23/03/2010 - Flow: 1113817
            If Propostas(indiceProposta).TipoAviso = "INC_OCORRENCIA" Then
            'If sProdutoAgricola = "INC_OCORRENCIA" Then 'asouza
            ' fim Odil Rincon Junior || GPTI - 23/03/2010 - Flow: 1113817
            
                .InclusaoOcorAgricola = "S"
                .ReanaliseAgricola = "N"
                
                '' Laurent Silva - 18/07/2018 - Demanda de faturamento pecu�rio
                '' N�o chamar a tela de questionario caso os eventos sejam da cobertura b�sica - faturamento.
                If frmObjSegurado.Evento_Cobertura_Basica_Faturamento_Pecuario = True Then
                    .sinistro_id = .Busca_Numero_Sinistro(CStr(.ramo_id), CStr(.Sucursal_id), .Seguradora_id, .Proposta_id)
                    .qtdMachos = Val(frmObjSegurado.Txt_Qtd_Machos(0).Text)
                    .qtdFemeas = Val(frmObjSegurado.Txt_Qtd_Femeas(0).Text)
                Else
                        .sinistro_id = txtSinistro
                End If
                
            Else
            .InclusaoOcorAgricola = "N"
            .ReanaliseAgricola = .Ind_reanalise
            If .Ind_reanalise = "S" Then
                .sinistro_id = sSinistro_id
                .MotivoReanaliseId = frmMotivoReanalise.Motivo_reanalise_id
                Call CarregaMotivo(Motivo)
                'INICIO - MU00416975 - Reabertura de Sinistro - inc 02/04/2018
                Call .IncluirMotivo(Motivo, LinhasArray)
                'FIM - MU00416975 - Reabertura de Sinistro -
            Else
            
                '' Laurent Silva - 18/07/2018 - Demanda de faturamento pecu�rio
                '' N�o chamar a tela de questionario caso os eventos sejam da cobertura b�sica - faturamento.
                ''Thiago - ConfitecRj - 2019/05 - Demanda N�o pedir numero de animais para evento 430 > inclus�o eventoID
                If frmObjSegurado.Evento_Cobertura_Basica_Faturamento_Pecuario = True And EventoId <> 430 Then
                    .sinistro_id = .Busca_Numero_Sinistro(CStr(.ramo_id), CStr(.Sucursal_id), .Seguradora_id, .Proposta_id)
                    .qtdMachos = Val(frmObjSegurado.Txt_Qtd_Machos(0).Text)
                    .qtdFemeas = Val(frmObjSegurado.Txt_Qtd_Femeas(0).Text)
                Else
                        .sinistro_id = .Busca_Numero_Sinistro(CStr(.ramo_id), CStr(.Sucursal_id), .Seguradora_id)
                End If
            
                
            End If
        End If
     
        Else
            .InclusaoOcorAgricola = "N"
            .ReanaliseAgricola = .Ind_reanalise
            If .Ind_reanalise = "S" Then
                .sinistro_id = sSinistro_id
                .MotivoReanaliseId = frmMotivoReanalise.Motivo_reanalise_id
                Call CarregaMotivo(Motivo)
            'INICIO - MU00416975 - Reabertura de Sinistro - inc 02/04/2018
                Call .IncluirMotivo(Motivo, LinhasArray)
            'FIM - MU00416975 - Reabertura de Sinistro
            Else
                .sinistro_id = .Busca_Numero_Sinistro(CStr(.ramo_id), CStr(.Sucursal_id), .Seguradora_id)
            End If
        End If
        
        sCoberturasJaInseridas = ""
        dVal_Informado = 0
        
        '---Insere o objeto sinistrado e as coberturas afetadas
        cod_sinistrado_atual = .IncluirObjetoSinistrado(Avisos(indice).ObjetoSegurado, "")
                
        lVlrEstimativa = Avisos(indice).ValorEstimativa
        dVal_Informado = dVal_Informado + Avisos(indice).valor_prejuizo
        
        'Diego Galizoni Caversan - GPTI
        'Buscando cobertura_bb
        estim_cobertura = getEstimativa(Avisos(indice).Proposta, Avisos(indice).Cobertura, frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex), AvisoSinistro.subcausa_id, 1, AvisoSinistro.dt_Ocorrencia)
        'fim
        'carrega as coberturas na DLL
        If Not .IncluirCobertura(Avisos(indice).Cobertura, _
                                Avisos(indice).DescricaoCobertura, _
                                CDbl(lVlrEstimativa), _
                                cod_sinistrado_atual, _
                                CDbl(Avisos(indice).valor_prejuizo), _
                                Avisos(indice).DescricaoCobertura, _
                                estim_cobertura.cobertura_bb) Then GoTo TrataErro
            
        .Val_Informado = dVal_Informado
        
        'Inclui outros seguros
        For i = 1 To grdOutrosSeg.Rows - 1
            If Trim(grdOutrosSeg.TextMatrix(i, 1)) <> "" Then
                If Not .IncluirOutrosSeguros(grdOutrosSeg.TextMatrix(i, 1), grdOutrosSeg.TextMatrix(i, 2)) Then GoTo TrataErro
            End If
        Next i
        
        'inclui documentos
        'Se n�o for cobertura generica, obtem os documentos de acordo com o produto, ramo, evento e cobertura
        Set AvisoRegras = CreateObject("SEGL0144.cls00326")
    
        AvisoRegras.mvarAmbienteId = glAmbiente_id
        AvisoRegras.mvarSiglaSistema = gsSIGLASISTEMA
        AvisoRegras.mvarSiglaRecurso = App.Title
        AvisoRegras.mvarDescricaoRecurso = App.FileDescription
        
        Set rsPesquisa = AvisoRegras.ObterDocumentos(.produto_id, _
                                                    .ramo_id, _
                                                    .subramo_id, _
                                                    CStr(tipo_ramo), _
                                                    CLng(.Causa_id), _
                                                    Avisos(indice).Cobertura, True)
                                                   
        Set AvisoRegras = Nothing
        
        If Not rsPesquisa.EOF Then
               
            Do While Not rsPesquisa.EOF
                If Not .IncluirDocumento(rsPesquisa("documento_id"), rsPesquisa("documento")) Then GoTo TrataErro
                rsPesquisa.MoveNext
            Loop
        Else
        
            'sergio.sn - 25/11/2010
            'CASO N�O RECUPERE DOCUMENTOS ADICIONA PELO Formul�rio ""aviso de sinistro"" assinado
            .IncluirDocumento "1", "Formul�rio ""aviso de sinistro"" assinado"
            'sergio.en
            
            'Alterado por Cleber da Stefanini - data: 24/05/2005
            ssituacao_aviso = "L"
            .tp_log_sinistro_id = 4
            '***************************************************
            'Add_Tp_Log_Sinistro 4
        End If
        rsPesquisa.Close
        
        Call CarregaObs(Obs)
        Call .IncluirObservacoes(Obs)
        
        'Para as informa��es finais das propostas
        frmAvisosRealizados.sinistro_id = .sinistro_id
        frmAvisosRealizados.Proposta_id = IIf(bSemProposta, "Sem proposta", .Proposta_id)
        frmAvisosRealizados.produto_nome = IIf(bSemProposta, "Sem produto espec�fico", .NomeProduto)
        
        frmDocumentos.produto_id = IIf(.produto_id = "", 0, .produto_id)
        frmDocumentos.ramo_id = .ramo_id
        frmDocumentos.subramo_id = lSubramo_id
        frmDocumentos.evento_sinistro_id = .Causa_id
        frmDocumentos.sCoberturas = sCoberturasJaInseridas
        
        .SituacaoAviso = ssituacao_aviso
        
        'Paulo Pelegrini - MU-2017-043439 - 11/05/2018 (INI)
        If CDbl(Avisos(indice).valor_prejuizo) > 0 And .Ind_reanalise = "N" Then
            'add produto 1240 -> 00421597-seguro-faturamento-pecuario
            'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
            'ADICIONADO PRODUTOS 230
            If .produto_id = 8 Or .produto_id = 14 Or .produto_id = 155 Or .produto_id = 156 Or .produto_id = 227 Or .produto_id = 228 Or .produto_id = 229 Or .produto_id = 300 Or .produto_id = 701 Or .produto_id = 722 Or .produto_id = 1204 Or .produto_id = 1240 Or .produto_id = 230 Then

                SQL = "Select IsNull(perc_estimativa,0) " & vbNewLine
                SQL = SQL & " From produto_estimativa_sinistro_tb " & vbNewLine
                SQL = SQL & " Where produto_id = " & .produto_id & vbNewLine
                If lSubramo_id > 0 Then
                    SQL = SQL & " And subramo_id = " & lSubramo_id & vbNewLine
                End If
                SQL = SQL & " And tp_cobertura_id = " & Avisos(indice).Cobertura & vbNewLine
                SQL = SQL & " And evento_sinistro_id = " & .Causa_id & vbNewLine
                SQL = SQL & " And utiliza_percentual_subevento = 'S' " & vbNewLine
                SQL = SQL & " And dt_fim_vigencia Is Null " & vbNewLine

                Set rsPesquisa = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

                If Not rsPesquisa.EOF Then
                    .Val_PercReduc = rsPesquisa(0)
                End If

                rsPesquisa.Close

            End If
        End If
        'Paulo Pelegrini - MU-2017-043439 - 11/05/2018 (FIM)
        
    End With
    
    Exit Sub
    Resume
TrataErro:
    Call TrataErroGeral("ObtemDadosSinistros", "Aviso de Sinistro")
    Resume Next
    Exit Sub

End Sub

Private Function GravaBenefTemp(objSinistro) As Boolean

    Dim strSql As String
    Dim numCon As Long
    Dim iRow As Integer
    Dim mvarSQL As String
    Dim AvisoSinistro As Object
    Dim Rs As ADODB.Recordset
    Dim RsProposta As ADODB.Recordset
    Dim Benef As BenefSinistro
    Dim Sinistro_Benef As Sinist
    Dim sinteste As SinistroBB

    '-------------------------------------------------------------------------------------------------------
    'Cadastro de Benefici�rios.
    'Durante o aviso de sinistro, o usu�rio poder� cadastrar benefici�rios.
    'Carlos de O. Cruz - CWI 02/03/2011.
    '-------------------------------------------------------------------------------------------------------
    Set Benef = New BenefSinistro

    On Error GoTo Error
    GravaBenefTemp = False

    numCon = AbrirConexao(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription)

    If AbrirTransacao(numCon) Then

'       Set Sinistro_Benef = New Sinist
'       With Sinistro_Benef
'             'strSQL = "set nocount on exec busca_proposta_sinistro_sps '" & frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.Row, 2) & "',1,'"
'             'If Len(frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.Row, 1)) > 0 Then strSQL = strSQL & frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.Row, 1)
'             'strSQL = strSQL & "', " & frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.Row, 3)
'             'Set RsProposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSQL, True)

'            strSQL = "Select p.proposta_id, a.apolice_id, a.sucursal_seguradora_id, a.seguradora_cod_susep, a.ramo_id " & _
'                     "  From cliente_tb c (nolock) " & _
'                     "  LEFT join pessoa_fisica_tb pf (nolock) " & _
'                     "    ON c.cliente_id = pf.pf_cliente_id " & _
'                     "  LEFT join pessoa_juridica_tb pj (nolock) " & _
'                     "    ON c.cliente_id = pj.pj_cliente_id " & _
'                     " INNER join proposta_tb p (nolock) " & _
'                     "    ON c.cliente_id = p.prop_cliente_id " & _
'                     " INNER join apolice_tb a (nolock) " & _
'                     "    ON p.proposta_id = a.proposta_id" & _
'                     " Where c.cliente_id = " & frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.Row, 3) & _
'                     "   and p.situacao <> 't'"

'            Set RsProposta = ExecutarSQL(gsSIGLASISTEMA, _
'                                         glAmbiente_id, _
'                                         App.Title, _
'                                         App.FileDescription, _
'                                         strSQL, _
'                                         True)

'            .Apolice_id = RsProposta(1) 'Ap�lice
'            .Sucursal_id = RsProposta(2) 'Sucursal
'            .Seguradora_id = RsProposta(3) 'Seguradora
'            .ramo_id = RsProposta(4) 'Ramo
'            Call Busca_Numero_Sinistro(Sinistro_Benef)
'       End With

       For Each Benef In frmBeneficiarios.BeneficiariosSinistro
            With Benef

                .tp_Documento = IIf(Len(.tp_Documento) = 0, "99", .tp_Documento)
                mvarSQL = "EXEC SEGS9470_SPI " & _
                        objSinistro.sinistro_id & "," & _
                        IIf(Len(objSinistro.Apolice_id) = 0, "NULL", objSinistro.Apolice_id) & "," & _
                        IIf(Len(objSinistro.Sucursal_id) = 0, "NULL", objSinistro.Sucursal_id) & "," & _
                        IIf(Len(objSinistro.Seguradora_id) = 0, "NULL", objSinistro.Seguradora_id) & "," & _
                        IIf(Len(objSinistro.ramo_id) = 0, "NULL", objSinistro.ramo_id) & "," & _
                        IIf(.tp_Documento <> "" And .tp_Documento < 256, .tp_Documento, "99") & ",'" & _
                        .Nome & "',"
                mvarSQL = mvarSQL & _
                        IIf(.dt_Nascimento <> "", "'" & _
                            Format(.dt_Nascimento, "yyyymmdd") & "'", "NULL") & ","
                mvarSQL = mvarSQL & _
                        IIf(.Nome_Responsavel <> "", "'" & _
                            .Nome_Responsavel & "'", "NULL") & "," & _
                        "'" & .tp_Responsavel & "'," & _
                        IIf(.Orgao <> "", "'" & .Orgao & "'", "NULL") & "," & _
                        IIf(.numero <> "", "'" & .numero & "'", "NULL") & "," & _
                        IIf(.Serie <> "", "'" & .Serie & "'", "NULL") & "," & _
                        IIf(.Complemento <> "", "'" & .Complemento & "'", "NULL") & "," & _
                        IIf(.Banco <> "", .Banco, "NULL") & "," & _
                        IIf(.Agencia <> "", .Agencia, "NULL") & "," & _
                        IIf(.Agencia_dv <> "", "'" & .Agencia_dv & "'", "NULL") & "," & _
                        IIf(.Conta <> "", .Conta, "NULL") & "," & _
                        IIf(.Conta_dv <> "", "'" & .Conta_dv & "'", "NULL") & ","
                mvarSQL = mvarSQL & _
                        IIf(.Sexo <> "", "'" & Left(.Sexo, 1) & "'", "'n'") & "," & _
                        IIf(.CGC <> "", "'" & LimpaMascara(.CGC) & "'", "NULL") & "," & _
                        IIf(.CPF <> "", "'" & LimpaMascara(.CPF) & "'", "NULL") & ","
                mvarSQL = mvarSQL & _
                        "'" & cUserName & "'"
            End With

            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             mvarSQL, _
                             glConexao, _
                             False, _
                             False, _
                             30000, _
                             adLockOptimistic, _
                             adUseClient)
       Next

       ConfirmarTransacao (numCon)
       GravaBenefTemp = True

       Call LimparColecao(frmBeneficiarios.BeneficiariosSinistro)
       Call LimparColecao(frmBeneficiarios.BeneficiariosSinistroExc)

       Set Benef = Nothing
       Set Sinistro_Benef = Nothing
    End If

Exit Function

Error:
    Call TrataErroGeral("Grava Beneficiario", "Aviso de Sinistro")
    RetornarTransacao (numCon)

End Function

Public Sub ObtemDadosSinistro(ByVal nProposta As Long, ByRef nSinistro As String, indice As Integer)
   
   Dim i As Long
   Dim Apolice_id As String
   
   Dim sucursal As String
   Dim seguradora As String
   Dim Moeda As Integer
   Dim Motivo(5) As String
   Dim Obs(10) As String
   
   'para reanalise
   Dim sinistro_id_ant As String
   Dim sinistro_bb_id As String
   Dim SQL As String
   Dim Rs As ADODB.Recordset
   Dim rsPesquisa As ADODB.Recordset
   Dim dVlrEstimativa As Double
   Dim sCoberturasJaInseridas As String
   
   Dim cod_sinistrado_atual As Integer
   Dim cod As Integer
   'Dim indice As Integer
   Dim nPropostaBB As Long
   Dim nProdutoID As Integer
   
   Dim str_Coberturas As String
   
   Dim AvisoRegras As Object
   
   ReDim Preserve iTp_Log_Sinistro(0)
   
   bExisteTpLogSinistro = False
   
   On Error GoTo TrataErro
   
   If AvisoSinistro Is Nothing Then Set AvisoSinistro = CreateObject("SEGL0144.cls00175")
       
   Call ConsultarDadosProposta(CDbl(nProposta), AvisoSinistro, nSinistro, indice)
   
Exit Sub
TrataErro:
   Call TrataErroGeral("ObtemDadosSinistro(x)", "Aviso de Sinistro")
   Exit Sub
End Sub

'AKIO.OKUNO - Envia_EMail_Sem_Doc - 17860335 - 26/06/2013
'Public Function Envia_email_Sem_Doc() As Boolean
'Dim rsEmail As New ADODB.Recordset
'Dim msg As String
'Dim email As String
'Dim SQL As String
'
'On Error GoTo Erro
'
'Envia_email_Sem_Doc = False
''Criado por Cleber da Stefanini - data: 24/05/2005
''Envio de email para a DITEC se for tipo 4(sem documentacao)
'
'If AvisoSinistro.tp_log_sinistro_id = 4 Then
'    SQL = "select valor from controle_sistema_db..parametro_tb with (nolock) where "
'    SQL = SQL & " sigla_sistema = 'SEGBR' and secao = 'SINISTRO' and campo = 'EMAIL_DOCUMENTACAO' "
'
'    Set rsEmail = ExecutarSQL(gsSIGLASISTEMA, _
'        glAmbiente_id, _
'        App.Title, _
'        App.FileDescription, _
'        SQL, _
'        True)
'    email = rsEmail("valor")
'    rsEmail.Close
'
'    SQL = " select distinct r.ramo_id, r.nome nome_ramo, "
'    SQL = SQL & " pro.produto_id, pro.nome nome_prod, "
'    SQL = SQL & " sr.subramo_id, sr.nome nome_sub, "
'    SQL = SQL & " ev.Evento_sinistro_id , ev.nome nome_ev "
'    SQL = SQL & " from    proposta_tb p                 with (nolock) "
'    SQL = SQL & " inner join ramo_tb r                  with (nolock) on r.ramo_id = p.ramo_id "
'    SQL = SQL & " inner join subramo_tb sr              with (nolock) on sr.subramo_id = p.subramo_id "
'    SQL = SQL & " inner join produto_tb pro             with (nolock) on pro.produto_id = p.produto_id "
'    SQL = SQL & " inner join evento_segbr_sinistro_tb s with (nolock) on s.proposta_id = p.proposta_id "
'    SQL = SQL & " inner join evento_sinistro_tb ev      with (nolock) on ev.evento_sinistro_id = s.evento_sinistro_id "
'    SQL = SQL & " Where p.proposta_id = " & AvisoSinistro.Proposta_id
'    SQL = SQL & " and     s.sinistro_id = " & AvisoSinistro.sinistro_id
'
'    Set rsEmail = ExecutarSQL(gsSIGLASISTEMA, _
'                                glAmbiente_id, _
'                                App.Title, _
'                                App.FileDescription, _
'                                SQL, _
'                                True)
'
'    'corpo da msg
'    msg = ""
'    msg = Chr(10)
'    msg = msg & "O Sinistro " & sinistro_id & " n�o foi enviado para o BB, por falta do cadastro da documenta��o." & Chr(10)
'    msg = msg & "As informa��es do Sinistro s�o: " & Chr(10) & Chr(10)
''                        msg = msg & "Produto: " & IIf(Produto_Id = "" Or IsNull(Produto_Id), 0, Produto_Id) & " - " & IIf(rsEmail("nome_prod") = "" Or IsNull(rsEmail("nome_prod")), "", rsEmail("nome_prod")) & Chr(10)
'    If rsEmail.EOF Then
'        msg = msg & "Produto: - " & Chr(10)
'        msg = msg & "Ramo: - " & Chr(10)
'        msg = msg & "Sub-ramo: - " & Chr(10)
'        msg = msg & "Evento sinistro: - " & Chr(10) & Chr(10)
'    Else
'        If Not IsNull(rsEmail("produto_id")) Then
'            msg = msg & "Produto: " & rsEmail("produto_id") & " - " & rsEmail("nome_prod") & "" & Chr(10)
'        Else
'            msg = msg & "Produto: - " & Chr(10)
'        End If
'        If Not IsNull(rsEmail("ramo_id")) Then
'            msg = msg & "Ramo: " & rsEmail("ramo_id") & " - " & rsEmail("nome_ramo") & "" & Chr(10)
'        Else
'            msg = msg & "Ramo: - " & Chr(10)
'        End If
'        If Not IsNull(rsEmail("subramo_id")) Then
'            msg = msg & "SubRamo: " & rsEmail("subramo_id") & " - " & rsEmail("nome_sub") & "" & Chr(10)
'        Else
'            msg = msg & "Sub-ramo: - " & Chr(10)
'        End If
'        If Not IsNull(rsEmail("Evento_sinistro_id")) Then
'            msg = msg & "Evento sinistro: " & rsEmail("Evento_sinistro_id") & " - " & rsEmail("nome_ev") & "" & Chr(10) & Chr(10)
'        Else
'            msg = msg & "Evento sinistro: - " & Chr(10) & Chr(10)
'        End If
'    End If
'    msg = msg & "Ap�s o cadastro, favor contactar a �rea respons�vel pelo sinistro enviado para o LOG. " & Chr(10)
'
'    SQL = " exec envia_email_sp '" & email & "' , 'Sinistro RE sem Documenta��o', '" & msg & "'"
'    rsEmail.Close
'
'    Call ExecutarSQL(gsSIGLASISTEMA, _
'                    glAmbiente_id, _
'                    App.Title, _
'                    App.FileDescription, _
'                    SQL, _
'                    False)

Public Function Envia_email_Sem_Doc() As Boolean
Dim rsEmail As New ADODB.Recordset
Dim msg As String
Dim email As String
Dim SQL As String

On Error GoTo Erro

Envia_email_Sem_Doc = False
'Criado por Cleber da Stefanini - data: 24/05/2005
'Envio de email para a DITEC se for tipo 4(sem documentacao)

If AvisoSinistro.tp_log_sinistro_id = 4 Then
    
    
    SQL = "select valor from controle_sistema_db..parametro_tb with (nolock) where "
    SQL = SQL & " sigla_sistema = 'SEGBR' and secao = 'SINISTRO' and campo = 'EMAIL_DOCUMENTACAO' "

    Set rsEmail = ExecutarSQL(gsSIGLASISTEMA, _
        glAmbiente_id, _
        App.Title, _
        App.FileDescription, _
        SQL, _
        True)
    email = rsEmail("valor")
    rsEmail.Close

    SQL = " select distinct r.ramo_id, r.nome nome_ramo, "
    SQL = SQL & " pro.produto_id, pro.nome nome_prod, "
    SQL = SQL & " sr.subramo_id, sr.nome nome_sub, "
    SQL = SQL & " ev.Evento_sinistro_id , ev.nome nome_ev "
    SQL = SQL & " from    proposta_tb p                 with (nolock) "
    SQL = SQL & " inner join ramo_tb r                  with (nolock) on r.ramo_id = p.ramo_id "
    SQL = SQL & " inner join subramo_tb sr              with (nolock) on sr.subramo_id = p.subramo_id "
    SQL = SQL & " inner join produto_tb pro             with (nolock) on pro.produto_id = p.produto_id "
    SQL = SQL & " inner join evento_segbr_sinistro_tb s with (nolock) on s.proposta_id = p.proposta_id "
    SQL = SQL & " inner join evento_sinistro_tb ev      with (nolock) on ev.evento_sinistro_id = s.evento_sinistro_id "
    SQL = SQL & " Where p.proposta_id = " & AvisoSinistro.Proposta_id
    SQL = SQL & " and     s.sinistro_id = " & AvisoSinistro.sinistro_id

    Set rsEmail = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, _
                                True)
                        
    'corpo da msg
    msg = ""
    msg = Chr(10)
    msg = msg & "O Sinistro " & sinistro_id & " n�o foi enviado para o BB, por falta do cadastro da documenta��o." & Chr(10)
    msg = msg & "As informa��es do Sinistro s�o: " & Chr(10) & Chr(10)
'                        msg = msg & "Produto: " & IIf(Produto_Id = "" Or IsNull(Produto_Id), 0, Produto_Id) & " - " & IIf(rsEmail("nome_prod") = "" Or IsNull(rsEmail("nome_prod")), "", rsEmail("nome_prod")) & Chr(10)
    If rsEmail.EOF Then
        msg = msg & "Produto: - " & Chr(10)
        msg = msg & "Ramo: - " & Chr(10)
        msg = msg & "Sub-ramo: - " & Chr(10)
        msg = msg & "Evento sinistro: - " & Chr(10) & Chr(10)
    Else
        If Not IsNull(rsEmail("produto_id")) Then
            msg = msg & "Produto: " & rsEmail("produto_id") & " - " & rsEmail("nome_prod") & "" & Chr(10)
        Else
            msg = msg & "Produto: - " & Chr(10)
        End If
        If Not IsNull(rsEmail("ramo_id")) Then
            msg = msg & "Ramo: " & rsEmail("ramo_id") & " - " & rsEmail("nome_ramo") & "" & Chr(10)
        Else
            msg = msg & "Ramo: - " & Chr(10)
        End If
        If Not IsNull(rsEmail("subramo_id")) Then
            msg = msg & "SubRamo: " & rsEmail("subramo_id") & " - " & rsEmail("nome_sub") & "" & Chr(10)
        Else
            msg = msg & "Sub-ramo: - " & Chr(10)
        End If
        If Not IsNull(rsEmail("Evento_sinistro_id")) Then
            msg = msg & "Evento sinistro: " & rsEmail("Evento_sinistro_id") & " - " & rsEmail("nome_ev") & "" & Chr(10) & Chr(10)
        Else
            msg = msg & "Evento sinistro: - " & Chr(10) & Chr(10)
        End If
    End If
    msg = msg & "Ap�s o cadastro, favor contactar a �rea respons�vel pelo sinistro enviado para o LOG. " & Chr(10)

    SQL = " exec envia_email_sp '" & email & "' , 'Sinistro RE sem Documenta��o', '" & msg & "'"
    rsEmail.Close
    
    Call ExecutarSQL(gsSIGLASISTEMA, _
                    glAmbiente_id, _
                    App.Title, _
                    App.FileDescription, _
                    SQL, _
                    False)

End If
    
Envia_email_Sem_Doc = True
    
Exit Function

Erro:
    Envia_email_Sem_Doc = False
    
End Function




'
'End If
'
'Envia_email_Sem_Doc = True
'
'Exit Function
'
'Erro:
'    Envia_email_Sem_Doc = False
'
'End Function
'
'
'
Private Sub CarregaMotivo(ByRef Motivo() As String)
Dim i As Integer

    'Obtem a descri��o do motivo de reanalise para os casos exceto o "Outros"
    If frmMotivoReanalise.Motivo_reanalise_id <> 3 Then
        Motivo(0) = frmMotivoReanalise.cboMotivoReanalise.Text
        'INICIO - MU00416975 - Reabertura de Sinistro - inc 02/04/2018
        LinhasArray = 4
        If produtosreanaliseok Then
        LinhasArray = 5
            For i = 1 To 5
                Motivo(i) = frmMotivoReanalise.txtMotivo(i - 1)
            Next i
        End If
        'FIM - MU00416975 - Reabertura de Sinistro
    Else
        For i = 0 To 4
            Motivo(i) = frmMotivoReanalise.txtMotivo(i)
        Next i
    End If
End Sub

Private Sub CarregaObs(ByRef Obs() As String)
Dim i As Integer
    For i = 0 To 9
        Obs(i) = txtExigencia(i)
    Next i
End Sub


Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub


Private Sub mskTelefoneVistoria_LostFocus(Index As Integer)
    '30/05/2012 : Nova - Mauro Vianna : Altera��o de telefone para 9 d�gitos : tratamento de 8 d�gitos
    If Right(mskTelefoneVistoria(Index).Text, 1) = "_" And Right(mskTelefoneVistoria(Index).Text, 2) <> "__" Then
        mskTelefoneVistoria(Index).Text = " " & Left(mskTelefoneVistoria(Index).Text, 4) + "-" + Mid(mskTelefoneVistoria(Index).Text, 5, 1) + Mid(mskTelefoneVistoria(Index).Text, 7, 3)
    End If
End Sub


Function valida_pecuario_agravamento(Proposta_id)

    valida_pecuario_agravamento = False
    On Error GoTo Trata_Erro

    Dim SQL As String
    Dim Rs As ADODB.Recordset
    Dim i As Integer
    
    SQL = ""
    SQL = "SET nocount on " & Chr(10)
    SQL = SQL & " select s.sinistro_id" & Chr(10)
    SQL = SQL & "from seguros_db.dbo.evento_segbr_sinistro_atual_tb s with(nolock)" & Chr(10)
    SQL = SQL & "inner join seguros_db.dbo.evento_SEGBR_sinistro_cobertura_tb c with(nolock)" & Chr(10)
    SQL = SQL & "        on s.evento_id = c.evento_id" & Chr(10)
    SQL = SQL & "where s.proposta_id = " & CStr(Proposta_id) & Chr(10)
    SQL = SQL & "and c.tp_cobertura_id = 1169" & Chr(10)
    
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    If Not Rs.EOF Then
        If frmObjSegurado.Evento_Cobertura_Basica_Faturamento_Pecuario = True Then
            valida_pecuario_agravamento = True
        End If
    End If
    
    Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "frmAviso.valida_pecuario_agravamento - " & Err.Description)
    
End Function

