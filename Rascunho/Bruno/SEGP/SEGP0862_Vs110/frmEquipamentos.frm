VERSION 5.00
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GRIDDINAMICO.OCX"
Begin VB.Form frmEquipamentos 
   Caption         =   "Equipamentos"
   ClientHeight    =   9030
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11700
   LinkTopic       =   "Form1"
   ScaleHeight     =   9030
   ScaleWidth      =   11700
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "Descri��o do Sinistro"
      Height          =   2130
      Left            =   0
      TabIndex        =   7
      Top             =   120
      Width           =   11730
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1680
         Index           =   0
         Left            =   90
         MaxLength       =   70
         TabIndex        =   8
         Top             =   240
         Width           =   11325
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Equipamentos Danificados"
      Height          =   3405
      Left            =   0
      TabIndex        =   3
      Top             =   2280
      Width           =   11700
      Begin VB.Frame frameLaudo 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   4800
         TabIndex        =   21
         Top             =   1080
         Width           =   2415
         Begin VB.OptionButton optLaudoSim 
            Caption         =   "Sim"
            Height          =   255
            Left            =   0
            TabIndex        =   23
            Top             =   360
            Width           =   735
         End
         Begin VB.OptionButton optLaudoNao 
            Caption         =   "N�o"
            Height          =   255
            Left            =   720
            TabIndex        =   22
            Top             =   360
            Width           =   735
         End
         Begin VB.Label lblLaudo 
            Caption         =   "J� realizou Laudo: "
            Height          =   255
            Left            =   0
            TabIndex        =   24
            Top             =   120
            Width           =   1455
         End
      End
      Begin VB.Frame frameDano 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   480
         TabIndex        =   17
         Top             =   1080
         Width           =   3375
         Begin VB.OptionButton Option 
            Caption         =   "Perda Total"
            Height          =   255
            Left            =   1920
            TabIndex        =   19
            Top             =   360
            Width           =   1215
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Pass�vel de reparo"
            Height          =   255
            Left            =   120
            TabIndex        =   18
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label lblTipoDano 
            Caption         =   "Dano:"
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   120
            Width           =   615
         End
      End
      Begin VB.TextBox txtTelefoneAssistencia 
         Height          =   285
         Left            =   600
         TabIndex        =   14
         Text            =   "txtTelefoneAssistencia"
         Top             =   2760
         Width           =   2295
      End
      Begin VB.TextBox txtNomeAssistencia 
         Height          =   285
         Left            =   600
         TabIndex        =   13
         Text            =   "txtNomeAssistencia"
         Top             =   2040
         Width           =   3495
      End
      Begin VB.TextBox txtEstimativaPrejuizo 
         Height          =   285
         Left            =   6480
         TabIndex        =   11
         Text            =   "txtEstimativaPrejuizo"
         Top             =   2520
         Width           =   2415
      End
      Begin VB.ComboBox cboModelo 
         Height          =   315
         Left            =   4800
         TabIndex        =   9
         Text            =   "cboEquipamentos"
         Top             =   720
         Width           =   3615
      End
      Begin VB.ComboBox cboTipoBem 
         Height          =   315
         Left            =   600
         TabIndex        =   5
         Text            =   "cboEquipamentos"
         Top             =   720
         Width           =   3615
      End
      Begin VB.CommandButton btnAddEquipamento 
         Caption         =   "Adiconar"
         Height          =   375
         Left            =   9240
         TabIndex        =   4
         Top             =   2400
         Width           =   1815
      End
      Begin VB.Label lblTelefoneAssistencia 
         Caption         =   "Telefone da Assist�ncia:"
         Height          =   255
         Left            =   600
         TabIndex        =   16
         Top             =   2520
         Width           =   1815
      End
      Begin VB.Label lblNomeAssistencia 
         Caption         =   "Nome da Assist�ncia:"
         Height          =   255
         Left            =   600
         TabIndex        =   15
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label lblEstimativaPrejuizo 
         Caption         =   "Estimativa do Preju�zo:"
         Height          =   255
         Left            =   6480
         TabIndex        =   12
         Top             =   2280
         Width           =   2055
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Modelo"
         Height          =   195
         Index           =   0
         Left            =   4800
         TabIndex        =   10
         Top             =   480
         Width           =   525
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Bem"
         Height          =   195
         Left            =   600
         TabIndex        =   6
         Top             =   480
         Width           =   900
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   9960
      TabIndex        =   2
      Top             =   8400
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   8520
      TabIndex        =   1
      Top             =   8400
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   7080
      TabIndex        =   0
      Top             =   8400
      Width           =   1275
   End
   Begin GridFrancisco.GridDinamico grdEquipamentos 
      Height          =   2385
      Left            =   0
      TabIndex        =   25
      Top             =   5760
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   4207
      BorderStyle     =   1
      AllowUserResizing=   3
      EditData        =   0   'False
      Highlight       =   1
      ShowTip         =   0   'False
      SortOnHeader    =   0   'False
      BackColor       =   -2147483643
      BackColorBkg    =   -2147483633
      BackColorFixed  =   -2147483633
      BackColorSel    =   -2147483635
      FixedCols       =   1
      FixedRows       =   1
      FocusRect       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      ForeColorFixed  =   -2147483630
      ForeColorSel    =   -2147483634
      GridColor       =   -2147483630
      GridColorFixed  =   12632256
      GridLine        =   1
      GridLinesFixed  =   2
      MousePointer    =   0
      Redraw          =   -1  'True
      Rows            =   2
      TextStyle       =   0
      TextStyleFixed  =   0
      Cols            =   2
      RowHeightMin    =   0
   End
End
Attribute VB_Name = "frmEquipamentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'C00210408 - Esteira Flash - Sinistros de Danos El�tricos Residenciais



Private Sub Form_Load()

    CarregarCboTipoBem
    
    cboModelo.Text = ""
    
    MontaCabecalhoGrdEquipamentos
    
   
End Sub

Private Sub cboTipoBem_Click()
  If cboTipoBem.ListIndex <> -1 Then
       CarregaCboModelo
    End If
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Private Sub cmdVoltar_Click()
    Me.Hide
    frmAvisoPropostas.Show
End Sub

Private Sub cmdContinuar_Click()

Me.Hide
frmObjSegurado.Show
End Sub

Public Sub CarregarCboTipoBem()
 Dim SQL As String
    Dim Rs As ADODB.Recordset
    
 
        SQL = ""
        SQL = SQL & "exec desenv_db.dbo.comboTipoBem"

        Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

        With cboTipoBem
            .Clear
            If Not Rs.EOF Then
               While Not Rs.EOF
                     .AddItem Trim(Rs(1))
                     .ItemData(.NewIndex) = Rs(0)
                     Rs.MoveNext
               Wend
            End If
            Rs.Close
        End With

End Sub

Public Sub MontaCabecalhoGrdEquipamentos()
With grdEquipamentos
        .Cols = 5
        
        .TextMatrix(0, 0) = "Tipo de Bem"
        .TextMatrix(0, 1) = "Modelo"
        .TextMatrix(0, 2) = "Dano"
        .TextMatrix(0, 3) = "Estimativa do prejuizo"
        .TextMatrix(0, 4) = "J� realizou laudo"

        .AutoFit H�brido
    End With
End Sub

Public Sub CarregaCboModelo()


Dim SQL As String
    Dim Rs As ADODB.Recordset
    
 
        SQL = ""
        SQL = SQL & "exec desenv_db.dbo.comboModelo 1"

        Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

        With cboModelo
            .Clear
            If Not Rs.EOF Then
               While Not Rs.EOF
                     .AddItem Trim(Rs(1))
                     .ItemData(.NewIndex) = Rs(0)
                     Rs.MoveNext
               Wend
            End If
            Rs.Close
        End With


End Sub
