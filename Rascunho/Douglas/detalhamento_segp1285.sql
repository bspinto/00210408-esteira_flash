
 set nocount on 
 if OBJECT_ID('tempdb..#Sinistro_Principal') IS NOT NULL 
 BEGIN
     DROP TABLE #Sinistro_Principal
 END
Create Table #Sinistro_Principal      ( Sinistro_ID                                   Numeric(11)         
     , Sucursal_Seguradora_ID                        Numeric(5)          
     , Seguradora_Cod_Susep                          Numeric(5)          
     , Apolice_ID                                    Numeric(9)          
     , Ramo_ID                                       Int                 
     , Ramo_Nome                                     VarChar(60)         
     , Seguradora_Nome_Lider                         VarChar(60)         
     , Apolice_Dt_Inicio_Vigencia                    SmallDateTime       
     , Apolice_Dt_Fim_Vigencia                       SmallDateTime       
     , Apolice_Dt_Emissao                            SmallDateTime       
     , Proposta_ID                                   Numeric(9)          
     , Produto_ID                                    Int                 
     , Produto_Nome                                  VarChar(60)         
     , Certificado_Dt_Inicio_Vigencia                SmallDateTime       
     , Certificado_Dt_Fim_Vigencia                   SmallDateTime       
     , Certificado_Dt_Emissao                         SmallDateTime       
     , Certificado_ID                                Numeric(9)          
     , Certificado_Sub_Grupo_ID                      Int                 
     , Quantidade_Endossos                           Int                 
     , Proposta_Situacao                             VarChar(1)          
     , Proposta_Situacao_Descricao                   VarChar(60)         
     , Tp_Cobertura_Nome                             VarChar(60)         
     , Corretor_Nome                                 VarChar(60)         
     , Cliente_ID                                    Int                 
     , Cliente_Nome                                  VarChar(60)         
     , Tp_Componente_ID                              TinyInt             
     , Tp_Componente_Nome                            VarChar(60)         
     , Evento_Sinistro_ID                            Int                 
     , Evento_Sinistro_Nome                          VarChar(60)         
     , SubEvento_Sinistro_ID                         Int                 
     , SubEvento_Sinistro_Nome                       VarChar(120)         
     , Dt_Ocorrencia_Sinistro                        SmallDateTime       
     , Dt_Entrada_Seguradora                         SmallDateTime       
     , Dt_Aviso_Sinistro                             SmallDateTime       
     , Dt_Inclusao                                   SmallDateTIme       
     , Agencia_ID                                    Numeric(4)          
     , Sinistro_Vida_Nome                            VarChar(60)         
     , Sinistro_Vida_CPF                             VarChar(11)         
     , Sinistro_Vida_Dt_Nasc                         DateTime            
     , Sinistro_Vida_Sexo                            Char(1)             
     , Sinistrado_Cliente_ID                         Int                 
     , Solicitante_ID                                Int                 
     , Solicitante_Sinistro_Nome                     VarChar(60)         
     , Solicitante_Sinistro_Endereco                 VarChar(60)         
     , Solicitante_Sinistro_Bairro                   VarChar(30)         
     , Solicitante_Sinistro_Municipio_ID             Numeric(3)          
     , Solicitante_Sinistro_Municipio                VarChar(60)         
     , Solicitante_Municipio_Nome                    VarChar(60)         
     , Solicitante_Sinistro_DDD1                     VarChar(4)          
     , Solicitante_Sinistro_Telefone1                VarChar(9)          
     , Solicitante_Sinistro_DDD2                     VarChar(4)          
     , Solicitante_Sinistro_Telefone2                VarChar(9)          
     , Solicitante_Sinistro_DDD3                     VarChar(4)          
     , Solicitante_Sinistro_Telefone3                VarChar(9)          
     , Solicitante_Sinistro_DDD4                     VarChar(4)          
     , Solicitante_Sinistro_Telefone4                VarChar(9)          
     , Solicitante_Sinistro_DDD5                     VarChar(4)          
     , Solicitante_Sinistro_Telefone5                VarChar(9)          
     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)          
     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)          
     , Solicitante_Sinistro_Tp_Telefone1             Char(1)             
     , Solicitante_Sinistro_Tp_Telefone2             Char(1)             
     , Solicitante_Sinistro_Tp_Telefone3             Char(1)             
     , Solicitante_Sinistro_Tp_Telefone4             Char(1)             
     , Solicitante_Sinistro_Tp_Telefone5             Char(1)             
     , Solicitante_Sinistro_Ramal1                   Char(4)             
     , Solicitante_Sinistro_Ramal2                   Char(4)             
     , Solicitante_Sinistro_Ramal3                   Char(4)             
     , Solicitante_Sinistro_Ramal4                   Char(4)             
     , Solicitante_Sinistro_Ramal5                   Char(4)             
     , Solicitante_Sinistro_Estado                   Char(2)             
     , Solicitante_Sinistro_EMail                    VarChar(60)          
     , plano_nome                    VarChar(60)          
     , Solicitante_Sinistro_CEP                      VarChar(8)         
     , Sinistro_Causa_Observacao_Item                Int                 
     , plano_id                Int                 
     , Sinistro_Causa_Observacao                     VarChar(300)        
     , Sinistro_Situacao_ID                          Int                 
     , Sinistro_Situacao                             VarChar(60)         
     , Max_Endosso_ID                                Int                 
     , Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito          Numeric(30,0)           
     , Apolice_Terceiros_Seg_Cod_Susep_Lider                 Numeric(5,0)            
     , Apolice_Terceiros_Ramo_ID                             Int                     
     , Apolice_Terceiros_Sucursal_Seg_Lider                  Numeric(5,0)            
     , Cosseguro_Aceito_sinistro_id_lider                    Numeric(30)             
     , Cosseguro_Aceito_Num_Apolice_Lider                    Numeric(30,0)           
     , Cosseguro_Aceito_Ramo_Lider                           Int                     
     , Cosseguro_Aceito_Sucursal_Seg_Lider                   Numeric(5,0)            
     , Cosseguro_Aceito_Seg_Cod_Susep_Lider                  Numeric(5,0)            
     , Apolice_Terceiros_Apolice_ID                          Numeric(9,0)            
     , Seguradora_Nome                                       VarChar(60)             
     , Cosseguro_Ramo_Nome                                   VarChar(60)             
     , Sucursal_Seguradora_Nome                              VarChar(60)             
     , Sinistro_Endereco                                     VarChar(60)             
     , Sinistro_Bairro                                       VarChar(30)             
     , Sinistro_Estado                                       VarChar(20)             
     , Sinistro_Municipio                                    VarChar(30)             
     , Passivel_Ressarcimento                                Char(1)                 
     , Solicitante_Sinistro_Grau_Parentesco_ID               Int                     
     , cpf_cnpj                                              VARCHAR(14)              
     ) 
     
Create Index PK_Sinistro_ID                          on #Sinistro_Principal (Sinistro_ID)
Create Index FK_Proposta_ID                          on #Sinistro_Principal (Proposta_ID)
Create Index FK_Evento_Sinistro_ID                   on #Sinistro_Principal (Evento_Sinistro_ID)
Create Index FK_SubEvento_Sinistro_ID                on #Sinistro_Principal (SubEvento_Sinistro_ID)
Create Index FK_A                                    on #Sinistro_Principal (Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_ID, Ramo_ID)
Create Index FK_Seguradora_Cod_Susep                 on #Sinistro_Principal (Seguradora_Cod_Susep)
Create Index FK_Cliente_ID                           on #Sinistro_Principal (Cliente_ID)
Create Index FK_Solicitante_ID                       on #Sinistro_Principal (Solicitante_ID)

 if OBJECT_ID('tempdb..#DadosGerais_Aviso') IS NOT NULL 
 BEGIN
     DROP TABLE #DadosGerais_Aviso
 END
Create Table #DadosGerais_Aviso 
     ( Sinistro_Bb                           Numeric(13)         
     , Dt_Fim_Vigencia                       smalldatetime       
     , Agencia_ID                            Numeric(4)          
     , Situacao                              varchar(20)  )      
  Exec seguros_db.DBO.SEGS10922_SPS '9127813','14202000009', '','2','1','6',2

 SET NOCOUNT ON  EXEC SEGS13110_SPS










 if isnull(OBJECT_ID('tempdb..#Detalhamento_Visualizacao'),0) >0
 BEGIN
     DROP TABLE #Detalhamento_Visualizacao
 END
Select Evento_SEGBR_Sinistro_Documento_Tb.Descricao                                                      
  Into #Detalhamento_Visualizacao                                                                        
  From Seguros_Db.Dbo.Evento_SEGBR_Sinistro_Tb               Evento_SEGBR_Sinistro_Tb WITH (NOLOCK)           
  Join Seguros_Db.Dbo.Sinistro_BB_Tb                         Sinistro_BB_Tb WITH (NOLOCK)                     
    on Sinistro_BB_Tb.Sinistro_BB                            = Evento_SEGBR_Sinistro_Tb.Sinistro_BB      
   and Sinistro_BB_Tb.Dt_Fim_Vigencia                        is null                                     
  Join Seguros_Db.Dbo.Evento_SEGBR_Sinistro_Documento_Tb     Evento_SEGBR_Sinistro_Documento_Tb WITH (NOLOCK) 
    on Evento_SEGBR_Sinistro_Documento_Tb.Evento_ID          = Evento_SEGBR_Sinistro_Tb.Evento_ID        
  Join #Sinistro_Principal                                                                               
    on #Sinistro_Principal.Sinistro_ID                       = Evento_SEGBR_Sinistro_Tb.Sinistro_ID      

 if isnull(OBJECT_ID('tempdb..#Detalhamento_Visualizacao_Detalhamento'),0) >0
 BEGIN
     DROP TABLE #Detalhamento_Visualizacao_Detalhamento
 END
Create Table #Detalhamento_Visualizacao_Detalhamento                                                                     
           ( sinistro_id              Numeric(11,0)                                                                      
           , apolice_id               Numeric(9,0)                                                                       
           , sucursal_seguradora_id   Numeric(5)                                                                         
           , seguradora_cod_susep     Numeric(5)                                                                         
           , ramo_id                  Int                                                                                
           , detalhamento_id          SmallInt                                                                           
           , dt_detalhamento          SmallDateTime                                                                      
           , tp_detalhamento          TinyInt                                                                            
           , restrito                 Char(1)                                                                            
           , dt_inclusao              SmallDateTime                                                                      
           , dt_alteracao             SmallDateTime                                                                      
           , usuario                  VarChar(20)                                                                        
           )                                                                                                             
Insert Into #Detalhamento_Visualizacao_Detalhamento                                                                      
Select Sinistro_Detalhamento_Tb.sinistro_id                                                                              
     , Sinistro_Detalhamento_Tb.apolice_id                                                                               
     , Sinistro_Detalhamento_Tb.sucursal_seguradora_id                                                                   
     , Sinistro_Detalhamento_Tb.seguradora_cod_susep                                                                     
     , Sinistro_Detalhamento_Tb.ramo_id                                                                                  
     , Sinistro_Detalhamento_Tb.detalhamento_id                                                                          
     , Sinistro_Detalhamento_Tb.dt_detalhamento                                                                          
     , Sinistro_Detalhamento_Tb.tp_detalhamento                                                                          
     , Sinistro_Detalhamento_Tb.restrito                                                                                 
     , Sinistro_Detalhamento_Tb.dt_inclusao                                                                              
     , Sinistro_Detalhamento_Tb.dt_alteracao                                                                             
     , Sinistro_Detalhamento_Tb.usuario                                                                                  
  From Seguros_Db.Dbo.Sinistro_Detalhamento_Tb       Sinistro_Detalhamento_Tb WITH (NOLOCK)                                   
  Join #Sinistro_Principal                                                                                               
    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Detalhamento_Tb.Sinistro_ID                              
 Where Tp_Detalhamento                               in (0,1,3)                                                          
 Order by Sinistro_Detalhamento_Tb.Dt_Inclusao                                                                           
 if isnull(OBJECT_ID('tempdb..#Detalhamento_Visualizacao_Detalhamento_Linha'),0) >0
 BEGIN
     DROP TABLE #Detalhamento_Visualizacao_Detalhamento_Linha
 END
Create Table #Detalhamento_Visualizacao_Detalhamento_Linha                                                               
           ( Sinistro_ID             Numeric(11,0)                                                                       
           , Detalhamento_ID         SmallInt                                                                            
           , Linha                   VarChar(80)                                                                         
           )                                                                                                             
Insert Into #Detalhamento_Visualizacao_Detalhamento_Linha                                                                
Select Sinistro_Linha_Detalhamento_Tb.Sinistro_ID                                                                        
     , Sinistro_Linha_Detalhamento_Tb.Detalhamento_ID                                                                    
     , Sinistro_Linha_Detalhamento_Tb.Linha                                                                              
  From Seguros_Db.Dbo.Sinistro_Linha_Detalhamento_Tb Sinistro_Linha_Detalhamento_Tb WITH (NOLOCK)                             
  Join #Detalhamento_Visualizacao_Detalhamento                                                                           
    on #Detalhamento_Visualizacao_Detalhamento.Sinistro_ID = Sinistro_Linha_Detalhamento_Tb.Sinistro_ID                  
   and #Detalhamento_Visualizacao_Detalhamento.Detalhamento_ID = Sinistro_Linha_Detalhamento_Tb.Detalhamento_ID          
 Order By Sinistro_Linha_Detalhamento_Tb.Sinistro_ID                                                                     
     , Sinistro_Linha_Detalhamento_Tb.Detalhamento_ID                                                                    
     , Sinistro_Linha_Detalhamento_Tb.Linha_ID                                                                           





	 Select Descricao 
  From #Detalhamento_Visualizacao order by descricao


  Select Dt_Detalhamento 
     , Usuario
     , Detalhamento_ID
     , Restrito
     , sinistro_id
     , Tp_Detalhamento
  From #Detalhamento_Visualizacao_Detalhamento


  Select Linha 
  From #Detalhamento_Visualizacao_Detalhamento_Linha
 Where Sinistro_ID = 14202000009
   and Detalhamento_ID = 1