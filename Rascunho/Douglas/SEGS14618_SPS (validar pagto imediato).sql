USE desenv_db
GO

ALTER PROCEDURE dbo.SEGS14618_SPS @produto_id INT
	,@ramo_id INT
	,@evento_sinistro_id INT
	,@tp_cobertura_id INT
	,@proposta_id INT
	,@estimativa_prejuizo NUMERIC(15, 2)
	,@dt_ocorrencia SMALLDATETIME
	,@aviso_rural INT = 0
AS
/*
	NTENDENCIA 09/01/2019
	DEMANDA: 00210408-ESTEIRA_FLASH
	DESCRI��O: PROCEDURE PARA VALIDAR PAGTO IMEDIATO E RETORNAR O DETALHAMENTO.
	BANCO: SEGUROS_DB	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
 		IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL BEGIN DROP TABLE #Equipamento END
		CREATE TABLE #Equipamento (Codigo INT  ,TipoBem VARCHAR(60)  ,Modelo VARCHAR(60)  ,Dano INT  ,Orcamento INT  ,Valor NUMERIC(15,2)  ,Laudo INT  ,NomeAssistencia VARCHAR(60)  ,TelAssistencia VARCHAR(15)  ,Descricao VARCHAR(255))

		INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
		SELECT '000000037', 'Ar Condicionado', 'Acima de 19000BTUS', '1', '1', '249.00', '1', 'Ar teC', '(32) 34414-444_', 'dano no ar'
		UNION
		SELECT '000000018', 'Baba Eletronica', 'Sem Camera', '2', '1', '251.00', '1', 'baby tec', '(32) 98888-4444', 'danos na baba'
		UNION
		SELECT '000000023', 'Ferro de Passar', 'Ferro de Passar', '2', '1', '200.00', '1', 'FERRO TEC', '(32) 32131-2313', 'teste novo item'

    IF @@TRANCOUNT > 0 EXEC desenv_db.dbo.SEGS14618_SPS 1241,14,3,16,50312473,'2020-02-06'   ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)
	DECLARE @PgtoImediato AS SMALLINT = 1 -- (1-Sim / N�o)
	DECLARE @Detalhamento AS VARCHAR(3000) = ''
	DECLARE @ValorTotal AS NUMERIC(15, 2)
	DECLARE @QuebraLinha AS NVARCHAR(20) = ''
	DECLARE @Dt_ini AS SMALLDATETIME
	DECLARE @Dt_fim AS SMALLDATETIME
	-- (fim) Declara��o e tratamento de variaveis 
	--constantes
	-- Laudo INT
	DECLARE @LaudoSim AS SMALLINT = 1
	DECLARE @LaudoNao AS SMALLINT = 0
	--DECLARE @OrcamentoSim AS SMALLINT = 1
	DECLARE @orcamentoNao AS SMALLINT = 0
	--Dano INT
	DECLARE @Reparo AS SMALLINT = 1
	DECLARE @PerdaTotal AS SMALLINT = 2

	-----------
	BEGIN TRY
		--SOMATORIO DA ESTIMATIVA DOS EQUIPAMENTOS
		--SELECT @ValorTotal = SUM(Valor)
		--FROM #Equipamento
		SELECT @ValorTotal = @estimativa_prejuizo

		SET @QuebraLinha = CONCAT (
				CHAR(13)
				,CHAR(10)
				)

		--VALIDA��O TELA: PERDA TOTAL         
		--LAUDO   
		IF EXISTS (
				SELECT TOP 1 1
				FROM #Equipamento a
				WHERE a.dano = @PerdaTotal
					AND a.laudo = @LaudoNao
				)
		BEGIN
			--RECUSA (SEM LAUDO)
			SET @PgtoImediato = 0
			SET @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Pagamento imediato negado:'
					,@QuebraLinha
					,'foram informados equipamentos com'
					,@QuebraLinha
					,'perda total e sem o laudo.'
					)
		END
		ELSE
		BEGIN
			SET @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Todos os equipamentos informados com'
					,@QuebraLinha
					,'perda total est�o com laudo.'
					)
		END

		--VALIDA��O TELA: PASS�VEL DE REPARO
		--LAUDO
		IF EXISTS (
				SELECT TOP 1 1
				FROM #Equipamento a
				WHERE a.dano = @Reparo
					AND a.laudo = @LaudoNao
					AND a.orcamento = @orcamentoNao
				)
		BEGIN
			--RECUSA (SEM LAUDO)
			SET @PgtoImediato = 0
			SET @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Pagamento imediato negado:'
					,@QuebraLinha
					,'foram informados equipamentos como'
					,@QuebraLinha
					,'pass�veis de reparo, sem o laudo e sem ora�amento.'
					)
		END
		ELSE
		BEGIN
			SET @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Todos os equipamentos informados como'
					,@QuebraLinha
					,'pass�veis de reparo, est�o com laudo ou or�amento.'
					)
		END

		--VALIDA��O DE TELA: EQUIPAMENTOS COM OR�AMENTENTO SUPERIOR AO PARAMETRIZADO
		IF EXISTS (
				SELECT TOP 1 1
				FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)
				INNER JOIN #equipamento b ON a.equipamento_id = b.codigo
				WHERE b.dano = @PerdaTotal
					AND b.Valor > a.vl_perda_total
					AND a.dt_fim_vigencia IS NULL
				)
			OR EXISTS (
				SELECT TOP 1 1
				FROM seguros_db.dbo.sinistro_equipamento_importacao_tb a WITH (NOLOCK)
				INNER JOIN #equipamento b WITH (NOLOCK) ON a.equipamento_id = b.codigo
				WHERE b.dano = @reparo
					AND b.Valor > a.vl_reparo
					AND a.dt_fim_vigencia IS NULL
				)
		BEGIN
			--RECUSA
			SET @PgtoImediato = 0
			SET @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Pagamento imediato negado: Foram informados equipamentos'
					,@QuebraLinha
					,'com valor de estimativa superior ao limite permitido.'
					)
		END
		ELSE
		BEGIN
			SET @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Todos os equipamentos est�o com valor de estimativa'
					,@QuebraLinha
					,'dentro do limite permitido para o pagamento imediato.'
					)
		END

		--VALIDA��O DOS PAR�MENTROS
		DECLARE @sinistro_parametro_chave_id INT
			,@CPF_CNPJ VARCHAR(30)
			,@SQL AS NVARCHAR(1000)
			,@ParmDefinition AS NVARCHAR(300)

		--PEGANDO A CHAVE DOS PARAMETROS
		--ENCONTRANDO A CHAVE VIGENTE DA PROPOSTA
		SELECT DISTINCT @sinistro_parametro_chave_id = B.sinistro_parametro_chave_id
		FROM desenv_db.dbo.tp_sinistro_parametro_tb a WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_chave_tb b WITH (NOLOCK) ON a.tp_sinistro_parametro_id = b.tp_sinistro_parametro_id
		INNER JOIN desenv_db.dbo.sinistro_parametro_chave_regra_tb c WITH (NOLOCK) ON b.sinistro_parametro_chave_id = c.sinistro_parametro_chave_id
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb d WITH (NOLOCK) ON c.sinistro_parametro_regra_id = d.sinistro_parametro_regra_id
		WHERE 1 = 1
			AND b.produto_id = @produto_id -- 1241
			AND b.ramo_id = @ramo_id -- 14 
			AND b.evento_sinistro_id = @evento_sinistro_id -- 3 --
			AND b.tp_cobertura_id = @tp_cobertura_id -- 16 --
			AND b.dt_fim_vigencia IS NULL

		IF @sinistro_parametro_chave_id IS NULL
		BEGIN
			SET @PgtoImediato = 0
			SET @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					--,'O produto ' + convert(VARCHAR(60), @produto_id) + ' ramo ' + convert(VARCHAR(60), @ramo_id) + ' evento ' + convert(VARCHAR(60), @evento_sinistro_id) + ' cobertura ' + convert(VARCHAR(60), @tp_cobertura_id)
					,'O produto,ramo,evento e cobertura selecionados no aviso'
					,@QuebraLinha
					,'n�o est�o habilitados para o fluxo do pagamento imediato'
					)
		END

		--PEGANDO O CPF/CNPJ DO CLIENTE
		SELECT @CPF_CNPJ = isnull(isnull(CLIENTE.cpf_cnpj, FISICA.cpf), juridica.cgc)
		FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)
		INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID
		LEFT JOIN SEGUROS_DB.DBO.pessoa_fisica_tb FISICA WITH (NOLOCK) ON FISICA.pf_cliente_id = CLIENTE.CLIENTE_ID
		LEFT JOIN SEGUROS_DB.DBO.pessoa_juridica_tb juridica WITH (NOLOCK) ON juridica.pj_cliente_id = CLIENTE.CLIENTE_ID
		WHERE PROPOSTA.PROPOSTA_ID = @proposta_id --42901994 --104 --

		-----------------------------------------------------------------------------------------------------------------------------------------------------
		--###################################################################################################################################################
		--1� VALIDA��O DA ESTIMATIVA 'valor_estimativa'
		--Regra: O valor do somatorio da estimativas dos equipamentos informados tem que ser "menor/maior/menor ou igual / maior ou igual/diferente" que o valor parametrizado.
		--BUSCANDO A REGRA PARAMETRIZADA
		/*
			SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo  ,* FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE
				INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA
			ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
			  AND REGRA.NOME = 'valor_estimativa'
		*/
		DECLARE @VALIDA_REGRAS INT = 1

		SELECT @sql = 'IF( ' + convert(VARCHAR(20), convert(NUMERIC(15, 2), @ValorTotal)) + ' ' + CHAVE.OPERADOR + ' ' + CHAVE.VALOR + ') 
									SELECT @retornoOUT = 1
								ELSE 
								SELECT @retornoOUT = 0
								'
		FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
		WHERE 1 = 1
			AND CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 
			AND REGRA.NOME = 'valor_estimativa' --PARAMETRO

		SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';

		EXECUTE sp_executesql @SQL
			,@ParmDefinition
			,@retornoOUT = @VALIDA_REGRAS OUTPUT

		IF @VALIDA_REGRAS = 1
		BEGIN
			SELECT @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Estimativa/Or�amento R$'
					,convert(VARCHAR(20), @ValorTotal)
					,' est� dentro do limite'
					,@QuebraLinha
					,'para o pagamento imediato R$'
					,convert(VARCHAR(20), CHAVE.valor)
					)
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
				AND REGRA.NOME = 'valor_estimativa'
		END
		ELSE
		BEGIN
			--RECUSA
			SET @PgtoImediato = 0

			SELECT @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Pagamento imediato negado: Estimativa/Or�amento R$'
					,convert(VARCHAR(20), @ValorTotal)
					,@QuebraLinha
					,'est� acima do limite para o pagamento imediato  R$ '
					,convert(VARCHAR(20), CHAVE.valor)
					)
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
				AND REGRA.NOME = 'valor_estimativa'
		END

		-----------------------------------------------------------------------------------------------------------------------------------------------------
		--###################################################################################################################################################
		--2� VALIDA��O DO PERIODO DE RELACIONAMENTO: 'periodo_relacionamento'
		-- Regra: O cliente tem que ter uma ou mais apolices vigentes, onde a soma dos dias das apolices vigentes � maior que o periodo parametrizado 
		-- e que neste periodo o cliente n�o tenha ficado nenhum dia sem ao menos uma apolice vigente.
		--BUSCANDO A REGRA PARAMETRIZADA
		/*
			SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE
				INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA
			ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
			  AND REGRA.NOME = 'periodo_relacionamento'
		 */
		IF (object_id('tempdb..#propostas') IS NOT NULL)
		BEGIN
			DROP TABLE #propostas
		END

		IF (object_id('tempdb..#propostas_aux') IS NOT NULL)
		BEGIN
			DROP TABLE #propostas_aux
		END

		CREATE TABLE #propostas_aux (
			ID INT IDENTITY(1, 1) NOT NULL
			,produto_id INT NULL
			,cpf_cnpj VARCHAR(14) NULL
			,proposta_id INT NULL
			,dt_inicio_vigencia SMALLDATETIME NULL
			,dt_fim_vigencia SMALLDATETIME NULL
			,data_limite SMALLDATETIME NULL
			,data_atual SMALLDATETIME NULL
			,DIAS INT NULL -- TOTAL DE DIAS VIGENTE DA PROPOSTA
			,FLAG BIT NULL -- TEVE INTERRUP�AO SIM OU NAO
			)

		CREATE TABLE #propostas (
			ID INT PRIMARY KEY IDENTITY(1, 1) NOT NULL
			,produto_id INT NULL
			,cpf_cnpj VARCHAR(14) NULL
			,proposta_id INT NULL
			,dt_inicio_vigencia SMALLDATETIME NULL
			,dt_fim_vigencia SMALLDATETIME NULL
			,data_limite SMALLDATETIME NULL
			,data_atual SMALLDATETIME NULL
			,DIAS INT NULL -- TOTAL DE DIAS VIGENTE DA PROPOSTA
			,FLAG BIT NULL -- TEVE INTERRUP�AO SIM OU NAO
			)

		DECLARE @PERIODO_MIN_DE_RELACIONAMENTO INT

		SELECT @PERIODO_MIN_DE_RELACIONAMENTO = CHAVE.valor
		FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
		WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id
			AND REGRA.NOME = 'periodo_relacionamento'

		--ENCONTRANDO AS PROPOSTAS DO CLIENTE 
		INSERT INTO #propostas_aux (
			produto_id
			,cpf_cnpj
			,proposta_id
			,dt_inicio_vigencia
			,dt_fim_vigencia
			,data_limite
			,data_atual
			)
		SELECT DISTINCT PROPOSTA.PRODUTO_ID
			,CLIENTE.cpf_cnpj AS CPF_CNPJ
			,PROPOSTA.proposta_id
			,FORMAT(ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig)), 'yyyy-MM-dd') AS dt_inicio_vigencia
			,FORMAT(ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig)), 'yyyy-MM-dd') AS dt_fim_vigencia
			,FORMAT(GETDATE() - @PERIODO_MIN_DE_RELACIONAMENTO, 'yyyy-MM-dd') AS data_limite
			,FORMAT(GETDATE(), 'yyyy-MM-dd') AS data_atual
		--INTO #propostas
		FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)
		INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID
		LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB FECHADA WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = FECHADA.PROPOSTA_ID
		LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ADESAO WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = ADESAO.PROPOSTA_ID
		LEFT JOIN seguros_db.dbo.apolice_tb APOLICE WITH (NOLOCK) ON PROPOSTA.proposta_id = APOLICE.proposta_id
		WHERE CLIENTE.cpf_cnpj = @CPF_CNPJ
			AND PROPOSTA.ramo_id = 14 --@RAMO_ID
			AND PROPOSTA.situacao = 'i'
		
		UNION ALL
		
		SELECT DISTINCT PROPOSTA.PRODUTO_ID
			,CLIENTE.cpf_cnpj AS CPF_CNPJ
			,PROPOSTA.proposta_id
			,FORMAT(ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig)), 'yyyy-MM-dd') AS dt_inicio_vigencia
			,FORMAT(ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig)), 'yyyy-MM-dd') AS dt_fim_vigencia
			,FORMAT(GETDATE() - @PERIODO_MIN_DE_RELACIONAMENTO, 'yyyy-MM-dd') AS data_limite
			,FORMAT(GETDATE(), 'yyyy-MM-dd') AS data_atual
		FROM [ABSS].SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)
		INNER JOIN [ABSS].SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID
		LEFT JOIN [ABSS].SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB FECHADA WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = FECHADA.PROPOSTA_ID
		LEFT JOIN [ABSS].SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ADESAO WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = ADESAO.PROPOSTA_ID
		LEFT JOIN [ABSS].seguros_db.dbo.apolice_tb APOLICE WITH (NOLOCK) ON PROPOSTA.proposta_id = APOLICE.proposta_id
		WHERE CLIENTE.cpf_cnpj = @CPF_CNPJ
			AND PROPOSTA.ramo_id = 14 --@RAMO_ID
			AND PROPOSTA.situacao = 'i'
		ORDER BY 4

		--REMOVENDO AS PROPOSTAS QUE TIVERAM vig�ncia FINDADAS ANTES DA DATA LIMITE
		DELETE PROP
		FROM #propostas_aux PROP
		WHERE dt_fim_vigencia < data_limite - 1

		--DELETANDO AS PROPOSTA QUE ESTAO COM vig�ncia DENTRO DE OUTRAS
		DELETE A
		FROM #propostas_aux A
		INNER JOIN #propostas_aux B ON A.ID = B.ID + 1
		WHERE A.dt_inicio_vigencia BETWEEN B.DT_INICIO_vigencia
				AND B.DT_FIM_vigencia
			AND A.DT_FIM_vigencia BETWEEN B.DT_INICIO_vigencia
				AND B.DT_FIM_vigencia

		--REMOVENDO TODOS EM QUE A DATA DE INICIO DE vig�ncia INICIA DEPOIS DA DATA ATUAL
		DELETE PROP
		FROM #propostas_aux PROP
		WHERE dt_inicio_vigencia > data_atual

		--INICIALIZADO OS ID
		INSERT INTO #propostas (
			produto_id
			,cpf_cnpj
			,proposta_id
			,dt_inicio_vigencia
			,dt_fim_vigencia
			,data_limite
			,data_atual
			)
		SELECT produto_id
			,cpf_cnpj
			,proposta_id
			,dt_inicio_vigencia
			,dt_fim_vigencia
			,data_limite
			,data_atual
		FROM #propostas_aux WITH (NOLOCK)
		ORDER BY 4

		--AJUSTANDO O FIM DE vig�ncia COM O INICIO DE vig�ncia DA PROXIMA
		UPDATE A
		SET A.DT_INICIO_vigencia = B.DT_FIM_vigencia
			,A.FLAG = 1
		FROM #propostas A
		INNER JOIN #propostas B ON A.ID = B.ID + 1
		WHERE B.DT_FIM_vigencia + 1 BETWEEN A.DT_INICIO_vigencia
				AND A.DT_FIM_vigencia

		--COLOCANDO A FLAG NOS PERIODOS INETERRUPTOS
		UPDATE B
		SET B.FLAG = 1
		FROM #propostas A
		INNER JOIN #propostas B ON A.ID = B.ID + 1
		WHERE B.FLAG IS NULL
			AND a.flag = 1

		--GUARDANDO A DATA DE INICIO DE vig�ncia DA MENOR PROPOSTA
		--OBS CASO A DATA SEJA NULA O CLIENTE N�O ESTA ELIGIVEL PARA O PAGAMENTO IMEDIATO
		DECLARE @INICIO_vigencia_INICIAL SMALLDATETIME

		SELECT @INICIO_vigencia_INICIAL = A.DT_INICIO_vigencia
		FROM #propostas A
		WHERE A.dt_inicio_vigencia < a.data_limite
			AND ID = (
				SELECT TOP (1) id
				FROM #propostas
				ORDER BY id ASC
				)

		--ajustar in�cio =  data_limite (do primeiro registro caso esteja anteda da data limite)
		UPDATE A
		SET A.DT_INICIO_vigencia = data_limite
		FROM #propostas A
		WHERE A.dt_inicio_vigencia < a.data_limite
			AND ID = (
				SELECT TOP (1) id
				FROM #propostas
				ORDER BY id ASC
				)

		--SELECT * FROM #propostas order by 4
		-- ajustar data final = data atual (do �ltimo registro)
		UPDATE A
		SET A.dt_fim_vigencia = data_atual
		FROM #propostas A
		WHERE A.dt_fim_vigencia > A.data_atual
			AND ID = (
				SELECT TOP (1) id
				FROM #propostas
				ORDER BY id DESC
				)

		DECLARE @TOTAL_DIAS_RELACIONAMENTO INT

		UPDATE A
		SET DIAS = DATEDIFF(day, DT_INICIO_vigencia, DT_FIM_vigencia)
		FROM #propostas A

		--CASO EXISTA APENAS UMA PROPOSTA E A MESMA N�O TENHA INTERRUPCAO NO PERIODO COLOCA A FLAG 1 NA MESMA PARA VALIDAR O PERIODO DE RELACIONAMENTO
		UPDATE A
		SET FLAG = 1
		FROM #propostas A
		WHERE DIAS = @PERIODO_MIN_DE_RELACIONAMENTO
			AND (
				SELECT COUNT(1)
				FROM #propostas
				) = 1

		SELECT @TOTAL_DIAS_RELACIONAMENTO = ISNULL(SUM(dias), 0)
		FROM #PROPOSTAS
		WHERE flag = 1

		--VALIDANDO SE O CLIETE TEVE ALGUMA INTERRUP��O NO PERIODO PARAMETRIZADO
		IF EXISTS (
				SELECT TOP 1 1
				FROM #propostas
				WHERE FLAG IS NULL
				)
		BEGIN
			SELECT @TOTAL_DIAS_RELACIONAMENTO = SUM(DIAS)
			FROM #PROPOSTAS
			WHERE FLAG IS NULL

			SET @VALIDA_REGRAS = 0
		END
		ELSE
			-- VALIDANDO A QUANTIDADE DE DIAS SEM INTERRUP��O
		BEGIN
			--PEGANDO O RESTANTE DA vig�ncia DA PROPOSTA QUE ANTECEDE A DATA LIMITE
			SELECT @TOTAL_DIAS_RELACIONAMENTO = @TOTAL_DIAS_RELACIONAMENTO + ISNULL(DATEDIFF(day, @INICIO_vigencia_INICIAL, GETDATE() - @PERIODO_MIN_DE_RELACIONAMENTO), 0)

			SET @sql = ''
			SET @ParmDefinition = ''

			SELECT @sql = 'IF( ' + CONVERT(VARCHAR(20), @TOTAL_DIAS_RELACIONAMENTO) + ' ' + CHAVE.OPERADOR + ' ' + CHAVE.VALOR + ') 
									SELECT @retornoOUT = 1
								ELSE 
								SELECT @retornoOUT = 0
								'
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE 1 = 1
				AND CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id
				AND REGRA.NOME = 'periodo_relacionamento' --PARAMETRO

			SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';

			EXECUTE sp_executesql @SQL
				,@ParmDefinition
				,@retornoOUT = @VALIDA_REGRAS OUTPUT
		END

		IF @VALIDA_REGRAS = 1
		BEGIN
			SELECT @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Per�odo de relacionamento do cliente com a BrasilSeg '
					,convert(VARCHAR(20), @TOTAL_DIAS_RELACIONAMENTO)
					,' dias'
					,@QuebraLinha
					,'atingiu o valor m�nimo de '
					,convert(VARCHAR(20), CHAVE.valor)
					,' dias para o pagamento imediato'
					)
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id
				AND REGRA.NOME = 'periodo_relacionamento'
		END
		ELSE
		BEGIN
			--RECUSA
			SET @PgtoImediato = 0

			SELECT @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Pagamento imediato negado:'
					,@QuebraLinha
					,'Per�odo de relacionamento do cliente com a BrasilSeg '
					,convert(VARCHAR(20), @TOTAL_DIAS_RELACIONAMENTO)
					,' dias'
					,@QuebraLinha
					,'n�o atingiu o valor m�nimo de '
					,convert(VARCHAR(20), CHAVE.valor)
					,' dias'
					)
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id
				AND REGRA.NOME = 'periodo_relacionamento'
		END

		-----------------------------------------------------------------------------------------------------------------------------------------------------
		--###################################################################################################################################################
		--3. VALIDA��O DO PERIODO SEM SINISTRO: 'periodo_sem_sinistro'
		--Regra: Contagem de dias do �ltimo sinsitro do cliente tem que ser superior ao per�odo parametrizado e no ramo 14
		--BUSCANDO A REGRA PARAMETRIZADA
		/*
			SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE
				INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA
			ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
			  AND REGRA.NOME = 'periodo_sem_sinistro'
		*/
		DECLARE @DT_ULTIMO_SINISTRO_AVISADO SMALLDATETIME
			,@CONTAGEM_DIAS_ULTIMO_SINISTRO INT

		SELECT @DT_ULTIMO_SINISTRO_AVISADO = max(evento_SEGBR_sinistro.dt_inclusao)
		FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_tb evento_SEGBR_sinistro WITH (NOLOCK)
		WHERE 1 = 1
			AND evento_SEGBR_sinistro.cpf_cgc_segurado = @CPF_CNPJ --'02979888087'
			AND evento_SEGBR_sinistro.evento_bb_id = 1100
			AND evento_SEGBR_sinistro.ramo_id = 14
			AND evento_SEGBR_sinistro.ind_reanalise <> 'S'

		--CASO O CLIENTE N�O TENHA NENHUM SINISTRO, PEGAR O TOTAL PARAMETRIZADO + 1
		SELECT @CONTAGEM_DIAS_ULTIMO_SINISTRO = isnull(CONVERT(INT, DATEDIFF(DAY, @DT_ULTIMO_SINISTRO_AVISADO, GETDATE())), CHAVE.VALOR + 1)
		FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
		WHERE 1 = 1
			AND CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
			AND REGRA.NOME = 'periodo_sem_sinistro' --PARAMETRO

		SET @sql = ''
		SET @ParmDefinition = ''

		SELECT @sql = 'IF( ' + convert(VARCHAR(20), @CONTAGEM_DIAS_ULTIMO_SINISTRO) + ' ' + CHAVE.OPERADOR + ' ' + CHAVE.VALOR + ') 
									SELECT @retornoOUT = 1
								ELSE 
								SELECT @retornoOUT = 0
								'
		FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
		WHERE 1 = 1
			AND CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
			AND REGRA.NOME = 'periodo_sem_sinistro' --PARAMETRO

		SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';

		EXECUTE sp_executesql @SQL
			,@ParmDefinition
			,@retornoOUT = @VALIDA_REGRAS OUTPUT

		IF @VALIDA_REGRAS = 1
		BEGIN
			SELECT @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Contagem de dias do �ltimo sinistro '
					,convert(VARCHAR(20), @CONTAGEM_DIAS_ULTIMO_SINISTRO)
					,' dias'
					,@QuebraLinha
					,'atingiu o valor m�nimo de '
					,convert(VARCHAR(20), CHAVE.valor)
					,' dias para o pagamento imediato'
					)
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 -
				AND REGRA.NOME = 'periodo_sem_sinistro'
		END
		ELSE
		BEGIN
			--RECUSA
			SET @PgtoImediato = 0

			SELECT @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					,'Pagamento imediato negado:'
					,@QuebraLinha
					,'Contagem de dias do �ltimo sinistro '
					,convert(VARCHAR(20), @CONTAGEM_DIAS_ULTIMO_SINISTRO)
					,' dias'
					,@QuebraLinha
					,'n�o atingiu o valor m�nimo de '
					,convert(VARCHAR(20), CHAVE.valor)
					,' dias'
					)
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
				AND REGRA.NOME = 'periodo_sem_sinistro'
		END

		-----------------------------------------------------------------------------------------------------------------------------------------------------
		--###################################################################################################################################################
		--4.  VALIDA��O DE RESTRI�AO, A��O JUDIXCIAL/INDICIO DE FRAUDE:
		--Regra: Validar ou n�o se o cliente tem algum tipo de restri��o do tipo (A��o Judicial ou Ind�cio de Fraude) que ainda estejam pendentes,
		--ou seja que ainda n�o foram liberadas (motivo da libera��o is null), em caso positivo o cliente n�o entra no pagamento imediato
		--BUSCANDO A REGRA PARAMETRIZADA
		/*
			SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE
				INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA
			ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
			  AND REGRA.NOME = 'verifica_restricao'

		 */
		DECLARE @VERIFICA_RESTRICAO CHAR(1)

		SELECT @VERIFICA_RESTRICAO = CHAVE.VALOR
		FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
		WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
			AND REGRA.NOME = 'verifica_restricao'

		IF @VERIFICA_RESTRICAO = 'S'
		BEGIN
			IF EXISTS (
					SELECT TOP 1 1
					FROM CLIENTE_DB.DBO.PESSOA_ANOTACAO_VW ANOTACAO
					WHERE 1 = 1
						AND CPF_CNPJ = @CPF_CNPJ
						AND ANOTACAO.MOTIVO_RESTRICAO_ID IN (
							6
							,12
							)
						AND ANOTACAO.MOTIVO_LIBERACAO_ID IS NULL
					)
			BEGIN
				--RECUSA
				SET @PgtoImediato = 0
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'Pagamento imediato negado: O CPF/CNPJ '
						,@CPF_CNPJ
						,@QuebraLinha
						,'possui restri��o (A��o Judicial/Ind�cio de Fraude)'
						)
			END
			ELSE
			BEGIN
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'O CPF/CNPJ '
						,@CPF_CNPJ
						,@QuebraLinha
						,'n�o possui restri��o (A��o Judicial/Ind�cio de Fraude)'
						)
			END
		END

		-----------------------------------------------------------------------------------------------------------------------------------------------------
		--###################################################################################################################################################
		--5� VALIDA��O DE SINISTRO COM PEDIDO DE REANALISE 
		--Regra: Validar ou n�o se o cliente possui algum sinistro com pedido de reanalise, em caso positivo n�o poder� entrar no pagamento imediato
		--BUSCANDO A REGRA PARAMETRIZADA
		/*
			SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE
				INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA
			ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
			  AND REGRA.NOME = 'verifica_reanalise_sinistro'


		 */
		DECLARE @VERIFICA_REANALISE CHAR(1)

		SELECT @VERIFICA_REANALISE = CHAVE.VALOR
		FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
		WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
			AND REGRA.NOME = 'verifica_reanalise_sinistro'

		IF @VERIFICA_REANALISE = 'S'
		BEGIN
			IF EXISTS (
					SELECT TOP 1 1
					FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_tb evento_SEGBR_sinistro WITH (NOLOCK)
					WHERE 1 = 1
						AND evento_SEGBR_sinistro.cpf_cgc_segurado = @CPF_CNPJ
						AND evento_SEGBR_sinistro.evento_bb_id = 1100
						AND evento_SEGBR_sinistro.ramo_id = 14
						AND evento_SEGBR_sinistro.ind_reanalise = 'S'
					)
			BEGIN
				--RECUSA
				SET @PgtoImediato = 0
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'Pagamento imediato negado: O CPF/CNPJ '
						,@CPF_CNPJ
						,@QuebraLinha
						,'possui sinistro com pedido de reanalise'
						)
			END
			ELSE
			BEGIN
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'O CPF/CNPJ '
						,@CPF_CNPJ
						,@QuebraLinha
						,'n�o possui sinistro com pedido de reanalise'
						)
			END
		END

		-----------------------------------------------------------------------------------------------------------------------------------------------------
		--###################################################################################################################################################
		--6� VALIDA��O DA ADIMPLENCIA 'verifica_pgto_em_dia'
		--Regra: Validar ou n�o se a proposta est� com  d�bito em aberto(pendente ou inadimplente) j� vencido, em caso positivo n�o podera entrar no pagamento imediato
		--BUSCANDO A REGRA PARAMETRIZADA
		/*
			SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE
				INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA
			ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
			  AND REGRA.NOME = 'verifica_pgto_em_dia'

		 */
		DECLARE @VALIDA_PAGAMENTO CHAR(1)

		SELECT @VALIDA_PAGAMENTO = CHAVE.VALOR
		FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
		WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
			AND REGRA.NOME = 'verifica_pgto_em_dia'

		IF @VALIDA_PAGAMENTO = 'S'
		BEGIN
			IF EXISTS (
					SELECT TOP 1 1
					FROM SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB COBRANCA WITH (NOLOCK)
					WHERE 1 = 1
						AND COBRANCA.PROPOSTA_ID = @PROPOSTA_ID -- 50616296 --inadimplente --
						AND getdate() > COBRANCA.dt_agendamento -- cobran�as vencidas
						AND COBRANCA.situacao = 'p' -- cobran�as pendente
						OR COBRANCA.situacao = 'i' --cobran�as inadimpletes
					)
			BEGIN
				SET @PgtoImediato = 0
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'Pagamento imediato negado: A proposta '
						,convert(VARCHAR(20), @proposta_id)
						,@QuebraLinha
						,'est� com d�bitos em aberto na data de abertura'
						,@QuebraLinha
						,'do sinistro '
						,convert(VARCHAR(20), getdate(), 103)
						)
			END
			ELSE
			BEGIN
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'A proposta '
						,convert(VARCHAR(20), @proposta_id)
						,' est� sem d�bitos em aberto'
						,@QuebraLinha
						,'na data da abertura do sinistro '
						,convert(VARCHAR(20), getdate(), 103)
						)
			END
		END

		-----------------------------------------------------------------------------------------------------------------------------------------------------
		--###################################################################################################################################################
		--7� VALIDA��O DA VIG�NCIA COBERTURA E APOLICE 'verifica_vigencia_cobertura_apolice'
		--Regra: VALIDA��O DE TELA: OCORRENCIA DENTRO DA VIG�NCIA DA (AP�LICE E COBERTURA)
		--BUSCANDO A REGRA PARAMETRIZADA
		/*
			SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
				INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
			ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE 1=1
			  AND CHAVE.sinistro_parametro_chave_id = 1402 --@sinistro_parametro_chave_id
			  AND REGRA.NOME = 'verifica_vigencia_cobertura_apolice'

		 */
		DECLARE @VALIDA_VIGENCIA_APOLICE_COBERTURA CHAR(1)

		SELECT @VALIDA_VIGENCIA_APOLICE_COBERTURA = CHAVE.VALOR
		FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK) ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
		WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --
			AND REGRA.NOME = 'verifica_vigencia_cobertura_apolice'

		IF @VALIDA_VIGENCIA_APOLICE_COBERTURA = 'S'
		BEGIN
			DECLARE @cod_objeto_segurado INT

			--VALIDA��O DE TELA: OCORRENCIA DENTRO DA VIG�NCIA DA (AP�LICE E COBERTURA)
			IF OBJECT_ID('TEMPDB..#vigencia') IS NOT NULL
			BEGIN
				DROP TABLE #vigencia
			END

			IF OBJECT_ID('TEMPDB..#coberturas_vigentes') IS NOT NULL
			BEGIN
				DROP TABLE #coberturas_vigentes
			END

			IF OBJECT_ID('TEMPDB..#INFO_OBJETO_SEGURADO') IS NOT NULL
			BEGIN
				DROP TABLE #INFO_OBJETO_SEGURADO
			END

			CREATE TABLE #VIGENCIA (
				id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
				,dt_inicio_vigencia SMALLDATETIME NULL
				,dt_fim_vigencia SMALLDATETIME NULL
				,vigencia CHAR(1) NULL --(A)police / (C)obertura
				)

			CREATE TABLE #COBERTURAS_VIGENTES (
				tp_cobertura_id INT
				,nome VARCHAR(600) NULL
				,val_is NUMERIC(15, 2) NULL
				,dt_inicio_vigencia_esc SMALLDATETIME NULL
				,dt_fim_vigencia_esc SMALLDATETIME NULL
				,texto_franquia VARCHAR(600) NULL
				)

			CREATE TABLE #INFO_OBJETO_SEGURADO (
				COD_OBJETO_SEGURADO INT NULL
				,END_RISCO_ID INT NULL
				,ENDERECO VARCHAR(600) NULL
				,BAIRRO VARCHAR(600) NULL
				,MUNICIPIO VARCHAR(100) NULL
				,ESTADO VARCHAR(2) NULL
				,CEP VARCHAR(30) NULL
				)

			--AP�LICE
			INSERT INTO #vigencia (
				dt_inicio_vigencia
				,dt_fim_vigencia
				,vigencia
				)
			SELECT ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig))
				,ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig))
				,'A' --AP�LICE
			FROM seguros_db.dbo.proposta_tb proposta WITH (NOLOCK)
			LEFT JOIN seguros_db.dbo.proposta_fechada_tb fechada WITH (NOLOCK) ON proposta.proposta_id = fechada.proposta_id
			LEFT JOIN seguros_db.dbo.proposta_adesao_tb adesao WITH (NOLOCK) ON proposta.proposta_id = adesao.proposta_id
			LEFT JOIN seguros_db.dbo.apolice_tb apolice WITH (NOLOCK) ON proposta.proposta_id = apolice.proposta_id
			WHERE proposta.proposta_id = @proposta_id

			--BUSCANDO INFORMA��ES DO OBJETO SEGURADO
			-------------------------------------------------------------------
			INSERT INTO #INFO_OBJETO_SEGURADO (
				COD_OBJETO_SEGURADO
				,END_RISCO_ID
				,ENDERECO
				,BAIRRO
				,MUNICIPIO
				,estado
				,CEP
				)
			EXEC SEGUROS_DB.DBO.SEGS7786_SPS 50312473 --@proposta_id -- 50312473 --
				,@aviso_rural --0 --

			SELECT TOP 1 @cod_objeto_segurado = COD_OBJETO_SEGURADO
			FROM #INFO_OBJETO_SEGURADO
			WHERE 1 = 1

			--BUSCANDO TODAS AS COBERTURAS VIGENTES PARA A PROPOSTA
			-------------------------------------------------------------------
			INSERT INTO #COBERTURAS_VIGENTES (
				tp_cobertura_id
				,nome
				,val_is
				,dt_inicio_vigencia_esc
				,dt_fim_vigencia_esc
				,texto_franquia
				)
			EXEC SEGUROS_DB.DBO.SEGS7752_SPS @dt_ocorrencia -- '2020-02-05'
				,@proposta_id --50312473 
				,@aviso_rural --0 
				,@cod_objeto_segurado --0 

			INSERT INTO #vigencia (
				dt_inicio_vigencia
				,dt_fim_vigencia
				,vigencia
				)
			SELECT dt_inicio_vigencia_esc
				,dt_fim_vigencia_esc
				,'C' --COBERTURA
			FROM #COBERTURAS_VIGENTES
			WHERE 1 = 1
				AND tp_cobertura_id = @tp_cobertura_id --16 --@tp_cobertura_id

			------------------------------------------------------------------
			--OCORRENCIA VIG�NCIA AP�LICE
			IF NOT EXISTS (
					SELECT TOP 1 1
					FROM #vigencia v
					WHERE v.vigencia = 'A'
						AND @dt_ocorrencia BETWEEN v.dt_inicio_vigencia
							AND v.dt_fim_vigencia
					)
			BEGIN
				--RECUSA (SEM VIG�NCIA)
				SELECT @Dt_ini = v.dt_inicio_vigencia
					,@Dt_fim = v.dt_fim_vigencia
				FROM #vigencia v
				WHERE v.vigencia = 'A'

				SET @PgtoImediato = 0
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'Pagamento imediato negado:'
						,@QuebraLinha
						,'A data de ocorrencia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'
						,@QuebraLinha
						,'n�o esta dentro do per�odo de vig�ncia da ap�lice.'
						,@QuebraLinha
						,convert(VARCHAR(20), @dt_ini, 103) + ' � ' + convert(VARCHAR(20), @dt_fim, 103)
						)
			END
			ELSE
			BEGIN
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'A data de ocorrencia do aviso'
						,@QuebraLinha
						,'esta dentro do per�odo de vig�ncia da ap�lice.'
						)
			END

			--OCORRENCIA VIG�NCIA COBERTURA
			IF NOT EXISTS (
					SELECT TOP 1 1
					FROM #vigencia v
					WHERE v.vigencia = 'C'
						AND @dt_ocorrencia BETWEEN v.dt_inicio_vigencia
							AND v.dt_fim_vigencia
					)
				OR NOT EXISTS (
					SELECT TOP 1 1
					FROM #COBERTURAS_VIGENTES
					WHERE tp_cobertura_id = @tp_cobertura_id
					)
			BEGIN
				--RECUSA (SEM VIG�NCIA)
				SELECT @Dt_ini = v.dt_inicio_vigencia
					,@Dt_fim = v.dt_fim_vigencia
				FROM #vigencia v
				WHERE v.vigencia = 'C'

				SET @PgtoImediato = 0
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'Pagamento imediato negado:'
						,@QuebraLinha
						,'A data de ocorrencia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'
						,@QuebraLinha
						,'n�o esta dentro do per�odo de vig�ncia da cobertura.'
						)

				IF @Dt_ini IS NOT NULL
					AND @Dt_fim IS NOT NULL
				BEGIN
					SET @Detalhamento = CONCAT (
							@Detalhamento
							,@QuebraLinha
							,FORMAT(@dt_ini, 'dd/MM/yyyy') + ' � ' + FORMAT(@dt_fim, 'dd/MM/yyyy')
							)
				END
			END
			ELSE
			BEGIN
				SET @Detalhamento = CONCAT (
						@Detalhamento
						,@QuebraLinha
						,'A data de ocorrencia do aviso ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy')
						,@QuebraLinha
						,'esta dentro do per�odo de vig�ncia da cobertura.'
						)
			END
		END

		-----------------------------------------------------------------------------------------------------------------------------------------------------
		--###################################################################################################################################################
		--FIM DA CLASSIFICA��O
		-----------------------------------------------------------------------------------------------------------------------------------------------------
		IF @PgtoImediato = 1
		BEGIN
			SET @detalhamento = CONCAT (
					'O SINISTRO FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO:'
					,@QuebraLinha
					,@QuebraLinha
					,'Informa��es:'
					,@QuebraLinha
					,@detalhamento
					)
		END
		ELSE
		BEGIN
			SET @detalhamento = CONCAT (
					'O SINISTRO N�O FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO:'
					,@QuebraLinha
					,@QuebraLinha
					,'Informa��es:'
					,@QuebraLinha
					,@detalhamento
					)
		END

		SELECT @ValorTotal AS Valor
			,@Detalhamento AS Detalhamento
			,@PgtoImediato AS PagtoImediato

		-- (fim) Bloco de codifica��o da procedure
		-----------				
		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


