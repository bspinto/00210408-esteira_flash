VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Begin VB.Form frm_hist_prazo_regulacao 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Evento"
   ClientHeight    =   5220
   ClientLeft      =   2055
   ClientTop       =   2370
   ClientWidth     =   7290
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   7290
   Begin VB.CommandButton cmd_def_inicio_contagem 
      Caption         =   "Definir in�cio da contagem"
      Height          =   315
      Left            =   5100
      TabIndex        =   23
      Top             =   120
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CheckBox chk_eventos_somente_ativos 
      Caption         =   "Somente eventos ativos"
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   4800
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.PictureBox pct_alteracao 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   780
      ScaleHeight     =   1185
      ScaleWidth      =   4965
      TabIndex        =   5
      Top             =   1500
      Width           =   4995
      Begin VB.TextBox txtAlteracao 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         Height          =   1155
         Left            =   0
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Text            =   "frm_hist_prazo_regulacao.frx":0000
         Top             =   0
         Width           =   4935
      End
   End
   Begin VB.ComboBox cmb_tipo 
      Height          =   315
      ItemData        =   "frm_hist_prazo_regulacao.frx":0006
      Left            =   3540
      List            =   "frm_hist_prazo_regulacao.frx":0013
      TabIndex        =   20
      Top             =   4020
      Visible         =   0   'False
      Width           =   1875
   End
   Begin VB.TextBox txt_tipo 
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   3720
      Locked          =   -1  'True
      TabIndex        =   21
      Top             =   4140
      Width           =   1875
   End
   Begin VB.TextBox txtJustificativa 
      Height          =   435
      Left            =   120
      MaxLength       =   200
      MultiLine       =   -1  'True
      TabIndex        =   14
      Top             =   4620
      Visible         =   0   'False
      Width           =   4455
   End
   Begin VB.TextBox txt_dias 
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   6600
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   4020
      Width           =   615
   End
   Begin VB.TextBox txt_data 
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   5460
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   4020
      Width           =   1095
   End
   Begin VB.TextBox txt_evento 
      BackColor       =   &H00E0E0E0&
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   4020
      Width           =   3375
   End
   Begin VB.CommandButton cmd_alterar 
      Caption         =   "Alterar"
      Enabled         =   0   'False
      Height          =   315
      Left            =   4680
      TabIndex        =   7
      Top             =   4800
      Width           =   1215
   End
   Begin VB.CommandButton cmd_fechar 
      Caption         =   "Voltar"
      Height          =   315
      Left            =   5940
      TabIndex        =   6
      Top             =   4800
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid grd_Hist_eventos 
      Height          =   2715
      Left            =   120
      TabIndex        =   1
      Top             =   540
      Width           =   7065
      _ExtentX        =   12462
      _ExtentY        =   4789
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      BackColorSel    =   16777088
      ScrollBars      =   2
      FormatString    =   "Evento                                                       |Tipo                                    |Data           |Dias    |||"
   End
   Begin VB.CommandButton cmd_gravar 
      Caption         =   "Confirmar"
      Height          =   315
      Left            =   4680
      TabIndex        =   8
      Top             =   4560
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmd_cancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   5940
      TabIndex        =   9
      Top             =   4560
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lbl_motivo 
      Caption         =   "Motivo:"
      Height          =   195
      Left            =   120
      TabIndex        =   19
      Top             =   4380
      Visible         =   0   'False
      Width           =   795
   End
   Begin VB.Label lbl 
      Caption         =   "Dias"
      Height          =   195
      Index           =   6
      Left            =   6600
      TabIndex        =   18
      Top             =   3780
      Width           =   675
   End
   Begin VB.Label lbl 
      Caption         =   "Data"
      Height          =   195
      Index           =   5
      Left            =   5460
      TabIndex        =   17
      Top             =   3780
      Width           =   675
   End
   Begin VB.Label lbl 
      Caption         =   "Tipo"
      Height          =   195
      Index           =   4
      Left            =   3480
      TabIndex        =   16
      Top             =   3780
      Width           =   675
   End
   Begin VB.Label lbl 
      Caption         =   "Evento"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   15
      Top             =   3780
      Width           =   675
   End
   Begin VB.Label lbl 
      Caption         =   "(*) Registro alterado"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   120
      TabIndex        =   4
      Top             =   3300
      Width           =   1770
   End
   Begin VB.Label lbl_prazo_seguradora 
      Caption         =   "0000"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   6120
      TabIndex        =   3
      Top             =   3360
      Width           =   675
   End
   Begin VB.Label lbl 
      Caption         =   "Prazo seguradora:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   4500
      TabIndex        =   2
      Top             =   3360
      Width           =   1680
   End
   Begin VB.Label lbl 
      Caption         =   "Hist�rico do prazo de regula��o do pagamento do recibo 99 do processo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   180
      Width           =   6990
   End
End
Attribute VB_Name = "frm_hist_prazo_regulacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'+-------------------------------------------------
'| FLOW 18225198
'| Petrauskas Jan/2016
'|
'| Hist�rico de prazos de dias de regula��o

Private mvar_sinistro_id    As Variant
Private mvar_conexao_id     As Integer
Private mvar_dt_aviso       As Date
Private mvar_usuario_ativo  As Boolean
Private mvar_alterou_prazo  As Boolean
'GENJUNIOR - FLOW 18225198 - CARREGANDO HIST�RICO ATRAV�S DA LISTA DE VOUCHER
Private mvar_num_recibo     As Integer

Private i_linha_atual       As Integer
Private sin_situacao        As Integer
Private l_prazo             As Long

Private mvar_tipo_ramo      As Integer

Public Property Let p_tipo_ramo(ByVal vNewValue As Integer)
  mvar_tipo_ramo = vNewValue
  'mvar_tipo_ramo = 3
  chk_eventos_somente_ativos.Visible = True '07/2019 (mvar_tipo_ramo = bytTipoRamo_Rural)
  
End Property

Public Property Get p_alterou_prazo() As Boolean
  p_alterou_prazo = mvar_alterou_prazo
End Property
Public Property Get p_sinistro_id() As Variant
  p_sinistro_id = mvar_sinistro_id
End Property

Public Property Let p_sinistro_id(ByVal vNewValue As Variant)
  mvar_sinistro_id = vNewValue
  Call carrega_historico_regulacao
End Property
'GENJUNIOR - FLOW 18225198
Public Property Let p_num_recibo(ByVal vNewValue As Variant)
  mvar_num_recibo = vNewValue
End Property
Public Property Get p_num_recibo() As Variant
  p_num_recibo = mvar_num_recibo
End Property
'FIM GENJUNIOR

Public Property Let p_usuario_ativo(ByVal vNewValue As Boolean)
  mvar_usuario_ativo = vNewValue
  cmd_alterar.Visible = mvar_usuario_ativo And (sin_situacao <> 2)
End Property

Public Property Let p_conexao_id(ByVal vNewValue As Integer)
  mvar_conexao_id = vNewValue
End Property
Public Property Let p_dt_aviso(ByVal vNewValue As Date)
  mvar_dt_aviso = vNewValue
End Property

Private Sub carrega_historico_regulacao()
  Dim l_prazo_seguradora As Long
  Dim rsAux As ADODB.Recordset
  Dim sLinha As String, sAux As String, last_tipo As String
  Dim sSQL As String
  Dim ipnt As Integer
  'GENJUNIOR - FLOW 18225198
  Dim strDt_Inclusao As String
  Dim dt_inc As Date
  
  If mvar_conexao_id = -1 Then
    Unload Me
    Exit Sub
  End If
  
  pct_alteracao.Visible = False
  cmd_def_inicio_contagem.Enabled = False
  
  Me.Caption = "SEGP1285 - Hist�rico do prazo de regula��o - Sinistro " & mvar_sinistro_id
  
  'carregar data
  sSQL = "SELECT dt_aviso_sinistro, situacao " & vbCrLf _
       & "FROM seguros_db.dbo.sinistro_tb WITH (NOLOCK) " & vbCrLf _
       & "WHERE sinistro_id = " & mvar_sinistro_id

  Set rsAux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  mvar_conexao_id, _
                                  True)
  sin_situacao = 2
  
  If Not rsAux.EOF Then
      
    If mvar_dt_aviso = Empty Then
      If Not IsNull(rsAux!dt_aviso_sinistro) Then
        mvar_dt_aviso = rsAux!dt_aviso_sinistro
      End If
    End If
    sin_situacao = IIf(IsNull(rsAux!Situacao), 2, rsAux!Situacao)
  End If
  rsAux.Close: Set rsAux = Nothing
  
  grd_Hist_eventos.FormatString = "Evento                                           |Tipo                                    |Data               |Dias   |evento_id|alterado"
  
  grd_Hist_eventos.Rows = 1
  
  grd_Hist_eventos.ColWidth(4) = 0
  grd_Hist_eventos.ColWidth(5) = 0
  grd_Hist_eventos.ColWidth(6) = 0
  grd_Hist_eventos.ColWidth(3) = 500
  grd_Hist_eventos.ColWidth(2) = 1050
  grd_Hist_eventos.ColWidth(1) = 2000
  grd_Hist_eventos.ColWidth(0) = 3165
  
  If mvar_num_recibo > 0 Then
      lbl(0).Caption = "Hist�rico do prazo de regula��o do pagamento do recibo " & mvar_num_recibo & " do processo"
      cmd_def_inicio_contagem.Visible = False
      
      'GENJUNIOR  FLOW 18225198 - OBTENDO A DATA DE INCLUSAO DO RECIBO
      sSQL = " SELECT dt_inclusao FROM seguros_db.dbo.pgto_sinistro_tb WITH (NOLOCK) WHERE sinistro_id = " & mvar_sinistro_id & " AND num_recibo = " & mvar_num_recibo & " "
      
      Set rsAux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                      glAmbiente_id, _
                                      App.Title, _
                                      App.FileDescription, _
                                      sSQL, _
                                      mvar_conexao_id, _
                                      True)
                                      
      If Not rsAux.EOF Then
        If Not IsNull(rsAux!Dt_Inclusao) Then
          dt_inc = rsAux!Dt_Inclusao
          dt_inc = DateAdd("n", 2, dt_inc)
          strDt_Inclusao = Format(dt_inc, "yyyymmdd hh:nn:ss")
        End If
      End If
      
      rsAux.Close
      Set rsAux = Nothing
    
      sSQL = "SET NOCOUNT ON " & vbCrLf _
           & "IF OBJECT_ID('tempdb..#temp') IS NOT NULL " & vbCrLf _
           & " BEGIN " & vbCrLf _
           & "    DROP TABLE #temp " & vbCrLf _
           & " END " & vbCrLf _
           & vbCrLf _
           & "SELECT es.seq_evento, es.evento_segbr_id, es.dt_evento, es.tipo, es.contagem, CASE WHEN ec.tipo = 'P' THEN ec.descricao + ' - Recibo: ' + CAST(ISNULL(ps.num_recibo, 0) as VARCHAR(20)) ELSE ec.descricao END as descricao, CONVERT(char(1),'n') as alterado, ISNULL(ps.num_recibo, 0) as recibo " & vbCrLf _
           & "INTO #temp " & vbCrLf _
           & "FROM seguros_db.dbo.evento_contagem_sinistro_tb  es WITH (NOLOCK) " & vbCrLf _
           & "inner join seguros_db.dbo.sinistro_tb                    si with (nolock) on si.sinistro_id = es.sinistro_id " & vbCrLf _
           & "inner join seguros_db.dbo.proposta_tb                    pr with (nolock) on pr.proposta_id = si.proposta_id " & vbCrLf _
           & "INNER JOIN seguros_db.dbo.evento_contagem_tb     ec WITH (NOLOCK) ON es.evento_segbr_id = ec.evento_segbr_id and pr.produto_id = ec.produto_id " & vbCrLf _
           & "INNER JOIN (SELECT seq_evento, MAX(dt_inicio_vigencia) as dt_inicio " & vbCrLf _
           & "            FROM evento_contagem_sinistro_tb " & vbCrLf _
           & "            WHERE sinistro_id = " & mvar_sinistro_id & " " & vbCrLf _
           & "              AND dt_inicio_vigencia <= '" & strDt_Inclusao & "' " & vbCrLf _
           & "            GROUP BY SEQ_EVENTO " & vbCrLf _
           & "            ) X  ON es.SEQ_EVENTO = X.SEQ_EVENTO AND es.dt_inicio_vigencia = X.dt_inicio " & vbCrLf _
           & "INNER JOIN seguros_db.dbo.sinistro_historico_tb          sh WITH (NOLOCK) ON es.sinistro_id = sh.sinistro_id AND es.seq_evento = sh.seq_evento " & vbCrLf _
           & " LEFT JOIN seguros_db.dbo.pgto_sinistro_tb               ps WITH (NOLOCK) ON sh.sinistro_id = ps.sinistro_id AND sh.detalhamento_id = ps.detalhamento_id " & vbCrLf _
           & "WHERE es.sinistro_id = " & mvar_sinistro_id & vbCrLf _
           & vbCrLf
      
      sSQL = sSQL _
           & "DELETE from #temp WHERE seq_evento > (select seq_evento from #temp where recibo = " & mvar_num_recibo & ") " & vbCrLf & vbCrLf
      
      sSQL = sSQL _
           & "UPDATE #temp " & vbCrLf _
           & "SET alterado = 's' " & vbCrLf _
           & "WHERE exists (SELECT '' " & vbCrLf _
           & "              FROM evento_contagem_sinistro_alteracao_tb WITH (NOLOCK) " & vbCrLf _
           & "              WHERE sinistro_id = " & mvar_sinistro_id & " AND SEQ_EVENTO = #temp.SEQ_EVENTO ) " & vbCrLf _
           & vbCrLf _
           & "SELECT seq_evento, evento_segbr_id, dt_evento, tipo, contagem, descricao, alterado, 'n' as desconsidera  from #temp order by seq_evento" & vbCrLf
    
  Else
      lbl(0).Caption = "Hist�rico do prazo de regula��o do processo"
      cmd_def_inicio_contagem.Visible = True
'      sSQL = "SET NOCOUNT ON " & vbCrLf _
'           & "IF OBJECT_ID('tempdb..#temp') IS NOT NULL " & vbCrLf _
'           & " BEGIN " & vbCrLf _
'           & "    DROP TABLE #temp " & vbCrLf _
'           & " END " & vbCrLf _
'           & vbCrLf _
'           & "SELECT es.seq_evento, es.evento_segbr_id, es.dt_evento, es.tipo, es.contagem, CASE WHEN ec.tipo = 'P' THEN ec.descricao + ' - Recibo: ' + CAST(ISNULL(ps.num_recibo, 0) as VARCHAR(20)) ELSE ec.descricao END as descricao, CONVERT(char(1),'n') as alterado " & vbCrLf _
'           & "INTO #temp " & vbCrLf _
'           & "FROM seguros_db.dbo.evento_contagem_sinistro_tb          es WITH (NOLOCK) " & vbCrLf _
'                    & "inner join seguros_db.dbo.sinistro_tb                    si with (nolock) on si.sinistro_id = es.sinistro_id " & vbCrLf _
'           & "inner join seguros_db.dbo.proposta_tb                    pr with (nolock) on pr.proposta_id = si.proposta_id " & vbCrLf _
'           & "INNER JOIN seguros_db.dbo.evento_contagem_tb     ec WITH (NOLOCK) ON es.evento_segbr_id = ec.evento_segbr_id and pr.produto_id = ec.produto_id " & vbCrLf _
'           & "INNER JOIN seguros_db.dbo.sinistro_historico_tb          sh WITH (NOLOCK) ON es.sinistro_id = sh.sinistro_id AND es.seq_evento = sh.seq_evento " & vbCrLf _
'           & " LEFT JOIN seguros_db.dbo.pgto_sinistro_tb               ps WITH (NOLOCK) ON sh.sinistro_id = ps.sinistro_id AND sh.detalhamento_id = ps.detalhamento_id " & vbCrLf _
'           & "WHERE es.sinistro_id = " & mvar_sinistro_id & " " & vbCrLf _
'           & "  AND dt_fim_vigencia IS NULL " & vbCrLf _
'           & vbCrLf _
'           & "UPDATE #temp " & vbCrLf _
'           & "SET alterado = 's' " & vbCrLf _
'           & "WHERE exists (SELECT '' " & vbCrLf _
'           & "              FROM evento_contagem_sinistro_alteracao_tb WITH (NOLOCK) " & vbCrLf _
'           & "              WHERE sinistro_id = " & mvar_sinistro_id & " AND SEQ_EVENTO = #temp.SEQ_EVENTO ) " & vbCrLf _
'           & vbCrLf _
'           & "SELECT seq_evento, evento_segbr_id, dt_evento, tipo, contagem, descricao, alterado from #temp order by seq_evento" & vbCrLf
  
      sSQL = "SET NOCOUNT ON " & vbCrLf _
           & "IF OBJECT_ID('tempdb..#temp') IS NOT NULL " & vbCrLf _
           & " BEGIN " & vbCrLf _
           & "    DROP TABLE #temp " & vbCrLf _
           & " END " & vbCrLf _
           & vbCrLf _
           & "SELECT es.seq_evento, es.evento_segbr_id, es.dt_evento, es.tipo, es.contagem, CASE WHEN ec.tipo = 'P' THEN ec.descricao + ' - Recibo: ' + CAST(ISNULL(ps.num_recibo, 0) as VARCHAR(20)) ELSE ec.descricao END as descricao, CONVERT(char(1),'n') as alterado,  " & vbCrLf _
           & "       CASE when es.seq_evento < isnull(corte.evento_corte,0) then 's' when es.seq_evento = isnull(corte.evento_corte,0) then 'c' else 'n' end as desconsidera " & vbCrLf _
           & "INTO #temp " & vbCrLf _
           & "FROM seguros_db.dbo.evento_contagem_sinistro_tb          es WITH (NOLOCK) " & vbCrLf _
                    & "inner join seguros_db.dbo.sinistro_tb                    si with (nolock) on si.sinistro_id = es.sinistro_id " & vbCrLf _
           & "inner join seguros_db.dbo.proposta_tb                    pr with (nolock) on pr.proposta_id = si.proposta_id " & vbCrLf _
           & "INNER JOIN seguros_db.dbo.evento_contagem_tb     ec WITH (NOLOCK) ON es.evento_segbr_id = ec.evento_segbr_id and pr.produto_id = ec.produto_id " & vbCrLf _
           & "INNER JOIN seguros_db.dbo.sinistro_historico_tb          sh WITH (NOLOCK) ON es.sinistro_id = sh.sinistro_id AND es.seq_evento = sh.seq_evento " & vbCrLf _
           & "LEFT  JOIN seguros_db.dbo.pgto_sinistro_tb               ps WITH (NOLOCK) ON sh.sinistro_id = ps.sinistro_id AND sh.detalhamento_id = ps.detalhamento_id " & vbCrLf _
           & "LEFT  JOIN (select sinistro_id, isnull(MAX(seq_evento),0) as evento_corte " & vbCrLf _
           & "            from seguros_db.dbo.controle_inicio_contagem_tb with (nolock) " & vbCrLf _
           & "            where dt_inicio_validade <= getdate() and dt_fim_validade is null " & vbCrLf _
           & "            group by sinistro_id )                       corte            ON es.sinistro_id = corte.sinistro_id " & vbCrLf
      
      sSQL = sSQL _
           & "WHERE es.sinistro_id = " & mvar_sinistro_id & " " & vbCrLf _
           & "  AND dt_fim_vigencia IS NULL " & vbCrLf _
           & vbCrLf _
           & "UPDATE #temp " & vbCrLf _
           & "SET alterado = 's' " & vbCrLf _
           & "WHERE exists (SELECT '' " & vbCrLf _
           & "              FROM evento_contagem_sinistro_alteracao_tb WITH (NOLOCK) " & vbCrLf _
           & "              WHERE sinistro_id = " & mvar_sinistro_id & " AND SEQ_EVENTO = #temp.SEQ_EVENTO ) " & vbCrLf _
           & vbCrLf _
           & "SELECT seq_evento, evento_segbr_id, dt_evento, tipo, contagem, descricao, alterado, desconsidera from #temp order by seq_evento" & vbCrLf
  
  
  End If
  
  Set rsAux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  mvar_conexao_id, _
                                  True)
  last_tipo = ""
  l_prazo_seguradora = 0
  
  Do While Not rsAux.EOF
      sAux = ""
      Select Case UCase(Trim(rsAux!Tipo & ""))
      Case "N":
        sAux = "N - Neutro"
      Case "F":
        sAux = "F - Finaliza contagem"
        If LCase(rsAux!desconsidera) <> "s" Then
          last_tipo = "F"
          l_prazo_seguradora = l_prazo_seguradora + rsAux!contagem
        End If
      Case "I":
        sAux = "I - Inicia contagem"
        If LCase(rsAux!desconsidera) <> "s" Then
          last_tipo = "I"
          l_prazo_seguradora = l_prazo_seguradora + rsAux!contagem
        End If
      Case "P":
        sAux = "P - Pagamento"
      End Select
    
      sLinha = Trim(rsAux!Descricao & "") & IIf(LCase(Trim(rsAux!alterado & "")) = "s", "(*)", "") & vbTab _
             & sAux & vbTab _
             & Format(rsAux!dt_Evento, "dd/mm/yyyy") & vbTab _
             & rsAux!contagem & vbTab _
             & rsAux!Seq_Evento & vbTab _
             & LCase(Trim(rsAux!alterado & "")) & vbTab _
             & LCase(Trim(rsAux!desconsidera & ""))
      
      If chk_eventos_somente_ativos.Value = vbUnchecked Or LCase(rsAux!desconsidera) <> "s" Then
        Me.grd_Hist_eventos.AddItem sLinha
        If LCase(rsAux!desconsidera) = "s" Then
          Me.grd_Hist_eventos.Row = Me.grd_Hist_eventos.Rows - 1
          For ipnt = 0 To Me.grd_Hist_eventos.Cols - 1
                      
            Me.grd_Hist_eventos.col = ipnt
            Me.grd_Hist_eventos.CellForeColor = &H606060
          Next ipnt
        Else
          Me.grd_Hist_eventos.Row = Me.grd_Hist_eventos.Rows - 1
          For ipnt = 0 To Me.grd_Hist_eventos.Cols - 1
                      
            Me.grd_Hist_eventos.col = ipnt
            Me.grd_Hist_eventos.CellForeColor = vbWindowText
          Next ipnt
        End If
      End If
      
      rsAux.MoveNext
  Loop
  
  rsAux.Close: Set rsAux = Nothing

  If last_tipo = "I" Then
    If mvar_num_recibo = 0 Or dt_inc >= Date Then
      'Incluir registro "data atual"
      sLinha = "Data atual" & vbTab _
             & "F - Finaliza contagem" & vbTab _
             & Format(Date, "dd/mm/yyyy") & vbTab _
             & DateDiff("D", mvar_dt_aviso, Date) & vbTab _
             & 9999 & vbTab _
             & "X"
      Me.grd_Hist_eventos.AddItem sLinha
      l_prazo_seguradora = l_prazo_seguradora + DateDiff("D", mvar_dt_aviso, Date)
    End If
  End If

  lbl_prazo_seguradora.Caption = Format(l_prazo_seguradora, "#,##0")
  l_prazo = l_prazo_seguradora
  
  cmd_alterar.Visible = mvar_usuario_ativo And (sin_situacao <> 2)

End Sub

Private Sub chk_eventos_somente_ativos_Click()
  Call carrega_historico_regulacao
End Sub

Private Sub cmd_alterar_Click()
  chk_eventos_somente_ativos.Visible = False
  cmd_alterar.Visible = False
  cmd_fechar.Visible = False
  cmd_cancelar.Visible = True
  cmd_gravar.Visible = True
  lbl_motivo.Visible = True
  txtJustificativa.Text = ""
  txtJustificativa.Visible = True
  
  grd_Hist_eventos.Enabled = False
  cmb_tipo.Visible = True: txt_tipo.Visible = False:                cmb_tipo.Tag = cmb_tipo.Text
  txt_data.Locked = False: txt_data.BackColor = vbWindowBackground: txt_data.Tag = txt_data.Text
  
End Sub

Private Sub cmd_cancelar_Click()
  cmd_alterar.Visible = mvar_usuario_ativo 'True
  cmd_fechar.Visible = True
  cmd_cancelar.Visible = False
  cmd_gravar.Visible = False
  lbl_motivo.Visible = False
  txtJustificativa.Visible = False
  chk_eventos_somente_ativos.Visible = True '07/2019(mvar_tipo_ramo = 3)
  
  grd_Hist_eventos.Enabled = True
  cmb_tipo.Visible = False: txt_tipo.Visible = True
  
  txt_data.Locked = True: txt_data.BackColor = &HC0C0C0: txt_data.Text = txt_data.Tag
  
End Sub

Private Sub cmd_def_inicio_contagem_Click()
  If Val(cmd_def_inicio_contagem.Tag) = 0 Then Exit Sub
  
  Dim sMsg As String
  
  sMsg = "Confirma definir o evento " & txt_evento.Text & " de " & txt_data.Text _
       & " como evento inicial de contagem?"
       
  If MsgBox(sMsg, vbYesNo + vbQuestion + vbDefaultButton2) = vbYes Then
        Screen.MousePointer = vbHourglass
        DoEvents
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 "SEGS13518_SPI @sinistro_id = " & mvar_sinistro_id & ", @seq_evento=" & cmd_def_inicio_contagem.Tag _
                                 & ",@dt_inicio_validade = '" & Format(Now, "yyyymmdd hh:nn") & "', @usuario='" & gsCPF & "'", _
                                 mvar_conexao_id, _
                                 False)
        
        Call MsgBox("Evento inicial de contagem definido.", vbInformation)
        
        Call carrega_historico_regulacao
        Screen.MousePointer = vbNormal
        DoEvents
  End If
       
End Sub

Private Sub cmd_fechar_Click()
    If Val(lbl_prazo_seguradora.Caption) <> l_prazo Then
        Screen.MousePointer = vbHourglass
        DoEvents
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 "SEGS12635_SPI '" & gsCPF & "'," & mvar_sinistro_id, _
                                 mvar_conexao_id, _
                                 False)
        mvar_alterou_prazo = True
        Screen.MousePointer = vbNormal

    End If

    'Paulo Pelegrini - MU-2017-049845 - 16/07/2018 (INI)
    If FrmConsultaSinistrocon.Visible Then
        Screen.MousePointer = vbHourglass
        FrmConsultaSinistrocon.txtDetalhamento_Visualizar.Text = CarregaDados_DetalhamentoSinistro
        Screen.MousePointer = vbDefault
    End If
    'Paulo Pelegrini - MU-2017-049845 - 16/07/2018 (FIM)


    Me.Hide
End Sub

Private Sub cmd_gravar_Click()
Dim ipnt As Integer, l_prazo_seguradora As Long
Dim sAux As String, sSQL As String
'validar
  
  If cmb_tipo.Text = cmb_tipo.Tag And _
     txt_data.Text = txt_data.Tag Then
     Beep
     MsgBox "N�o foram feitas altera��es", vbExclamation
     cmd_cancelar_Click
     Exit Sub
  End If
    
  If Not IsDate(txt_data.Text) Then
    Beep
    MsgBox "Data inv�lida", vbExclamation
    txt_data.SetFocus
    Exit Sub
  End If
  
  txtJustificativa.Text = Trim(txtJustificativa.Text)
  sAux = Trim(Replace(Replace(Replace(txtJustificativa.Text, vbCr, ""), vbLf, ""), vbTab, ""))
  
  If txtJustificativa.Text = "" Then
    Beep
    MsgBox "Necess�rio informar o motivo", vbExclamation
    txtJustificativa.SetFocus
    Exit Sub
  End If
  
  'bloquear controles
  cmd_cancelar.Enabled = False
  cmd_gravar.Enabled = False
  txtJustificativa.Enabled = False
  
  'atualizar registro
  sAux = ""
  If cmb_tipo.Tag <> cmb_tipo.Text Then
    sAux = "tipo de evento alterado de " & Left(cmb_tipo.Tag, 1) & " para " & Left(cmb_tipo.Text, 1)
  End If
  
  If txt_data.Tag <> txt_data.Text Then
    sAux = sAux & IIf(sAux = "", "", vbCrLf) & "data do evento alterado de " & txt_data.Tag & " para " & txt_data.Text
  End If
  
  sSQL = "EXEC SEGS09476_SPU " & mvar_sinistro_id & "," _
       & grd_Hist_eventos.TextMatrix(i_linha_atual, 4) & ",'" _
       & Left(cmb_tipo.Text, 1) & "','" _
       & Format(CDate(txt_data.Text), "yyyymmdd") & "','" _
       & Left(Trim(txtJustificativa.Text), 200) & "','" _
       & sAux & "','" _
       & gsCPF & "'"
  
  Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                           glAmbiente_id, _
                           App.Title, _
                           App.FileDescription, _
                           sSQL, _
                           mvar_conexao_id, _
                           False)


  'Atualizar valores da tela
  grd_Hist_eventos.Row = i_linha_atual
  grd_Hist_eventos.TextMatrix(i_linha_atual, 1) = cmb_tipo.Text
  grd_Hist_eventos.TextMatrix(i_linha_atual, 2) = Format(CDate(txt_data.Text), "dd/mm/yyyy")
  grd_Hist_eventos.TextMatrix(i_linha_atual, 3) = DateDiff("D", mvar_dt_aviso, CDate(txt_data.Text)) * (IIf(Left(grd_Hist_eventos.TextMatrix(i_linha_atual, 1), 1) = "I", -1, 1))
  
  If UCase(grd_Hist_eventos.TextMatrix(i_linha_atual, 5)) <> "S" Then
    grd_Hist_eventos.TextMatrix(i_linha_atual, 5) = "S"
    grd_Hist_eventos.TextMatrix(i_linha_atual, 0) = grd_Hist_eventos.TextMatrix(i_linha_atual, 0) & " (*)"
  End If
  
  'Ajustar Prazo seguradora
  l_prazo_seguradora = 0
  For ipnt = 1 To grd_Hist_eventos.Rows - 1
    sAux = UCase(Left(grd_Hist_eventos.TextMatrix(ipnt, 1), 1))
    If sAux = "I" Or sAux = "F" Then
      l_prazo_seguradora = l_prazo_seguradora + Val(grd_Hist_eventos.TextMatrix(ipnt, 3))
    End If
  Next ipnt
  lbl_prazo_seguradora.Caption = Format(l_prazo_seguradora, "#,##0")
  
  'Ajustar controles
  cmd_cancelar.Visible = False
  cmd_cancelar.Enabled = True
  cmd_gravar.Visible = False
  cmd_gravar.Enabled = True
  lbl_motivo.Visible = False
  txtJustificativa.Visible = False
  txtJustificativa.Enabled = True
  
  grd_Hist_eventos.Enabled = True
  txt_tipo.Text = cmb_tipo.Text
  txt_tipo.Visible = True:  cmb_tipo.Visible = False
  txt_data.Locked = True:   txt_data.BackColor = &HC0C0C0
  cmd_alterar.Visible = mvar_usuario_ativo 'True
  chk_eventos_somente_ativos.Visible = True '07/2019 (mvar_tipo_ramo = 3)
  cmd_fechar.Visible = True

End Sub


Private Sub Form_Load()
'Ajustar form
  grd_Hist_eventos.ColWidth(4) = 0
  grd_Hist_eventos.ColWidth(5) = 0
  grd_Hist_eventos.ColWidth(6) = 0
  
  cmd_cancelar.Top = cmd_fechar.Top
  cmd_gravar.Top = cmd_alterar.Top
  txt_tipo.Left = cmb_tipo.Left: txt_tipo.Top = cmb_tipo.Top
  txtAlteracao.BorderStyle = 0
  pct_alteracao.Visible = False
  cmd_def_inicio_contagem.Visible = False
  
'Limpar propriedades
  mvar_dt_aviso = Empty
  mvar_sinistro_id = Null
  mvar_conexao_id = -1
  mvar_usuario_ativo = False
  mvar_alterou_prazo = False
  mvar_num_recibo = 0
End Sub

Private Sub grd_Hist_eventos_Click()
    Dim Seq_Evento As Integer
    Dim rsAux As ADODB.Recordset
    Dim sSQL As String, sTexto As String, sTipo As String

    i_linha_atual = Me.grd_Hist_eventos.Row
    Call Seleciona_linha(grd_Hist_eventos)

    If i_linha_atual > 0 Then

        txt_evento.Text = grd_Hist_eventos.TextMatrix(i_linha_atual, 0)
        cmb_tipo.Text = grd_Hist_eventos.TextMatrix(i_linha_atual, 1)
        txt_tipo.Text = grd_Hist_eventos.TextMatrix(i_linha_atual, 1)
        txt_data.Text = grd_Hist_eventos.TextMatrix(i_linha_atual, 2)
        txt_dias.Text = grd_Hist_eventos.TextMatrix(i_linha_atual, 3)

        sTipo = Left(grd_Hist_eventos.TextMatrix(grd_Hist_eventos.Row, 1), 1)

        If Val(grd_Hist_eventos.TextMatrix(i_linha_atual, 4)) = 9999 Then
            cmd_alterar.Enabled = False
            cmd_def_inicio_contagem.Enabled = False
            cmd_def_inicio_contagem.Tag = ""
        Else
            cmd_alterar.Enabled = (sTipo = "I" Or sTipo = "F")
            If cmd_def_inicio_contagem.Visible Then
              If LCase(grd_Hist_eventos.TextMatrix(i_linha_atual, 6)) = "n" And sTipo = "I" Then
                If grd_Hist_eventos.TextMatrix(i_linha_atual, 0) = "Reabertura do sinistro" Then
                    cmd_def_inicio_contagem.Enabled = True
                    cmd_def_inicio_contagem.Tag = grd_Hist_eventos.TextMatrix(i_linha_atual, 4)
                Else
                    cmd_def_inicio_contagem.Enabled = False
                End If
              Else
                cmd_def_inicio_contagem.Enabled = False
                cmd_def_inicio_contagem.Tag = ""
              End If
            End If
        
        End If
        
    Else
        cmd_alterar.Enabled = False
        cmd_def_inicio_contagem.Enabled = False
    End If

    If pct_alteracao.Visible Then
        pct_alteracao.Visible = False

    ElseIf i_linha_atual > 0 Then
        If LCase(Me.grd_Hist_eventos.TextMatrix(i_linha_atual, 5)) = "s" Then
            Seq_Evento = Me.grd_Hist_eventos.TextMatrix(i_linha_atual, 4)
            'abrir RS das altera��es (evento_contagem_sinistro_alteracao_tb, where sinistro_id, seq_evento order seq_alteracao

            sSQL = "SELECT justificativa, alteracao, usuario, dt_inclusao " & vbCrLf _
                 & "FROM evento_contagem_sinistro_alteracao_tb WITH (NOLOCK) " & vbCrLf _
                 & "WHERE sinistro_id = " & mvar_sinistro_id & " AND seq_evento = " & Seq_Evento & vbCrLf _
                 & "ORDER BY seq_alteracao DESC"

            Set rsAux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSQL, _
                                            mvar_conexao_id, _
                                            True)
            'carregar txtAlteracao
            sTexto = ""
            Do While Not rsAux.EOF
                sTexto = sTexto & "Alterado em " & Format(rsAux!Dt_Inclusao, "dd/mm/yyyy") & " por " & rsAux!Usuario & vbCrLf _
                       & IIf(Trim(rsAux!alteracao & "") <> "", Trim(rsAux!alteracao & "") & vbCrLf, "") _
                       & "Motivo: " & Trim(rsAux!justificativa & "") & vbCrLf & vbCrLf
                rsAux.MoveNext
            Loop
            rsAux.Close: Set rsAux = Nothing

            Do While Right(sTexto, 2) = vbCrLf
                sTexto = Left(sTexto, Len(sTexto) - 2)
            Loop
            'teste
            '      sTexto = "Nono no nonon nononon nonononono nono non ononono nono nononono non ononono nonono nononon nonon ono nonononon non no"
            '      sTexto = sTexto & vbCrLf & "No nononon nononon nononon no nonon on ononon onono nono no"
            '      sTexto = sTexto & vbCrLf & "Nonononon non nononon nonon o on onono no"
            '      sTexto = sTexto & vbCrLf & sTexto

            If Trim(sTexto) <> "" Then
                txtAlteracao.Text = sTexto
                'posicionar pct_alteracao
                pct_alteracao.Top = grd_Hist_eventos.Top + ((i_linha_atual - grd_Hist_eventos.TopRow + 2) * grd_Hist_eventos.RowHeight(i_linha_atual))
                'tornar pct_alteracao vis�vel
                pct_alteracao.Visible = True
            End If
        End If
    End If

    'Paulo Pelegrini - MU-2017-049845 - 16/07/2018 (INI)
    If i_linha_atual > 0 And FrmConsultaSinistrocon.Visible Then
        Screen.MousePointer = vbHourglass
        FrmConsultaSinistrocon.txtDetalhamento_Visualizar.Text = ""
        FrmConsultaSinistrocon.txtDetalhamento_Visualizar.Text = CarregaDados_DetalhamentoSinistroPorData(grd_Hist_eventos.TextMatrix(i_linha_atual, 2))
        Screen.MousePointer = vbDefault
    End If
    'Paulo Pelegrini - MU-2017-049845 - 16/07/2018 (FIM)

End Sub


Private Sub Seleciona_linha(ByRef obj As MSFlexGrid)
Dim jPnt As Integer, ipnt As Integer, iLinha As Integer
  
  iLinha = obj.Row
  For ipnt = 1 To obj.Rows - 1
    obj.Row = ipnt
    For jPnt = 0 To obj.Cols - 1
      obj.col = jPnt
      obj.CellBackColor = IIf(ipnt = iLinha, &HFFFF80, vbWhite)
    Next jPnt
  Next ipnt
  obj.Row = iLinha
End Sub

Private Sub grd_Hist_eventos_Scroll()

  If pct_alteracao.Visible Then
    pct_alteracao.Visible = False
  End If
End Sub

