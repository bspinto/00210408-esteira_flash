VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmDadosCliente 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5460
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8925
   Icon            =   "frmDadosCliente.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5460
   ScaleWidth      =   8925
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Local de Risco"
      Height          =   1815
      Left            =   0
      TabIndex        =   16
      Top             =   2880
      Width           =   8895
      Begin VB.ComboBox cmbMunicipio 
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Text            =   "cmbMunicipio"
         Top             =   1320
         Width           =   6015
      End
      Begin VB.ComboBox cboUFRisco 
         Height          =   315
         Left            =   6240
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1320
         Width           =   930
      End
      Begin VB.TextBox txtBairroRisco 
         Height          =   330
         Left            =   5760
         MaxLength       =   30
         TabIndex        =   4
         Top             =   600
         Width           =   2985
      End
      Begin VB.TextBox txtEndRisco 
         Height          =   330
         Left            =   120
         MaxLength       =   60
         TabIndex        =   3
         Top             =   600
         Width           =   5535
      End
      Begin MSMask.MaskEdBox txtCEP 
         Height          =   285
         Left            =   7320
         TabIndex        =   7
         Top             =   1320
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   9
         Mask            =   "#####-###"
         PromptChar      =   "_"
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "CEP:"
         Height          =   195
         Index           =   2
         Left            =   7320
         TabIndex        =   21
         Top             =   1080
         Width           =   360
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "U.F.:"
         Height          =   195
         Left            =   6255
         TabIndex        =   20
         Top             =   1080
         Width           =   345
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio:"
         Height          =   195
         Left            =   135
         TabIndex        =   19
         Top             =   1080
         Width           =   750
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         Height          =   195
         Left            =   5760
         TabIndex        =   18
         Top             =   360
         Width           =   450
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   7470
      TabIndex        =   10
      Top             =   4920
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   6075
      TabIndex        =   8
      Top             =   4920
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   4680
      TabIndex        =   9
      Top             =   4920
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados da Proposta"
      Height          =   1095
      Left            =   0
      TabIndex        =   12
      Top             =   1680
      Width           =   8880
      Begin VB.TextBox txtPropostaBB 
         Height          =   330
         Left            =   120
         MaxLength       =   9
         TabIndex        =   2
         Top             =   600
         Width           =   2100
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Proposta BB:"
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   930
      End
   End
   Begin VB.Frame frmCabecalho 
      Caption         =   "Dados do Segurado"
      Height          =   1575
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   8880
      Begin VB.TextBox txtNome 
         Height          =   330
         Left            =   150
         MaxLength       =   60
         TabIndex        =   0
         Top             =   465
         Width           =   8580
      End
      Begin MSMask.MaskEdBox mskCPF 
         Height          =   330
         Left            =   120
         TabIndex        =   1
         Top             =   1095
         Width           =   2130
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C.P.F. / C.N.P.J.:"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   870
         Width           =   1230
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nome:"
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   465
      End
   End
End
Attribute VB_Name = "frmDadosCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbMunicipio_Change()
    If cmbMunicipio.ListIndex <> -1 Then
        Me.cboUFRisco.Text = obtemEstado(cmbMunicipio.ItemData(cmbMunicipio.ListIndex), cmbMunicipio.Text)
    End If
End Sub

Private Sub cmbMunicipio_Click()
  If cmbMunicipio.ListIndex <> -1 Then
        Me.cboUFRisco.Text = obtemEstado(cmbMunicipio.ItemData(cmbMunicipio.ListIndex), cmbMunicipio.Text)
    End If
End Sub

Private Sub cmdContinuar_Click()

    If validarDados Then
        With frmEvento
            If .txtContato.Text = "" Then
                frmEvento.txtContato.Text = txtNome.Text
            End If
        End With
        Me.Hide
        Call MontaComboEvento(frmEvento.cboEvento)
        frmEvento.Show
    End If

End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call Sair(Me)
End Sub

Private Sub cmdVoltar_Click()
    bSemProposta = False
    frmConsulta.Show
    Me.Hide
End Sub

Private Sub Form_Activate()
    txtNome.SetFocus
   
End Sub

Private Sub Form_Load()

'AKIO.OKUNO - INICIO - 17860335 - 26/06/2013
'    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2, "Produ��o", "Qualidade")
    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2 Or glAmbiente_id = 6, "Produ��o", "Qualidade")
'AKIO.OKUNO - FIM - 17860335 - 26/06/2013

    CentraFrm Me
    
    Call CarregaUF(cboUFRisco)
    Call CarregaMunicipio(cmbMunicipio)
    
End Sub

Private Function validarDados() As Boolean

    validarDados = True

    'Se n�o achou na base, � obrigatorio entrar com os dados do sinistrado
    If txtNome.Text = "" Then
        validarDados = False
        MsgBox "Indique o nome do Segurado.", vbCritical, SIGLA_PROGRAMA
        txtNome.SetFocus
        Exit Function
    End If
    
    If mskCPF.Text = "___.___.___-__" Or mskCPF.Text = "" Then
        validarDados = False
        MsgBox "Indique o CPF/CNPJ do Segurado.", vbCritical, SIGLA_PROGRAMA
        mskCPF.SetFocus
        Exit Function
    End If
    
'    If txtPropostaBB = "" Then
'        validarDados = False
'        MsgBox "Indique a Proposta Banco do Brasil.", vbCritical, SIGLA_PROGRAMA
'        txtPropostaBB.SetFocus
'        Exit Function
'    End If
   
    
    '--------------------------------------------------------------------------
    'Adicionando valida��es nos campos de Local de Risco - Flow 149180 - Cleber
    '--------------------------------------------------------------------------
    If txtEndRisco.Text = "" Then
        MsgBox "Indique o Endere�o do Local de Risco.", vbCritical, App.Title
        validarDados = False
        txtEndRisco.SetFocus
        Exit Function
    End If
    
    If txtBairroRisco.Text = "" Then
        MsgBox "Indique o Bairro do Local de Risco.", vbCritical, App.Title
        validarDados = False
        txtBairroRisco.SetFocus
        Exit Function
    End If
    
    'Verifica se o munic�pio foi selecionado
    If cmbMunicipio.ListIndex = -1 Then
        If cmbMunicipio.Text = "" Then
            MsgBox "Indique o Munic�pio do Local de Risco.", vbCritical, App.Title
            validarDados = False
            cmbMunicipio.SetFocus
            Exit Function
        End If
    End If
    '--------------------------------------------------------------------------
    
    If txtPropostaBB.Text <> "" Then
        If Not IsNumeric(txtPropostaBB) Then
            validarDados = False
            MsgBox "O valor deve ser num�rico.", vbCritical, SIGLA_PROGRAMA
            txtPropostaBB.SetFocus
            Exit Function
        End If
    End If
        
    Exit Function

Erro:
    Call TratarErro("validarDados", Me.name)
    Call FinalizarAplicacao
    
End Function

Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub

Private Sub mskCPF_LostFocus()
    If mskCPF.Text <> "" Then
        If Len(mskCPF.Text) = 11 Then
            mskCPF.Text = MasCPF(mskCPF.Text)

            
        Else
            mskCPF.Text = MasCGC(mskCPF.Text)

        End If
    End If
End Sub

