VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmQuestionarioSinistro 
   Caption         =   "Question�rio de Sinistro"
   ClientHeight    =   7335
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11025
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7335
   ScaleWidth      =   11025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frBotoes 
      Height          =   975
      Left            =   240
      TabIndex        =   1
      Top             =   6120
      Width           =   10635
      Begin VB.CommandButton btnAplicar 
         Caption         =   "Aplicar"
         Height          =   360
         Left            =   9450
         TabIndex        =   2
         Top             =   360
         Width           =   990
      End
   End
   Begin VB.Frame frQuestionario 
      Caption         =   "Question�rio"
      Height          =   5655
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   10635
      Begin VB.ComboBox cmbResposta 
         Height          =   315
         Left            =   1665
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1395
         Width           =   1935
      End
      Begin MSFlexGridLib.MSFlexGrid flexQuest 
         Height          =   5055
         Left            =   270
         TabIndex        =   3
         Top             =   405
         Width           =   10155
         _ExtentX        =   17912
         _ExtentY        =   8916
         _Version        =   393216
         Rows            =   1
         Cols            =   5
         FormatString    =   "Seq | Pergunta | Resposta | Codigo"
      End
   End
End
Attribute VB_Name = "frmQuestionarioSinistro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private iTecnicoId As Integer
Private rsQuest As Recordset

Public questionario_id As Integer
Public evento_sinistro_id As Integer


Private Sub btnAplicar_Click()
    Debug.Print "frmQuestionarioSinistro.btnAplicar_CLick"
    Debug.Print "I " & Format(Now(), "hh:mm:ss,ms")
    AplicarQuestoesSinistro
    Debug.Print "F " & Format(Now(), "hh:mm:ss,ms")
End Sub

Private Sub cmbResposta_LostFocus()
    cmbResposta.Visible = False
End Sub


Private Sub Form_Load()

    frmQuestionarioPntAlerta.form_quest = "sin"
    evento_sinistro_id = frmDocumentos.evento_sinistro_id

    'gHW = flexQuest.hwnd
    'Hook   ' Start checking messages.
    flexQuest.AllowUserResizing = flexResizeNone
    flexQuest.Cols = 5

    flexQuest.RowHeightMin = cmbResposta.Height
    cmbResposta.Visible = False
    cmbResposta.ZOrder (0)
    cmbResposta.Width = flexQuest.CellWidth

    ' Load the ComboBox's list.
    cmbResposta.AddItem "V"
    cmbResposta.AddItem "F"

    ConsultarQuestionario
    
End Sub



Private Sub ConsultarQuestionario()

        Dim SQL As String
        Dim rsConsultarQuestionario As ADODB.Recordset
        Dim rsPerguntas As ADODB.Recordset
        Dim j As Integer
        Dim k As Integer
        Dim linhas As New Collection
        Dim l As Integer
        Dim totalLinha As Integer
        
        j = 1
        k = 0

        SQL = "exec SEGS9411_SPS " & frmDocumentos.produto_id & ", " & evento_sinistro_id
        
                
        Set rsConsultarQuestionario = ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                SQL, _
                                                True)
                                                
        
        
        Do While Not rsConsultarQuestionario.EOF
            
            questionario_id = rsConsultarQuestionario(0)
                    
            SQL = "exec SEGS9064_SPS " & questionario_id
            
            Set rsPerguntas = ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                SQL, _
                                                True)
            
              
            If Not rsPerguntas.EOF Then
                                       
                Dim i As Integer
                i = 0
                
                Do While Not rsPerguntas.EOF
                
                    rsPerguntas.MoveNext
                    i = i + 1
                Loop
                
                rsPerguntas.MoveFirst
                
                
                linhas.Add (i + 1)
                
                For l = 0 To k
                
                    If k = 0 Then
                    
                        totalLinha = totalLinha + CInt(linhas(k + 1))
                    
                    Else
                    
                        totalLinha = totalLinha + CInt(linhas(k + 1)) - 1
                    
                    End If
                    
                    
                    
                    l = k
                
                Next
                                
                l = 0
                
                flexQuest.Rows = totalLinha
                            
                Do While Not rsPerguntas.EOF
    
                       flexQuest.TextMatrix(j, 0) = j
                       flexQuest.TextMatrix(j, 1) = rsPerguntas("TEXTO_PERGUNTA")
                       flexQuest.TextMatrix(j, 2) = ""
                       flexQuest.TextMatrix(j, 3) = rsPerguntas("pergunta_id")
                       flexQuest.TextMatrix(j, 4) = rsConsultarQuestionario("questionario_id")
                       flexQuest.WordWrap = True
                       flexQuest.RowHeight(j) = 700
                             
                       j = j + 1
                       rsPerguntas.MoveNext
                Loop
                
                flexQuest.ColWidth(1) = 8480
                flexQuest.ColWidth(3) = 0
                flexQuest.ColWidth(4) = 0
                
                
            End If
            k = k + 1
            rsConsultarQuestionario.MoveNext
        Loop
        
        
               
    End Sub
    
    
    Private Sub AplicarQuestoesSinistro()
        Dim i As Integer
        Dim sSQL As String
        If flexQuest.Rows - 1 > 0 Then
        
            For i = 1 To flexQuest.Rows - 1
                If flexQuest.TextMatrix(i, 2) = "" Then
                    MsgBox ("Favor Responder a Pergunta de n�mero " & i & "!")
                    Exit Sub
                End If
            Next
        
            For i = 1 To flexQuest.Rows - 1
                Dim rsQuest As New ADODB.Recordset
                sSQL = "exec SEGS9412_SPI "
                sSQL = sSQL & flexQuest.TextMatrix(i, 4)
                sSQL = sSQL & ", " & flexQuest.TextMatrix(i, 3)
                sSQL = sSQL & ", " & sinistro_id
                sSQL = sSQL & ", '" & flexQuest.TextMatrix(i, 2) & "'"
                sSQL = sSQL & ", '" & cUserName & "'"
                
                ExecutarSQL gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSQL, _
                                            False
                                            
            Next
            
            For i = 1 To flexQuest.Rows - 1
                flexQuest.TextMatrix(i, 2) = ""
            Next
        End If
        
        Me.Hide
        
        Dim SQLl As String
        Dim rsPntAlerta As ADODB.Recordset

           'Cleber Sardo - INC000004254383 - Erro no agravamento - 29/01/2014 - In�cio
           'SQLl = "exec SEGURO_FRAUDE_DB..SPFS0247_SPS " & sinistro_id
            SQLl = "exec SEGURO_FRAUDE_DB..SPFS0247_SPS " & IIf(Not IsNumeric(Trim(sinistro_id)), 0, sinistro_id)
           'Cleber Sardo - INC000004254383 - Erro no agravamento - 29/01/2014 - Fim


        Set rsPntAlerta = ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                SQLl, _
                                                True)

        If rsPntAlerta.EOF Then
            frmAvisosRealizados.Show
            frmAvisosRealizados.cmdOK.Enabled = False
        Else
            frmQuestionarioPntAlerta.Show
        End If
        
    End Sub
    
    

     
      Private Sub flexQuest_MouseUp(Button As Integer, _
         Shift As Integer, x As Single, y As Single)
         
          Static CurrentWidth As Single
          ' Check to see if the Cell's width has changed.
          If flexQuest.CellWidth < 0 Then Exit Sub
          If flexQuest.CellWidth <> CurrentWidth Then
              cmbResposta.Width = flexQuest.CellWidth
              CurrentWidth = flexQuest.CellWidth
              
          End If
      End Sub
      
      Private Sub flexQuest_Click()
        If flexQuest.CellWidth < 0 Then Exit Sub
          'ConsultarQuestionario
          cmbResposta.Width = flexQuest.CellWidth
          cmbResposta.Left = flexQuest.CellLeft + flexQuest.Left
          cmbResposta.Top = flexQuest.CellTop + flexQuest.Top
         
          
            If flexQuest.CellLeft = "8925" Then
            
                cmbResposta.Visible = True
            
            Else
                
                cmbResposta.Visible = False
            
            End If
        
      End Sub

Private Sub cmbResposta_Click()
          ' Place the selected item into the Cell and hide the ComboBox.
          flexQuest.Text = cmbResposta.Text
          cmbResposta.Visible = False
          
End Sub

