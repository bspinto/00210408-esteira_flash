

-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
--INFORMA��ES
--C00210408 - Esteira Flash - Sinistros de Danos El�tricos Residenciais

-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
/*
##Os produtos contemplados para este projeto s�o:

	104	    - COMPREENSIVO RESIDENCIAL ALTA RENDA � ESTILO 
	106		- COMPREENSIVO RESIDENCIAL ALTA RENDA � PRIVATE 
	108		- SEGUR BRASIL � RESIDENCIAL 
	116		- OURO RESIDENCIAL PROMOCIONAL 
	119		- OURO RESIDENCIAL GRUPO ESPECIAL 
	809		- RESIDENCIAL PERSONALIZADO 
	1167	- RESIDENCIAL PREVI 
	1176	- BB PROTE��O RESIDENCIAL � INFORM�TICA 
	1178	- BB PROTE��O RESIDENCIAL � FAMILIAR 
	1220	- BB SEGURO RESIDENCIAL 
	1221	- BB SEGURO RESIDENCIAL 
	1222	- BB SEGURO RESIDENCIAL 
	1223	- BB SEGURO RESIDENCIAL ESTILO 
	1224	- BB Seguro Residencial Estilo 
	1241	� BB Seguro Residencial Essencial 


##Coberturas Atingidas
------------------------------------------------------------------------------------------------
|tp_cobertura_id	|Nome
------------------------------------------------------------------------------------------------
|16					|	Danos El�tricos
|68					|	Danos El�tricos
|130				|	Danos El�tricos
|498				|	Danos El�tricos e Descarga El�trica Dec.de Queda de Raio
|867				|	Danos El�tricos e Eletr�nicos para Equipamentos
|1001				|	Cobertura Adicional de danos el�tricos e queda de raio
|1012				|	Cobertura Adicional de danos el�tricos e queda de raio


*/

--#####################################################################
-- TABELAS DA DEMANDA
--#####################################################################
use desenv_db
go

--TABELAS DE PARAMETRO
select * from desenv_db.dbo.sinistro_parametro_regra_tb
select * from desenv_db.dbo.sinistro_parametro_chave_tb
select * from desenv_db.dbo.sinistro_parametro_chave_regra_tb
select * from desenv_db.dbo.tp_sinistro_parametro_tb


--TABELAS DE EQUIPAMENTO
SELECT * FROM SEGUROS_DB.DBO.sinistro_equipamento_tb
SELECT * FROM ABSS.SEGUROS_DB.DBO.sinistro_equipamento_tb


--#####################################################################
-- SEGP0862 - Aviso de sinistro
--#####################################################################



select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 104	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 106	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 108	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 116	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 119	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 809	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1167	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1176	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1178	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1220	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1221	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1222	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1223	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1224	and situacao ='i'
select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 1241	and situacao ='i'



    grdEquipamentos.Row = 0
    grdEquipamentos.Col = 0
    grdEquipamentos.Text = "Tipo de Bem"

	    grdEquipamentos.Row = 0
    grdEquipamentos.Col = 0
    grdEquipamentos.Text = "Tipo de Bem"

	   |Modelo   |Dano    |Estimativa do prejuizo         |J� realizou laudo 

/*
    
    grdEquipamentos.ColWidth(0) = 500  'n�
    grdEquipamentos.ColWidth(1) = 2750 'Tipo de Bem
    grdEquipamentos.ColWidth(2) = 2750 'Modelo
    grdEquipamentos.ColWidth(3) = 1500 'Dano
    grdEquipamentos.ColWidth(4) = 2000 'Estimativa do prejuizo
    grdEquipamentos.ColWidth(5) = 1000 'J� realizou laudo
    grdEquipamentos.ColWidth(6) = 3500 'Nome Assist�ncia
    grdEquipamentos.ColWidth(7) = 1500 'Tel. Assist�ncia
    grdEquipamentos.ColWidth(8) = 8000 'Descricao

*/



/*
proposta_id	produto_id	dt_proposta	dt_contratacao	prop_cliente_id	situacao	lock_aceite	num_endosso	dt_inclusao	dt_alteracao	usuario	lock	funcionario_bb	proposta_id_anterior	dt_contabilizacao_emissao	produto_externo_id	dt_envio_assistencia	e_mail	assistencia	canal_venda	ramo_id	subramo_id	dt_inicio_vigencia_sbr	pessoa_id	taxa_segmento	dt_retorno_bb	num_remessa	origem_proposta_id	dt_avaliacao_resseguro	num_cotacao	num_versao_cotacao	dt_solicitacao_cotacao	link_inspecao	canal_id	renovacao_automatica	meio_recebimento	dt_contabilizacao_antecipada	situacao_ant_trans	grupo_ramo	dt_emissao_proposta	fl_verba_unica	canal_venda_conducao	canal_venda_confirmacao	num_utilizacao_geo	dt_solicitacao_contratacao	dt_vencimento_operacao_credito	PIN	dt_confirma_contratacao	proposta_oferta_agrupada	dt_envio_exclusao_assist	tp_documento
17204106	104	2009-02-11 00:00:00	2009-02-11 00:00:00	2221678	i	n	NULL	2009-02-11 20:37:00	2009-03-07 09:19:00	28196889000	0x00000001135D513E	000000000	NULL	2009-03-04 00:00:00	1410	NULL	NULL	N	NULL	14	1410	2006-11-24 00:00:00	NULL	NULL	NULL	NULL	1	2009-03-06 00:00:00	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL

*/



-->AvisoSinistroRE
-->FrmConsulta
-->FrmEvento
-->FrmSolicitante
-->FrmAgencia
	-->FrmAvisoPropostas
-->Incluir novo frm para inclus�o dos equipamentos afetados pelos danos eletricos dos produtos relacionados a demanda.

-->FrmObjSegurado
-->FrmOutrosSeguros
-->frmAviso


segs14008_sps








select distinct b.nome,b.evento_sinistro_id  from produto_estimativa_sinistro_tb  a
inner join  evento_sinistro_tb b
on a.evento_sinistro_id = b.evento_sinistro_id
where produto_id in 
(104
,106
,108
,116
,119
,809
,1167
,1176
,1178
,1220
,1221
,1222
,1223
,1224
,1241
)
and tp_cobertura_id in (
16 
,68 
,130 
,498 
,867 
,1001 
,1012 
)
and isnull(a.dt_fim_vigencia, getdate()+1)  >  getdate()
and exists (select 1 from produto_estimativa_sinistro_tb x where
x.produto_id = a.produto_id
and x.tp_cobertura_id <> a.tp_cobertura_id
and x.evento_sinistro_id = a.evento_sinistro_id
and isnull(x.dt_fim_vigencia, getdate()+1)  >  getdate()
 )
order by 2



select * from evento_sinistro_tb where nome like '%voltaico%'



select * from produto_estimativa_sinistro_tb a
 where produto_id = 106 and evento_sinistro_id = 300



 select *  from tp_cobertura_tb where tp_cobertura_id in (123,498)


 select * from escolha_tp_cob_generico_tb where proposta_id = 49999108
 

 select * from proposta_tb where proposta_id = 49999108

 select * from escolha_tp_cob_res_tb where proposta_id = 49999108

 sp_tables '%escolha%'

select * from evento_sinistro_tb where evento_sinistro_id in (

3
,324)





select a.sinistro_id,count(1) from evento_segbr_sinistro_atual_tb a
join evento_segbr_sinistro_cobertura_tb b
on a.evento_id = b.evento_id
where evento_sinistro_id is not null 
and dt_evento > '2015-01-01'
and evento_bb_id = 1100 
and ind_reanalise = 'n'
and a.evento_sinistro_id in (
3
,34
,300
,302
,311
,313
,324
) and produto_id in (
104
,106
,108
,116
,119
,809
,1167
,1176
,1178
,1220
,1221
,1222
,1223
,1224
,1241



)
group by a.sinistro_id
having count(1) > 1

select ind_reanalise,* from evento_segbr_sinistro_atual_tb where sinistro_id = 14201908995 and evento_bb_id = 1100 




 select * from produto_estimativa_sinistro_tb with (nolock)  where produto_id = 1241 and ramo_id = 14 and subramo_id = 1423 and evento_sinistro_id = 3 and tp_cobertura_id = 16 and situacao = 'A'
  select * from produto_estimativa_sinistro_tb with (nolock)  where produto_id = 1241 and ramo_id = 14 and subramo_id = 1423 and evento_sinistro_id = 3 and tp_cobertura_id = 13 and situacao = 'A'


   exec SEGS7786_SPS 50312473, 0

  exec SEGS7752_SPS '20200206', 50312473





   BEGIN TRAN
 		IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL BEGIN DROP TABLE #Equipamento END
		CREATE TABLE #Equipamento (Codigo INT  ,TipoBem VARCHAR(60)  ,Modelo VARCHAR(60)  ,Dano INT  ,Orcamento INT  ,Valor NUMERIC(15,2)  ,Laudo INT  ,NomeAssistencia VARCHAR(60)  ,TelAssistencia VARCHAR(15)  ,Descricao VARCHAR(255))


		--TESTE SEM REPARO
		INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
		SELECT '000000037', 'Ar Condicionado', 'Acima de 19000BTUS', '2', '0', '249.00', '1', 'Ar teC', '(32) 34414-444_', 'dano no ar'
		UNION
		SELECT '000000018', 'Baba Eletronica', 'Sem Camera', '2', '0', '251.00', '1', 'baby tec', '(32) 98888-4444', 'danos na baba'
		UNION
		SELECT '000000023', 'Ferro de Passar', 'Ferro de Passar', '2', '0', '200.00', '1', 'FERRO TEC', '(32) 32131-2313', 'teste novo item'


		-- TESTE REPARO
		--INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
		--SELECT '000000037', 'Ar Condicionado', 'Acima de 19000BTUS', '1', '1', '249.00', '0', 'Ar teC', '(32) 34414-444_', 'dano no ar'
		--UNION
		--SELECT '000000018', 'Baba Eletronica', 'Sem Camera', '1', '0', '251.00', '1', 'baby tec', '(32) 98888-4444', 'danos na baba'
		--UNION
		--SELECT '000000023', 'Ferro de Passar', 'Ferro de Passar', '1', '0', '200.00', '0', 'FERRO TEC', '(32) 32131-2313', 'teste novo item'

    IF @@TRANCOUNT > 0 EXEC desenv_db.dbo.SEGS14618_SPS 1241,14,3,16,50312473,3501.00,'2020-02-06'   ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  