drop table #teste

select top 100  e.tp_cobertura_id,c.cpf_cnpj,e.tp_cobertura_id,p.* into #teste from seguros_db.dbo.proposta_tb p ( nolock)
join escolha_tp_cob_generico_tb e ( nolock)
on e.proposta_id = p.proposta_id
and p.situacao = 'i' 
and p.dt_inclusao > '20190101'
join seguros_db.dbo.cliente_tb c (nolock)
on c.cliente_id = p.prop_cliente_id
where p.proposta_id not in (50675590,49076350,49104286)  
and p.produto_id = 1223
and e.dt_fim_vigencia_esc is null
 and exists (SELECT TOP 1 1
						FROM CLIENTE_DB.DBO.PESSOA_ANOTACAO_VW ANOTACAO
						WHERE 1 = 1
							AND CPF_CNPJ = c.CPF_CNPJ
							AND ANOTACAO.MOTIVO_RESTRICAO_ID IN (
								6
								,12
								)
							AND ANOTACAO.MOTIVO_LIBERACAO_ID IS NULL)


select * from #teste a
join seguros_db.dbo.apolice_tb ap
on a.proposta_id = ap.proposta_id
where a.produto_id in ( 104,106,106,108,116,809,1176,1220,1221,1222,1223,1224,1241)
and a.tp_cobertura_id in (16,68,130,498,867,1001,1012)
and (ap.dt_fim_vigencia is null or ap.dt_fim_vigencia > getdate()+50)
order by 2


01138536024	50650137
--massa extra cenário 15,16
06522911477	50220126
07127189315	49878124


00535250193	49852838	1220  --fraude
01310224820	50316885	1221  --fraude
01190518775	49700561	1222  --fraude
00856759970	49076350	1223  --acao judicial
01138536024	49104286	1223  --fraude
05450521812	50698673	1241
29749695852	50279623	1241
--massa extra 1241 acao judicial
50470915

select e.tp_cobertura_id,tp.nome,* from escolha_tp_cob_generico_tb e
join tp_cobertura_tb tp
on e.tp_cobertura_id = tp.tp_cobertura_id where proposta_id = 50316885



select * from sinistro_equipamento_tb (nolock)
select * from sinistro_equipamento_importacao_tb (nolock)
select * from seguros_db.dbo.sinistro_esteira_flash_tb (nolock)
select * from seguros_db.dbo.evento_segbr_sinistro_detalhe_atual_tb where evento_id = 63569627

[14:25] Douglas da Silva
    
SELECT B.* FROM SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_ATUAL_TB  A WITH(NOLOCK)
INNER JOIN SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_DETALHE_ATUAL_TB B  WITH(NOLOCK)
ON A.EVENTO_ID = B.EVENTO_ID 
WHERE SINISTRO_ID = 14202000009


--EQUIPAMENTOS GRAVADOS
SELECT B.vl_perda_total,B.vl_reparo,A.tipo_dano,
A.* FROM SEGUROS_DB.DBO.SINISTRO_EQUIPAMENTO_TB A WITH(NOLOCK)
INNER JOIN SEGUROS_DB.DBO.sinistro_equipamento_importacao_tb B WITH(NOLOCK)
ON A.equipamento_id = B.equipamento_id
 WHERE SINISTRO_ID = 14202000009


 --STATUS DO PROCESSAMENTO
SELECT * FROM SEGUROS_DB.DBO.sinistro_esteira_flash_tb WITH(NOLOCK) WHERE SINISTRO_ID = 14202000009


--novos cenários de produtos nao afetados
massas selecionadas
50781967	1229	2019-11-11 00:00:00	2019-11-07 00:00:00	14287395	i
50781671	1229	2019-11-11 00:00:00	2019-11-08 00:00:00	14287467	i
50781726	1229	2019-11-11 00:00:00	2019-11-08 00:00:00	14287985	i

massas extras a serem conferidas caso necessário

50781792	1229	2019-11-11 00:00:00	2019-11-08 00:00:00	14288013	i
50781905	1229	2019-11-11 00:00:00	2019-11-08 00:00:00	14288015	i
50781896	1229	2019-11-11 00:00:00	2019-11-08 00:00:00	14288019	i
50782002	1229	2019-11-11 00:00:00	2019-11-08 00:00:00	14288081	i
50782091	1229	2019-11-11 00:00:00	2019-11-08 00:00:00	14288088	i
50782027	1229	2019-11-11 00:00:00	2019-11-08 00:00:00	14288158	i

50845949	1242	2019-11-18 00:00:00	2019-11-18 00:00:00	15348936	i
50845952	1242	2019-11-18 00:00:00	2019-11-18 00:00:00	15348938	i
50845951	1242	2019-11-18 00:00:00	2019-11-18 00:00:00	15348939	i
50845948	1242	2019-11-18 00:00:00	2019-11-18 00:00:00	15348941	i
50845953	1242	2019-11-18 00:00:00	2019-11-18 00:00:00	15348943	i
50845947	1242	2019-11-18 00:00:00	2019-11-18 00:00:00	15348944	i
50845946	1242	2019-11-18 00:00:00	2019-11-18 00:00:00	2175354	i






--cai em critica (31,32,33)

01138536024	50739524	1223 --tst
01558586962	50090851	1223 --cert
06522911477	50220126	1223 --hmg

--cai em critica (34,5)
01190518775	49700561	1222 --tst
05105048504	50519666	1222 --cert
06785735572	50438415	1222 --hmg

--Massas que passam em tudo produto 809: (36,37)
49655279  --teste
49850902 --cert
49698483 --hmg
49718585

--cae em critica 809 (38,39)
49669537 --tst
49669570 --cert
49607159 --hmg
49655280
 