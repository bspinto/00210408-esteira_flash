CREATE PROCEDURE dbo.SEGS14618_SPS 
AS
/*
	ntendencia 09/01/2019
	Demanda: 00210408-esteira_flash\
	Descri��o: procedure para validar pagto imediato.
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
 		IF OBJECT_ID('tempdb..#Equipamento') IS NOT NULL
		BEGIN
			DROP TABLE #Equipamento
		END

    CREATE TABLE #Equipamento (Codigo INT  
      ,TipoBem VARCHAR(60)  
      ,Modelo VARCHAR(60)  
      ,Dano INT  
      ,Orcamento INT
      ,Valor NUMERIC(15,2)  
      ,Laudo INT
      ,NomeAssistencia VARCHAR(60)  
      ,TelAssistencia VARCHAR(15)  
      ,Descricao VARCHAR(255))

    INSERT INTO #Equipamento (Codigo, TipoBem, Modelo, Dano, Orcamento, Valor, Laudo, NomeAssistencia, TelAssistencia, Descricao)
    SELECT TOP 1 ISNULL(a.equipamento_id, 0) as Codigo
      ,ISNULL(a.tipo_bem, 'Outros') AS TipoBem
      ,ISNULL(a.modelo, 'Outros') as Modelo
      ,1
      ,1
      ,'23.30'
      ,1
      ,'Assistencia teste'
      ,'(32) 3211-3444'
      ,'Descricao teste'
    FROM seguros_db.dbo.sinistro_equipamento_tb a WITH (NOLOCK)
    
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14618_SPS ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON


	-- Declara��o e tratamento de variaveis (inicio)
	DECLARE @PgtoImediato AS SMALLINT = 1 -- (1-Sim / N�o)
	DECLARE @Detalhamento AS VARCHAR(3000)
    DECLARE @ValorTotal   AS NUMERIC(15,2)
 	-- (fim) Declara��o e tratamento de variaveis 
 	
 	--constantes
  DECLARE @LaudoSim AS SMALLINT = 0 
  DECLARE @LaudoNao AS SMALLINT = 1
  DECLARE @OrcamentoSim AS SMALLINT = 1
  DECLARE @orcamentoNao AS SMALLINT = 0
  DECLARE @reparo AS SMALLINT = 1
  DECLARE @PerdaTotal AS SMALLINT = 2
 	
	-----------
	BEGIN TRY
		-- Bloco de codifica��o da procedure (inicio)
		--Valor total estimado
	  SELECT @ValorTotal = SUM(Valor) 
	  FROM #Equipamento	
	  
	  --or�amentos n�o
	  IF EXISTS(SELECT TOP 1 1 FROM #Equipamento a WHERE a.orcamento = @orcamentoNao)
	  BEGIN	  
	    SET @PgtoImediato = 0	    	    
	    SET @Detalhamento = 'O pagamento imediato foi negado, pois foram informados equipamentos sem valor de or�amento.' 
	  END
	  ELSE
	  BEGIN
	    --VALIDA��O DO VALOR ESTIMATIVA DE PREJUIZO: VALOR MAIOR QUE O PARAMETRIZADO FLUXO COMUM
	    IF EXISTS(SELECT TOP 1 1
	              FROM seguros_db.dbo.sinistro_equipamento_tb a WITH (NOLOCK)
	              INNER JOIN #equipamento b
	              ON a.equipamento_id = b.codigo
	              WHERE b.dano = @PerdaTotal
	              AND b.Valor > a.vl_perda_total)
	    OR EXISTS(SELECT TOP 1 1
	              FROM seguros_db.dbo.sinistro_equipamento_tb a WITH (NOLOCK)
	              INNER JOIN #equipamento b
	              ON a.equipamento_id = b.codigo
	              WHERE b.dano = @reparo
	              AND b.Valor > a.vl_reparo)
	    BEGIN
	      SET @PgtoImediato = 0	  
	      SET @Detalhamento = 'O pagamento imediato foi negado, pois foram informados equipamentos com valor de or�amento superior ao limite permitido.'      
	    END
	  --VALIDA��O LAUDO: EQUIPAMENTOS SEM LAUDO FLUXO COMUM
	  IF EXISTS(SELECT TOP 1 1 FROM #Equipamento a WHERE a.laudo = @LaudoNao)
	  BEGIN	  
	    SET @PgtoImediato = 0	    	    
	    SET @Detalhamento = 'O pagamento imediato foi negado, pois foram informados equipamentos sem valor de or�amento.' 
	  END
	  END
	  
	  if @PgtoImediato = 1
	  BEGIN
	  
				DECLARE @proposta_id INT,
				@produto_id INT,
				@ramo_id	INT,
				@evento_id INT,
				@tp_cobertura_id INT,
				@sinistro_parametro_chave_id INT,
				@estimativa_prejuizo numeric(15,2),

				@CPF_CNPJ VARCHAR(30)


				select * from desenv_db.dbo.tp_sinistro_parametro_tb
				select * from desenv_db.dbo.sinistro_parametro_regra_tb
				select * from desenv_db.dbo.sinistro_parametro_chave_tb
				select * from desenv_db.dbo.sinistro_parametro_chave_regra_tb



			--1. Valida��o do produto/ramo/evento/cobertura
			--ALS_PRODUTO_TB  ITEM_REGRA_TB
			-- PEGANDO A CHAVE DOS PARAMETROS
			--OBS.: UTILIZAR A TAG(NOME)  DA PARAMETRO_REGRA_TB PARA BUSCAR O PARAMETRO CORRESPONDENTE A VALIDACAO EX.: "valor_estimativa" 	

				--ENCONTRANDO A CHAVE VIGENTE DA PROPOSTA
				select b.sinistro_parametro_chave_id,* from desenv_db.dbo.tp_sinistro_parametro_tb a
				inner join desenv_db.dbo.sinistro_parametro_chave_tb b
				on a.tp_sinistro_parametro_id = b.tp_sinistro_parametro_id
				inner join desenv_db.dbo.sinistro_parametro_chave_regra_tb c
				on b.sinistro_parametro_chave_id = c.sinistro_parametro_chave_id
				inner join desenv_db.dbo.sinistro_parametro_regra_tb d
				on c.sinistro_parametro_regra_id = d.sinistro_parametro_regra_id
				where 1 = 1
				and b.produto_id = 1241 -- @produto_id
				and b.ramo_id = 14 --@ramo_id
				and b.evento_sinistro_id = 3 --@evento_id
				and b.tp_cobertura_id = 16 --@tp_cobertura_id
				and b.dt_fim_vigencia is null
				

		
--######################  INICIO VALIDA��O DOS PARAMETROS ######################
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
			--1� VALIDA��O DA ESTIMATIVA 'valor_estimativa'

				--BUSCANDO A REGRA PARAMETRIZADA
				/*
				SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo  ,* FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
				ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
				WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
				  AND REGRA.NOME = 'valor_estimativa'
				 */
				DECLARE @SQL AS NVARCHAR(1000),
						@ParmDefinition AS NVARCHAR(300),
						@ValorTotal AS  NUMERIC(15,2) = 3000,
						@PgtoImediato AS SMALLINT = 1, 
						@sinistro_parametro_chave_id  AS int = 1079

					SELECT @sql = 'IF( ' + convert(VARCHAR(20), convert(NUMERIC(15, 2), @ValorTotal)) 
										 + ' ' 
										 + OPERADOR 
										 + ' ' 
										 + VALOR + ') 
										SELECT @retornoOUT = 1
									ELSE 
									SELECT @retornoOUT = 0
									'									
					FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					inner join desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						on CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
					 WHERE 1=1
						 AND CHAVE.sinistro_parametro_chave_id = 1079  --@sinistro_parametro_chave_id
						 AND REGRA.NOME = 'valor_estimativa' --PARAMETRO

					SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';

					EXECUTE sp_executesql @SQL,
						@ParmDefinition,
						@retornoOUT = @PgtoImediato OUTPUT

						select @sql

					SELECT @PgtoImediato as PgtoImediato

					IF @PgtoImediato 
					BEGIN
						SELECT @Detalhamento = @Detalhamento + CHAR(13)+'Estimativa/Or�amento dentro do limite para pagamento imediato: ' + @ValorTotal +' '+ CHAVE.operador +' '+CHAVE.valor + CHAR(13) FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
							INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
						WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
						  AND REGRA.NOME = 'valor_estimativa'
					END
					ELSE
					BEGIN
					 SELECT @Detalhamento = @Detalhamento + CHAR(13)+'Estimativa/Or�amento acima do limite para pagamento imediato: ' + @ValorTotal +' '+ CHAVE.operador +' '+CHAVE.valor + CHAR(13)FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
							INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
						WHERE CHAVE.sinistro_parametro_chave_id = 158 --@sinistro_parametro_chave_id
						  AND REGRA.NOME = 'valor_estimativa'
					END
-----------------------------------------------------------------------------------------------------------------------------------------------------
			--2� VALIDA��O DA ESTIMATIVA 'periodo_relacionamento'

				--BUSCANDO A REGRA PARAMETRIZADA
				/*
				SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
				ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
				WHERE CHAVE.sinistro_parametro_chave_id = 158 --@sinistro_parametro_chave_id
				  AND REGRA.NOME = 'periodo_relacionamento'
				 */

				-- Ap�lices emitidas para o cliente
				-- ENCONTRANDO O CLIENTE 

-----------------------------------------------------------------------------------------------------------------------------------------------------
			--3� VALIDA��O DA ESTIMATIVA 'periodo_sem_sinistro'							

					--ENCONTRANDO AS PROPOSTAS DO CLIENTE                    
					IF (object_id('tempdb..#propostas') IS NOT NULL)
					BEGIN
                        DROP TABLE #propostas
					END				
                
					SELECT DISTINCT 
                        PROPOSTA.PRODUTO_ID,
                        CLIENTE.cpf_cnpj AS CPF_CNPJ,
                        PROPOSTA.proposta_id,
                        ISNULL(APOLICE.dt_inicio_vigencia,ISNULL(ADESAO.dt_inicio_vigencia,FECHADA.dt_inicio_vig)) AS dt_inicio_vigencia,
                        ISNULL(APOLICE.dt_fim_vigencia,ISNULL(ADESAO.dt_fim_vigencia,FECHADA.dt_fim_vig)) AS dt_fim_vigencia,                        
                        GETDATE() - 365 as data_limite,
						GETDATE() as data_atual
                    into #propostas
                    FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)
                    INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK)
                        ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID
                    LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB FECHADA WITH(NOLOCK)
                    ON PROPOSTA.PROPOSTA_ID = FECHADA.PROPOSTA_ID
                    LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ADESAO WITH(NOLOCK)
                    ON PROPOSTA.PROPOSTA_ID = ADESAO.PROPOSTA_ID
                    LEFT JOIN seguros_db.dbo.apolice_tb APOLICE WITH(NOLOCK)
                    ON PROPOSTA.proposta_id = APOLICE.proposta_id
                    WHERE CLIENTE.cpf_cnpj = @CPF_CNPJ
                    AND PROPOSTA.ramo_id = 14 --@RAMO_ID
                    and PROPOSTA.situacao = 'i'
                    order by 4
					--SELECT * FROM #propostas										
                    
					--REMOVENDO AS PROPOSTAS QUE TIVERAM VIGENCIA FINDADAS ANTES DA DATA LIMITE
                    DELETE PROP
                    --SELECT *
                    FROM #PROPOSTAS PROP
                    WHERE dt_fim_vigencia < data_limite
					--SELECT * FROM #propostas order by 4
                    
                    --criando campos de apoio
					ALTER TABLE #propostas ADD ID INT IDENTITY(11,1)					
                    
					ALTER TABLE #propostas ADD DIAS INT 					
                    
					ALTER TABLE #propostas ADD FLAG BIT -- VAI MOSTRAR QUE N�O TEM BRECHAS                                        
					--SELECT * FROM #propostas order by 4
					       
					-- 
					UPDATE A
                    SET A.DT_INICIO_VIGENCIA = B.DT_FIM_VIGENCIA , A.FLAG = 1
                    FROM #propostas A
                    INNER JOIN #propostas B
                    ON A.ID = B.ID + 1
                    WHERE B.DT_FIM_VIGENCIA BETWEEN A.DT_INICIO_VIGENCIA AND A.DT_FIM_VIGENCIA                        
                    --SELECT * FROM #propostas order by 4

					DELETE PROP                    
                    FROM #PROPOSTAS PROP
                    WHERE dt_inicio_vigencia > data_atual
					--SELECT * FROM #propostas order by 4
                              
                    -- ajustar in�cio =  data_limite (do primeiro registro caso esteja anteda da data limite)
					UPDATE A
                    SET A.DT_INICIO_VIGENCIA = data_limite
                    FROM #propostas A                    
                    WHERE A.dt_inicio_vigencia < a.data_limite AND --A.dt_inicio_vigencia BETWEEN A.data_limite AND A.data_atual and
					ID = (select top(1) id from #propostas order by id asc)
                    --SELECT * FROM #propostas order by 4
					
					-- ajustar data final = data atual (do ultimo registro)
					UPDATE A
                    SET A.dt_fim_vigencia = data_atual
                    FROM #propostas A                    
                    WHERE A.dt_fim_vigencia > A.data_atual  AND --A.dt_inicio_vigencia BETWEEN A.data_limite AND A.data_atual and
					ID = (select top(1) id from #propostas order by id desc)
					--SELECT * FROM #propostas order by 4
					
					UPDATE #propostas
                    SET DIAS = DATEDIFF(day, DT_INICIO_VIGENCIA,DT_FIM_VIGENCIA)
                    FROM #propostas
					--SELECT * FROM #propostas order by 4															
					
					if ((SELECT sum(dias)  FROM #propostas) < 365) or ((SELECT sum(dias)  FROM #propostas) is null)
						--select 0
						Set @PgtoImediato = 0
					else
						--select 1					                            			
						Set @PgtoImediato = 1

					select @ValorTotal = sum(dias)  FROM #propostas

					SELECT @sql = 'IF( ' + convert(VARCHAR(20), convert(NUMERIC(15, 2), @ValorTotal)) 
										 + ' ' 
										 + OPERADOR 
										 + ' ' 
										 + VALOR + ') 
										SELECT @retornoOUT = 1
									ELSE 
									SELECT @retornoOUT = 0
									'									 					
					FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					inner join desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						on CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
					 WHERE 1=1
						 AND CHAVE.sinistro_parametro_chave_id = 1079  --@sinistro_parametro_chave_id
						 AND REGRA.NOME = 'periodo_sem_sinistro' --PARAMETRO

					SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';

					EXECUTE sp_executesql @SQL,
						@ParmDefinition,
						@retornoOUT = @PgtoImediato OUTPUT						

					--select @sql					

					--SELECT @PgtoImediato as PgtoImediato

					IF @PgtoImediato <> 1 
					BEGIN						
						select @Detalhamento = ''						
						SELECT @Detalhamento = @Detalhamento + CHAR(13)+'N�o eleg�vel para pagamento imediato com ' + convert(nvarchar,@ValorTotal) +' dias sem sinistro.'+CHAR(13) FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
							INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
						WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
						  AND REGRA.NOME = 'periodo_sem_sinistro'
					END
					ELSE
					BEGIN
						select @Detalhamento = ''						
						SELECT @Detalhamento = @Detalhamento + CHAR(13)+'Eleg�vel para pagamento imediato com mais de ' + convert(nvarchar,@ValorTotal) +' dias sem sinistro.'+CHAR(13) FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
							INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
							ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
							WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
							AND REGRA.NOME = 'periodo_sem_sinistro'
					END									

-----------------------------------------------------------------------------------------------------------------------------------------------------
			--5. Contagem de dias do ultimo sinsitro tem que ser superior ao per�odo de 1 ano no CPF e no ramo 14
				--BUSCANDO A REGRA PARAMETRIZADA
				/*
				SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
				ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
				WHERE CHAVE.sinistro_parametro_chave_id = 158 --@sinistro_parametro_chave_id
				  AND REGRA.NOME = 'periodo_possui_sinistro'

				  -- ENCONTRANDO CASO DE TESTE
				  select cpf_cgc_segurado ,count(*) as qntd from SEGUROS_DB.DBO.evento_SEGBR_sinistro_tb where evento_bb_id = 1100   group by cpf_cgc_segurado HAVING COUNT(*)> 2
				

					select evento_SEGBR_sinistro.dt_inclusao, evento_SEGBR_sinistro.evento_bb_id, C.descricao, * from SEGUROS_DB.DBO.evento_SEGBR_sinistro_tb evento_SEGBR_sinistro WITH(NOLOCK)
					INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB B WITH(NOLOCK)
					ON evento_SEGBR_sinistro.PROPOSTA_ID = B.PROPOSTA_ID
					Inner join  seguros_db.dbo.evento_SEGBR_tb C WITH(NOLOCK)
					on C.evento_segbr_id = evento_SEGBR_sinistro.evento_segbr_id
					WHERE 1=1
				    and evento_SEGBR_sinistro.cpf_cgc_segurado = '02979888087'
					and evento_SEGBR_sinistro.evento_bb_id = 1100
					--and b.proposta_id = 47659601
					ORDER BY 1

					 */


					DECLARE @DT_ULTIMO_SINISTRO_AVISADO SMALLDATETIME,
					@CONTAGEM_DIAS_ULTIMO_SINISTRO INT

					select @DT_ULTIMO_SINISTRO_AVISADO = max(evento_SEGBR_sinistro.dt_inclusao) from SEGUROS_DB.DBO.evento_SEGBR_sinistro_tb evento_SEGBR_sinistro WITH(NOLOCK)
					WHERE 1=1
				    and evento_SEGBR_sinistro.cpf_cgc_segurado = '02979888087'
					and evento_SEGBR_sinistro.evento_bb_id = 1100
					AND evento_SEGBR_sinistro.ramo_id =14 

					SELECT @CONTAGEM_DIAS_ULTIMO_SINISTRO = CONVERT(INT, DATEDIFF(DAY, @DT_ULTIMO_SINISTRO_AVISADO, GETDATE())) 
					
					SELECT @CONTAGEM_DIAS_ULTIMO_SINISTRO


					DECLARE @SQL AS NVARCHAR(1000),
						@ParmDefinition AS NVARCHAR(300),
						@ValorTotal AS  NUMERIC(15,2) = 3000,
						@PgtoImediato AS SMALLINT = 1, 
						@sinistro_parametro_chave_id  AS int = 1079

					SELECT @sql = 'IF( ' + convert(VARCHAR(20), convert(NUMERIC(15, 2), @CONTAGEM_DIAS_ULTIMO_SINISTRO)) 
										 + ' ' 
										 + OPERADOR 
										 + ' ' 
										 + VALOR + ') 
										SELECT @retornoOUT = 1
									ELSE 
									SELECT @retornoOUT = 0
									'
					FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					inner join desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						on CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
					 WHERE 1=1
						 AND CHAVE.sinistro_parametro_chave_id = 1079  --@sinistro_parametro_chave_id
						 AND REGRA.NOME = 'periodo_possui_sinistro' --PARAMETRO


					SELECT @sql


					SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';

					EXECUTE sp_executesql @SQL,
						@ParmDefinition,
						@retornoOUT = @PgtoImediato OUTPUT

					SELECT @PgtoImediato as PgtoImediato

					IF @PgtoImediato 
					BEGIN
						SELECT @Detalhamento = @Detalhamento + CHAR(13)+ 'Contagem de dias do ultimo sinistro esta dentro do limite para pagamento imediato: ' + @ValorTotal +' '+ CHAVE.operador +' '+CHAVE.valor  FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
							INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
						WHERE CHAVE.sinistro_parametro_chave_id = 158 --@sinistro_parametro_chave_id
						  AND REGRA.NOME = 'periodo_possui_sinistro'
					END
					ELSE
					BEGIN
					 SELECT @Detalhamento = @Detalhamento + CHAR(13)+ 'Contagem de dias do ultimo sinistro esta acima do limite para pagamento imediato: ' + @ValorTotal +' '+ CHAVE.operador +' '+CHAVE.valor FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
							INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
						WHERE CHAVE.sinistro_parametro_chave_id = 158 --@sinistro_parametro_chave_id
						  AND REGRA.NOME = 'periodo_possui_sinistro'
					END


-----------------------------------------------------------------------------------------------------------------------------------------------------
			--6. **************************************	N�o possuir restri��o = A��o Judicial / Indicio de Fraude
				 --??
					--Consta na Lista de Pr�-Analise (A��o Judicial / Indicio de Fraude): <Sim / N�o>. 
					--Olhar pelo CPF se est� nesta lista. Caso sim, e estiver ativo, n�o poder� ser Pagamento Imediato.





-----------------------------------------------------------------------------------------------------------------------------------------------------
			--7� VALIDA��O DA ADIMPLENCIA 'verifica_pgto_em_dia'

				--BUSCANDO A REGRA PARAMETRIZADA
				/*
				SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
				ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
				WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
				  AND REGRA.NOME = 'verifica_pgto_em_dia'
				 */

				DECLARE @VALIDA_PAGAMENTO CHAR(1)

				SELECT @VALIDA_PAGAMENTO = CHAVE.VALOR FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
				ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
				WHERE CHAVE.sinistro_parametro_chave_id = 158 --@sinistro_parametro_chave_id
				  AND REGRA.NOME = 'verifica_pgto_em_dia'

				  SELECT @VALIDA_PAGAMENTO

				 IF @VALIDA_PAGAMENTO = 'S'
				 BEGIN
					
					
					IF EXISTS (SELECT 1 FROM SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB COBRANCA WITH(NOLOCK)
					WHERE 1=1
					AND PROPOSTA_ID = 43224828 --@PROPOSTA_ID
					AND COBRANCA.situacao <> 'I')

					BEGIN
					 SET @PgtoImediato = 0	    	    
					 SET @Detalhamento = @Detalhamento + CHAR(13)+ 'A proposta '  + @proposta_id + ' est� adimplente'
					END
					ELSE 
					BEGIN
					 SET @Detalhamento = @Detalhamento + CHAR(13)+'A proposta '  + @proposta_id + ' est� inadimplente'
					END

				 END


	    END
	 
		SELECT @ValorTotal AS Valor
		,@Detalhamento AS Detalhamento
		,@PgtoImediato AS PagtoImediato								
		-- (fim) Bloco de codifica��o da procedure
		-----------				
		SET NOCOUNT OFF

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO

			
	