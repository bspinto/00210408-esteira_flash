VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Sinistro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarSinistro_id As String
Private mvarSinistro_id_lider As String
Private mvarSinistro_bb As String
Private mvarSucursal_id As String
Private mvarSeguradora_id As String

Private mvarSucursal_nome As String
Private mvarCod_Objeto_Segurado As String
Private mvarRamo_id As String
Private mvarNomeRamo As String
Private mvarProposta_id As String
Private mvarPropostaBB As String
Private mvarCausa_id As String
Private mvarCausa As String
Private mvarSubCausa As String
Private mvarSubCausa_id As String
Private mvardt_Aviso As String
Private mvardt_Ocorrencia As String
Private mvarEvento_id As String
Private mvarHoraOcorrencia As String
Private mvarLocalOcorrencia As String

Private mvarSolicitanteNome As String
Private mvarSolicitanteEndereco As String
Private mvarSolicitanteBairro As String
Private mvarSolicitanteEmail As String
Private mvarSolicitanteCEP As String
Private mvarSolicitanteCPF As String '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante
Private mvarSolicitanteMunicipio_id As String
Private mvarSolicitanteMunicipio As String
Private mvarSolicitanteEstado As String
Private mvarSolicitanteGrauParentesco As String

Private mvarddd1 As String
Private mvartelefone1 As String
Private mvarramal1 As String
Private mvartp_telefone1 As String

Private mvarddd2 As String
Private mvartelefone2 As String
Private mvarramal2 As String
Private mvartp_telefone2 As String

Private mvarddd3 As String
Private mvartelefone3 As String
Private mvarramal3 As String
Private mvartp_telefone3 As String

Private mvarddd4 As String
Private mvartelefone4 As String
Private mvarramal4 As String
Private mvartp_telefone4 As String

Private mvarddd5 As String
Private mvartelefone5 As String
Private mvarramal5 As String
Private mvartp_telefone5 As String

Private mvarApolice_id As String
Private mvarProduto_id As String
Private mvarNomeProduto As String
Private mvarEndosso_id As String

Private mvarSeguradoCPF_CNPJ As String
Private mvarSeguradoDtNasc As String
Private mvarSeguradoCliente_id As String
Private mvarSeguradoNomeCliente As String
Private mvarSeguradoSexo As String
Private mvarSeguradoComponente As String
Private mvarNomeTipoComponente As String

Private mvarSinistradoClienteId As String
Private mvarSinistradoCPF As String
Private mvarSinistradoDtNasc As String
Private mvarSinistradoNomeCliente As String
Private mvarSinistradoSexo As String
Private mvarIndTpSinistrado As String

Private mvarNumProcRemessa As String

Private mvarNumRemessa As String
Private mvarSitProposta As String
Private mvarSituacaoSinistro As String
Private mvarBanco As String
Private mvarAgenciaNome As String
Private mvarAgenciaAviso As String
Private mvarAgenciaDvAviso As String
Private mvarAgenciaEndereco As String
Private mvardt_inicio_vigencia_seg As String

Private mvarInd_reanalise As String
Private mvarMoeda As String
Private mvarLocalizacao As String
Private mvarEvento_bb_id As String
Private mvarEvento_Segbr_id As String
Private mvarCodRamo As String


Private mvarCD_PRD As String
Private mvarCD_MDLD As String
Private mvarCD_ITEM_MDLD As String
Private mvarCod_transacao As String

Private mvarVal_informado As String

Private mvarFlagCredito As String

Public Property Let FlagCredito(ByVal vData As String)
    mvarFlagCredito = vData
End Property

Public Property Get FlagCredito() As String
    FlagCredito = mvarFlagCredito
End Property

Public Property Let sinistro_id(ByVal vData As String)
    mvarSinistro_id = vData
End Property

Public Property Get sinistro_id() As String
    sinistro_id = mvarSinistro_id
End Property

Public Property Let Sinistro_id_lider(ByVal vData As String)
   mvarSinistro_id_lider = vData
End Property

Public Property Get Sinistro_id_lider() As String
    Sinistro_id_lider = mvarSinistro_id_lider
End Property

Public Property Let Sinistro_bb(ByVal vData As String)
    mvarSinistro_bb = vData
End Property

Public Property Get Sinistro_bb() As String
    Sinistro_bb = mvarSinistro_bb
End Property

Public Property Let Sucursal_id(ByVal vData As String)
    mvarSucursal_id = vData
End Property

Public Property Get Sucursal_id() As String
    Sucursal_id = mvarSucursal_id
End Property

Public Property Let Seguradora_id(ByVal vData As String)
    mvarSeguradora_id = vData
End Property

Public Property Get Seguradora_id() As String
   Seguradora_id = mvarSeguradora_id
End Property

Public Property Let Sucursal_nome(ByVal vData As String)
    mvarSucursal_nome = vData
End Property

Public Property Get Sucursal_nome() As String
    Sucursal_nome = mvarSucursal_nome
End Property

Public Property Let Cod_Objeto_Segurado(ByVal vData As String)
    mvarCod_Objeto_Segurado = vData
End Property

Public Property Get Cod_Objeto_Segurado() As String
    Cod_Objeto_Segurado = mvarCod_Objeto_Segurado
End Property

Public Property Let ramo_id(ByVal vData As String)
    mvarRamo_id = vData
End Property

Public Property Get ramo_id() As String
    ramo_id = mvarRamo_id
End Property

Public Property Let NomeRamo(ByVal vData As String)
    mvarNomeRamo = vData
End Property

Public Property Get NomeRamo() As String
    NomeRamo = mvarNomeRamo
End Property

Public Property Let Causa_id(ByVal vData As String)
    mvarCausa_id = vData
End Property

Public Property Get Causa_id() As String
    Causa_id = mvarCausa_id
End Property

Public Property Let SubCausa_id(ByVal vData As String)
    mvarSubCausa_id = vData
End Property

Public Property Get SubCausa_id() As String
    SubCausa_id = mvarSubCausa_id
End Property

Public Property Let proposta_id(ByVal vData As String)
    mvarProposta_id = vData
End Property

Public Property Get proposta_id() As String
    proposta_id = mvarProposta_id
End Property

Public Property Let PropostaBB(ByVal vData As String)
    mvarPropostaBB = vData
End Property

Public Property Get PropostaBB() As String
    PropostaBB = mvarPropostaBB
End Property

Public Property Let Causa(ByVal vData As String)
    mvarCausa = vData
End Property

Public Property Get Causa() As String
    Causa = mvarCausa
End Property

Public Property Let SubCausa(ByVal vData As String)
    mvarSubCausa = vData
End Property

Public Property Get SubCausa() As String
    SubCausa = mvarSubCausa
End Property

Public Property Let dt_ocorrencia(ByVal vData As String)
    mvardt_Ocorrencia = vData
End Property

Public Property Get dt_ocorrencia() As String
    dt_ocorrencia = mvardt_Ocorrencia
End Property

Public Property Get HoraOcorrencia() As String
    HoraOcorrencia = mvarHoraOcorrencia
End Property

Public Property Let HoraOcorrencia(ByVal vData As String)
    mvarHoraOcorrencia = vData
End Property

Public Property Get LocalOcorrencia() As String
    LocalOcorrencia = mvarLocalOcorrencia
End Property

Public Property Let LocalOcorrencia(ByVal vData As String)
    mvarLocalOcorrencia = vData
End Property

Public Property Let evento_id(ByVal vData As String)
     mvarEvento_id = vData
End Property

Public Property Get evento_id() As String
    evento_id = mvarEvento_id
End Property

Public Property Let dt_Aviso(ByVal vData As String)
    mvardt_Aviso = vData
End Property

Public Property Get dt_Aviso() As String
    dt_Aviso = mvardt_Aviso
End Property

Public Property Let SolicitanteEstado(ByVal vData As String)
    mvarSolicitanteEstado = vData
End Property

Public Property Get SolicitanteEstado() As String
    SolicitanteEstado = mvarSolicitanteEstado
End Property

Public Property Let SolicitanteMunicipio(ByVal vData As String)
    mvarSolicitanteMunicipio = vData
End Property

Public Property Get SolicitanteMunicipio() As String
    SolicitanteMunicipio = mvarSolicitanteMunicipio
End Property

Public Property Let SolicitanteMunicipio_id(ByVal vData As String)
    mvarSolicitanteMunicipio_id = vData
End Property

Public Property Get SolicitanteMunicipio_id() As String
    SolicitanteMunicipio_id = mvarSolicitanteMunicipio_id
End Property

Public Property Let SolicitanteBairro(ByVal vData As String)
    mvarSolicitanteBairro = vData
End Property

Public Property Get SolicitanteBairro() As String
    SolicitanteBairro = mvarSolicitanteBairro
End Property

Public Property Let SolicitanteEndereco(ByVal vData As String)
    mvarSolicitanteEndereco = vData
End Property

Public Property Get SolicitanteEndereco() As String
    SolicitanteEndereco = mvarSolicitanteEndereco
End Property

Public Property Let SolicitanteEmail(ByVal vData As String)
    mvarSolicitanteEmail = vData
End Property

Public Property Get SolicitanteEmail() As String
    SolicitanteEmail = mvarSolicitanteEmail
End Property

Public Property Let SolicitanteCEP(ByVal vData As String)
    mvarSolicitanteCEP = vData
End Property

Public Property Get SolicitanteCEP() As String
    SolicitanteCEP = mvarSolicitanteCEP
End Property

'07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante (inicio)
Public Property Let SolicitanteCPF(ByVal vData As String)
    mvarSolicitanteCPF = vData
End Property

Public Property Get SolicitanteCPF() As String
    SolicitanteCPF = mvarSolicitanteCPF
End Property
'07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante (fim)

Public Property Let SolicitanteGrauParentesco(ByVal vData As String)
   mvarSolicitanteGrauParentesco = vData
End Property

Public Property Get SolicitanteGrauParentesco() As String
    SolicitanteGrauParentesco = mvarSolicitanteGrauParentesco
End Property

Public Property Let ddd1(ByVal vData As String)
    mvarddd1 = vData
End Property

Public Property Get ddd1() As String
    ddd1 = mvarddd1
End Property

Public Property Let ddd2(ByVal vData As String)
    mvarddd2 = vData
End Property

Public Property Get ddd2() As String
    ddd2 = mvarddd2
End Property

Public Property Let ddd3(ByVal vData As String)
    mvarddd3 = vData
End Property

Public Property Get ddd3() As String
    ddd3 = mvarddd3
End Property

Public Property Let ddd4(ByVal vData As String)
    mvarddd4 = vData
End Property

Public Property Get ddd4() As String
    ddd4 = mvarddd4
End Property

Public Property Let ddd5(ByVal vData As String)
    mvarddd5 = vData
End Property

Public Property Get ddd5() As String
    ddd5 = mvarddd5
End Property

Public Property Get telefone1() As String
    telefone1 = mvartelefone1
End Property

Public Property Let telefone1(ByVal vData As String)
    mvartelefone1 = vData
End Property

Public Property Get telefone2() As String
    telefone2 = mvartelefone2
End Property

Public Property Let telefone2(ByVal vData As String)
    mvartelefone2 = vData
End Property

Public Property Get telefone3() As String
    telefone3 = mvartelefone3
End Property

Public Property Let telefone3(ByVal vData As String)
    mvartelefone3 = vData
End Property

Public Property Get telefone4() As String
    telefone4 = mvartelefone4
End Property

Public Property Let telefone4(ByVal vData As String)
    mvartelefone4 = vData
End Property

Public Property Get telefone5() As String
    telefone5 = mvartelefone5
End Property

Public Property Let telefone5(ByVal vData As String)
    mvartelefone5 = vData
End Property

Public Property Let ramal1(ByVal vData As String)
    mvarramal1 = vData
End Property

Public Property Get ramal1() As String
    ramal1 = mvarramal1
End Property

Public Property Let ramal2(ByVal vData As String)
    mvarramal2 = vData
End Property

Public Property Get ramal2() As String
    ramal2 = mvarramal2
End Property

Public Property Let ramal3(ByVal vData As String)
    mvarramal3 = vData
End Property

Public Property Get ramal3() As String
    ramal3 = mvarramal3
End Property

Public Property Let ramal4(ByVal vData As String)
    mvarramal4 = vData
End Property

Public Property Get ramal4() As String
    ramal4 = mvarramal4
End Property

Public Property Let ramal5(ByVal vData As String)
    mvarramal5 = vData
End Property

Public Property Get ramal5() As String
    ramal5 = mvarramal5
End Property

Public Property Let tp_telefone1(ByVal vData As String)
     mvartp_telefone1 = vData
End Property

Public Property Get tp_telefone1() As String
    tp_telefone1 = mvartp_telefone1
End Property

Public Property Let tp_telefone2(ByVal vData As String)
     mvartp_telefone2 = vData
End Property

Public Property Get tp_telefone2() As String
    tp_telefone2 = mvartp_telefone2
End Property

Public Property Let tp_telefone3(ByVal vData As String)
     mvartp_telefone3 = vData
End Property

Public Property Get tp_telefone3() As String
    tp_telefone3 = mvartp_telefone3
End Property

Public Property Let tp_telefone4(ByVal vData As String)
     mvartp_telefone4 = vData
End Property

Public Property Get tp_telefone4() As String
    tp_telefone4 = mvartp_telefone4
End Property

Public Property Let tp_telefone5(ByVal vData As String)
     mvartp_telefone5 = vData
End Property

Public Property Get tp_telefone5() As String
    tp_telefone5 = mvartp_telefone5
End Property

Public Property Let SeguradoNomeCliente(ByVal vData As String)
    mvarSeguradoNomeCliente = vData
End Property

Public Property Get SeguradoNomeCliente() As String
   SeguradoNomeCliente = mvarSeguradoNomeCliente
End Property

Public Property Let SeguradoCliente_id(ByVal vData As String)
    mvarSeguradoCliente_id = vData
End Property

Public Property Get SeguradoCliente_id() As String
    SeguradoCliente_id = mvarSeguradoCliente_id
End Property

Public Property Let SeguradoCPF_CNPJ(ByVal vData As String)
    mvarSeguradoCPF_CNPJ = vData
End Property

Public Property Get SeguradoCPF_CNPJ() As String
    SeguradoCPF_CNPJ = mvarSeguradoCPF_CNPJ
End Property

Public Property Let SeguradoDtNasc(ByVal vData As String)
    mvarSeguradoDtNasc = vData
End Property

Public Property Get SeguradoDtNasc() As String
    SeguradoDtNasc = mvarSeguradoDtNasc
End Property

Public Property Let SeguradoSexo(ByVal vData As String)

    mvarSeguradoSexo = vData
End Property

Public Property Get SeguradoSexo() As String
    SeguradoSexo = mvarSeguradoSexo
End Property

Public Property Let NomeProduto(ByVal vData As String)
    mvarNomeProduto = vData
End Property

Public Property Get NomeProduto() As String
    NomeProduto = mvarNomeProduto
End Property

Public Property Let produto_id(ByVal vData As String)
    mvarProduto_id = vData
End Property

Public Property Get produto_id() As String
    produto_id = mvarProduto_id
End Property

Public Property Let Apolice_id(ByVal vData As String)
    mvarApolice_id = vData
End Property

Public Property Get Apolice_id() As String
    Apolice_id = mvarApolice_id
End Property

Public Property Let Endosso_id(ByVal vData As String)
    mvarEndosso_id = vData
End Property

Public Property Get Endosso_id() As String
    Endosso_id = mvarEndosso_id
End Property

Public Property Let SitProposta(ByVal vData As String)
    mvarSitProposta = vData
End Property

Public Property Get SitProposta() As String
    SitProposta = mvarSitProposta
End Property

Public Property Let AgenciaAviso(ByVal vData As String)
    mvarAgenciaAviso = vData
End Property

Public Property Get AgenciaAviso() As String
    AgenciaAviso = mvarAgenciaAviso
End Property

Public Property Let AgenciaNome(ByVal vData As String)
   mvarAgenciaNome = vData
End Property

Public Property Get AgenciaNome() As String
   AgenciaNome = mvarAgenciaNome
End Property

Public Property Let AgenciaDvAviso(ByVal vData As String)
   mvarAgenciaDvAviso = vData
End Property

Public Property Get AgenciaDvAviso() As String
   AgenciaDvAviso = mvarAgenciaDvAviso
End Property

Public Property Let AgenciaEndereco(ByVal vData As String)
   mvarAgenciaEndereco = vData
End Property

Public Property Get AgenciaEndereco() As String
   AgenciaEndereco = mvarAgenciaEndereco
End Property

Public Property Let dt_inicio_vigencia_seg(ByVal vData As String)
    mvardt_inicio_vigencia_seg = vData
End Property

Public Property Get dt_inicio_vigencia_seg() As String
    dt_inicio_vigencia_seg = mvardt_inicio_vigencia_seg
End Property

Public Property Let SolicitanteNome(ByVal vData As String)
    mvarSolicitanteNome = vData
End Property

Public Property Get SolicitanteNome() As String
    SolicitanteNome = mvarSolicitanteNome
End Property

Public Property Let SeguradoComponente(ByVal vData As String)
    mvarSeguradoComponente = vData
End Property

Public Property Get SeguradoComponente() As String
    SeguradoComponente = mvarSeguradoComponente
End Property

Public Property Let NomeTipoComponente(ByVal vData As String)
    mvarNomeTipoComponente = vData
End Property

Public Property Get NomeTipoComponente() As String
    NomeTipoComponente = mvarNomeTipoComponente
End Property


Public Property Let SinistradoClienteId(ByVal vData As String)
   
    mvarSinistradoClienteId = vData
End Property

Public Property Get SinistradoClienteId() As String
    SinistradoClienteId = mvarSinistradoClienteId
End Property

Public Property Let SinistradoNomeCliente(ByVal vData As String)
    mvarSinistradoNomeCliente = vData
End Property

Public Property Get SinistradoNomeCliente() As String
    SinistradoNomeCliente = mvarSinistradoNomeCliente
End Property

Public Property Let SinistradoCPF(ByVal vData As String)
    mvarSinistradoCPF = vData
End Property

Public Property Get SinistradoCPF() As String
    SinistradoCPF = mvarSinistradoCPF
End Property

Public Property Let SinistradoDtNasc(ByVal vData As String)
    mvarSinistradoDtNasc = vData
End Property

Public Property Get SinistradoDtNasc() As String
    SinistradoDtNasc = mvarSinistradoDtNasc
End Property

Public Property Let SinistradoSexo(ByVal vData As String)
   mvarSinistradoSexo = vData
End Property

Public Property Get SinistradoSexo() As String
    SinistradoSexo = mvarSinistradoSexo
End Property

Public Property Let Ind_reanalise(ByVal vData As String)
    mvarInd_reanalise = vData
End Property

Public Property Get Ind_reanalise() As String
    Ind_reanalise = mvarInd_reanalise
End Property

Public Property Let SQL_reanalise(ByVal vData As String)
    mvarSQL_reanalise = vData
End Property

Public Property Get SQL_reanalise() As String
    SQL_reanalise = mvarSQL_reanalise
End Property

Public Property Let Banco(ByVal vData As String)
    mvarBanco = vData
End Property

Public Property Get Banco() As String
   Banco = mvarBanco
End Property

Public Property Let NumRemessa(ByVal vData As String)
    mvarNumRemessa = vData
End Property

Public Property Get NumRemessa() As String
    NumRemessa = mvarNumRemessa
End Property

Public Property Let IndTpSinistrado(ByVal vData As String)
    mvarIndTpSinistrado = vData
End Property

Public Property Get IndTpSinistrado() As String
    IndTpSinistrado = mvarIndTpSinistrado
End Property

Public Property Let NumProcRemessa(ByVal vData As String)
    mvarNumProcRemessa = vData
End Property

Public Property Get NumProcRemessa() As String
    NumProcRemessa = mvarNumProcRemessa
End Property

Public Property Let Moeda(ByVal vData As String)
    mvarMoeda = vData
End Property

Public Property Get Moeda() As String
    Moeda = mvarMoeda
End Property

Public Property Let Localizacao(ByVal vData As String)
    mvarLocalizacao = vData
End Property

Public Property Get Localizacao() As String
    Localizacao = mvarLocalizacao
End Property

Public Property Let Evento_bb_id(ByVal vData As String)
    mvarEvento_bb_id = vData
End Property

Public Property Get Evento_bb_id() As String
    Evento_bb_id = mvarEvento_bb_id
End Property

Public Property Let Evento_Segbr_id(ByVal vData As String)
    mvarEvento_Segbr_id = vData
End Property

Public Property Get Evento_Segbr_id() As String
     Evento_Segbr_id = mvarEvento_Segbr_id
End Property

Public Property Let CodRamo(ByVal vData As String)
    mvarCodRamo = vData
End Property

Public Property Get CodRamo() As String
    CodRamo = mvarCodRamo
End Property

Public Property Let SituacaoSinistro(ByVal vData As String)
    mvarSituacaoSinistro = vData
End Property

Public Property Get SituacaoSinistro() As String
    SituacaoSinistro = mvarSituacaoSinistro
End Property
 
Public Property Let CD_PRD(ByVal vData As String)
    mvarCD_PRD = vData
End Property

Public Property Get CD_PRD() As String
    CD_PRD = mvarCD_PRD
End Property

Public Property Let CD_MDLD(ByVal vData As String)
    mvarCD_MDLD = vData
End Property

Public Property Get CD_MDLD() As String
    CD_MDLD = mvarCD_MDLD
End Property

Public Property Let CD_ITEM_MDLD(ByVal vData As String)
    mvarCD_ITEM_MDLD = vData
End Property

Public Property Get CD_ITEM_MDLD() As String
    CD_ITEM_MDLD = mvarCD_ITEM_MDLD
End Property

Public Property Let Cod_transacao(ByVal vData As String)
    mvarCod_transacao = vData
End Property

Public Property Get Cod_transacao() As String
    Cod_transacao = mvarCod_transacao
End Property

Public Property Let Val_informado(ByVal vData As String)
    mvarVal_informado = vData
End Property

Public Property Get Val_informado() As String
    Val_informado = mvarVal_informado
End Property

