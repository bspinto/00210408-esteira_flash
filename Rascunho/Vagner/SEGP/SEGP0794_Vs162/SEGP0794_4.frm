VERSION 5.00
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GridDinamico.ocx"
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GridAlianca.OCX"
Begin VB.Form SEGP0794_4 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   8070
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11775
   Icon            =   "SEGP0794_4.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8070
   ScaleWidth      =   11775
   StartUpPosition =   1  'CenterOwner
   Begin GridAlianca.grdAlianca GrdResultadoPesquisa 
      Height          =   5175
      Left            =   120
      TabIndex        =   9
      Top             =   240
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   9128
      BorderStyle     =   1
      AllowUserResizing=   3
      EditData        =   0   'False
      Highlight       =   1
      ShowTip         =   0   'False
      SortOnHeader    =   0   'False
      BackColor       =   -2147483643
      BackColorBkg    =   -2147483633
      BackColorFixed  =   -2147483633
      BackColorSel    =   -2147483635
      FixedCols       =   1
      FixedRows       =   1
      FocusRect       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      ForeColorFixed  =   -2147483630
      ForeColorSel    =   -2147483634
      GridColor       =   -2147483630
      GridColorFixed  =   12632256
      GridLine        =   1
      GridLinesFixed  =   2
      MousePointer    =   0
      Redraw          =   -1  'True
      Rows            =   2
      TextStyle       =   0
      TextStyleFixed  =   0
      Cols            =   2
      RowHeightMin    =   0
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   10320
      TabIndex        =   6
      Top             =   7560
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   8955
      TabIndex        =   5
      Top             =   7560
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   7605
      TabIndex        =   4
      Top             =   7560
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Propostas do Cliente"
      Height          =   7470
      Left            =   0
      TabIndex        =   0
      Top             =   45
      Width           =   11760
      Begin GridFrancisco.GridDinamico GridTipo 
         Height          =   1260
         Left            =   180
         TabIndex        =   8
         Top             =   5580
         Width           =   11475
         _ExtentX        =   20241
         _ExtentY        =   2223
         BorderStyle     =   1
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
      Begin VB.CommandButton cmdCobertura 
         Caption         =   "Exibir Coberturas"
         Height          =   430
         Left            =   240
         TabIndex        =   7
         Top             =   6960
         Width           =   1440
      End
      Begin VB.CommandButton cmdNAvisar 
         Caption         =   "N�o Avisar"
         Height          =   430
         Left            =   8955
         TabIndex        =   3
         Top             =   6915
         Width           =   1275
      End
      Begin VB.CommandButton cmdReanalise 
         Caption         =   "Reanalise"
         Height          =   430
         Left            =   10305
         TabIndex        =   2
         Top             =   6915
         Width           =   1320
      End
      Begin VB.CommandButton cmdAvisar 
         Caption         =   "Avisar"
         Height          =   430
         Left            =   7560
         TabIndex        =   1
         Top             =   6915
         Width           =   1320
      End
   End
End
Attribute VB_Name = "SEGP0794_4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public CLIENTE_DOC As String
Public Nome As String
Public LISTA_CLIENTE As String
Public NaoPodeAvisar, FlagMicrosseguro As Boolean

'Demanda MU-2017-040141 - IN�CIO
Dim PodeAvisar As Boolean
Dim CPF As String
'Demanda MU-2017-040141 - FIM

'INICIO - MU00416967 - melhoria de aviso de sinistro
Public propostas As String
Public ramo_analise As String
Public produto_analise As String
Public proposta_analise As String '
'FIM - MU00416967 - melhoria de aviso de sinistro

Public Sub CarregaProdutoBBProtecaoVida()
   
   cProdutoBBProtecao = ""
   'Alessandra Grig�rio - 28.04.2009 - Seleciona produtos BB Protecao Vida
   SQL = "SELECT PRODUTO_ID FROM MODULO_PLANO_TB with (nolock) "
   Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
   
   Do While Not rs.EOF
      cProdutoBBProtecao = cProdutoBBProtecao & ", " & rs("PRODUTO_ID")
      rs.MoveNext
   Loop
   rs.Close
   
   cProdutoBBProtecao = Mid(cProdutoBBProtecao, 3)
   'Alessandra Grig�rio - 28.04.2009 - Fim

End Sub

Private Sub cmdAvisar_Click()
Dim flagReanalise As String
Dim sinistro_id As String
Dim Sinistro_bb As String

   NaoPodeAvisar = False
    ' CONFITEC - INCIDENTE (IM01031198)- inicio
   If (((GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 2) = "Vida") Or _
      (GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 2) = "Elementar") And _
      Left(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1), 1) <> "*") Or _
      GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 12) = 1218) Then 'Tiago Vignoli - Nova Consultoria - maio/2013; 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro; permitir marcar o microseguro com AVSR
    ' CONFITEC - INCIDENTE (IM01031198)- fim
        If GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) <> "AVSR" And _
           GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) <> "RNLS" Then

            If ExisteEventoSinistroProduto(SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                               GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 12)) Then


                  ' GPTI - Raimundo - 13/02/2009 - FLOW : 714343 e 758110
                  'Verifica se existe proposta BB e se ela � num�rica
                    If IsNumeric(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 6)) Then

                ' GPTI - Osmar - 26/01/09 - FLOW : 708705
                  'Corre��o na passagem  dos par�metros para VerificaPropostaBB
                  'Call VerificaPropostaBB(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 6), IIf(mskDataOcorrencia.Text = "__/__/____", "", "'" & Format(mskDataOcorrencia.Text, "yyyymmdd") & "'"))
                     Call VerificaPropostaBB(CLng("0" & GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 6)), IIf(SEGP0794_3.mskDataOcorrencia.Text = "__/__/____", "", "'" & Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd") & "'"))
                    End If

                ' Raimundo - Fim

                  If ExisteProdutoBB(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 5)) Then

                        ' G&P - AFONSO - FLOW : 286647
                        'Verifica se o segurado da proposta coincide com o sinistrado informado
                        'If Not verificaCoincideSinistradoSegurado(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 9)) Then
                        '     MsgBox "Esta proposta possui um segurado diferente do selecionado nas telas anteriores." & vbCrLf & _
                        '            "N�o � poss�vel avisar esta proposta.", vbCritical, "Aviso de Sinistro"

                        'Else

                             flagReanalise = VerificaReanalise(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 5), _
                                            Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                                            SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                            GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 19), _
                                            Format(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 20), "yyyymmdd"), _
                                            sinistro_id, _
                                            Sinistro_bb)
                             If flagReanalise = "" Then
                                 GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = "AVSR"

                             ElseIf flagReanalise = "RNLS" Then
                                 GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = flagReanalise

                             Else
                                 MsgBox "Esta proposta j� possui um aviso de sinistro em aberto" & vbCrLf & _
                                      "para os parametros: proposta, evento de sinistro, " & vbCrLf & _
                                      "data de ocorr�ncia, CPF e data de nascimento do sinistrado." & vbCrLf & _
                                      "N�o � poss�vel avisar esta proposta.", vbCritical, "Aviso de Sinistro"

                                 ' GPTI - CAIO C�ZAR LOPES - FLOW : 745690
                                 'Ap�s exibir mensagem de que n�o � poss�vel realizar o aviso setamos
                                 'a flag para TRUE, pois assim quando clicarmos no bot�o continuar
                                 'n�o ser� poss�vel efetuar o aviso de forma manual
                                 NaoPodeAvisar = True
                             End If
                        'End If 'verificaCoincideSinistradoSegurado
                  Else
                        MsgBox "O aviso de sinistro para proposta deve ser feito no SEGUR" & vbCrLf & _
                               "(Proposta: " & GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 5) & ")", vbInformation, "Aviso de Sinistro"

                  End If
            Else
                MsgBox "N�o existe a cobertura " & SEGP0794_3.cboEvento.Text & " para o produto selecionado.", _
                       vbInformation, "Aviso de Sinistro"
            End If
        End If
        
   End If
End Sub

Private Function ExisteProdutoBB(proposta_id As Long) As Boolean
Dim SQL As String
Dim rs As ADODB.Recordset

SQL = SQL & " SELECT produto_bb "
SQL = SQL & " FROM produto_bb_sinistro_tb b with (nolock) "
SQL = SQL & " INNER JOIN proposta_tb a with (nolock)"
SQL = SQL & " ON a.proposta_id = " & proposta_id
SQL = SQL & " WHERE b.produto_id = a.produto_id "
SQL = SQL & " AND b.dt_inicio_vigencia <= a.dt_contratacao "
SQL = SQL & " AND (dt_fim_vigencia >= a.dt_contratacao OR dt_fim_vigencia is null)"
'Camila Kume - 19.12.2008 - Inclus�o de query para liberar o aviso para as propostas do ALS
SQL = SQL & " UNION "
SQL = SQL & " SELECT PRODUTO_ID "
SQL = SQL & " FROM PROPOSTA_TB with (nolock)"
SQL = SQL & " WHERE PROPOSTA_ID = " & proposta_id
SQL = SQL & " AND ORIGEM_PROPOSTA_ID=2"
'Camila Kume - FIM

Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

If Not rs.EOF Then
    ExisteProdutoBB = True
    produto_id = rs.Fields(0)
    
    rs.Close
    
    'Camila Kume - 29.12.2008 - Selecionar os dados de produtos do ALS
    SQL = "set nocount on exec obtem_produto_sps 1," & produto_id
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    If Not rs.EOF Then
        iCD_PRD = rs(0)
        iCD_MDLD = rs(1)
        lCD_ITEM_MDLD = rs(2)
    Else
        iCD_PRD = 0
        iCD_MDLD = 0
        lCD_ITEM_MDLD = 0
    End If
    rs.Close
'Camila Kume - 29.12.2008 - Fim
Else
    ExisteProdutoBB = False
    rs.Close
End If

End Function


Public Function VerificaPropostaBB(Proposta_BB As Long, dt_ocorrencia As String) As Boolean
Dim SQL As String
Dim rs As ADODB.Recordset


SQL = "set nocount on exec obtem_Versao_Endosso_sps " & Proposta_BB & ", " & dt_ocorrencia

Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

If Not rs.EOF Then
    VerificaPropostaBB = True
    lNR_VRS_EDS = rs("NR_VRS_EDS")
    'Alterado por Cleber - Data: 28/11/2006 - Flow 189587
    'Segundo o Junior estava buscando o campo errado.
    'lNR_CTR_SGRO = rs("NR_CTR_SGRO")
    lNR_CTR_SGRO = rs("NR_CTR_SGRO_BB")
Else
    VerificaPropostaBB = False
    lNR_VRS_EDS = 0
    lNR_CTR_SGRO = 0
End If
rs.Close

End Function

Private Sub cmdCobertura_Click()

Dim linhas As Integer
Dim proposta As Long
Dim dtOcorrencia As String
Dim tp_componente_id As Integer

    If Trim(GrdResultadoPesquisa.TextMatrix(1, 2)) = "" Then Exit Sub
    
    If GrdResultadoPesquisa.Rows > 1 Then
        
            Me.MousePointer = vbHourglass
            linhas = 0
            With SEGP0794_8
                .proposta_id = GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 5)
                .dtOcorrencia = Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd")
                
                'sergio.so - 30/11/2010 - CORRE��O TITULAR/CONJUGE
                '.tp_componente_id = IIf(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 10) = "Titular", 1, 3)
                'sergio.eo - 30/11/2010 - CORRE��O TITULAR/CONJUGE
                
                'sergio.sn - 30/11/2010 - CORRE��O TITULAR/CONJUGE
                '.tp_componente_id = IIf((UCase$(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 10)) Like "TITULAR*") = True, 1, 3)
                'eduardo.amaral(nova consultoria) 27/10/2011 12363945 - BB Seguro Vida Empresa FLEX
                .tp_componente_id = GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 26)
                'sergio.en - 30/11/2010 - CORRE��O TITULAR/CONJUGE
                
                
                .sub_grupo_id = IIf(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 14) = "", 0, GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 14))
                
                .lblNome.Caption = GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 9)
                .lblProduto = ObtemProduto(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 12))
                .lblProposta = .proposta_id
                .lblEvento = SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex) & " - " & SEGP0794_3.cboEvento.Text
                If SEGP0794_3.cboSubEvento.ListIndex = -1 Then
                   .lblSubEvento = ""
                Else
                   .lblSubEvento = SEGP0794_3.cboSubEvento.ItemData(SEGP0794_3.cboSubEvento.ListIndex) & " - " & SEGP0794_3.cboSubEvento.Text
                End If
                
                Me.Hide
                Me.MousePointer = vbDefault
                
                .Show vbModal
                Me.Show
                
            End With
      
    End If
End Sub

'(In�cio) Tiago Vignoli - Nova Consultoria - maio/2013; 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro
Private Function VerificaMicrosseguroFamiliar() As Boolean
Dim SQL As String
Dim i As Long

VerificaMicrosseguroFamiliar = False
    For i = 1 To GrdResultadoPesquisa.Rows - 1
        If GrdResultadoPesquisa.TextMatrix(i, 1) = "AVSR" And VerificarProdutoBB("1218,1217", GrdResultadoPesquisa.TextMatrix(i, 12)) Then
                SQL = ""
                SQL = SQL & "SELECT COUNT(*) "
                SQL = SQL & "FROM seguros_db.dbo.proposta_processo_susep_tb ps "
                SQL = SQL & "JOIN als_operacao_db..cbt_ctr cbt_ctr "
                SQL = SQL & "ON cbt_ctr.nr_ctr_sgro = ps.num_contrato_seguro "
                SQL = SQL & "AND cbt_ctr.cd_prd = ps.cod_produto "
                SQL = SQL & "AND cbt_ctr.cd_mdld = ps.cod_modalidade "
                SQL = SQL & "AND cbt_ctr.cd_item_mdld = ps.cod_item_modalidade "
                SQL = SQL & "AND cbt_ctr.nr_vrs_eds = ps.num_versao_endosso "
                SQL = SQL & "WHERE cbt_ctr.cd_cjt_cbt > 2 "
                SQL = SQL & "AND ps.proposta_id = " & CStr(GrdResultadoPesquisa.TextMatrix(i, 5))
                Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
                If rs(0) > 0 Then VerificaMicrosseguroFamiliar = True
        End If
    Next i
End Function
'(Fim) Tiago Vignoli - Nova Consultoria - maio/2013; 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro

'INICIO - MU00416967 -  melhoria de aviso de sinistro
Private Sub cmdContinuar_Click()
Dim i As Long
Dim rs As ADODB.Recordset
''INC 21\03\2018
If GrdResultadoPesquisa.Rows > 0 Then
    For i = 1 To GrdResultadoPesquisa.Rows - 1
        If (GrdResultadoPesquisa.TextMatrix(i, 1) = "AVSR" _
        And ((GrdResultadoPesquisa.TextMatrix(i, 12) = "1201" And GrdResultadoPesquisa.TextMatrix(i, 7) = "61") _
        Or (GrdResultadoPesquisa.TextMatrix(i, 12) = "1226" And GrdResultadoPesquisa.TextMatrix(i, 7) = "77") _
        Or (GrdResultadoPesquisa.TextMatrix(i, 12) = "1227" And GrdResultadoPesquisa.TextMatrix(i, 7) = "77"))) Then
        ramo_analise = GrdResultadoPesquisa.TextMatrix(i, 7)
        produto_analise = GrdResultadoPesquisa.TextMatrix(i, 12)
        proposta_analise = GrdResultadoPesquisa.TextMatrix(i, 5)
      End If
Next i

 
End If
'FIM - MU00416967 -  melhoria de aviso de sinistro

    '(IN�CIO) -- RSOUZA -- 04/08/2010 ---------------------------------------------------------------------------------------------------------------------------------------------'
    If GrdResultadoPesquisa.Rows = 2 Then
       If GrdResultadoPesquisa.TextMatrix(1, 1) = "SRGL" Then
          If Not MsgBox("Sinistro em regula��o " & GrdResultadoPesquisa.TextMatrix(1, 15) & " (SRGL). Caso queira registrar alguma solicita��o, dever� ser realizada no sistema SIR. Deseja continuar?", vbYesNo) = vbYes Then
              Exit Sub
          End If
       End If
    End If
    '(FIM)    -- RSOUZA -- 04/08/2010 ---------------------------------------------------------------------------------------------------------------------------------------------'
    
    If ObtemPropostasAvisadas = False Or NaoPodeAvisar Then Exit Sub
    
    'Bruno Medeiros - Fev/2015 18232638
    
     If propostasReanalise <> "" Then
    MsgBox ("Para os casos de rean�lise, utilizar o m�dulo de reabertura de sinistro, SEGP1285.4 - REABRIR SINISTRO")
    propostasReanalise = ""
        End If
    
        '(In�cio) Tiago Vignoli - Nova Consultoria - maio/2013; 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro
        If VerificaMicrosseguroFamiliar() Then
            FlagMicrosseguro = True
            Me.Hide
            SEGP0794_4_aviso_micro_seguro.Show
        Else
            Call Continuar
        End If
        '(Fim) Tiago Vignoli - Nova Consultoria - maio/2013; 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro
End Sub

'(In�cio) Tiago Vignoli - Nova Consultoria - maio/2013; 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro
Public Sub Continuar()
'(Fim) Tiago Vignoli
    Me.MousePointer = vbHourglass
    SEGP0794_6.BuscaUltimaContratacao
    Me.MousePointer = vbDefault
    
    If propostasAvisadas = "" And propostasReanalise = "" Then
        If MsgBox("N�o h� propostas a serem avisadas. " & vbCrLf & _
               "Deseja continuar mesmo assim?", vbYesNo, "Aviso de Proposta n�o especificada") = vbNo Then
            Exit Sub
        Else
            Me.Hide
            'vai para a tela para ser preenchido manualmente os dados do segurado
            
            'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
            'SEGP0794_9.txtNomeSegurado.Text = SEGP0794_3.txtNomeSinistrado.Text
            SEGP0794_9.txtNomeSegurado.Text = MudaAspaSimples(SEGP0794_3.txtNomeSinistrado.Text)
            'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : fim
            
            SEGP0794_9.mskCPF.Text = SEGP0794_3.mskCPF.Text
            SEGP0794_9.mskDtNascimento.Text = SEGP0794_3.mskDtNascimento.Text
            SEGP0794_9.cboSexo.Text = SEGP0794_3.cboSexo.Text
            
            SEGP0794_9.Origem = 4
            SEGP0794_9.Show
            Exit Sub
        End If
    End If
    
    If propostasReanalise <> "" Then
        Me.Hide
        SEGP0794_4_1.Show
    Else
   
        If ((produto_analise = "1201" Or produto_analise = "1226" Or produto_analise = "1227") And (ramo_analise = "77" Or ramo_analise = "61")) Then
            FlagCredito = False
        Else
            'Gerson - CWI - Mensagem Vinda de SEGP0794_3.cmdContinuar_Click
            FlagCredito = MsgBox("Solicitante autoriza cr�dito em conta? ", vbQuestion + vbYesNo) = vbYes
        End If
        
        Me.Hide
        SEGP0794_5.Show
    End If
    
    End Sub

Private Sub cmdNAvisar_Click()
Dim subevento_sinistro_aux As Long

If SEGP0794_3.cboSubEvento.ListIndex = -1 Then
   subevento_sinistro_aux = 0
Else
   subevento_sinistro_aux = SEGP0794_3.cboSubEvento.ItemData(SEGP0794_3.cboSubEvento.ListIndex)
End If

   Me.MousePointer = vbHourglass
   ' CONFITEC - INCIDENTE (IM01031198)- inicio
   If (((GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 2) = "Vida") Or _
       (GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 2) = "Elementar") And _
      Left(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1), 1) <> "*") Or _
      GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 12) = 1218) Then 'Tiago Vignoli - Nova Consultoria - maio/2013; 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro; permitir marcar o microseguro com NAVS
   ' CONFITEC - INCIDENTE (IM01031198)- fim
        If Not GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = "NAVS" Then
            If GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = "RNLS" Then
                GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = "*NAVS"
            Else
                If Not VerificaCobertAtingida(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 5), _
                       Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                       IIf(Left(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 10), 1) = "T", 1, 3), _
                       SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                       subevento_sinistro_aux, _
                       IIf(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 14) = "", 0, GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 14))) Then
                    GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = "PSCB"
                
                Else
                    GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = "NAVS"
                
                End If
            End If
        End If
   End If
   Me.MousePointer = vbDefault
   
End Sub

Private Sub cmdReanalise_Click()
  If GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 2) = "Vida" Then
  
        If Not GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = "RNLS" And _
           Left(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1), 1) = "*" Then
           
              GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 1) = "RNLS"
            
        Else
              MsgBox "N�o � poss�vel abrir esta proposta para rean�lise de sinistro.", vbCritical, "Aviso de Sinistro"
        End If
   End If
End Sub

Private Sub Form_Load()

    Me.MousePointer = vbHourglass
    
    cmdReanalise.Visible = False
    CentraFrm Me
    PreencheGridTipo
    CarregaProdutoBBProtecaoVida
    montacabecalhoGrid
    Me.MousePointer = vbDefault
    If GrdResultadoPesquisa.Rows = 1 Then
        SEGP0794_5.Show
        Me.Hide
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub cmdVoltar_Click()
    Me.Hide
    SEGP0794_3.Show
End Sub
Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Public Sub montacabecalhoGrid()
    With GrdResultadoPesquisa
        .Cols = 14
        '.Rows = 1
        '.FixedCols = 0
        .TextMatrix(0, 0) = "Aviso"
        .TextMatrix(0, 1) = "Tipo Ramo"
        .TextMatrix(0, 2) = "Situa��o"
        .TextMatrix(0, 3) = "Produto"
        .TextMatrix(0, 4) = "Proposta AB"
        .TextMatrix(0, 5) = "Proposta BB"
        .TextMatrix(0, 6) = "Ramo"
        .TextMatrix(0, 7) = "Ap�lice"
        .TextMatrix(0, 8) = "Proponente"
        .TextMatrix(0, 9) = "Titular/Conjuge"
        .TextMatrix(0, 10) = "Vig�ncia"
        .TextMatrix(0, 11) = "Produto"
        .TextMatrix(0, 12) = "Cliente"
        .TextMatrix(0, 13) = "Subgrupo"
        .AutoFit H�brido
    End With

End Sub

Public Sub montaRelacaoCliente()
    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    LISTA_CLIENTE = ""
    
    If CDbl(CLIENTE_DOC) = 0 Then
        SQL = ""
        SQL = SQL & " select cliente_id from cliente_tb with (nolock) where nome like '" & Nome & "%'"
        
        Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        While Not rs.EOF
            If LISTA_CLIENTE = "" Then
                LISTA_CLIENTE = rs(0)
            Else
                LISTA_CLIENTE = LISTA_CLIENTE & ", " & rs(0)
            End If
            rs.MoveNext
        Wend
        Exit Sub
    End If
    
    If CLIENTE_DOC <> "" Then
        SQL = ""
        SQL = SQL & " select distinct pf_cliente_id from pessoa_fisica_tb with (nolock) where cpf = '" & CLIENTE_DOC & "'"
        SQL = SQL & " UNION"
        SQL = SQL & " select distinct pj_cliente_id from pessoa_juridica_tb with (nolock) where cgc = '" & CLIENTE_DOC & "'"
        Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        While Not rs.EOF
            If LISTA_CLIENTE = "" Then
                LISTA_CLIENTE = rs(0)
            Else
                LISTA_CLIENTE = LISTA_CLIENTE & ", " & rs(0)
            End If
            rs.MoveNext
        Wend
    End If
End Sub


Private Function ObtemPropostasAvisadas() As Boolean
Dim SQL As String
Dim i As Long
Dim propNAVSBBProtecao As String
Dim propAVSRBBProtecao As String

    propostasAvisadas = ""
    propostasReanalise = ""
    clientesPropostas = ""
    propAVSRBBProtecao = ""
    propNAVSBBProtecao = ""
    
    ObtemPropostasAvisadas = True
    
    'Obtem as propostas e os clientes afetados
    For i = 1 To GrdResultadoPesquisa.Rows - 1
        If GrdResultadoPesquisa.TextMatrix(i, 1) = "AVSR" Then
              If InStr(1, propostasAvisadas, GrdResultadoPesquisa.TextMatrix(i, 5), vbTextCompare) Then
                MsgBox "A proposta AB " & GrdResultadoPesquisa.TextMatrix(i, 5) & " est� sendo avisada mais de uma vez. Favor verificar.", vbCritical, "Proposta Duplicada"
                ObtemPropostasAvisadas = False
                Exit Function
              End If
              If propostasAvisadas <> "" Then propostasAvisadas = propostasAvisadas & " , "
              
              propostasAvisadas = propostasAvisadas & GrdResultadoPesquisa.TextMatrix(i, 5)
              
               'BRUNO MEDEIROS - Demanda 18232638
              If clientesPropostas <> "" Then clientesPropostas = clientesPropostas & " , "
             ' clientesPropostas = clientesPropostas & grdResultadoPesquisa.TextMatrix(i, 13)
              
              'DIEGO GALIZONI CAVERSAN - DEMANDA 1113817 - 17/06/2010 - CORRE��O NA VERIFICA��O DO BB PROTE��O
              'USANDO O INSTR ELE CONSIDERA QUE O PRODUTO 11 � DO BB PROTE��O POR 11 ESTAR CONTIDO EM 1176
                'If InStr(1, cProdutoBBProtecao, GrdResultadoPesquisa.TextMatrix(i, 12), vbTextCompare) Then
                If VerificarProdutoBB(cProdutoBBProtecao, GrdResultadoPesquisa.TextMatrix(i, 12)) Then

                    'Ricardo Toledo - Confitec : 14/09/2011 : Flow 12164346 : Inicio
                    'Conforme email enviado (De: Emerson Miasato / Para: Thiago Erustes(Confitec) em 13/07/2011))
                    'essa regra s� se aplica para Ramos elementares. Para processos de vida a regra n�o � v�lida.
                    'OBSERVA��O: Esse email est� contido no anexo do Flow.
                    'If GrdResultadoPesquisa.TextMatrix(i, 2) = "Vida" Then
                    If GrdResultadoPesquisa.TextMatrix(i, 2) <> "Vida" Then
                    'Ricardo Toledo - Confitec : 14/09/2011 : Flow 12164346 : fim
                      
                      If propNAVSBBProtecao = "" Then
                         If ExisteEventoSinistroProduto(SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                              GrdResultadoPesquisa.TextMatrix(i, 12)) Then
                            If propAVSRBBProtecao <> "" Then propAVSRBBProtecao = propAVSRBBProtecao & " , "
                            propAVSRBBProtecao = propAVSRBBProtecao & GrdResultadoPesquisa.TextMatrix(i, 5)
                         End If
                      Else
                         MsgBox "Todas as propostas BB Prote��o dever�o ser avisadas.", vbCritical, "Proposta BB Prote��o"
                         ObtemPropostasAvisadas = False
                         Exit Function
                      End If
                   End If
                End If
        
        ElseIf GrdResultadoPesquisa.TextMatrix(i, 1) = "RNLS" Then
        
              If propostasReanalise <> "" Then propostasReanalise = propostasReanalise & " , "
              propostasReanalise = propostasReanalise & GrdResultadoPesquisa.TextMatrix(i, 5)
              
              If clientesPropostas <> "" Then clientesPropostas = clientesPropostas & " , "
              clientesPropostas = clientesPropostas & GrdResultadoPesquisa.TextMatrix(i, 13)

        'Alessandra Grig�rio - 28/04/2009 - BB Prote��o
        ElseIf GrdResultadoPesquisa.TextMatrix(i, 1) = "NAVS" Then
           'Denis Silva - FLOW 3807301 - 05/07/2010 - CORRE��O NA VERIFICA��O DO BB PROTE��O (DEMANDA 1113817)
           'USANDO O INSTR ELE CONSIDERA QUE O PRODUTO 11 � DO BB PROTE��O POR 11 ESTAR CONTIDO EM 1176
           'If InStr(1, cProdutoBBProtecao, GrdResultadoPesquisa.TextMatrix(i, 12), vbTextCompare) Then
           If VerificarProdutoBB(cProdutoBBProtecao, GrdResultadoPesquisa.TextMatrix(i, 12)) Then
              
              'Ricardo Toledo - Confitec : 14/09/2011 : Flow 12164346 : Inicio
              'Conforme email enviado (De: Emerson Miasato / Para: Thiago Erustes(Confitec) em 13/07/2011))
              'essa regra s� se aplica para Ramos elementares. Para processos de vida a regra n�o � v�lida.
              'OBSERVA��O: Esse email est� contido no anexo do Flow.
              'If GrdResultadoPesquisa.TextMatrix(i, 2) = "Vida" Then
              If GrdResultadoPesquisa.TextMatrix(i, 2) <> "Vida" Then
              'Ricardo Toledo - Confitec : 14/09/2011 : Flow 12164346 : fim
                 If propAVSRBBProtecao <> "" Then
                    MsgBox "Todas as propostas BB Prote��o dever�o ser avisadas.", vbCritical, "Proposta BB Prote��o"
                    ObtemPropostasAvisadas = False
                    Exit Function
                 Else
                    If ExisteEventoSinistroProduto(SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                       GrdResultadoPesquisa.TextMatrix(i, 12)) Then
                       If propNAVSBBProtecao <> "" Then propNAVSBBProtecao = propNAVSBBProtecao & " , "
                       propNAVSBBProtecao = propNAVSBBProtecao & GrdResultadoPesquisa.TextMatrix(i, 5)
                    End If
                 End If
              End If
           End If
        End If
    Next i
End Function

Public Sub buscaProposta()
    On Error GoTo TratarErro
    Dim SQL As String
    Dim linhas As Integer
    
'    SQL = ""
'    SQL = "set nocount on exec busca_proposta_sinistro_sps '" & CLIENTE_DOC & "', "
'    SQL = SQL & IIf(SEGP0794_1.optTipoRamo(0).value, 1, 2) & ","
'    'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
'    'SQL = SQL & "'" & Nome & "'"
'    SQL = SQL & "'" & MudaAspaSimples(Nome) & "'"
'    'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : fim
    
    'cristovao.rodrigues 30/06/2014 incluido IIF...CLIENTE_DOC e NOME
    'cristovao.rodrigues 04/04/2014 select anterior alterado  - 16429073 - Amparo Familiar - retirar chamada para DESENV_DB
    SQL = ""
    SQL = "set nocount on exec seguros_db..busca_proposta_sinistro_sps '" & SEGP0794_3.sTitularClienteId & "', "
    'SQL = "set nocount on exec seguros_db..busca_proposta_sinistro_sps '" & IIf(SEGP0794_3.sTitularClienteId = "", CLIENTE_DOC, SEGP0794_3.sTitularClienteId) & "', "
    'SQL = "set nocount on exec seguros_db..busca_proposta_sinistro_sps '" & SEGP0794_3.sTitularClienteId & "', "
    SQL = SQL & IIf(SEGP0794_1.optTipoRamo(0).value, 1, 2) & ","
    SQL = SQL & "'" & MudaAspaSimples(SEGP0794_3.sTitularNome) & "'"
    'SQL = SQL & "'" & MudaAspaSimples(IIf(SEGP0794_3.sTitularNome = "", Nome, SEGP0794_3.sTitularNome)) & "'"
    
    SQL = SQL & ", null"
    SQL = SQL & ", " & IIf(SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "", "Null", SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11)) ' produto
    
    
    ''flow 4883613
    'Obs.: A SP busca_proposta_sinistro_sps foi alterada para receber um parametro
    'opcional, cliente_id, selecionado em SEGP0794_2
    'Caso o parametro n�o seja passado a SP ir� atuar com base no Cliente_DOC ou NOME
    'Podendo trazer avisos inv�lidos, este erro n�o ocorrer� ao passar o ClienteIdSelecionado
    
    'Inicio Customizado por Artemir Confitec - 18/01/2011
    'Desabilitado para voltar a mostrar todas as propostas do cliente que tenha o mesmo
    '    If Not IsNull(AvisoSinistro.ClienteIdSelecionado) Then
    '        SQL = SQL & ", "
    '        SQL = SQL & AvisoSinistro.ClienteIdSelecionado
    '    End If
        'CPF/CNPJ - conforme solicita��o do Sr. Marcio.
    'Fim Customizado por Artemir Confitec - 18/01/2011
    
    Set RsProposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
             
    GridResposta = GrdResultadoPesquisa.PopulaGrid(RsProposta)
    'eduardo.amaral(nova consultoria) 27/10/2011 12363945 - BB Seguro Vida Empresa FLEX
    Call GrdResultadoPesquisa.TamanhoColuna(26, 0)
   
   Exit Sub
Resume
TratarErro:
    Call TratarErro("BuscaProposta", "M�dulo de Aviso de Sinistro")
    Call FinalizarAplicacao
End Sub

Public Sub buscaCobertura()
Dim contLinha As Long
Dim sinistro_id As String
Dim Sinistro_bb As String
Dim flagReanalise As String
Dim subevento_sinistro_aux As Long

If SEGP0794_3.cboSubEvento.ListIndex = -1 Then
   subevento_sinistro_aux = 0
Else
   subevento_sinistro_aux = SEGP0794_3.cboSubEvento.ItemData(SEGP0794_3.cboSubEvento.ListIndex)
End If
    '--Procedimento de valida��o/cr�tica inicial das propostas a serem avisadas--
    ' Verifica se o produto_bb est� cadastrado para a proposta
    ' Verifica se o evento de sinistro indicado existe para o produto
    ' Verifica se o nome do sinistrado assemelha-se com o nome do segurado da proposta
    ' Verifica se h� caso de aviso de sinistro anterior j� encerrado para a ocorr�ncia
    ' Verifica se a proposta estava vigente no periodo da ocorrencia do sinistro
    ' Verifica se a proposta tem cobertura atingida pelo evento de sinistro informado
    
    If GrdResultadoPesquisa.Rows = 1 Then Exit Sub
    If Trim(GrdResultadoPesquisa.TextMatrix(1, 2)) = "" Then Exit Sub
         ' GPTI - Raimundo - 13/02/2009 - FLOW : 714343 e 758110
    'Verifica se existe proposta BB e se ela � num�rica
    If IsNumeric(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 6)) Then
        Call VerificaPropostaBB(CLng("0" & GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 6)), IIf(SEGP0794_3.mskDataOcorrencia.Text = "__/__/____", "", "'" & Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd") & "'"))
        
    ' Raimundo - Fim
        
        End If
    
    For contLinha = 1 To GrdResultadoPesquisa.Rows - 1
        If GrdResultadoPesquisa.TextMatrix(contLinha, 2) = "Vida" And TIPO_RAMO <> 1 Then
            GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "NAVS"
            
            '-- (IN�CIO) -- RSouza -- 23/07/2010 -----------------------------------------------------------'
            'Verifica se h� caso de aviso de sinistro anterior - reanalise
            flagReanalise = VerificaReanalise(GrdResultadoPesquisa.TextMatrix(contLinha, 5), _
                                         Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                                         SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                         GrdResultadoPesquisa.TextMatrix(contLinha, 19), _
                                         Format(GrdResultadoPesquisa.TextMatrix(contLinha, 20), "yyyymmdd"), _
                                         sinistro_id, _
                                         Sinistro_bb)
                                        
            If flagReanalise <> "" And flagReanalise <> "RNLS" Then
                GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "SRGL"
            End If
            '-- (FIM)    -- RSouza -- 23/07/2010 -----------------------------------------------------------'
            
        'INICIO - MU00416967 -  melhoria de aviso de sinistro
            'Tiago Vignoli
        'ElseIf (GrdResultadoPesquisa.TextMatrix(contLinha, 2) = "Elementar" And (IIf((GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1201), TIPO_RAMO = 2, TIPO_RAMO <> 2)) And GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1218) Then
           ElseIf (GrdResultadoPesquisa.TextMatrix(contLinha, 2) = "Elementar" And (IIf((GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1201), TIPO_RAMO = 0, TIPO_RAMO <> 2)) And GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1218) Then
           ' FIM - MU00416967 -  melhoria de aviso de sinistro
           GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "NAVS"
            
            '-- (IN�CIO) -- RSouza -- 23/07/2010 -----------------------------------------------------------'
            'Verifica se h� caso de aviso de sinistro anterior - reanalise
            flagReanalise = VerificaReanalise(GrdResultadoPesquisa.TextMatrix(contLinha, 5), _
                                         Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                                         SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                         GrdResultadoPesquisa.TextMatrix(contLinha, 19), _
                                         Format(GrdResultadoPesquisa.TextMatrix(contLinha, 20), "yyyymmdd"), _
                                         sinistro_id, _
                                         Sinistro_bb)
                                        
            If flagReanalise <> "" And flagReanalise <> "RNLS" Then
                GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "SRGL"
            End If
            '-- (FIM)    -- RSouza -- 23/07/2010 -----------------------------------------------------------'
            
        ElseIf Not ExisteProdutoBB(GrdResultadoPesquisa.TextMatrix(contLinha, 5)) Then
             GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "NAVS"
             
            '-- (IN�CIO) -- RSouza -- 23/07/2010 -----------------------------------------------------------'
            'Verifica se h� caso de aviso de sinistro anterior - reanalise
            flagReanalise = VerificaReanalise(GrdResultadoPesquisa.TextMatrix(contLinha, 5), _
                                         Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                                         SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                         GrdResultadoPesquisa.TextMatrix(contLinha, 19), _
                                         Format(GrdResultadoPesquisa.TextMatrix(contLinha, 20), "yyyymmdd"), _
                                         sinistro_id, _
                                         Sinistro_bb)
                                        
            If flagReanalise <> "" And flagReanalise <> "RNLS" Then
                GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "SRGL"
            End If
            '-- (FIM)    -- RSouza -- 23/07/2010 -----------------------------------------------------------'
             
             
        ElseIf Not ExisteEventoSinistroProduto(SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                               GrdResultadoPesquisa.TextMatrix(contLinha, 12)) Then
             GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "NAVS"
             
            '-- (IN�CIO) -- RSouza -- 23/07/2010 -----------------------------------------------------------'
            'Verifica se h� caso de aviso de sinistro anterior - reanalise
            flagReanalise = VerificaReanalise(GrdResultadoPesquisa.TextMatrix(contLinha, 5), _
                                         Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                                         SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                         GrdResultadoPesquisa.TextMatrix(contLinha, 19), _
                                         Format(GrdResultadoPesquisa.TextMatrix(contLinha, 20), "yyyymmdd"), _
                                         sinistro_id, _
                                         Sinistro_bb)
                                        
            If flagReanalise <> "" And flagReanalise <> "RNLS" Then
                GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "SRGL"
            End If
            '-- (FIM)    -- RSouza -- 23/07/2010 -----------------------------------------------------------'
        '(INI) Demanda 18584657 - Wilson Funaki.

        ' Demanda MU-2017-040141 - IN�CIO
        'ElseIf GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1225 Or GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1231 Then 'Marcio.Nogueira 24/07/2015 - Inclus�o Produto 1231
            'GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "AVSR"
        
        ElseIf (GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1225 Or GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1231) And VerificaSinistradoSegurado(GrdResultadoPesquisa.TextMatrix(contLinha, 5), SEGP0794_3.mskCPF, GrdResultadoPesquisa.TextMatrix(contLinha, 12)) = False Then
            GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "NAVS"
                                                                
        ' Demanda MU-2017-040141 - FIM

        '(FIM) Demanda 18584657.
        Else
            'Verifica se o segurado da proposta coincide com o sinistrado informado
                        'eduardo.amaral(nova consultoria) 27/03/2012 12363945 - BB Seguro Vida Empresa FLEX
            'Demanda MU-2017-040141 - IN�CIO
            'If GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1206 And Not verificaCoincideSinistradoSegurado(GrdResultadoPesquisa.TextMatrix(contLinha, 9)) Then
            If GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1206 And GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1225 And GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1231 And Not verificaCoincideSinistradoSegurado(GrdResultadoPesquisa.TextMatrix(contLinha, 9)) And (GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1201) And (GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1226) And (GrdResultadoPesquisa.TextMatrix(contLinha, 12) <> 1227) Then
            'Demanda MU-2017-040141 - FIM
                GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "NAVS"
            Else
                'Verifica se h� caso de aviso de sinistro anterior - reanalise
                flagReanalise = VerificaReanalise(GrdResultadoPesquisa.TextMatrix(contLinha, 5), _
                                         Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                                         SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                         GrdResultadoPesquisa.TextMatrix(contLinha, 19), _
                                         Format(GrdResultadoPesquisa.TextMatrix(contLinha, 20), "yyyymmdd"), _
                                         sinistro_id, _
                                         Sinistro_bb)
                If flagReanalise <> "" Then
                        GrdResultadoPesquisa.TextMatrix(contLinha, 1) = flagReanalise
                       
                        GrdResultadoPesquisa.TextMatrix(contLinha, 15) = sinistro_id
                        GrdResultadoPesquisa.TextMatrix(contLinha, 16) = Sinistro_bb
                        
                        '-- (IN�CIO) -- RSouza -- 23/07/2010 -------------------------------------------'
                        If flagReanalise <> "RNLS" Then
                             GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "SRGL"
                        End If
                        '-- (FIM)    -- RSouza -- 23/07/2010 -------------------------------------------'
                Else
                    'Verifica se proposta vigente na data de ocorrencia do sinistro

                    'Demanda MU-2017-040141 - IN�CIO
'                    If Not VerificaSePodeAvisar(Left(GrdResultadoPesquisa.TextMatrix(contLinha, 11), InStr(1, GrdResultadoPesquisa.TextMatrix(contLinha, 11), " - ", vbTextCompare)), _
'                                            Right(GrdResultadoPesquisa.TextMatrix(contLinha, 11), Len(GrdResultadoPesquisa.TextMatrix(contLinha, 11)) - (InStr(1, GrdResultadoPesquisa.TextMatrix(contLinha, 11), " - ", vbTextCompare) + 2))) Then
'                        GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "NAVS"
                                                                                    
                    If GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1198 Or GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1205 Or GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1208 Or GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1211 Or GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1225 Or GrdResultadoPesquisa.TextMatrix(contLinha, 12) = 1231 Then
                        PodeAvisar = VerificaSePodeAvisar(Left(GrdResultadoPesquisa.TextMatrix(contLinha, 11), InStr(1, GrdResultadoPesquisa.TextMatrix(contLinha, 11), " - ", vbTextCompare)), _
                                                        Right(GrdResultadoPesquisa.TextMatrix(contLinha, 11), Len(GrdResultadoPesquisa.TextMatrix(contLinha, 11)) - (InStr(1, GrdResultadoPesquisa.TextMatrix(contLinha, 11), " - ", vbTextCompare) + 2)), _
                                                        True, GrdResultadoPesquisa.TextMatrix(contLinha, 3), GrdResultadoPesquisa.TextMatrix(contLinha, 5))
                    Else
                        PodeAvisar = VerificaSePodeAvisar(Left(GrdResultadoPesquisa.TextMatrix(contLinha, 11), InStr(1, GrdResultadoPesquisa.TextMatrix(contLinha, 11), " - ", vbTextCompare)), _
                                                        Right(GrdResultadoPesquisa.TextMatrix(contLinha, 11), Len(GrdResultadoPesquisa.TextMatrix(contLinha, 11)) - (InStr(1, GrdResultadoPesquisa.TextMatrix(contLinha, 11), " - ", vbTextCompare) + 2)), _
                                                        False, "", 0)
                    End If
                    
                    If PodeAvisar = False Then
                        GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "NAVS"
                    'Demanda MU-2017-040141 - FIM
                            
                    Else
                        'Verifica se a proposta tem cobertura atingida pelo evento informado
                                                'eduardo.amaral(nova consultoria) 27/03/2012 12363945 - BB Seguro Vida Empresa FLEX
                        
                        '------------------------------------------------------------------------------
                        'Ricardo Toledo (Confitec) : 17/05/2013 : INC000004048292 : INICIO
                        
                        'If VerificaCobertAtingida(GrdResultadoPesquisa.TextMatrix(contLinha, 5), _
                           Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                           GrdResultadoPesquisa.TextMatrix(1, 26), _
                           SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                           subevento_sinistro_aux, _
                           GrdResultadoPesquisa.TextMatrix(contLinha, 14)) Then
                                
                        If VerificaCobertAtingida(GrdResultadoPesquisa.TextMatrix(contLinha, 5), _
                           Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                           GrdResultadoPesquisa.TextMatrix(contLinha, 26), _
                           SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                           subevento_sinistro_aux, _
                           GrdResultadoPesquisa.TextMatrix(contLinha, 14)) Then
                        'Ricardo Toledo (Confitec) : 17/05/2013 : INC000004048292 : FIM
                        '------------------------------------------------------------------------------
                                GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "AVSR"
                        Else
                                GrdResultadoPesquisa.TextMatrix(contLinha, 1) = "PSCB"
                        End If
                                                'fim eduardo.amaral(nova consultoria)
                    End If
                End If
            End If
        End If
    Next contLinha
    
    '-- (IN�CIO) -- RSouza -- 23/07/2010 -----------------------------------------------------------'
    click_tela = True
    GrdResultadoPesquisa_Click
    '-- (FIM)    -- RSouza -- 23/07/2010 -------------------------------------------'
    
End Sub

Private Function verificaCoincideSinistradoSegurado(Segurado As String) As Boolean

Dim primeiroNomeSinis As String
Dim ultimoNomeSinis As String
Dim primeiroNomeSegu As String
Dim ultimoNomeSegu As String
Dim ultimoEspacoSinis As Long
Dim ultimoEspacoSegu As Long

Dim SQL As String
Dim rs As ADODB.Recordset

verificaCoincideSinistradoSegurado = False

Segurado = Trim(Segurado) 'Demanda 18584657 - Wilson Funaki: Tira espa�os para pegar corretamente o �ltimo nome.

'mfujino 24/11/2004
'tratamento para cliente sem sobrenome
If InStr(1, SEGP0794_3.txtNomeSinistrado.Text, " ", vbTextCompare) > 0 Then
    primeiroNomeSinis = Mid(SEGP0794_3.txtNomeSinistrado.Text, 1, InStr(1, SEGP0794_3.txtNomeSinistrado.Text, " ", vbTextCompare) - 1)
    
    'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
    'primeiroNomeSegu = Mid(Segurado, 1, InStr(1, Segurado, " ", vbTextCompare) - 1)
    primeiroNomeSegu = MudaAspaSimples(Mid(Segurado, 1, InStr(1, Segurado, " ", vbTextCompare) - 1))
    'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim
Else
    primeiroNomeSinis = SEGP0794_3.txtNomeSinistrado.Text
    
    'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
    'primeiroNomeSegu = Segurado
    primeiroNomeSegu = MudaAspaSimples(Segurado)
    'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim
End If


'Henrique H. Henrique - INC000004654562 - INI
ultimoEspacoSinis = InStrRev(Trim(SEGP0794_3.txtNomeSinistrado.Text), " ", , vbTextCompare)
ultimoNomeSinis = Mid(Trim(SEGP0794_3.txtNomeSinistrado.Text), ultimoEspacoSinis + 1, Len(Trim(SEGP0794_3.txtNomeSinistrado.Text)) - ultimoEspacoSinis)
'Henrique H. Henrique - INC000004654562 - FIM

ultimoEspacoSegu = InStrRev(Segurado, " ", , vbTextCompare)
ultimoNomeSegu = Mid(Segurado, ultimoEspacoSegu + 1, Len(Segurado) - ultimoEspacoSegu)

'cristovao.rodrigues 30/06/2014
ultimoNomeSinis = MudaAspaSimples(ultimoNomeSinis)
ultimoNomeSegu = MudaAspaSimples(ultimoNomeSegu)

SQL = "select difference('" & primeiroNomeSinis & "','" & primeiroNomeSegu & "')"
Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
If rs(0) >= 2 Then 'G&P - AFONSO - Antigo: If rs(0) > 2 Then
    rs.Close
    SQL = "select difference('" & ultimoNomeSinis & "','" & ultimoNomeSegu & "')"
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    If rs(0) > 2 Then
        verificaCoincideSinistradoSegurado = True
    End If
End If

End Function

Private Function ExisteEventoSinistroProduto(Evento_sinistro_id As Integer, produto_id As Integer) As Boolean
Dim SQL As String
Dim rs As ADODB.Recordset

    'Verifica se o evento de sinistro est� cadastrado para o produto
    ExisteEventoSinistroProduto = False
    
    SQL = ""
    SQL = SQL & " select 1 from produto_estimativa_sinistro_tb with (nolock) "
    SQL = SQL & " where produto_id = " & produto_id
    SQL = SQL & " and evento_sinistro_id = " & Evento_sinistro_id
        
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    
    If Not rs.EOF Then
        ExisteEventoSinistroProduto = True
    End If
    
    rs.Close
End Function

'Demanda MU-2017-040141 - IN�CIO
'Private Function VerificaSePodeAvisar(dt_inicio_vigencia As String, dt_fim_vigencia As String) As Boolean
Private Function VerificaSePodeAvisar(dt_inicio_vigencia As String, dt_fim_vigencia As String, verificarCancelamento As Boolean, situacao_proposta As String, proposta_id As Long) As Boolean
'Demanda MU-2017-040141 - FIM

Dim dt_ocorrencia As String

'Demanda MU-2017-040141 - IN�CIO
Dim SQL As String
Dim rc As ADODB.Recordset

Dim dt_cancelamento As String
'Demanda MU-2017-040141 - FIM

dt_ocorrencia = Format(SEGP0794_3.mskDataOcorrencia, "yyyymmdd")
VerificaSePodeAvisar = False

'Demanda MU-2017-040141 - IN�CIO
If verificarCancelamento = True And situacao_proposta = "Cancelada" Then
        
    SQL = "Select dt_inicio_cancelamento " & _
            "From cancelamento_proposta_tb " & _
            "Where proposta_id = " & proposta_id
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, _
                        True)
                                
    If Not rc.EOF Then
        dt_cancelamento = Format(rc!dt_inicio_cancelamento, "yyyymmdd")
    Else
        dt_cancelamento = ""
    End If
    
    'Verifica vigencia
    If Trim(dt_inicio_vigencia) <> "" Then
        dt_inicio_vigencia = Format(dt_inicio_vigencia, "yyyymmdd")
        
        If Trim(dt_fim_vigencia) <> "" Then
            dt_fim_vigencia = Format(dt_fim_vigencia, "yyyymmdd")
            
            If Trim(dt_cancelamento) <> "" Then
                If dt_inicio_vigencia <= dt_ocorrencia And dt_fim_vigencia >= dt_ocorrencia And dt_cancelamento > dt_ocorrencia Then
                   VerificaSePodeAvisar = True
                End If
            Else
                If dt_inicio_vigencia <= dt_ocorrencia And dt_fim_vigencia >= dt_ocorrencia Then
                   VerificaSePodeAvisar = True
                End If
            End If
        Else
            If Trim(dt_cancelamento) <> "" Then
                If dt_inicio_vigencia <= dt_ocorrencia And dt_cancelamento > dt_ocorrencia Then
                     VerificaSePodeAvisar = True
                End If
            Else
                If dt_inicio_vigencia <= dt_ocorrencia Then
                     VerificaSePodeAvisar = True
                End If
            End If
        End If
    Else
        If dt_fim_vigencia <> "" Then
            dt_fim_vigencia = Format(dt_fim_vigencia, "yyyymmdd")
            
            If Trim(dt_cancelamento) <> "" Then
                If dt_fim_vigencia >= dt_ocorrencia And dt_cancelamento > dt_ocorrencia Then
                   VerificaSePodeAvisar = True
                End If
            Else
                If dt_fim_vigencia >= dt_ocorrencia Then
                   VerificaSePodeAvisar = True
                End If
            End If
        End If
    End If

Else
'Demanda MU-2017-040141 - FIM

    'Verifica vigencia
    If Trim(dt_inicio_vigencia) <> "" Then
        dt_inicio_vigencia = Format(dt_inicio_vigencia, "yyyymmdd")
        If Trim(dt_fim_vigencia) <> "" Then
            dt_fim_vigencia = Format(dt_fim_vigencia, "yyyymmdd")
            If dt_inicio_vigencia <= dt_ocorrencia And _
               dt_fim_vigencia >= dt_ocorrencia Then
               VerificaSePodeAvisar = True
            End If
        Else
             If dt_inicio_vigencia <= dt_ocorrencia Then
                  VerificaSePodeAvisar = True
            End If
        End If
    Else
        If dt_fim_vigencia <> "" Then
            dt_fim_vigencia = Format(dt_fim_vigencia, "yyyymmdd")
            If dt_fim_vigencia >= dt_ocorrencia Then
               VerificaSePodeAvisar = True
            End If
        End If
    End If

'Demanda MU-2017-040141 - IN�CIO
End If
'Demanda MU-2017-040141 - FIM

End Function

'Demanda MU-2017-040141 - IN�CIO
Private Function VerificaSinistradoSegurado(proposta_id As Long, CPF As String, produto_id As Integer) As Boolean

Dim SQL As String
Dim rc As ADODB.Recordset

VerificaSinistradoSegurado = False

If produto_id = 1225 Or produto_id = 1231 Then

    CPF = Replace(Replace(Replace(CPF, ".", ""), "-", ""), "/", "")
    
    SQL = "Select 1 " & _
            " From proposta_complementar_tb A With (Nolock) " & _
            " Join cliente_tb B With (Nolock) " & _
            "    On A.prop_cliente_id = B.cliente_id " & _
            " Where proposta_id = " & proposta_id & _
            " And B.cpf_cnpj = '" & CPF & "'"
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, _
                        True)
                                
    If Not rc.EOF Then
        VerificaSinistradoSegurado = True
    End If
    
End If
    
End Function
'Demanda MU-2017-040141 - FIM

Private Sub PreencheGridTipo()

    With GridTipo
        .Cols = 5
        
        '(IN�CIO) -- RSouza -- 27/07/2010 -----------------------
        '.Rows = 3
        .Rows = 4
        '(FIM)    -- RSouza -- 27/07/2010 -----------------------
      
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "C�digo"
        .TextMatrix(0, 2) = "Descri��o"
        .TextMatrix(1, 1) = "AVSR"
        .TextMatrix(1, 2) = "A Avisar"
        .TextMatrix(2, 1) = "RNLS"
        .TextMatrix(2, 2) = "Solicita��o de Reanalise"
        
    
        '(IN�CIO) -- RSouza -- 27/07/2010 -----------------------
        .TextMatrix(3, 1) = "SRGL"
        .TextMatrix(3, 2) = "Sinistro em regula��o"
        '(FIM)    -- RSouza -- 27/07/2010 -----------------------
        
       
        .TextMatrix(0, 3) = "C�digo"
        .TextMatrix(0, 4) = "Descri��o"
        .TextMatrix(1, 3) = "PSCB"
        .TextMatrix(1, 4) = "N�o Avisar - Proposta Sem Cobertura"
        .TextMatrix(2, 3) = "NAVS"
        .TextMatrix(2, 4) = "N�o Avisar"
        .AutoFit H�brido
    End With
    
End Sub

Private Function VerificaCobertAtingida(proposta_id As Long, _
                                        dtOcorrencia As String, _
                                        tp_componente_id As Integer, _
                                        Evento_sinistro_id As Long, _
                                        SubEvento_sinistro_id As Long, _
                                        sub_grupo_id As Integer) As Boolean

Dim SQL As String
Dim rs As ADODB.Recordset
Dim estim_cobertura As estimativa
Dim linhas As Integer

    'Seleciona as coberturas da proposta
    VerificaCobertAtingida = False
    SQL = ""
    SQL = SQL & " exec consultar_coberturas_sps '" & dtOcorrencia & "'," & _
                                  proposta_id & "," & tp_componente_id
    If sub_grupo_id > 0 Then
        SQL = SQL & " ," & sub_grupo_id
        SQL = SQL & " , 1"
    Else
        SQL = SQL & " ,null "
        SQL = SQL & " , 1"
    End If
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    
    While Not rs.EOF
        'Verifica se o evento de sinistro atinge a cobertura
        estim_cobertura = getEstimativa(proposta_id, rs!tp_cobertura_id, Evento_sinistro_id, SubEvento_sinistro_id, tp_componente_id, dtOcorrencia)
        
        If estim_cobertura.atinge Then
            VerificaCobertAtingida = True
            Exit Function
        End If
                
        rs.MoveNext
    Wend
    
End Function

Private Function ObtemProduto(produto_id As Long) As String

Dim SQL As String
Dim rc As ADODB.Recordset


SQL = " select produto_id, nome produto" & _
      " from produto_tb with (nolock) " & _
      " where produto_id = " & produto_id

Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, _
                            True)
If Not rc.EOF Then
    ObtemProduto = rc!produto_id & " - " & rc!produto
Else
    ObtemProduto = ""
End If

End Function

Public Function VerificaReanalise(proposta_id As Long, _
                                   dtOcorrencia As String, _
                                   Evento_sinistro_id As Integer, _
                                   CPF As String, _
                                   dt_Nascimento As String, _
                                   ByRef sinistro_id As String, _
                                   ByRef sinistro_bb_id As String) As String

Dim SQL As String
Dim rc As ADODB.Recordset
Dim eventos_equivalentes As String
Dim Situacao As String


SQL = ""

SQL = "set nocount on exec verifica_reanalise_sps " & proposta_id & ", " & Evento_sinistro_id & ",'" & _
    dtOcorrencia & "','" & CPF & "','" & dt_Nascimento & "'"


Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, _
                            True)
                            
If Not rc.EOF Then 'se existe aviso, verifica se est� aberto

    If IsNull(rc!Situacao) Then
        VerificaReanalise = "NAVS"
    Else
        Select Case Trim(rc!Situacao)
            Case "0", "1", ""
                VerificaReanalise = "NAVS"
            Case "2"
                VerificaReanalise = "RNLS"
            Case Else
                VerificaReanalise = "RNLS"
        End Select
    End If
    
    '******************************************************************
    'Alterado por Cleber da Stefanini - data: 14/01/2005
    
    'N�o estava sendo feito o tratamento de null somente para o
        'sinistro_bb e estava era necess�rio o mesmo para o sinistro_id
    
    'sinistro_id = rc!sinistro_id
    
    sinistro_id = IIf(IsNull(rc!sinistro_id), "", rc!sinistro_id)
    '******************************************************************
    sinistro_bb_id = IIf(IsNull(rc!Sinistro_bb), "", rc!Sinistro_bb)

Else 'if n�o existe aviso
    VerificaReanalise = ""
    sinistro_id = ""
    sinistro_bb_id = ""
End If

End Function

Public Function SelecionarLinha(ByVal iLinha As Integer, _
                                iDesmarca As Boolean)

Dim i As Integer

On Error GoTo Trata_Erro

    'marcando a linha selecionada''''''''''''''''''''''''''''''''''''''''''''''
  
    GrdResultadoPesquisa.Row = iLinha
    If iDesmarca Then
        For i = 1 To (GrdResultadoPesquisa.Cols - 1)
            GrdResultadoPesquisa.col = i
            GrdResultadoPesquisa.ForeColorFixed = vbBack
        Next
    Else
        For i = 0 To (GrdResultadoPesquisa.Cols - 1)
            GrdResultadoPesquisa.col = i
            GrdResultadoPesquisa.ForeColor = vbBlue
        Next
    End If
    
  Exit Function

Trata_Erro:
  Call TrataErroGeral("SelecionarLinha", Me.Caption)

End Function

'DIEGO GALIZONI CAVERSAN - DEMANDA 1113817 - CORRE��O BB PROTE��O - USANDO SOLU��O USADA NO SEGP0862
Public Function VerificarProdutoBB(ByVal lista As String, value As String) As Boolean
'********************************************************************
'* AFONSO NOGUEIRA - GPTI                                           *
'* DATA: 04/03/2010                                                 *
'* CRIA��O DO METODO PARA LOCALIZAR O PRODUTO BB NO MEIO DA STRING  *
'********************************************************************

VerificarProdutoBB = False
Dim arr As Variant

On Error GoTo Trata_Erro

arr = Split(lista, ",")

For i = 0 To UBound(arr)
    If Trim(CStr(arr(i))) = Trim(CStr(value)) Then
        VerificarProdutoBB = True
        Exit For
    End If
Next

Exit Function
Trata_Erro:
    VerificarProdutoBB = False
End Function



Private Sub GrdResultadoPesquisa_Click()

    Dim controleBenef As Boolean
    Dim SinistroAB As String
    Dim SinistroBB As String

  '(IN�CIO) -- RSouza -- 23/07/2010 -----------------------------------------------'
  If GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 1) = "SRGL" Then
    cmdAvisar.Enabled = False
    cmdNAvisar.Enabled = False
    cmdReanalise.Enabled = False
  Else
    cmdAvisar.Enabled = True
    cmdReanalise.Enabled = True
    cmdNAvisar.Enabled = True
  End If
  '(FIM)    -- RSouza -- 23/07/2010 -----------------------------------------------'

If click_tela = False Then
    controleBenef = MsgBox("Deseja realizar o cadastro dos Benefici�rios? ", vbQuestion + vbYesNo) = vbYes
    If controleBenef = True Then
        SinistroAB = GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 5)
        SinistroBB = GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 6)
        
        SEGP0794_4_2.SinistroAB = SinistroAB
        SEGP0794_4_2.SinistroBB = SinistroBB
        
        
        SEGP0794_4_2.Monta_flexBenefSinistro
        SEGP0794_4_2.Show 1
    End If
End If
click_tela = False

End Sub
