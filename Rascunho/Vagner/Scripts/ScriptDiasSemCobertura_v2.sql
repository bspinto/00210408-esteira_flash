--*************************************************************************************************************************************************************
					/*
					Bloco de c�digo para verificar per�odos sem cobertura
					Pronto para executar
					Esse c�digo foi adaptado e inserido na procedure SEGS14618_SPS
					*/				
					--trocar tag:
					--inner com ab					
					--valor total para outra vari�vel
					--verifica��o de 0 e 1 na string

					--Essas declara��es saem quando forem para a SEGS14618_SPS
					DECLARE @CPF_CNPJ VARCHAR(14)                                				
					SET @CPF_CNPJ = '34608095072'                								
					--Usando cpf_cnpj 00689968000177 como base. Que est� toda fora.
					--

					--ENCONTRANDO AS PROPOSTAS DO CLIENTE                    
					IF (object_id('tempdb..#propostas') IS NOT NULL)
					BEGIN
                        DROP TABLE #propostas
					END				
                
					SELECT DISTINCT PROPOSTA.PRODUTO_ID
					,CLIENTE.cpf_cnpj AS CPF_CNPJ
					,PROPOSTA.proposta_id
					,ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig)) AS dt_inicio_vigencia
					,ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig)) AS dt_fim_vigencia
					,GETDATE() - 365 AS data_limite
					,GETDATE() AS data_atual
					INTO #propostas
					FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)
					INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK) ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID
					LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB FECHADA WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = FECHADA.PROPOSTA_ID
					LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ADESAO WITH (NOLOCK) ON PROPOSTA.PROPOSTA_ID = ADESAO.PROPOSTA_ID
					LEFT JOIN seguros_db.dbo.apolice_tb APOLICE WITH (NOLOCK) ON PROPOSTA.proposta_id = APOLICE.proposta_id
					WHERE CLIENTE.cpf_cnpj = @CPF_CNPJ
						AND PROPOSTA.ramo_id = 14 --@RAMO_ID
						AND PROPOSTA.situacao = 'i'					

					UNION ALL

					SELECT DISTINCT 
                        PROPOSTA.PRODUTO_ID,
                        CLIENTE.cpf_cnpj AS CPF_CNPJ,
                        PROPOSTA.proposta_id,
                        ISNULL(APOLICE.dt_inicio_vigencia,ISNULL(ADESAO.dt_inicio_vigencia,FECHADA.dt_inicio_vig)) AS dt_inicio_vigencia,
                        ISNULL(APOLICE.dt_fim_vigencia,ISNULL(ADESAO.dt_fim_vigencia,FECHADA.dt_fim_vig)) AS dt_fim_vigencia,                        
                        GETDATE() - 365 as data_limite,
						GETDATE() as data_atual                    
                    FROM [ABSS].SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)
                    INNER JOIN [ABSS].SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK)
                        ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID
                    LEFT JOIN [ABSS].SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB FECHADA WITH(NOLOCK)
                    ON PROPOSTA.PROPOSTA_ID = FECHADA.PROPOSTA_ID
                    LEFT JOIN [ABSS].SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ADESAO WITH(NOLOCK)
                    ON PROPOSTA.PROPOSTA_ID = ADESAO.PROPOSTA_ID
                    LEFT JOIN [ABSS].seguros_db.dbo.apolice_tb APOLICE WITH(NOLOCK)
                    ON PROPOSTA.proposta_id = APOLICE.proposta_id
                    WHERE CLIENTE.cpf_cnpj = @CPF_CNPJ
                    AND PROPOSTA.ramo_id = 14 --@RAMO_ID
                    and PROPOSTA.situacao = 'i'					
                    order by 4
					--SELECT * FROM #propostas					

					/*--Essas declara��es saem quando forem para a SEGS14618_SPS
					Ajustando dados de teste
					Usando cpf_cnpj 00689968000177 como base. Que est� toda fora.
					Ao efetuar os updates abaixo colocarei alguns dentro. Mas que n�o satisfar�o as exig�ncias
					Os "x" representam per�odos em que tem cobertura
					Caso 1: xxxx|xxxx_____________xxxxxxx____xxxx|xxxxx    | 
						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2018-12-01', A.DT_FIM_VIGENCIA = '2019-04-01'
						FROM #propostas A						
						WHERE proposta_id = '13512244'
						SELECT * FROM #propostas order by 4

						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-05-01', A.DT_FIM_VIGENCIA = '2019-06-01'
						FROM #propostas A						
						WHERE proposta_id = '13512255'
						SELECT * FROM #propostas order by 4

						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-12-01', A.DT_FIM_VIGENCIA = '2020-04-01'
						FROM #propostas A						
						WHERE proposta_id = '13560598'
						SELECT * FROM #propostas order by 4										

					Caso 2: |_________xxxxxxx____xxxx____|
						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-05-01', A.DT_FIM_VIGENCIA = '2019-06-01'
						FROM #propostas A						
						WHERE proposta_id = '13512244'
						SELECT * FROM #propostas order by 4

						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-08-01', A.DT_FIM_VIGENCIA = '2019-09-01'
						FROM #propostas A						
						WHERE proposta_id = '13512255'
						SELECT * FROM #propostas order by 4						

					Caso 3: xxxx|xxxxxxxxxxxxx|xxxxx
						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2018-12-01', A.DT_FIM_VIGENCIA = '2020-06-01'
						FROM #propostas A						
						WHERE proposta_id = '13512244'
						SELECT * FROM #propostas order by 4						

					Caso 4: |___xxxxxxxxxxx____xxxx___xxx|xxxxx
					                xxxx
						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-03-01', A.DT_FIM_VIGENCIA = '2019-07-01'
						FROM #propostas A						
						WHERE proposta_id = '13512244'
						SELECT * FROM #propostas order by 4

						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-04-01', A.DT_FIM_VIGENCIA = '2019-05-01'
						FROM #propostas A						
						WHERE proposta_id = '13512255'
						SELECT * FROM #propostas order by 4						

						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-09-01', A.DT_FIM_VIGENCIA = '2019-10-01'
						FROM #propostas A						
						WHERE proposta_id = '13512241'
						SELECT * FROM #propostas order by 4						

						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-12-01', A.DT_FIM_VIGENCIA = '2020-03-01'
						FROM #propostas A						
						WHERE proposta_id = '13512259'
						SELECT * FROM #propostas order by 4
						
					Caso 5: |___xxxxxxxxxxxxxxxxxx|xxxxx
					    
						UPDATE A
						SET A.DT_INICIO_VIGENCIA = '2019-04-01', A.DT_FIM_VIGENCIA = '2020-07-01'
						FROM #propostas A						
						WHERE proposta_id = '13512244'
						SELECT * FROM #propostas order by 4													
					*/
                    
					--REMOVENDO AS PROPOSTAS QUE TIVERAM VIGENCIA FINDADAS ANTES DA DATA LIMITE
                    DELETE PROP
                    --SELECT *
                    FROM #PROPOSTAS PROP
                    WHERE dt_fim_vigencia < data_limite
					--SELECT * FROM #propostas order by 4
                    
                    --criando campos de apoio
					ALTER TABLE #propostas ADD ID INT IDENTITY(11,1)					
                    
					ALTER TABLE #propostas ADD DIAS INT 					
                    
					ALTER TABLE #propostas ADD FLAG BIT -- VAI MOSTRAR QUE N�O TEM BRECHAS                                        
					--SELECT * FROM #propostas order by 4
					       
					-- 
					UPDATE A
                    SET A.DT_INICIO_VIGENCIA = B.DT_FIM_VIGENCIA , A.FLAG = 1
                    FROM #propostas A
                    INNER JOIN #propostas B
                    ON A.ID = B.ID + 1
                    WHERE B.DT_FIM_VIGENCIA BETWEEN A.DT_INICIO_VIGENCIA AND A.DT_FIM_VIGENCIA                        
                    --SELECT * FROM #propostas order by 4

					DELETE PROP                    
                    FROM #PROPOSTAS PROP
                    WHERE dt_inicio_vigencia > data_atual
					--SELECT * FROM #propostas order by 4
                              
                    -- ajustar in�cio =  data_limite (do primeiro registro caso esteja anteda da data limite)
					UPDATE A
                    SET A.DT_INICIO_VIGENCIA = data_limite
                    FROM #propostas A                    
                    WHERE A.dt_inicio_vigencia < a.data_limite AND --A.dt_inicio_vigencia BETWEEN A.data_limite AND A.data_atual and
					ID = (select top(1) id from #propostas order by id asc)
                    --SELECT * FROM #propostas order by 4
					
					-- ajustar data final = data atual (do ultimo registro)
					UPDATE A
                    SET A.dt_fim_vigencia = data_atual
                    FROM #propostas A                    
                    WHERE A.dt_fim_vigencia > A.data_atual  AND --A.dt_inicio_vigencia BETWEEN A.data_limite AND A.data_atual and
					ID = (select top(1) id from #propostas order by id desc)
					--SELECT * FROM #propostas order by 4
					
					UPDATE #propostas
                    SET DIAS = DATEDIFF(day, DT_INICIO_VIGENCIA,DT_FIM_VIGENCIA)
                    FROM #propostas
					--SELECT * FROM #propostas order by 4										

					--Buscando o par�metro
					--Essas declara��es saem quando forem para a SEGS14618_SPS
					DECLARE @SQL AS NVARCHAR(1000),
							@ParmDefinition AS NVARCHAR(300),
							@ValorTotal AS  NUMERIC(15,2) = 3000,
							@PgtoImediato AS SMALLINT = 1, 
							@sinistro_parametro_chave_id  AS int = 1079,
							@TOTAL_DIAS_RELACIONAMENTO as int
					---

					/*
					if ((SELECT sum(dias)  FROM #propostas) < 365) or ((SELECT sum(dias)  FROM #propostas) is null)
						--select 0
						Set @PgtoImediato = 0
					else
						--select 1					                            			
						Set @PgtoImediato = 1
					*/

					select @ValorTotal = sum(dias)  FROM #propostas
					select @ValorTotal

					SELECT @sql = 'IF( ' + convert(VARCHAR(20), convert(NUMERIC(15, 2), @ValorTotal)) 
										 + ' ' 
										 + OPERADOR 
										 + ' ' 
										 + VALOR + ') 
										SELECT @retornoOUT = 1
									ELSE 
									SELECT @retornoOUT = 0
									'									 														
					FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
					inner join desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						on CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
					 WHERE 1=1
						 AND CHAVE.sinistro_parametro_chave_id = 1162--2--1079  --@sinistro_parametro_chave_id
						 AND REGRA.NOME = 'periodo_relacionamento' --PARAMETRO

					SET @ParmDefinition = '@retornoOUT varchar(300) OUTPUT';

					EXECUTE sp_executesql @SQL,
						@ParmDefinition,
						@retornoOUT = @PgtoImediato OUTPUT						

					select @sql as sql

					--Essas declara��es saem quando forem para a SEGS14618_SPS
					DECLARE @Detalhamento AS VARCHAR(3000)					
					--

					SELECT @PgtoImediato as PgtoImediato
					/*
					IF @PgtoImediato <> 1 
					BEGIN						
						select @Detalhamento = ''						
						SELECT @Detalhamento = @Detalhamento + CHAR(13)+'N�o eleg�vel para pagamento imediato com ' + convert(nvarchar,@ValorTotal) +' dias sem relacionamento.'+CHAR(13) FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
							INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
						ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
						WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
						  AND REGRA.NOME = 'periodo_relacionamento'
					END
					ELSE
					BEGIN
						select @Detalhamento = ''						
						SELECT @Detalhamento = @Detalhamento + CHAR(13)+'Eleg�vel para pagamento imediato com mais de ' + convert(nvarchar,@ValorTotal) +' dias sem relacionamento.'+CHAR(13) FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
							INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA
							ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
							WHERE CHAVE.sinistro_parametro_chave_id = 1079 --@sinistro_parametro_chave_id
							AND REGRA.NOME = 'periodo_relacionamento'
					END
					*/
					IF @PgtoImediato <> 1
					BEGIN
						set @Detalhamento = ''
						SELECT @Detalhamento = CONCAT (
								@Detalhamento
								,CHAR(13)
								,CHAR(10)
								,'Per�odo de relacionamento do cliente com a BrasilSeg est� abaixo do limite para pagamento imediato: '
								,convert(VARCHAR, @TOTAL_DIAS_RELACIONAMENTO)
								,' '
								,convert(VARCHAR, CHAVE.operador)
								,' '
								,convert(VARCHAR, CHAVE.valor)
								)
						FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
						INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
						WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id
							AND REGRA.NOME = 'periodo_relacionamento'
					END
					ELSE
					BEGIN
						set @Detalhamento = ''
						SELECT @Detalhamento = CONCAT (
								@Detalhamento
								,CHAR(13)
								,CHAR(10)
								,'Per�odo de relacionamento do cliente com a BrasilSeg est� dentro do limite para pagamento imediato'
								,convert(VARCHAR, @TOTAL_DIAS_RELACIONAMENTO)
								,' '
								,convert(VARCHAR, CHAVE.operador)
								,' '
								,convert(VARCHAR, CHAVE.valor)
								)
						FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE
						INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
						WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id
							AND REGRA.NOME = 'periodo_relacionamento'
					END
					select @Detalhamento as detalhamento
--*************************************************************************************************************************************************************