-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
--C00210408 - Esteira Flash - Sinistros de Danos El�tricos Residenciais
--Regras de elegibilidade para pagamento imediato
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------


--1. **************************************	Evento = Danos El�tricos = evento_sinistro_id = 3
     --Essa verifica��o dever� ser feita consultando uma tabela tempor�ria previamente ou a pr�pria interface
	 --select * from Evento_Sinistro_Tb

--2. **************************************	Coberturas listadas na demanda
	-- tp_cobertura_id in(16,68,130,498,867,1001,1012)

--3. **************************************	Estimativa / Or�amento <= R$ 3.500 
	 -- o valor de estimativa/or�amento tamb�m vir� de fora

--4. **************************************	Tempo de Relacionamento > 12 meses --cpf/cnpf
	 --??
		--Campo de Tempo de Relacionamento do Cliente com a Brasilseg (em dias), considerando os segurados emitidos na AB e na ABS. 
		--A contagem deve ser feita desde a 1� Ap�lice emitida para o cliente (CPF / CNPJ), e que n�o tenha ocorrido uma interrup��o (falta de seguro) maior que 365 Dias.
		select * from apolice_tb
		select * from proposta_tb where proposta_id = '50619143'
		select cpf_cnpj, * from cliente_tb where cliente_id = 10443

		--Ap�lices emitidas para o cliente
		select * from apolice_tb a inner join proposta_tb p on a.proposta_id = p.proposta_id where p.prop_cliente_id = 10443
		

--5. **************************************	N�o possuir sinistro no per�odo de 1 ano no CPF e no ramo 14
	 --??
		--Tempo Decorrido (em dias) do Pen�ltimo Sinistro (Data de Ocorr�ncia) e o Sinistro que est� sendo aberto.


--6. **************************************	N�o possuir restri��o = A��o Judicial / Indicio de Fraude
	 --??
		--Consta na Lista de Pr�-Analise (A��o Judicial / Indicio de Fraude): <Sim / N�o>. 
		--Olhar pelo CPF se est� nesta lista. Caso sim, e estiver ativo, n�o poder� ser Pagamento Imediato.

--7. **************************************	Estar com os pagamentos em dia
	 --??
		--Status de Pagamento da Ap�lice: <Adimplente / Inadimplente> - Seria igual a ADIMPLENTE?
