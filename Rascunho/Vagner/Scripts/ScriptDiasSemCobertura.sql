-- Sript para verifica��o se h� dias sem cobertura nos �ltimos 365 dias
--Parametros: CPF_CNPJ
--RETORNO:	0 = pgmnt imediato negado, tem dias sem cobertura
--			1 = pgmnt imediato autorizado, n�o tem dias sem cobertura
	
--Tarefas:	- obter a quantidade de dias da tabela de par�metros
--			- testar com caso negativo
--			- confirmar outros par�metros que ser�o passados

	-- ENCONTRANDO O CLIENTE 
                DECLARE @CPF_CNPJ VARCHAR(14)                                
				SET @CPF_CNPJ = '07508344634'
                --SELECT @CPF_CNPJ

	--ENCONTRANDO AS PROPOSTAS DO CLIENTE                    
                IF (object_id('tempdb..#propostas') IS NOT NULL)
                BEGIN
                        DROP TABLE #propostas
                END                    
                
				SELECT DISTINCT 
                        PROPOSTA.PRODUTO_ID,
                        CLIENTE.cpf_cnpj AS CPF_CNPJ,
                        PROPOSTA.proposta_id,
                        ISNULL(APOLICE.dt_inicio_vigencia,ISNULL(ADESAO.dt_inicio_vigencia,FECHADA.dt_inicio_vig)) AS dt_inicio_vigencia,
                        ISNULL(APOLICE.dt_fim_vigencia,ISNULL(ADESAO.dt_fim_vigencia,FECHADA.dt_fim_vig)) AS dt_fim_vigencia,                        
                        GETDATE() - 365 as data_limite,
						GETDATE() as data_atual
                    into #propostas
                    FROM SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)
                    INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB CLIENTE WITH (NOLOCK)
                        ON PROPOSTA.prop_cliente_id = CLIENTE.CLIENTE_ID
                    LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB FECHADA WITH(NOLOCK)
                    ON PROPOSTA.PROPOSTA_ID = FECHADA.PROPOSTA_ID
                    LEFT JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB ADESAO WITH(NOLOCK)
                    ON PROPOSTA.PROPOSTA_ID = ADESAO.PROPOSTA_ID
                    LEFT JOIN seguros_db.dbo.apolice_tb APOLICE WITH(NOLOCK)
                    ON PROPOSTA.proposta_id = APOLICE.proposta_id
                    WHERE CLIENTE.cpf_cnpj = @CPF_CNPJ
                    AND PROPOSTA.ramo_id = 14 --@RAMO_ID
                    and PROPOSTA.situacao = 'i'
                    order by 4
					--SELECT * FROM #propostas
                    
	--REMOVENDO AS PROPOSTAS QUE TIVERAM VIGENCIA FINDADAS ANTES DA DATA LIMITE
                    DELETE PROP
                    --SELECT *
                    FROM #PROPOSTAS PROP
                    WHERE dt_fim_vigencia < data_limite
					--SELECT * FROM #propostas					
                    
                    --criando campos de apoio
					ALTER TABLE #propostas ADD ID INT IDENTITY(11,1)
					--SELECT * FROM #propostas
                    
					ALTER TABLE #propostas ADD DIAS INT 
					--SELECT * FROM #propostas
                    
					ALTER TABLE #propostas ADD FLAG BIT -- VAI MOSTRAR QUE N�O TEM BRECHAS                                        
					--SELECT * FROM #propostas    
					       
					-- 
					  UPDATE A
                    SET A.DT_INICIO_VIGENCIA = B.DT_FIM_VIGENCIA , A.FLAG = 1
                    FROM #propostas A
                    INNER JOIN #propostas B
                    ON A.ID = B.ID + 1
                    WHERE B.DT_FIM_VIGENCIA BETWEEN A.DT_INICIO_VIGENCIA AND A.DT_FIM_VIGENCIA    
                    
                    --SELECT * FROM #propostas

					DELETE PROP
                    --SELECT *
                    FROM #PROPOSTAS PROP
                    WHERE dt_inicio_vigencia > data_atual
					--SELECT * FROM #propostas
                              
                    -- ajustar in�cio =  data_limite (do primeiro registro)
					UPDATE A
                    SET A.DT_INICIO_VIGENCIA = data_limite
                    FROM #propostas A                    
                    WHERE --A.dt_inicio_vigencia BETWEEN A.data_limite AND A.data_atual and
					ID = (select top(1) id from #propostas order by id asc)
                    --SELECT * FROM #propostas
					
					-- ajustar data final = data atual (do ultimo registro)
					UPDATE A
                    SET A.dt_fim_vigencia = data_atual
                    FROM #propostas A                    
                    WHERE --A.dt_inicio_vigencia BETWEEN A.data_limite AND A.data_atual and
					ID = (select top(1) id from #propostas order by id desc)
					--SELECT * FROM #propostas
					
					UPDATE #propostas
                    SET DIAS = DATEDIFF(day, DT_INICIO_VIGENCIA,DT_FIM_VIGENCIA)
                    FROM #propostas
					--SELECT * FROM #propostas
					
					if (SELECT sum(dias)  FROM #propostas) < 365					
						select 0
					else
						select 1					                            