VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmBeneficiarios 
   Caption         =   "Benefeci�rios"
   ClientHeight    =   7065
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10830
   LinkTopic       =   "Form1"
   ScaleHeight     =   7065
   ScaleWidth      =   10830
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnBenefExcluir 
      Caption         =   "<< Excluir"
      Enabled         =   0   'False
      Height          =   315
      Left            =   240
      TabIndex        =   18
      Top             =   6480
      Width           =   1215
   End
   Begin VB.CommandButton btnBenefAlterar 
      Caption         =   "<< Alterar >>"
      Enabled         =   0   'False
      Height          =   315
      Left            =   240
      TabIndex        =   17
      Top             =   5775
      Width           =   1215
   End
   Begin VB.CommandButton btnBenefAdicionar 
      Caption         =   "Adicionar >>"
      Height          =   315
      Left            =   240
      TabIndex        =   16
      Top             =   5070
      Width           =   1215
   End
   Begin VB.CommandButton btnCancelar 
      Caption         =   "Cancelar"
      Height          =   360
      Left            =   4320
      TabIndex        =   20
      Top             =   7200
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton btnConcluir 
      Caption         =   "Sair"
      Height          =   480
      Left            =   9405
      TabIndex        =   19
      Top             =   7080
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Frame Frame1 
      Caption         =   " Benefici�rios do Sinistro"
      Height          =   6975
      Left            =   120
      TabIndex        =   22
      Top             =   0
      Width           =   10575
      Begin VB.Frame FraDadosBanc�rios 
         Caption         =   "Dados Banc�rios"
         Height          =   2415
         Left            =   240
         TabIndex        =   41
         Top             =   2400
         Width           =   2535
         Begin MSMask.MaskEdBox mskBanco 
            Height          =   285
            Left            =   120
            TabIndex        =   4
            Top             =   600
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   503
            _Version        =   393216
            MaxLength       =   3
            Mask            =   "###"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskAgencia 
            Height          =   285
            Left            =   120
            TabIndex        =   5
            Top             =   1200
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   503
            _Version        =   393216
            MaxLength       =   4
            Mask            =   "####"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskContaCorrente 
            Height          =   285
            Left            =   120
            TabIndex        =   7
            Top             =   1920
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   503
            _Version        =   393216
            MaxLength       =   11
            Mask            =   "###########"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskContaCorrenteDv 
            Height          =   315
            Left            =   1710
            TabIndex        =   8
            Top             =   1920
            Width           =   375
            _ExtentX        =   661
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   2
            Mask            =   "AA"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskAgenciaDv 
            Height          =   315
            Left            =   1680
            TabIndex        =   6
            Top             =   1200
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   1
            Mask            =   "A"
            PromptChar      =   " "
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Agencia:"
            Height          =   255
            Left            =   120
            TabIndex        =   46
            Top             =   960
            Width           =   735
         End
         Begin VB.Label lblBanco 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Banco:"
            Height          =   195
            Left            =   120
            TabIndex        =   45
            Top             =   360
            Width           =   510
         End
         Begin VB.Label lblDvag 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "DV:"
            Height          =   195
            Left            =   1680
            TabIndex        =   44
            Top             =   960
            Width           =   270
         End
         Begin VB.Label lblDVCC 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "DV:"
            Height          =   195
            Left            =   1680
            TabIndex        =   43
            Top             =   1680
            Width           =   270
         End
         Begin VB.Label lblContaCorrente 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Conta Corrente:"
            Height          =   195
            Left            =   120
            TabIndex        =   42
            Top             =   1680
            Width           =   1110
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flexBenefSinistro 
         Height          =   1815
         Left            =   1680
         TabIndex        =   38
         Top             =   5040
         Width           =   8655
         _ExtentX        =   15266
         _ExtentY        =   3201
         _Version        =   393216
         Cols            =   14
         FormatString    =   $"frmBeneficiarios.frx":0000
      End
      Begin MSMask.MaskEdBox mskCPF 
         Height          =   285
         Left            =   3120
         TabIndex        =   9
         Top             =   3360
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   14
         Mask            =   "###.###.###-##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDataNascimento 
         Height          =   285
         Left            =   8400
         TabIndex        =   1
         Top             =   600
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.TextBox txtNomeBeneficiario 
         Height          =   285
         Left            =   360
         TabIndex        =   0
         Top             =   600
         Width           =   7815
      End
      Begin VB.Frame FraDocumentoAuxiliar 
         Caption         =   "Documento Auxiliar"
         Height          =   2415
         Left            =   5520
         TabIndex        =   26
         Top             =   2400
         Width           =   4815
         Begin VB.ComboBox cmbDocumentoBeneficiario 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   720
            Width           =   4575
         End
         Begin VB.TextBox txtDoctoBenefComp 
            Height          =   285
            Left            =   120
            TabIndex        =   15
            Top             =   2040
            Width           =   4455
         End
         Begin VB.TextBox txtSerie 
            Height          =   285
            Left            =   3240
            TabIndex        =   14
            Top             =   1440
            Width           =   1455
         End
         Begin VB.TextBox txtOrgao 
            Height          =   285
            Left            =   1560
            TabIndex        =   13
            Top             =   1440
            Width           =   1575
         End
         Begin VB.TextBox txtNumero 
            Height          =   285
            Left            =   120
            TabIndex        =   12
            Top             =   1440
            Width           =   1335
         End
         Begin VB.Label lblComplemento 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Complemento:"
            Height          =   195
            Left            =   120
            TabIndex        =   36
            Top             =   1800
            Width           =   1005
         End
         Begin VB.Label lblSerie 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "S�rie"
            Height          =   195
            Left            =   3240
            TabIndex        =   35
            Top             =   1200
            Width           =   360
         End
         Begin VB.Label lblOrgao 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Org�o"
            Height          =   195
            Left            =   1560
            TabIndex        =   34
            Top             =   1200
            Width           =   435
         End
         Begin VB.Label lblNumero 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Numero"
            Height          =   195
            Left            =   120
            TabIndex        =   33
            Top             =   1200
            Width           =   555
         End
         Begin VB.Label lblTipo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Tipo:"
            Height          =   195
            Left            =   120
            TabIndex        =   32
            Top             =   480
            Width           =   360
         End
      End
      Begin VB.Frame FraCPFCNPJ 
         Caption         =   "CPF ou CNPJ"
         Height          =   2415
         Left            =   2880
         TabIndex        =   25
         Top             =   2400
         Width           =   2535
         Begin MSMask.MaskEdBox mskCGC 
            Height          =   285
            Left            =   240
            TabIndex        =   10
            Top             =   1800
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   503
            _Version        =   393216
            MaxLength       =   17
            Mask            =   "##.###.###/####-#"
            PromptChar      =   "_"
         End
         Begin VB.Label lblCNPJ 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "CNPJ:"
            Height          =   195
            Left            =   240
            TabIndex        =   31
            Top             =   1440
            Width           =   450
         End
         Begin VB.Label lblCPF 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "CPF:"
            Height          =   195
            Left            =   240
            TabIndex        =   30
            Top             =   600
            Width           =   345
         End
      End
      Begin VB.Frame FraSexo 
         Caption         =   "Sexo"
         Height          =   1215
         Left            =   8640
         TabIndex        =   24
         Top             =   1080
         Width           =   1695
         Begin VB.CheckBox chkFeminino 
            Caption         =   "Feminino"
            Height          =   255
            Left            =   240
            TabIndex        =   40
            TabStop         =   0   'False
            Top             =   840
            Width           =   1095
         End
         Begin VB.CheckBox chkMasculino 
            Caption         =   "Masculino"
            Height          =   255
            Left            =   240
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   360
            Width           =   1095
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Dados do Respons�vel"
         Height          =   1215
         Left            =   240
         TabIndex        =   23
         Top             =   1080
         Width           =   8295
         Begin VB.CheckBox optAssistido 
            Caption         =   "Assistido por"
            Height          =   255
            Left            =   240
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   360
            Width           =   1335
         End
         Begin VB.CheckBox optCurador 
            Caption         =   "Curador"
            Height          =   255
            Left            =   240
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   840
            Width           =   1455
         End
         Begin VB.TextBox txtNomeAuxiliar 
            Height          =   285
            Left            =   1920
            TabIndex        =   3
            Top             =   720
            Width           =   6255
         End
         Begin VB.Label lblNomeResp 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Nome"
            Height          =   195
            Left            =   1920
            TabIndex        =   29
            Top             =   360
            Width           =   540
         End
      End
      Begin MSMask.MaskEdBox MskCC 
         Height          =   285
         Left            =   360
         TabIndex        =   37
         Top             =   4320
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   4
         Mask            =   "####"
         PromptChar      =   "_"
      End
      Begin VB.Label lblDataNascimento 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Data Nascimento"
         Height          =   195
         Left            =   8400
         TabIndex        =   28
         Top             =   360
         Width           =   1230
      End
      Begin VB.Label lblNome 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nome:"
         Height          =   195
         Left            =   360
         TabIndex        =   27
         Top             =   360
         Width           =   465
      End
   End
End
Attribute VB_Name = "frmBeneficiarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Implementaca��o para Montagem do Grid com os dados dos Benefici�rios Adicionais
'Andr� Stoicov - CWI:08/12/2010
'Demanda: 1113817
Option Explicit

Dim SinistroEmUso               As New Collection
Dim Solicitante                 As New Collection
Public BeneficiariosSinistro       As New Collection
Public BeneficiariosSinistroExc    As New Collection
Dim SinistrosBB                 As New Collection

Dim FlagAlteracao               As Boolean
Dim vSelBeneficiario            As Boolean
Dim vSelBenefProposta           As Boolean
Dim vFRMAVISO_ESTA_VISIVEL      As Boolean
Dim vEstaIniciando              As Boolean
Dim vTranspInternacional        As Boolean
Dim GTR_Implantado              As Boolean
Dim Incluiu_Novo_SinistroBB     As Boolean
Dim retorno_entrada_GTR_tb      As Boolean

Dim vEnd_Risco_id               As Integer
Dim Qtde_Motivo_encerramento    As Integer

Dim vDtInicioSeg                As String
Dim Tecnico_atual               As String
Dim vSinistro                   As String
Dim vProduto                    As String
Dim vTpRamo                     As String
Dim vTpComponente               As String
Dim vopercoseguro               As String
Dim sBancoAnt                   As String
Dim voper                       As String

Private Sub btnBenefAdicionar_Click()

    Dim Benef As BenefSinistro

    If ValidaBenef Then

        Set Benef = New BenefSinistro
        Monta_Um_Benef Benef, "I"
        Monta_flexBenefSinistro
        LimpaTabBeneficiarios
        Dim X As Object
        
    End If

End Sub

Public Sub Monta_flexBenefSinistro()

    Dim Benef As BenefSinistro
    Dim Linha As String
    Dim vSql As String
    Dim Seq As Integer
    
    flexBenefSinistro.Rows = 1
        For Each Benef In BeneficiariosSinistro
            If Benef.tp_cadastro = "" Then
                flexBenefSinistro.Row = flexBenefSinistro.Rows - 1
                Linha = Benef.Seq & vbTab & Benef.Nome & vbTab & _
                        Benef.dt_Nascimento & vbTab
                If Benef.Sexo = "Masc." Then
                    Linha = Linha & "Masculino" & vbTab
                ElseIf Benef.Sexo = "Fem." Then
                    Linha = Linha & "Feminino" & vbTab
                Else
                    Linha = Linha & " " & vbTab
                End If
                Linha = Linha & Benef.Nome_Responsavel & vbTab & _
                        Benef.Banco & vbTab & _
                        Benef.Agencia & IIf(Benef.Agencia_dv <> "", "-" & Benef.Agencia_dv, "") & vbTab & _
                        Benef.Conta & IIf(Benef.Conta_dv <> "", "-" & Benef.Conta_dv, "") & vbTab & _
                        IIf(Benef.CGC <> "", Benef.CGC, Benef.CPF) & vbTab & _
                        Benef.Nome_Documento & vbTab & Benef.numero & vbTab & _
                        Benef.Orgao & vbTab & Benef.Serie & vbTab & Benef.Complemento
                flexBenefSinistro.AddItem Linha
                If Benef.Cong_Cod_Susep <> "" Then
                    flexBenefSinistro.Tag = Benef.Seq
                End If
            End If
        Next
        
        flexBenefSinistro.Col = 0
        flexBenefSinistro.ColSel = 0
        flexBenefSinistro.Sort = 0
        flexBenefSinistro.Refresh
        btnConcluir.Enabled = True

End Sub

Public Sub LimpaTabBeneficiarios()
    txtNomeBeneficiario = ""
    mskDataNascimento.Mask = ""
    mskDataNascimento = ""
    mskDataNascimento.Mask = "##/##/####"
    chkMasculino.value = 0
    chkFeminino.value = 0
    optAssistido.value = False
    optCurador.value = False
    txtNomeAuxiliar = ""
    mskBanco.Mask = ""
    mskBanco = ""
    mskBanco.Mask = "###"
    mskAgencia.Mask = ""
    mskAgencia = ""
    mskAgencia.Mask = "####"
    mskAgenciaDv.Mask = ""
    mskAgenciaDv = ""
    mskAgenciaDv.Mask = "A"
    
    
    mskContaCorrente.Mask = ""
    mskContaCorrente = ""
    mskContaCorrente.Mask = "###########"
    mskContaCorrenteDv.Mask = ""
    mskContaCorrenteDv = ""
    
    mskContaCorrenteDv.Mask = "AA"
    
    mskCGC.Mask = ""
    mskCGC = ""
    mskCGC.Mask = "##.###.###/####-##"
    mskCPF.Mask = ""
    mskCPF = ""
    mskCPF.Mask = "###.###.###-##"
    cmbDocumentoBeneficiario.ListIndex = -1
    txtOrgao = ""
    txtNumero = ""
    txtSerie = ""
    txtDoctoBenefComp = ""
    txtNomeBeneficiario.SetFocus
End Sub

Public Sub Monta_Um_Benef(ByRef Benef As Object, Operacao As String)

    Dim vSeq As Integer

    With Benef
        If Operacao = "I" Then
            If BeneficiariosSinistro.Count > 0 Then 'flexBenefSinistro.Rows > 1 Then
                vSeq = BeneficiariosSinistro.Count + 1 'CInt(flexBenefSinistro.TextMatrix(flexBenefSinistro.Rows - 1, 0)) + 1
            Else
                vSeq = 1
            End If
            .Seq = vSeq
        End If
        .Nome = MudaAspaSimples(txtNomeBeneficiario)
        .dt_Nascimento = IIf(Trim(mskDataNascimento.ClipText) <> "", _
                                mskDataNascimento.FormattedText, "")
        If chkMasculino.value = 1 Then
            .Sexo = "Masc."
        ElseIf chkFeminino.value = 1 Then
            .Sexo = "Fem."
        Else
            .Sexo = ""
        End If
        .tp_Responsavel = IIf(optAssistido.value = 1, "1", IIf(optCurador.value = 1, "2", "0"))
        .Nome_Responsavel = IIf(Trim(txtNomeAuxiliar) <> "", MudaAspaSimples(txtNomeAuxiliar), "")
        .Banco = IIf(Trim(mskBanco.ClipText) <> "", mskBanco.ClipText, "")
        .Agencia = IIf(Trim(mskAgencia.ClipText) <> "", mskAgencia.ClipText, "")
        .Agencia_dv = IIf(Trim(mskAgenciaDv.ClipText) <> "", mskAgenciaDv.ClipText, "")
        .Conta = IIf(Trim(mskContaCorrente.ClipText) <> "", mskContaCorrente.ClipText, "")
        .Conta_dv = IIf(Trim(mskContaCorrenteDv.ClipText) <> "", mskContaCorrenteDv.ClipText, "")
        .CGC = IIf(mskCGC.ClipText <> "", mskCGC, "")
        .CPF = IIf(mskCPF.ClipText <> "", mskCPF, "")
        If cmbDocumentoBeneficiario.ListIndex > 0 Then
            .tp_Documento = cmbDocumentoBeneficiario.ItemData(cmbDocumentoBeneficiario.ListIndex)
        Else
            .tp_Documento = ""
        End If
        .Nome_Documento = IIf(Trim(cmbDocumentoBeneficiario.Text) <> "", _
                    cmbDocumentoBeneficiario.Text, "")
        .Orgao = IIf(Trim(txtOrgao) <> "", MudaAspaSimples(txtOrgao), "")
        .numero = IIf(Trim(txtNumero) <> "", MudaAspaSimples(txtNumero), "")
        .Serie = IIf(Trim(txtSerie) <> "", MudaAspaSimples(txtSerie), "")
        .Complemento = IIf(Trim(txtDoctoBenefComp) <> "", MudaAspaSimples(txtDoctoBenefComp), "")
        If Operacao = "I" Then
            .Tipo = "N"
        Else
            If .Tipo = "E" Then
                .Tipo = "A"
            End If
        End If
        .Usuario = cUserName
    End With
    If Operacao = "I" Then BeneficiariosSinistro.Add Benef
End Sub

Private Sub btnBenefAlterar_Click()

     Dim Benef As BenefSinistro
    Dim vSql As String
   
    
    If vSelBeneficiario And flexBenefSinistro.Rows > 1 Then
        If flexBenefSinistro.Tag <> "" And flexBenefSinistro.Tag = flexBenefSinistro.TextMatrix(flexBenefSinistro.Row, 0) Then
            MsgBox "Os dados da l�der n�o podem ser alterados!"
            vSelBeneficiario = False
            LimpaTabBeneficiarios
            Exit Sub
        Else
            If ValidaBenef Then
                Set Benef = BeneficiariosSinistro.Item(CInt(flexBenefSinistro.TextMatrix(flexBenefSinistro.Row, 0)))
                Monta_Um_Benef Benef, "A"
                btnBenefAdicionar.Enabled = True
                btnBenefAlterar.Enabled = False
                btnBenefExcluir.Enabled = False
                
                vSelBeneficiario = False
                Monta_flexBenefSinistro
                LimpaTabBeneficiarios
                
                FlagAlteracao = True
            End If
        End If
    End If

End Sub

Private Sub btnBenefExcluir_Click()

    Dim Benef As New BenefSinistro
    Dim BenefElemento As BenefSinistro
    Dim i As Integer
    Dim vSql As String
    Dim Excluido As Integer

    i = 0
    Excluido = 0
    
    If vSelBeneficiario And flexBenefSinistro.Rows > 1 Then
        If MsgBox("Confirma a exclus�o do benefici�rio selecionado?", _
                    vbYesNo + vbQuestion + vbDefaultButton2) = vbYes Then
            For Each BenefElemento In BeneficiariosSinistro
                i = i + 1
                If BenefElemento.Seq = _
                    flexBenefSinistro.TextMatrix(flexBenefSinistro.Row, 0) And _
                    BenefElemento.Tipo <> "N" Then
                                                          
                    If BenefElemento.Tipo = "E" Then
                        With Benef
                            .Agencia = BenefElemento.Agencia
                            .Agencia_dv = BenefElemento.Agencia_dv
                            .Banco = .Banco
                            .Beneficiario_id = BenefElemento.Beneficiario_id
                            .Complemento = BenefElemento.Complemento
                            .Conta = BenefElemento.Conta
                            .Conta_dv = BenefElemento.Conta_dv
                            .dt_Nascimento = BenefElemento.dt_Nascimento
                            .Nome = BenefElemento.Nome
                            .Nome_Responsavel = BenefElemento.Nome_Responsavel
                            .Nome_Documento = BenefElemento.Nome_Documento
                            .numero = BenefElemento.numero
                            .Orgao = BenefElemento.Orgao
                            .Seq = BenefElemento.Seq
                            .Serie = BenefElemento.Serie
                            .Tipo = BenefElemento.Tipo
                            .tp_Responsavel = BenefElemento.tp_Responsavel
                            .tp_Documento = BenefElemento.tp_Documento
                            .Usuario = BenefElemento.Usuario
                        End With
                        BeneficiariosSinistroExc.Add Benef
                        Excluido = i
                    End If
                    Exit For
                ElseIf BenefElemento.Seq = _
                       flexBenefSinistro.TextMatrix(flexBenefSinistro.Row, 0) And _
                       BenefElemento.Tipo = "E" Then
                    
                    BeneficiariosSinistroExc.Add Benef
                    Excluido = i
                ElseIf BenefElemento.Seq = _
                       flexBenefSinistro.TextMatrix(flexBenefSinistro.Row, 0) And _
                       BenefElemento.Tipo = "N" Then
                       
                       Excluido = i
                End If
            Next
            BeneficiariosSinistro.Remove Excluido
            
            If vopercoseguro = "C" Then
                If Excluido = flexBenefSinistro.Tag Then
                    flexBenefSinistro.Tag = "0"
                End If
            End If
            
            For Each Benef In BeneficiariosSinistro
                If Benef.Seq >= Excluido Then
                    Benef.Seq = Benef.Seq - 1
                End If
            Next
            
            Monta_flexBenefSinistro
            FlagAlteracao = True
        End If
        
        LimpaTabBeneficiarios
        btnBenefAdicionar.Enabled = True
        vSelBeneficiario = False
        btnBenefAlterar.Enabled = False
        btnBenefExcluir.Enabled = False
    End If

End Sub

Private Sub btnCancelar_Click()

    If MsgBox("Confirma o Cancelamento da opera��o?", _
           vbYesNo + vbQuestion + vbDefaultButton2) = vbYes Then
           Set BeneficiariosSinistro = Nothing
       Me.Hide
    End If

End Sub

Private Sub btnConcluir_Click()

   'GRAVA BENFICI�RIOS Temp.
    'Call GravaBenefTemp
    Me.Hide
    'click_tela = True

End Sub

Private Sub flexBenefSinistro_Click()

     Dim Benef As BenefSinistro, i As Integer

      If flexBenefSinistro.Rows > 1 Then

        vSelBeneficiario = True
        btnBenefAdicionar.Enabled = False
        btnBenefAlterar.Enabled = True
        btnBenefExcluir.Enabled = True

        Set Benef = BeneficiariosSinistro.Item(CInt(flexBenefSinistro.TextMatrix(flexBenefSinistro.Row, 0)))

        txtNomeBeneficiario = Benef.Nome
        mskDataNascimento.Mask = ""
        mskDataNascimento = Benef.dt_Nascimento
        mskDataNascimento.Mask = "##/##/####"
        If Benef.Sexo = "Masc." Then
            chkMasculino.value = 1
            chkFeminino.value = 0
        ElseIf Benef.Sexo = "Fem." Then
            chkMasculino.value = 0
            chkFeminino.value = 1
        Else
            chkMasculino.value = 0
            chkFeminino.value = 0
        End If
        If Benef.tp_Responsavel = "" Then
            optAssistido.value = 0
            optCurador.value = 0
        Else
            optAssistido.value = IIf(Benef.tp_Responsavel = 1, 1, 0)
            optCurador.value = IIf(Benef.tp_Responsavel = 2, 1, 0)
        End If
        txtNomeAuxiliar = Benef.Nome_Responsavel
        
       
        sBancoAnt = Trim(Benef.Banco)
        
        mskBanco.Mask = "###"
        mskBanco.Mask = ""
        mskBanco = Trim(Benef.Banco)
        
        mskAgencia.Mask = "####"
        mskAgencia.Mask = ""
        mskAgencia = Trim(Benef.Agencia)
        
        mskAgenciaDv.Mask = ""
        mskAgenciaDv = Trim(Benef.Agencia_dv)
        mskAgenciaDv.Mask = "A"
        
        mskContaCorrente.Mask = "###########"
        mskContaCorrente.Mask = ""
        mskContaCorrente = Trim(Benef.Conta)
        mskContaCorrenteDv.Mask = ""
        mskContaCorrenteDv = Trim(Benef.Conta_dv)
        
        
        If mskBanco.Text = "001" Then 'Banco do Brasil
            mskContaCorrenteDv.Mask = "A"
        Else ' Outros Bancos
            mskContaCorrenteDv.Mask = "AA"
        End If
        
        If Benef.CGC <> "" Then
            mskCGC.Mask = ""
            mskCGC = Benef.CGC
            mskCGC.Mask = "##.###.###/####-##"
        ElseIf Benef.CPF <> "" Then
            mskCPF.Mask = ""
            mskCPF = Benef.CPF
            mskCPF.Mask = "###.###.###-##"
        Else
            mskCPF.Mask = ""
            mskCPF = ""
            mskCPF.Mask = "###.###.###-##"
            mskCGC.Mask = ""
            mskCGC = ""
            mskCGC.Mask = "##.###.###/####-##"
        End If
        
        If Benef.Nome_Documento <> "" And Benef.tp_Documento <> "" Then
            With cmbDocumentoBeneficiario
                For i = 0 To .ListCount - 1
                    .ListIndex = i
                    If .ItemData(.ListIndex) = Benef.tp_Documento Then
                       Exit For
                    End If
                Next
            End With
        Else
            cmbDocumentoBeneficiario.ListIndex = -1
        End If
        
        txtOrgao = Benef.Orgao
        txtNumero = Benef.numero
        txtSerie = Benef.Serie
        txtDoctoBenefComp = Benef.Complemento
    End If
  

End Sub


Private Function ValidaBenef() As Boolean

    ValidaBenef = True
    
    If Trim(txtNomeBeneficiario) = "" Then
        ValidaBenef = False
        mensagem_erro 3, "Nome Benefici�rio"
        txtNomeBeneficiario.SetFocus
        Exit Function
    End If
    
    If Trim(mskDataNascimento.ClipText) <> "" Then
        If Not VerificaData2(mskDataNascimento) Then
            ValidaBenef = False
            mensagem_erro 3, "Data Nascimento"
            mskDataNascimento.SetFocus
            Exit Function
        End If
        If DateDiff("yyyy", mskDataNascimento, Data_Sistema) < 18 And Trim(txtNomeAuxiliar) = "" Then
            If MsgBox("Menor de 18 anos sem indica��o de assist�ncia ou curador. Confirma?", _
                    vbYesNo + vbQuestion + vbDefaultButton2) = vbNo Then
                ValidaBenef = False
                If txtNomeAuxiliar.Enabled Then
                    txtNomeAuxiliar.SetFocus
                Else
                    txtNomeBeneficiario.SetFocus
                End If
                Exit Function
            End If
        End If
    Else
        'Caso n�o tenha CGC, � necess�rio cadastrar a dt nascimento
        If Trim(mskCGC.ClipText) = "" Then
            ValidaBenef = False
            mensagem_erro 3, "Data Nascimento"
            mskDataNascimento.SetFocus
            Exit Function
        Else
            mskDataNascimento.Text = "01/01/1900"
        End If
    End If
    If chkMasculino.value = 1 And chkFeminino.value = 1 Then
        ValidaBenef = False
        mensagem_erro 6, "Informe somente um sexo!"
        chkMasculino.SetFocus
        Exit Function
    End If

    If optAssistido.value = 1 And optCurador.value = 1 Then
        ValidaBenef = False
        mensagem_erro 6, "Informe somente um tipo de respons�vel, Curador ou Assistido por!"
        optAssistido.SetFocus
        Exit Function
    End If
    If Trim(txtNomeAuxiliar) <> "" And (optAssistido.value = 0 And optCurador.value = 0) Then
        ValidaBenef = False
        mensagem_erro 6, "Assistido por ou Curador deve ser assinalado!"
        txtNomeAuxiliar.SetFocus
        Exit Function
    End If
    

    If Trim(txtNomeAuxiliar) = "" And (optAssistido.value = 1 Or optCurador.value = 1) Then
        ValidaBenef = False
        mensagem_erro 6, "O nome de quem Assiste ou do Curador deve ser informado!"
        txtNomeAuxiliar.SetFocus
        Exit Function
    End If
    If (Trim(mskBanco) <> "___" And Trim(mskAgencia) = "____" And Trim(mskContaCorrente) = "") Or _
        (Trim(mskBanco) = "___" And Trim(mskAgencia) <> "____" And Trim(mskContaCorrente) = "") Or _
        (Trim(mskBanco) = "___" And Trim(mskAgencia) = "____" And (Trim(mskContaCorrente) <> "___________" And Trim(mskContaCorrente) <> "")) Then
        ValidaBenef = False
        mensagem_erro 6, "Dados Banc�rios incompletos! Informe Banco/Ag�ncia/Conta Corrente!"
        cmbDocumentoBeneficiario.SetFocus
        Exit Function
    Else
        If Right("000" & Trim(mskBanco), 3) = "001" Then
            If (mskAgenciaDv <> Modulo11(Trim(Replace(mskAgencia, "_", "")))) And (mskAgenciaDv <> "") Then
                ValidaBenef = False
                mensagem_erro 6, "D�gito verificador da ag�ncia inv�lido !"
                mskAgenciaDv.SetFocus
                Exit Function
            End If
            If (mskContaCorrenteDv <> Modulo11(Trim(Replace(mskContaCorrente, "_", "")))) And (mskContaCorrenteDv <> "") Then
                ValidaBenef = False
                mensagem_erro 6, "D�gito verificador da Conta Corrente inv�lido !"
                mskContaCorrenteDv.SetFocus
                Exit Function
            End If
        End If
    End If
    
    If Trim(mskCGC.ClipText) <> "" And Trim(mskCPF.ClipText) <> "" Then
        ValidaBenef = False
        mensagem_erro 6, "Informe CPF ou CNPJ!"
        mskCPF.SetFocus
        Exit Function
    End If

    'If Trim(mskCGC.ClipText) = "" And Trim(mskCPF.ClipText) = "" Then
    '    ValidaBenef = False
    '    mensagem_erro 6, "Informe CPF ou CNPJ!"
    '    mskCPF.SetFocus
    '    Exit Function
    'End If
    
    If Trim(mskCGC.ClipText) <> "" Then
        If Not CGC_OK(mskCGC.ClipText) Then
            ValidaBenef = False
            mensagem_erro 3, "CNPJ"
            mskCGC.SetFocus
            Exit Function
        End If
    End If
    If Trim(mskCPF.ClipText) <> "" Then
        If Not CPF_Ok(mskCPF.ClipText) Then
            ValidaBenef = False
            mensagem_erro 3, "CPF"
            mskCPF.SetFocus
            Exit Function
        End If
    End If


    If Trim(mskCGC.ClipText) <> "" And (chkMasculino.value = 1 Or chkFeminino.value = 1) Then
        ValidaBenef = False
        mensagem_erro 6, "O campo sexo n�o pode ser selecionado para pessoa jur�dica!"
        chkMasculino.SetFocus
        Exit Function
    End If
     
    If (cmbDocumentoBeneficiario.ListIndex = -1 Or cmbDocumentoBeneficiario.Text = " ") And _
        (Trim(txtOrgao) <> "" Or Trim(txtNumero) <> "" Or Trim(txtSerie) <> "" Or _
         Trim(txtDoctoBenefComp) <> "") Then
        ValidaBenef = False
        mensagem_erro 6, "Tipo de Documento n�o selecionado!"
        cmbDocumentoBeneficiario.SetFocus
        Exit Function
    End If

End Function


Public Function Modulo11(ByVal numero As String) As String

    Dim Ponteiro_Numero As Integer
    Dim Multiplicador As Integer
    Dim Somatoria As Integer
    Dim resto As Integer, Digito_Verificador As String

    numero = Right(String(50, "*") + Trim(numero), 50)
    
    Ponteiro_Numero = 50
    
    Multiplicador = 2
    
    Somatoria = 0
    
    While Ponteiro_Numero > 0 And Mid$(numero, Ponteiro_Numero, 1) <> "*"
        Somatoria = Somatoria + (Val(Mid$(numero, Ponteiro_Numero, 1)) * Multiplicador)
        
        Ponteiro_Numero = Ponteiro_Numero - 1
        
        Multiplicador = Multiplicador + 1
        If Multiplicador = 10 Then
            Multiplicador = 2
        End If
    Wend
    
    resto = Somatoria Mod 11
    
    If 11 - resto > 9 And resto <> 0 Then
        Digito_Verificador = "X"
    Else
        If resto = 0 Then
            Digito_Verificador = "0"
        Else
            Digito_Verificador = Trim(Str(11 - resto))
        End If
    End If

    Modulo11 = Digito_Verificador
    
End Function

Public Sub CarregaTipoDocumento()
    Dim SQL As String
    Dim Rs As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & " SELECT   tp_documento_id, nome"
    SQL = SQL & " FROM     tp_documento_tb with (nolock)"
    SQL = SQL & " ORDER BY  nome ASC"
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    
    With cmbDocumentoBeneficiario
        .Clear
        If Not Rs.EOF Then
           While Not Rs.EOF
                 .AddItem Trim(Rs(1))
                 .ItemData(.NewIndex) = Rs(0)
                 Rs.MoveNext
           Wend
        End If
        Rs.Close
    End With

End Sub

Private Sub Form_Load()
    Call CarregaTipoDocumento
End Sub

Public Function Busca_Numero_Sinistro(ByRef sin As Sinist) As Boolean
    Dim rc As ADODB.Recordset
    Dim vSql As String

    Busca_Numero_Sinistro = False
    On Error GoTo Trata_Erro

    vSql = "SET nocount on "
    vSql = vSql & "EXEC seguros_db..SEGS8151_SPS '" & Format(Trim(Str(sin.ramo_id)), "00") & "'," & _
                        sin.Sucursal_id & "," & sin.Seguradora_id & ",'" & cUserName & "'"

    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)

    If Not rc.EOF Then sin.sinistro_id = rc(0)
    rc.Close
    Busca_Numero_Sinistro = True
    Exit Function

Trata_Erro:
    TrataErroGeral "Busca N�mero Sinistro"

End Function
