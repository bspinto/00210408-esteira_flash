VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmEquipamentos 
   Caption         =   "Equipamentos"
   ClientHeight    =   9030
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13635
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9030
   ScaleWidth      =   13635
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frameGrid 
      Height          =   3735
      Left            =   0
      TabIndex        =   26
      Top             =   4680
      Width           =   13575
      Begin VB.CommandButton btnRemoveGrid 
         Caption         =   "Remover"
         Height          =   375
         Left            =   240
         TabIndex        =   27
         Top             =   3240
         Width           =   1815
      End
      Begin MSFlexGridLib.MSFlexGrid grdEquipamentos 
         Height          =   3105
         Left            =   0
         TabIndex        =   30
         Top             =   0
         Width           =   13575
         _ExtentX        =   23945
         _ExtentY        =   5477
         _Version        =   393216
         Rows            =   1
         Cols            =   10
         Appearance      =   0
         FormatString    =   "                                                "
      End
      Begin VB.Label lblTotEquipamento 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Total de Equipamentos: 0"
         Height          =   195
         Left            =   11400
         TabIndex        =   28
         Top             =   3240
         Width           =   1815
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Descri��o do Dano"
      Height          =   2010
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   13650
      Begin VB.TextBox txtDescricao 
         Height          =   1575
         Left            =   120
         MaxLength       =   250
         TabIndex        =   0
         Top             =   240
         Width           =   13335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Equipamentos Danificados"
      Height          =   2685
      Left            =   0
      TabIndex        =   20
      Top             =   2040
      Width           =   13620
      Begin VB.Frame grpTipoDeDano 
         Caption         =   "Tipo de Dano"
         Height          =   1575
         Left            =   11280
         TabIndex        =   3
         Top             =   240
         Width           =   2175
         Begin VB.OptionButton optDanoReparo 
            Caption         =   "Pass�vel de reparo"
            Height          =   255
            Left            =   240
            TabIndex        =   4
            Top             =   480
            Value           =   -1  'True
            Width           =   1815
         End
         Begin VB.OptionButton optDanoPerdaTotal 
            Caption         =   "Perda Total"
            Height          =   255
            Left            =   240
            TabIndex        =   5
            Top             =   960
            Width           =   1215
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "J� Realizou Laudo?"
         Height          =   735
         Left            =   240
         TabIndex        =   6
         Top             =   960
         Width           =   1695
         Begin VB.OptionButton optLaudoNao 
            Caption         =   "N�o"
            Height          =   255
            Left            =   840
            TabIndex        =   8
            Top             =   360
            Value           =   -1  'True
            Width           =   735
         End
         Begin VB.OptionButton optLaudoSim 
            Caption         =   "Sim"
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   360
            Width           =   615
         End
      End
      Begin VB.TextBox txtValor 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2040
         MaxLength       =   12
         TabIndex        =   15
         Text            =   "0,00"
         Top             =   2160
         Width           =   2175
      End
      Begin VB.Frame Frame3 
         Caption         =   "Tem Or�amento?"
         Height          =   735
         Left            =   240
         TabIndex        =   12
         Top             =   1800
         Width           =   1695
         Begin VB.OptionButton optOrcamentoNao 
            Caption         =   "N�o"
            Height          =   255
            Left            =   840
            TabIndex        =   14
            Top             =   360
            Value           =   -1  'True
            Width           =   615
         End
         Begin VB.OptionButton optOrcamentoSim 
            Caption         =   "Sim"
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   360
            Width           =   1095
         End
      End
      Begin VB.TextBox txtNomeAssistencia 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         MaxLength       =   60
         TabIndex        =   9
         Top             =   1320
         Width           =   5205
      End
      Begin VB.ComboBox cboModelo 
         Height          =   315
         Left            =   5760
         TabIndex        =   2
         Text            =   "cboModelo"
         Top             =   600
         Width           =   5445
      End
      Begin VB.ComboBox cboTipoBem 
         Height          =   315
         Left            =   240
         TabIndex        =   1
         Text            =   "cboTipoBem"
         Top             =   600
         Width           =   5445
      End
      Begin VB.CommandButton btnAddEquipamento 
         Caption         =   "Adiconar"
         Height          =   375
         Left            =   11280
         TabIndex        =   16
         Top             =   2040
         Width           =   2175
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   1
         Left            =   7440
         TabIndex        =   10
         Top             =   1320
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   1
         Left            =   8160
         TabIndex        =   11
         Top             =   1320
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin VB.Label lblEstimativaPrejuizo 
         Caption         =   "Estimativa do Preju�zo:"
         Height          =   255
         Left            =   2040
         TabIndex        =   29
         Top             =   1920
         Width           =   2055
      End
      Begin VB.Label lblTelefoneAssistencia 
         Caption         =   "Telefone da Assist�ncia:"
         Height          =   255
         Left            =   7440
         TabIndex        =   25
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label lblNomeAssistencia 
         Caption         =   "Nome da Assist�ncia:"
         Height          =   255
         Left            =   2040
         TabIndex        =   24
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Modelo"
         Height          =   195
         Index           =   0
         Left            =   5760
         TabIndex        =   23
         Top             =   360
         Width           =   525
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Bem"
         Height          =   195
         Left            =   240
         TabIndex        =   21
         Top             =   360
         Width           =   900
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   11760
      TabIndex        =   19
      Top             =   8520
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   10320
      TabIndex        =   18
      Top             =   8520
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   8880
      TabIndex        =   17
      Top             =   8520
      Width           =   1275
   End
End
Attribute VB_Name = "frmEquipamentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'C00210408 - Esteira Flash - Sinistros de Danos El�tricos Residenciais

Public grdEquipamentoID As Integer

Private Sub btnRemoveGrid_Click()
Dim Aa As String


    If grdEquipamentos.Rows > 2 Then
  
        Aa = MsgBox("Confirma a exclusao da linha " + Str$(grdEquipamentos.RowSel), vbYesNo, "Question")
    
        If Aa = vbYes Then
            grdEquipamentos.RemoveItem (grdEquipamentos.RowSel)
    
        End If
        
    Else
        grdEquipamentos.Rows = 1
    End If
    
    lblTotEquipamento.Caption = "Total de Equipamentos: " & grdEquipamentos.Rows - 1

End Sub

Private Sub cboTipoBem_Validate(Cancel As Boolean)
    If cboTipoBem.ListIndex <> -1 Then
       CarregaCboModelo (cboTipoBem.Text)
    End If
End Sub

Private Sub Form_Load()
    optDanoPerdaTotal.Caption = cCapPerdaTotal
    optDanoReparo.Caption = cCapReparo
    optLaudoSim.Caption = cCapSim
    optLaudoNao.Caption = cCapNao
    optOrcamentoSim.Caption = cCapSim
    optOrcamentoNao.Caption = cCapNao

    LimparCamposEquipamentos
    CarregarCboTipoBem
    cboModelo.Clear
    MontaCabecalhoGrdEquipamentos
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Private Sub cmdVoltar_Click()
    limparGridFrmAviso
    Me.Hide
    frmObjSegurado.Show
End Sub

Private Sub CopiaGrid()
        
   limparGridFrmAviso
            
    With frmAviso
        .grdEquipamentos.Clear
        
        .grdEquipamentos.Row = 0
        .grdEquipamentos.Col = 0
        .grdEquipamentos.Text = "C�digo"
        .grdEquipamentos.Col = 1
        .grdEquipamentos.Text = "Tipo de Bem"
        .grdEquipamentos.Col = 2
        .grdEquipamentos.Text = "Modelo"
        .grdEquipamentos.Col = 3
        .grdEquipamentos.Text = "Dano"
        .grdEquipamentos.Col = 4
        .grdEquipamentos.Text = "Or�amento"
        .grdEquipamentos.Col = 5
        .grdEquipamentos.Text = "Estimativa do prejuizo"
        .grdEquipamentos.Col = 6
        .grdEquipamentos.Text = "J� realizou laudo"
        .grdEquipamentos.Col = 7
        .grdEquipamentos.Text = "Nome Assist�ncia"
        .grdEquipamentos.Col = 8
        .grdEquipamentos.Text = "Tel. Assist�ncia"
        .grdEquipamentos.Col = 9
        .grdEquipamentos.Text = "Descricao"
            
        .grdEquipamentos.ColWidth(0) = 1000  'C�digo
        .grdEquipamentos.ColWidth(1) = 2750  'Tipo de bem
        .grdEquipamentos.ColWidth(2) = 2750 'Modelo
        .grdEquipamentos.ColWidth(3) = 1500 'Dano
        .grdEquipamentos.ColWidth(4) = 1500 'Orcamento
        .grdEquipamentos.ColWidth(5) = 1800 'Estimativa do prejuizo
        .grdEquipamentos.ColWidth(6) = 1500 'J� realizou laudo
        .grdEquipamentos.ColWidth(7) = 3500 'Nome Assist�ncia
        .grdEquipamentos.ColWidth(8) = 1500 'Tel. Assist�ncia
        .grdEquipamentos.ColWidth(9) = 8000 'Descricao
                           
        grdEquipamentoID = 0
    End With

    For i = 1 To grdEquipamentos.Rows - 1
        sCodigo = grdEquipamentos.TextMatrix(i, 0)
        sTipoBem = grdEquipamentos.TextMatrix(i, 1)
        sModelo = grdEquipamentos.TextMatrix(i, 2)
        sDano = grdEquipamentos.TextMatrix(i, 3)
        sOrcamento = grdEquipamentos.TextMatrix(i, 4)
        sValor = grdEquipamentos.TextMatrix(i, 5)
        sLaudo = grdEquipamentos.TextMatrix(i, 6)
        sNome = grdEquipamentos.TextMatrix(i, 7)
        sTelefone = grdEquipamentos.TextMatrix(i, 8)
        sDescricao = grdEquipamentos.TextMatrix(i, 9)
                                
         frmAviso.grdEquipamentos.AddItem sCodigo + Chr$(9) + _
        sTipoBem + Chr$(9) + _
        sModelo + Chr$(9) + _
        sDano + Chr$(9) + _
        sOrcamento + Chr$(9) + _
        sValor + Chr$(9) + _
        sLaudo + Chr$(9) + _
        sNome + Chr$(9) + _
        sTelefone + Chr$(9) + _
        sDescricao + Chr$(9), 1
    Next i
End Sub

Private Sub cmdContinuar_Click()
    Dim iRet As Integer
        
    If grdEquipamentos.Rows > 1 Then
        CopiaGrid
        Me.Hide
        frmOutrosSeguros.Show
    Else
        iRet = MsgBox("� obrigat�rio adicinar 1 equipamento a Grid.", vbOKOnly, "Preenchimento Obrigat�rio")
    End If
End Sub

Public Sub CarregarCboTipoBem()
 Dim SQL As String
    Dim Rs As ADODB.Recordset
    
        SQL = ""
        SQL = SQL & "exec seguros_db.dbo.SEGS14614_SPS NULL"

        Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

        With cboTipoBem
            .Clear
            If Not Rs.EOF Then
               While Not Rs.EOF
                     .AddItem Trim(Rs(0))
                     '.ItemData(.NewIndex) = Rs(1)
                     Rs.MoveNext
               Wend
            End If
            Rs.Close
        End With
        
        cboModelo.Clear
        cboModelo.ListIndex = -1
End Sub

Public Sub MontaCabecalhoGrdEquipamentos()
    grdEquipamentos.Row = 0
    grdEquipamentos.Col = 0
    grdEquipamentos.Text = "C�digo"
    grdEquipamentos.Col = 1
    grdEquipamentos.Text = "Tipo de Bem"
    grdEquipamentos.Col = 2
    grdEquipamentos.Text = "Modelo"
    grdEquipamentos.Col = 3
    grdEquipamentos.Text = "Dano"
    grdEquipamentos.Col = 4
    grdEquipamentos.Text = "Or�amento"
    grdEquipamentos.Col = 5
    grdEquipamentos.Text = "Estimativa do prejuizo"
    grdEquipamentos.Col = 6
    grdEquipamentos.Text = "J� realizou laudo"
    grdEquipamentos.Col = 7
    grdEquipamentos.Text = "Nome Assist�ncia"
    grdEquipamentos.Col = 8
    grdEquipamentos.Text = "Tel. Assist�ncia"
    grdEquipamentos.Col = 9
    grdEquipamentos.Text = "Descricao"
        
    grdEquipamentos.ColWidth(0) = 1000  'C�digo
    grdEquipamentos.ColWidth(1) = 2750  'Tipo de bem
    grdEquipamentos.ColWidth(2) = 2750 'Modelo
    grdEquipamentos.ColWidth(3) = 1500 'Dano
    grdEquipamentos.ColWidth(4) = 1500 'Orcamento
    grdEquipamentos.ColWidth(5) = 1800 'Estimativa do prejuizo
    grdEquipamentos.ColWidth(6) = 1500 'J� realizou laudo
    grdEquipamentos.ColWidth(7) = 3500 'Nome Assist�ncia
    grdEquipamentos.ColWidth(8) = 1500 'Tel. Assist�ncia
    grdEquipamentos.ColWidth(9) = 8000 'Descricao
                       
    grdEquipamentoID = 0
    
End Sub



Private Sub btnAddEquipamento_Click()
   Dim sDano As String
   Dim sLaudo As String
   Dim sOrcamento As String
   Dim sNomeAssistencia As String
   Dim sTelAssistencia As String
   Dim sCodigo As String
   Dim sModelo As String

   If optDanoReparo.value = True Then
    sDano = optDanoReparo.Caption
   Else
    sDano = optDanoPerdaTotal.Caption
   End If
   
   If optLaudoSim.value = True Then
    sLaudo = optLaudoSim.Caption
   Else
    sLaudo = optLaudoNao.Caption
   End If
   
   If optOrcamentoSim.value = True Then
    sOrcamento = optOrcamentoSim.Caption
   Else
    sOrcamento = optOrcamentoNao.Caption
   End If
      
   'VALIDAR SE OS DADOS FORAM PREENCHIDOS ANTES DE INSERIR
   If FomularioValido Then
        sCodigo = Left(cboModelo.Text, 9)
        sModelo = Mid(cboModelo.Text, 13, Len(cboModelo.Text))
        
        grdEquipamentos.AddItem sCodigo + Chr$(9) + _
        cboTipoBem + Chr$(9) + _
        sModelo + Chr$(9) + _
        sDano + Chr$(9) + _
        sOrcamento + Chr$(9) + _
        txtValor.Text + Chr$(9) + _
        sLaudo + Chr$(9) + _
        txtNomeAssistencia.Text + Chr$(9) + _
        "(" & mskDDD(1).Text & ") " & mskTelefone(1).Text + Chr$(9) + _
        txtDescricao.Text + Chr$(9), 1
        
        'AP�S INSERIR LIMPAR OS CAMPOS DO FORMUL�RIO
        LimparCamposEquipamentos
        lblTotEquipamento.Caption = "Total de Equipamentos: " & grdEquipamentos.Rows - 1
        txtDescricao.SetFocus
    End If
End Sub

Public Function FomularioValido() As Boolean
    Dim iRet As Integer
    iRet = 0
'    If Trim(txtDescricao.Text) = "" Then
'       txtDescricao.SetFocus
'       iRet = MsgBox("� obrigat�rio preencher o campo: Descri��o.", vbOKOnly, "Preenchimento Obrigat�rio")
'    End If
    If Trim(cboTipoBem.Text) = "" Then
       cboTipoBem.SetFocus
       iRet = MsgBox("� obrigatorio selecionar um item para o campo: Tipo de Bem .", vbOKOnly, "Preenchimento Obrigat�rio")
    ElseIf Trim(cboModelo.Text) = "" Then
       cboModelo.SetFocus
       iRet = MsgBox("� obrigatorio selecionar um item para o campo: Modelo .", vbOKOnly, "Preenchimento Obrigat�rio")
    ElseIf optLaudoSim.value Then
        If Trim(txtNomeAssistencia.Text) = "" Then
            txtNomeAssistencia.SetFocus
            iRet = MsgBox("� obrigat�rio informar um valor para o campo: Nome da Assist�ncia.", vbOKOnly, "Preenchimento Obrigat�rio")
        ElseIf Trim(mskDDD(1).Text) = "__" Then
            mskDDD(1).SetFocus
            iRet = MsgBox("� obrigat�rio informar um numero para o campo: Telefone da Assist�ncia.", vbOKOnly, "Preenchimento Obrigat�rio")
        ElseIf Trim(mskTelefone(1).Text) = "_____-____" Then
            mskTelefone(1).SetFocus
            iRet = MsgBox("� obrigat�rio informar um numero para o campo: Telefone da Assist�ncia.", vbOKOnly, "Preenchimento Obrigat�rio")
        End If
    End If
'    If optOrcamentoSim.value Then
'        If Format(txtValor.Text, "0.00") = Format(0, "0.00") Then
'            txtValor.SetFocus
'            iRet = MsgBox("� obrigat�rio informar um valor para o campo: Estimativa do Preju�zo", vbOKOnly, "Preenchimento Obrigat�rio")
'        End If
'    End If
    FomularioValido = (iRet = 0)
End Function

Public Sub CarregaCboModelo(sTipoBem As String)
    Dim SQL As String
    Dim Rs As ADODB.Recordset
        
        cboModelo.Clear
        cboModelo.ListIndex = -1
        
        SQL = ""
        SQL = SQL & "exec seguros_db.dbo.SEGS14614_SPS '" & sTipoBem & "'"

        Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

        With cboModelo
            .Clear
            If Not Rs.EOF Then
               While Not Rs.EOF
                     .AddItem Trim(Rs(0))
                     '.ItemData(.NewIndex) = Rs(0)
                     Rs.MoveNext
               Wend
            End If
            Rs.Close
        End With
End Sub


Public Sub LimparCamposEquipamentos()
        cboTipoBem.ListIndex = -1
        cboModelo.Clear
        txtDescricao.Text = ""
        optDanoReparo.value = True
        optLaudoNao.value = True
        optOrcamentoNao.value = True
        optLaudo (False)
        optOrcamento (False)
        txtValor.Text = Format("0", "0.00")
End Sub

Private Sub optLaudo(bHabilita As Boolean)
        lblNomeAssistencia.Enabled = bHabilita
        txtNomeAssistencia.Enabled = bHabilita
        lblTelefoneAssistencia.Enabled = bHabilita
        mskDDD(1).Enabled = bHabilita
        mskTelefone(1).Enabled = bHabilita
        If Not Habilita Then
            txtNomeAssistencia.Text = ""
            mskDDD(1).Text = "__"
            mskTelefone(1).Text = "_____-____"
        End If
End Sub

Private Sub mskDDD_GotFocus(Index As Integer)
  mskDDD(1).SelStart = 0
  mskDDD(1).SelLength = Len(mskDDD(1).Text)
End Sub

Private Sub mskTelefone_GotFocus(Index As Integer)
  mskTelefone(1).SelStart = 0
  mskTelefone(1).SelLength = Len(mskTelefone(1).Text)
End Sub

Private Sub optLaudoNao_Click()
        optLaudo (Not optLaudoNao.value)
End Sub

Private Sub optLaudoSim_Click()
        optLaudo (optLaudoSim.value)
        txtNomeAssistencia.SetFocus
End Sub

Private Sub optOrcamentoNao_Click()
    optOrcamento (Not optOrcamentoNao.value)
End Sub

Private Sub optOrcamentoSim_Click()
   optOrcamento (optOrcamentoSim.value)
   txtValor.SetFocus
End Sub

Private Sub optOrcamento(bHabilita As Boolean)
'  txtValor.Enabled = bHabilita
'  lblEstimativaPrejuizo.Enabled = bHabilita
'  If Not bHabilita Then
'    txtValor.Text = Format("0", "0.00")
'  End If
End Sub

Private Sub txtValor_GotFocus()
  txtValor.SelStart = 0
  txtValor.SelLength = Len(txtValor.Text)
End Sub

Private Sub txtValor_KeyPress(KeyAscii As Integer)
    Dim digitos As String
    digitos = "0123456789," & Chr(vbKeyBack)
    Caracter = Chr(KeyAscii)
    If InStr(1, digitos, Caracter) <= 0 Then
        KeyAscii = 0
    Else
        If InStr(1, ",", Caracter) > 0 Then
            If InStr(1, txtValor.Text, Caracter) > 0 Then
                KeyAscii = 0
            End If
        End If
    End If
End Sub

Private Sub txtValor_LostFocus()
    txtValor.Text = Format(txtValor.Text, "0.00")
End Sub

Public Sub limparGridFrmAviso()
Dim iQtde As Integer

    iQtde = frmAviso.grdEquipamentos.Rows
    If iQtde > 2 Then
       For i = 1 To iQtde - 1
            If frmAviso.grdEquipamentos.Rows > 2 Then
               frmAviso.grdEquipamentos.RemoveItem (i)
            Else
               frmAviso.grdEquipamentos.Rows = 1
            End If
       Next i
    Else
        frmAviso.grdEquipamentos.Rows = 1
    End If
End Sub
