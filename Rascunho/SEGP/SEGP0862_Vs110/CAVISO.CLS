VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cAviso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private proposta_id       As String
Private num_proposta_bb   As String
Private produto_id        As Integer
Private ramo_id           As Integer
Private apolice_id        As String
Private evento_sinistro   As Integer
Private val_prejuizo      As Double
Private nome_prod         As String
Private subramo_id        As Integer
Private situacao_prop     As String
Private cod_cobertura     As Integer
Private cod_obj_segurado  As Integer
Private descr_cobertura   As String
Private valor_estimativa  As Double
Private processa_reint_is As String
Public Property Let Proposta(ByVal vData As String)
    proposta_id = vData
End Property
Public Property Get Proposta() As String
    Proposta = proposta_id
End Property
Public Property Let Produto(ByVal vData As Integer)
    produto_id = vData
End Property
Public Property Get Produto() As Integer
    Produto = produto_id
End Property
Public Property Let Ramo(ByVal vData As Integer)
    ramo_id = vData
End Property
Public Property Get Ramo() As Integer
    Ramo = ramo_id
End Property
Public Property Let proposta_bb(ByVal vData As String)
    num_proposta_bb = vData
End Property
Public Property Get proposta_bb() As String
    proposta_bb = num_proposta_bb
End Property
Public Property Let apolice(ByVal vData As String)
    apolice_id = vData
End Property
Public Property Get apolice() As String
    apolice = apolice_id
End Property
Public Property Let evento(ByVal vData As Integer)
    evento_sinistro = vData
End Property
Public Property Get evento() As Integer
    evento = evento_sinistro
End Property
Public Property Let valor_prejuizo(ByVal vData As Double)
    val_prejuizo = vData
End Property
Public Property Get valor_prejuizo() As Double
    valor_prejuizo = val_prejuizo
End Property
Public Property Let nome_produto(ByVal vData As String)
    nome_prod = vData
End Property
Public Property Get nome_produto() As String
    nome_produto = nome_prod
End Property
Public Property Let SubRamo(ByVal vData As Integer)
    subramo_id = vData
End Property
Public Property Get SubRamo() As Integer
    SubRamo = subramo_id
End Property
Public Property Let SituacaoProposta(ByVal vData As String)
    situacao_prop = vData
End Property
Public Property Get SituacaoProposta() As String
    SituacaoProposta = situacao_prop
End Property
Public Property Let Cobertura(ByVal vData As Integer)
    cod_cobertura = vData
End Property
Public Property Get Cobertura() As Integer
    Cobertura = cod_cobertura
End Property
Public Property Let ObjetoSegurado(ByVal vData As Integer)
    cod_obj_segurado = vData
End Property
Public Property Get ObjetoSegurado() As Integer
    ObjetoSegurado = cod_obj_segurado
End Property
Public Property Let DescricaoCobertura(ByVal vData As String)
    descr_cobertura = vData
End Property
Public Property Get DescricaoCobertura() As String
    DescricaoCobertura = descr_cobertura
End Property
Public Property Let ValorEstimativa(ByVal vData As Double)
    valor_estimativa = vData
End Property
Public Property Get ValorEstimativa() As Double
    ValorEstimativa = valor_estimativa
End Property
Public Property Let ProcessaReintegracaoIs(ByVal vData As String)
    processa_reint_is = vData
End Property
Public Property Get ProcessaReintegracaoIs() As String
    ProcessaReintegracaoIs = processa_reint_is
End Property
