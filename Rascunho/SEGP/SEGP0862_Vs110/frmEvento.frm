VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmEvento 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dados da Ocorr�ncia"
   ClientHeight    =   8550
   ClientLeft      =   8610
   ClientTop       =   915
   ClientWidth     =   8985
   ClipControls    =   0   'False
   Icon            =   "frmEvento.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8550
   ScaleMode       =   0  'User
   ScaleWidth      =   8985
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   6300
      TabIndex        =   34
      Top             =   8040
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   7695
      TabIndex        =   35
      Top             =   8025
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   4890
      TabIndex        =   33
      Top             =   8025
      Width           =   1275
   End
   Begin VB.Frame Frame3 
      Caption         =   "Dados da Vistoria"
      Height          =   3015
      Left            =   0
      TabIndex        =   21
      Top             =   4920
      Width           =   8970
      Begin VB.CommandButton cmdPesquisarCEP 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   380
         Left            =   8400
         TabIndex        =   49
         Top             =   1800
         Width           =   440
      End
      Begin VB.ComboBox cmbMunicipio 
         Height          =   315
         Left            =   1200
         TabIndex        =   25
         Text            =   "cmbMunicipio"
         Top             =   1875
         Width           =   5895
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Height          =   315
         Index           =   1
         ItemData        =   "frmEvento.frx":0442
         Left            =   6840
         List            =   "frmEvento.frx":0455
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   2520
         Width           =   2055
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Height          =   315
         Index           =   0
         ItemData        =   "frmEvento.frx":0487
         Left            =   2160
         List            =   "frmEvento.frx":049A
         Style           =   2  'Dropdown List
         TabIndex        =   29
         Top             =   2520
         Width           =   2055
      End
      Begin VB.TextBox txtContato 
         Height          =   330
         Left            =   120
         MaxLength       =   60
         TabIndex        =   6
         Top             =   615
         Width           =   8805
      End
      Begin VB.TextBox txtLocalRisco 
         Height          =   330
         Left            =   120
         MaxLength       =   60
         TabIndex        =   22
         Top             =   1200
         Width           =   5175
      End
      Begin VB.TextBox txtBairro 
         Height          =   330
         Left            =   5400
         MaxLength       =   30
         TabIndex        =   23
         Top             =   1200
         Width           =   3540
      End
      Begin VB.ComboBox cmbUF 
         Height          =   315
         Left            =   120
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   1880
         Width           =   945
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   0
         Left            =   135
         TabIndex        =   27
         Top             =   2505
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   0
         Left            =   720
         TabIndex        =   28
         Top             =   2520
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   1
         Left            =   4815
         TabIndex        =   30
         Top             =   2505
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   1
         Left            =   5400
         TabIndex        =   31
         Top             =   2520
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtCEP 
         Height          =   285
         Left            =   7200
         TabIndex        =   26
         Top             =   1875
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   9
         Mask            =   "#####-###"
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "D.D.D.:"
         Height          =   195
         Index           =   3
         Left            =   4800
         TabIndex        =   48
         Top             =   2280
         Width           =   555
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Telefone:"
         Height          =   195
         Index           =   2
         Left            =   5445
         TabIndex        =   47
         Top             =   2280
         Width           =   675
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Telefone:"
         Height          =   195
         Left            =   6840
         TabIndex        =   46
         Top             =   2280
         Width           =   1035
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Telefone:"
         Height          =   195
         Left            =   2160
         TabIndex        =   44
         Top             =   2280
         Width           =   1035
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Telefone:"
         Height          =   195
         Index           =   1
         Left            =   765
         TabIndex        =   43
         Top             =   2280
         Width           =   675
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "D.D.D.:"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   42
         Top             =   2280
         Width           =   555
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nome de Contato:"
         Height          =   195
         Left            =   120
         TabIndex        =   41
         Top             =   360
         Width           =   1290
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         Height          =   195
         Left            =   120
         TabIndex        =   40
         Top             =   980
         Width           =   735
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         Height          =   195
         Left            =   5400
         TabIndex        =   39
         Top             =   980
         Width           =   450
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio:"
         Height          =   195
         Left            =   1215
         TabIndex        =   38
         Top             =   1620
         Width           =   750
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "U.F.:"
         Height          =   195
         Left            =   135
         TabIndex        =   37
         Top             =   1620
         Width           =   345
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "CEP:"
         Height          =   195
         Left            =   7200
         TabIndex        =   36
         Top             =   1620
         Width           =   360
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Descri��o do Evento"
      Height          =   3330
      Left            =   0
      TabIndex        =   5
      Top             =   1560
      Width           =   8970
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   90
         MaxLength       =   70
         TabIndex        =   11
         Top             =   360
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   90
         MaxLength       =   70
         TabIndex        =   12
         Top             =   660
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   90
         MaxLength       =   70
         TabIndex        =   13
         Top             =   915
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   90
         MaxLength       =   70
         TabIndex        =   14
         Top             =   1200
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   90
         MaxLength       =   70
         TabIndex        =   15
         Top             =   1470
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   90
         MaxLength       =   70
         TabIndex        =   16
         Top             =   1740
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   90
         MaxLength       =   70
         TabIndex        =   17
         Top             =   2010
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         Left            =   90
         MaxLength       =   70
         TabIndex        =   18
         Top             =   2280
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   90
         MaxLength       =   70
         TabIndex        =   19
         Top             =   2580
         Width           =   8805
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   90
         MaxLength       =   70
         TabIndex        =   20
         Top             =   2880
         Width           =   8805
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ocorr�ncia"
      Height          =   1515
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   8970
      Begin VB.ComboBox cboSubEvento 
         Height          =   315
         Left            =   150
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1110
         Width           =   6255
      End
      Begin VB.ComboBox cboEvento 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   120
         TabIndex        =   0
         Text            =   "cboEvento"
         Top             =   480
         Width           =   6255
      End
      Begin MSMask.MaskEdBox mskDataOcorrencia 
         Height          =   330
         Left            =   6450
         TabIndex        =   1
         Top             =   465
         Width           =   1185
         _ExtentX        =   2090
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskHora 
         Height          =   330
         Left            =   6450
         TabIndex        =   4
         Top             =   1095
         Width           =   660
         _ExtentX        =   1164
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   5
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDataOcorrenciaFim 
         Height          =   330
         Left            =   7665
         TabIndex        =   2
         Top             =   465
         Width           =   1185
         _ExtentX        =   2090
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Sub Evento"
         Height          =   195
         Left            =   150
         TabIndex        =   45
         Top             =   840
         Width           =   840
      End
      Begin VB.Label lblHora 
         AutoSize        =   -1  'True
         Caption         =   "Hora"
         Height          =   195
         Left            =   6450
         TabIndex        =   10
         Top             =   840
         Width           =   345
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Ocorr�ncia"
         Height          =   195
         Index           =   0
         Left            =   6450
         TabIndex        =   9
         Top             =   240
         Width           =   780
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Evento Causador da Ocorr�ncia"
         Height          =   195
         Index           =   0
         Left            =   150
         TabIndex        =   8
         Top             =   240
         Width           =   2280
      End
   End
End
Attribute VB_Name = "frmEvento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const EVENTO_SINISTRO_PONTUAL = 1
Const EVENTO_SINISTRO_PERIODICO = 2
Public iEvento As Integer
'AFONSO FILHO - GPTI
'DEMANDA: 554094 - DATA: 05/11/2008
Public sProduto As Integer
Public DtaServidor As Date 'DEMANDA 12554726 -  Paulo Fantin

Private Sub cboEvento_Click()

Dim SQL As String
Dim Rs As ADODB.Recordset
Dim oAviso As Object

   If cboEvento.ListIndex <> -1 Then
   
        'Obtem o tipo de evento: peri�dico ou pontual.
        Set oAviso = CreateObject("SEGL0144.cls00175")
        oAviso.mvarSiglaSistema = "SEGBR"
        oAviso.mvarSiglaRecurso = App.Title
        oAviso.mvarDescricaoRecurso = App.FileDescription
        oAviso.mvarAmbienteId = glAmbiente_id
        oAviso.mvarAmbiente = ConvAmbiente(glAmbiente_id)
        
        iEventoId = cboEvento.ItemData(cboEvento.ListIndex)
                
                EventoId = cboEvento.ItemData(cboEvento.ListIndex)      'Demanda 18723625
        
        'A pedido do Marcello Iannetta - Demanda 11113817
        'If oAviso.ObterTpEventoSinistro(cboEvento.ItemData(cboEvento.ListIndex)) = EVENTO_SINISTRO_PERIODICO Then
            'lblPeriodico.Visible = True
        '    mskDataOcorrenciaFim.Visible = True
           ' mskHora.Left = 9360
            'lblHora.Left = 9360
        'Else
            'lblPeriodico.Visible = False
            mskDataOcorrenciaFim.Visible = False
            mskDataOcorrenciaFim.Text = "__/__/____"
            'mskHora.Left = 8040
            'lblHora.Left = 8040
        'End If
        
        SQL = ""
        SQL = SQL & " SELECT    a.subevento_sinistro_id, a.nome"
        SQL = SQL & " FROM      subevento_sinistro_tb a with (nolock),"
        SQL = SQL & "           evento_subevento_sinistro_tb b with (nolock)"
        SQL = SQL & " WHERE     b.evento_sinistro_id  = " & cboEvento.ItemData(cboEvento.ListIndex)
        SQL = SQL & " and       b.subevento_sinistro_id = a.subevento_sinistro_id "
        SQL = SQL & " ORDER BY  a.nome"
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQL, True)
        
        With cboSubEvento
        .Clear
        
        If Not Rs.EOF Then
           cboSubEvento.Enabled = True
           While Not Rs.EOF
                 .AddItem UCase(Rs("nome"))
                 .ItemData(.NewIndex) = Rs("subevento_sinistro_id")
                 Rs.MoveNext
           Wend
        Else
           cboSubEvento.ListIndex = -1
           cboSubEvento.Enabled = False
        End If
        
        Rs.Close
      
    End With
    
    '' Laurent Silva - Demanda de faturamento pecu�rio
    ''ExibeEscondeFrameAnimaisMortos (CInt(cboEvento.ItemData(cboEvento.ListIndex)))
    ''ExibeEscondeFrameAnimaisMortos (iEventoId)
    
  End If

End Sub

'' Laurent Silva
'Private Sub ExibeEscondeFrameAnimaisMortos(evento As Integer)
'
'    If ExisteEventoAgropecuarioBasico(evento) Then '' Se for algum dos eventos da cob b�sica da demanda, exibo o frame de bois mortos
'
'        Frame4.Visible = True
'        Frame2.Height = 2490
'        Frame2.Top = 2400
'        Evento_Cobertura_Basica_Faturamento_Pecuario = True
'    Else
'        Frame4.Visible = False
'        Frame2.Height = 3330
'        Frame2.Top = 1560
'        Evento_Cobertura_Basica_Faturamento_Pecuario = False
'    End If
'
'End Sub
'' Wilder
'Function ExisteEventoAgropecuarioBasico(evento As Integer) As Boolean
'
'    SQL = " select distinct evento_sinistro_id"
'    SQL = SQL & " from seguros_db..produto_estimativa_sinistro_tb"
'    SQL = SQL & " Where produto_id = 1240"
'    SQL = SQL & " and tp_cobertura_id = 1169"
'    SQL = SQL & " and evento_sinistro_id = " & evento
'
'    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
'                                glAmbiente_id, _
'                                App.Title, _
'                                App.FileDescription, _
'                                SQL, True)
'
'    If Not Rs.EOF Then
'        ExisteEventoAgropecuarioBasico = True
'    Else
'        ExisteEventoAgropecuarioBasico = False
'    End If
'    Rs.Close
'
'End Function

Private Sub cmbMunicipio_Click()
'    If cmbMunicipio.ListIndex <> -1 Then                               'AKIO.OKUNO - 17860335 - 10/06/2013
    If cmbMunicipio.ListIndex <> -1 And cmbUF.ListIndex = -1 Then       'AKIO.OKUNO - 17860335 - 10/06/2013
        Me.cmbUF.Text = obtemEstado(cmbMunicipio.ItemData(cmbMunicipio.ListIndex), cmbMunicipio.Text)
    End If
End Sub

'AKIO.OKUNO - cmbUF_Click - 17860335 - 10/06/2013
Private Sub cmbUF_Click()
    Dim SQL As String
    Dim Rs As ADODB.Recordset
    
    If cmbUF.ListIndex <> -1 Then
        SQL = ""
        SQL = SQL & " select   municipio_id, estado, nome"
        SQL = SQL & " from     municipio_tb with (nolock)"
        SQL = SQL & " wHERE    Estado = '" & cmbUF & "'"
        SQL = SQL & " order by  nome"
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        
        With cmbMunicipio
            .Clear
            If Not Rs.EOF Then
               While Not Rs.EOF
                     .AddItem Trim(Rs(2))
                     .ItemData(.NewIndex) = Rs(0)
                     Rs.MoveNext
               Wend
            End If
            Rs.Close
        End With
    End If
End Sub

Sub cmdContinuar_Click()
Dim cont As Long
Dim flagReanalise As String

    'DEMANDA: 7979370 - FELIPPE MENDES - 20111020
    'RN01.07 - N�O PERMITINDO ABERTURA DE MAIS DE UM AVISO DE SINISTRO DE EVENTO DE PRE�O PARA A PROPOSTA.

    If frmEvento.cboEvento.ListIndex <> -1 Then     'AKIO.OKUNO - 17860335 - 11/06/2013
        If frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex) = 430 Then
            If mVerificaEventoPreco(1, frmEvento.cboEvento.ListIndex) Then
                MsgBox "J� existe um aviso de sinistro de evento de pre�o para esta proposta!", vbCritical, "Aviso de Sinistro"
                Exit Sub
            End If
        End If
    End If                                          'AKIO.OKUNO - 17860335 - 11/06/2013

    'Verifica se foi selecionado um evento
    If cboEvento.ListIndex = -1 Then
        MsgBox "Indique o evento causador da ocorr�ncia.", vbCritical, App.Title
        cboEvento.SetFocus
        Exit Sub
    End If
    
    'Veririca se a data da ocorr�ncia foi preenchida
    If mskDataOcorrencia.Text = "__/__/____" Then
        MsgBox "Indique a Data de Ocorr�ncia do Sinistro.", vbCritical, App.Title
        mskDataOcorrencia.SetFocus
        Exit Sub
    End If
    
        'Ricardo Toledo (Confitec) : IM00016861 : 16/03/2017 : INI
    'Estava dando ao avisar o sinistro devido ao caracter "'" (aspas simples) conforme anexo do incidente
    For cont = 0 To 9
        If (Trim(frmEvento.txtExigencia(cont).Text <> "")) Then
            frmEvento.txtExigencia(cont).Text = Funcoes.MudaAspaSimples(frmEvento.txtExigencia(cont).Text)
        End If
    Next cont
    'Ricardo Toledo (Confitec) : IM00016861 : 16/03/2017 : FIM
    
    '***********************************************************************************************
                            ' INICIO DEMANDA  : 12554726 Parte 2
    '***********************************************************************************************
    'Alterado por   : Paulo Fantin
    'Objetivo       : O Sistema n�o permitia o aviso de sinistro em alguns casos, por exemplo:
    '                 1-)Fins de semana
    '                 2-)Feriados
    'Objetivo       : Foi Feito um getdate()no banco de dados para pegar a data atual e setar o valor
    '                 na v�riavel "DtaServidor" do servidor assim possibilitando o usu�rio
    '                 cadastrar os avisos de sinistros nas datas citadas acima
    '***********************************************************************************************
        If Format(mskDataOcorrencia.Text, "yyyymmdd") > Format(DtaServidor, "yyyymmdd") Then
        'If Format(mskDataOcorrencia.Text, "yyyymmdd") > Format(Data_Sistema, "yyyymmdd") Then
            'MsgBox "Data de Ocorr�ncia posterior a data atual (" & Data_Sistema & ").", vbCritical, App.Title
            MsgBox "Data de Ocorr�ncia posterior a data atual (" & Format(DtaServidor, "dd/MM/yyyy") & ").", vbCritical, App.Title
            mskDataOcorrencia.SetFocus
            Exit Sub
        End If
    '***********************************************************************************************
                'FIM DA DEMANDA : 12554726
    '***********************************************************************************************

    ' --- Incluido por asouza 06.12.05
    If mskDataOcorrenciaFim.Visible = True Then
        If mskDataOcorrenciaFim.Text = "__/__/____" Then
            MsgBox "Indique a Data Final de Ocorr�ncia do Sinistro.", vbCritical, App.Title
            mskDataOcorrenciaFim.SetFocus
            Exit Sub
        End If
    End If
    
    
    'Verifica se o nome do contato est� preenchido
    If Trim(txtContato.Text) = "" Then
        MsgBox "Indique o contato da vistoria.", vbCritical, App.Title
        txtContato.SetFocus
        Exit Sub
    End If
          'Racras 20/10/2010
     'Verifica se o descricao da ocrrencia foi feita
    If Trim(txtExigencia(0).Text) = "" Then
        MsgBox "Preencha a descri��o do evento.(Inicie pela 1� linha).", vbCritical, App.Title
        txtExigencia(0).SetFocus
        Exit Sub
    End If
    'Verifica se o Local de risco foi preenchido
    If Trim(txtLocalRisco.Text) = "" Then
        MsgBox "Indique o Endere�o da vistoria.", vbCritical, App.Title
        txtLocalRisco.SetFocus
        Exit Sub
    End If
    
    'Verifica se o Bairro foi preenchido
    If Trim(txtBairro.Text) = "" Then
        MsgBox "Indique o Bairro da vistoria.", vbCritical, App.Title
        txtBairro.SetFocus
        Exit Sub
    End If
    
    'Verifica se o munic�pio foi selecionado
'AKIO.OKUNO - INICIO - 17860335 - 20/06/2013
'    If cmbMunicipio.ListIndex = -1 Then
'        If cmbMunicipio.Text = "" Then
    If cmbMunicipio.ListIndex = -1 Or LenB(Trim(cmbMunicipio)) = 0 Then
'        If LenB(Trim(cmbUF)) <> 0 And LenB(Trim(cmbMunicipio)) <> 0 Then
'            MsgBox "inserir busca de cep"
'            Exit Sub
'        Else
'AKIO.OKUNO - FIM - 17860335 - 20/06/2013
            MsgBox "Indique o Munic�pio da vistoria.", vbCritical, App.Title
            cmbMunicipio.SetFocus
            Exit Sub
'        End If
    End If
    
    If cmbUF.ListIndex = -1 Then
        MsgBox "Indique a UF da vistoria.", vbCritical, App.Title
        cmbUF.SetFocus
        Exit Sub
    End If
    
    'Verifica se foi digitada a hora
    If mskHora.Text = "__:__" Or _
       Len(mskHora.ClipText) < 4 Or _
       Left(mskHora.ClipText, 2) < 0 Or _
       Left(mskHora.ClipText, 2) > 23 Or _
       Right(mskHora.ClipText, 2) < 0 Or _
       Right(mskHora.ClipText, 2) > 59 Then
       MsgBox "Hora da Ocorr�ncia do Sinistro inv�lida.", vbCritical, App.Title
       mskHora.SetFocus
       Exit Sub
    End If
    
    

   
    If cboEvento.ListIndex <> -1 And cboSubEvento.ListCount > 0 And cboSubEvento.ListIndex = -1 Then
        MsgBox "Indique o Subevento causador da ocorr�ncia.", vbCritical, App.Title
        cboSubEvento.SetFocus
        Exit Sub
    End If
    
    
    'Verifica se algum DDD foi preenchido
    If Trim(Replace(mskDDD(0).Text & mskDDD(1).Text, "_", "")) = "" Then
        MsgBox "Indique o DDD do telefone para contato.", vbCritical, App.Title
        mskDDD(0).SetFocus
        Exit Sub
    End If
        
    'Verifica se algum telefone foi preenchido
    If Trim(Replace(Replace(mskTelefone(0).Text & mskTelefone(1).Text, "_", ""), "-", "")) = "" Then
        MsgBox "Indique o telefone para contato.", vbCritical, App.Title
        mskTelefone(0).SetFocus
        Exit Sub
    End If
        
    'Verifica se algum tipo de telefone foi selecionado
    If cmbTipoTelefone(0).ListIndex + cmbTipoTelefone(1).ListIndex = -2 Then
        MsgBox "Indique o tipo de telefone para contato.", vbCritical, App.Title
        cmbTipoTelefone(0).SetFocus
        Exit Sub
    End If
    
    For cont = 0 To 9
        frmAviso.txtExigencia(cont).Text = txtExigencia(cont).Text
    Next cont


    'Demanda 4766934
    bolVazio = 0
    For cont = 0 To 9
        If (Trim(frmAviso.txtExigencia(cont).Text <> "")) Then
            bolVazio = 1
            Exit For
        End If
    Next cont

    If Not bolVazio <> 0 Then
        MsgBox "Campo Descri��o do Evento � obrigat�rio!.", vbCritical, App.Title
        Exit Sub
    End If



    With frmAviso
        Call CarregaUF(.cmbUFVistoria)
        .txtEndVistoria.Text = Me.txtLocalRisco.Text
        .txtCEPVistoria.Text = Me.txtCEP.Text
        .txtMunicipioVistoria.Text = Me.cmbMunicipio.Text
        .txtBairroVistoria.Text = Me.txtBairro.Text
        .txtContatoVistoria.Text = Me.txtContato.Text
        '-- Inicio da Altera��o - FLOW 181858 - Stefanini - 21/11/2006 - Victor Neto
        If Trim(Me.cmbUF) <> "" Then .cmbUFVistoria = Me.cmbUF
        '-- Fim da Altera��o - Stefanini

        For cont = 0 To 1
'            Call CarregaTipoTel(.cmbTpTelefoneVistoria(cont))      'AKIO.OKUNO - 17860335 - 11/06/2013
            If Not Me.mskDDD(cont).Text = "" Then .mskDDDVistoria(cont).Text = Me.mskDDD(cont).Text
            If Not Me.mskTelefone(cont).Text = "" Then .mskTelefoneVistoria(cont).Text = Me.mskTelefone(cont).Text
            'If Not Me.cmbTipoTelefone(cont).Text = "" Then .cmbTpTelefoneVistoria(cont).Text = Me.cmbTipoTelefone(cont).Text
            If Trim(Me.cmbTipoTelefone(cont)) <> "" Then .cmbTpTelefoneVistoria(cont) = Me.cmbTipoTelefone(cont)
        Next
        If iCod_Obj_segurado = 0 And Not bSemProposta Then
            .txtEndRisco.Text = Me.txtLocalRisco.Text
            .txtCEPRisco.Text = Me.txtCEP.Text
            .txtMunicipioRisco.Text = Me.cmbMunicipio.Text
            .txtBairroRisco.Text = Me.txtBairro.Text
            '-- Inicio da Altera��o - FLOW 181858 - Stefanini - 21/11/2006 - Victor Neto
            If Trim(Me.cmbUF) <> "" Then .cboUFRisco.Text = Me.cmbUF
            '-- Fim da Altera��o - Stefanini
        End If
        
    End With
    


Me.Hide
frmSolicitante.Show
End Sub

Private Sub cmdPesquisarCEP_Click()
Dim SQL As String
Dim Rs As Recordset

Dim i As Integer
Dim Municipio_id As String
Dim Municipio As String
Dim Uf As String
Dim chvlocal_dne As String
Dim municipio_descricao As String


If txtCEP.ClipText <> "" Then
     Me.MousePointer = vbHourglass
     
     'Obtem o endere�o, de acordo com o CEP
     SQL = ""
     SQL = SQL & " SELECT uf_log UF, nome_local municipio, chvlocal_dne chave_correio,  "
     SQL = SQL & "  tp_log tp_logra, nome_log logra,"
     SQL = SQL & "      cep8_log CEP, nombai1_log bairro"
     SQL = SQL & " FROM seguros_temp_db..CEP_LOg with (nolock) "
     SQL = SQL & " WHERE CEP8_LOg = '" & Replace(txtCEP.Text, "-", "") & "'"
     
     Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          True)
     If Not Rs.EOF Then
        txtLocalRisco.Text = Trim(Rs!tp_logra) & " " & Trim(Rs!logra)
        If frmSolicitante.AchouBase(Trim(Rs!Municipio), Trim(Rs!Uf), municipio_descricao) Then
            cmbMunicipio.Text = Trim(municipio_descricao)
        Else
            Municipio_id = frmSolicitante.InsereMunicipio(Rs!Municipio, Rs!Uf, Rs!chave_correio)
            Call CarregaMunicipio(cmbMunicipio)
            cmbMunicipio.Text = Trim(Rs!Municipio)
        End If
        
        txtBairro.Text = IIf(IsNull(Rs!Bairro), "", Trim(Rs!Bairro))
        If cmbUF.Text <> Rs!Uf Then     'AKIO.OKUNO - 17860335 - 11/06/2013
            cmbUF.Text = Rs!Uf
            'AKIO.OKUNO - INICIO - 17860335 - 20/06/2013
            For i = 0 To cmbMunicipio.ListCount - 1
                If cmbMunicipio.List(i) = Trim(UCase(municipio_descricao)) Then
                    cmbMunicipio.ListIndex = i
                    If cmbUF.Text = UCase(Rs("UF")) Then
                        Exit For
                    End If
                End If
            Next
            'AKIO.OKUNO - FIM - 17860335 - 20/06/2013
        End If                          'AKIO.OKUNO - 17860335 - 11/06/2013
        Municipio = Trim(Rs!Municipio)  'AKIO.OKUNO - 17860335 - 11/06/2013
        Uf = Rs!Uf
        chvlocal_dne = Rs!chave_correio
        
        txtContato.SetFocus
        
        'Verifica se existe na municpio_tb
        SQL = ""
        SQL = SQL & " SELECT municipio_id, estado, nome, tp_pessoa_id, nome, municipio_ibge_id, "
        SQL = SQL & "        municipio_BACEN_id, municipio_BACEN_dv, chvlocal_dne"
        SQL = SQL & " FROM municipio_tb with (nolock)"
        SQL = SQL & " WHERE (nome = '" & Trim(Municipio) & "' "
        SQL = SQL & " AND estado = '" & Trim(Uf) & "') "
        SQL = SQL & " OR (chvlocal_dne = '" & chvlocal_dne & "')"
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          True)
        If Not Rs.EOF Then
            
            'Caso n�o tenha o ID da base dos Correios
            If IsNull(Rs!chvlocal_dne) Then
               
                SQL = ""
                SQL = SQL & "EXEC municipio_spu " & Rs!Municipio_id & "," & Rs!tp_pessoa_id & ","
                SQL = SQL & "'" & Trim(Rs!Nome) & "','" & Trim(Rs!estado) & "','" & cUserName & "',"
                SQL = SQL & IIf(IsNull(Rs!municipio_ibge_id), "null", Rs!municipio_ibge_id) & "," & IIf(IsNull(Rs!municipio_bacen_id), "null", Rs!municipio_bacen_id) & ","
                SQL = SQL & IIf(IsNull(Rs!municipio_bacen_dv), "null", Rs!municipio_bacen_dv) & ", '" & Format(chvlocal_dne, "00000000") & "'"
                
                Call ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          False)
            
            End If
            
        Else
            'Caso n�o exista em municipio_tb
            Municipio_id = frmSolicitante.InsereMunicipio(Municipio, Uf, chvlocal_dne)
            Call CarregaMunicipio(cmbMunicipio)
            
            cmbMunicipio.Text = Municipio
        End If
        
        ' Flow 203212 - Marcelo - 30/01/2007
        For i = 0 To cmbMunicipio.ListCount - 1
            If cmbMunicipio.List(i) = Municipio Then
                cmbMunicipio.ListIndex = i
                If cmbUF.Text = Uf Then
                    Exit For
                End If
            End If
        Next
        
     End If
     Me.MousePointer = vbDefault
 End If
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call Sair(Me)
End Sub

Private Sub cmdVoltar_Click()
    Me.Hide
    frmConsulta.Show
End Sub

Private Sub Form_Activate()
    rRespostaQuestionario = False
End Sub

'AKIO.OKUNO - Form_Activate - 17860335 - 07/06/2013
'Private Sub Form_Load()
'    Dim wAuxCounter As Integer
'    Debug.Print "Form_Load"
'    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss,ms")
'    CentraFrm Me
'    For wAuxCounter = 0 To 1
'        Call CarregaTipoTel(cmbTipoTelefone(wAuxCounter))
'    Next
'    Call CarregaUF(cmbUF)
'    Call CarregaMunicipio(cmbMunicipio)
'    'AFONSO FILHO - GPTI
'    'DEMANDA: 554094 - DATA: 05/11/2008
'    mskHora.Text = "00:00"
'
'    '***********************************************************************************************
'                            ' INICIO DEMANDA  : 12554726 Parte 1
'    '***********************************************************************************************
'    'Alterado por   : Paulo Fantin
'    'Objetivo       : O Sistema n�o permitia o aviso de sinistro em alguns casos, por exemplo:
'    '                 1-)Fins de semana
'    '                 2-)Feriados
'    'Objetivo       : Foi Feito um getdate()no banco de dados para pegar a data atual e setar o valor
'    '                 na v�riavel "DtaServidor" do servidor assim possibilitando o usu�rio
'    '                 cadastrar os avisos de sinistros nas datas citadas acima
'    '***********************************************************************************************
'        Dim RSaux As New ADODB.Recordset
'        Dim sSQL As String
'        sSQL = "Select Getdate()DataHoraServidor"
'        Set RSaux = ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             True)
'        If Not RSaux.EOF Then
'            DtaServidor = RSaux!DataHoraServidor
'        Else
'            DtaServidor = Now()
'        End If
'    Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss,ms")
'
'End Sub

'AKIO.OKUNO - Form_Load - 17860335 - 10/06/2013
'Private Sub Form_Load()
'    Dim wAuxCounter As Integer
'    Debug.Print "Form_Load"
'    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss,ms")
'
'    CentraFrm Me
'    For wAuxCounter = 0 To 1
'        Call CarregaTipoTel(cmbTipoTelefone(wAuxCounter))
'    Next
'    Call CarregaUF(cmbUF)
'    Call CarregaMunicipio(cmbMunicipio)
'    mskHora.Text = "00:00"
'    Dim RSaux As New ADODB.Recordset
'    Dim sSQL As String
'    sSQL = "Select Getdate()DataHoraServidor"
'    Set RSaux = ExecutarSQL(gsSIGLASISTEMA, _
'                         glAmbiente_id, _
'                         App.Title, _
'                         App.FileDescription, _
'                         sSQL, _
'                         True)
'    If Not RSaux.EOF Then
'        DtaServidor = RSaux!DataHoraServidor
'    Else
'        DtaServidor = Now()
'    End If
'    Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss,ms")
'
'End Sub

Private Sub Form_Load()
    Dim rsRecordSet             As ADODB.Recordset
    Dim sSQL                    As String
    
    Debug.Print "frmEvento.Form_Load"
    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss,ms")
    
    CentraFrm Me
    mskHora.Text = "00:00"
    CarregaCombo_CmbUF
    cmbMunicipio.Text = ""     'AKIO.OKUNO - 17860335 - 11/06/2013
    
    Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss,ms")
    
End Sub

'AKIO.OKUNO - CarregaCombo_CmbUF - 11/06/2013
Private Sub CarregaCombo_CmbUF()
    Dim rsRecordSet             As ADODB.Recordset
    Dim sSQL                    As String
    
    Debug.Print "Form_Load"
    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss,ms")
    sSQL = ""
    sSQL = sSQL & "Set NoCount On                                                                                               " & vbNewLine
    sSQL = sSQL & "Select Estado                                                                                                " & vbNewLine
    sSQL = sSQL & "     , DataHoraServidor                  = GetDate()                                                          " & vbNewLine
    sSQL = sSQL & "  From Seguros_Db.Dbo.UF_Tb              UF_Tb with (NoLock) " & vbNewLine
    Set rsRecordSet = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         True)
    cmbUF.Clear
    With rsRecordSet
        If Not .EOF Then
            DtaServidor = .Fields(1)
            Do While Not .EOF
               If .Fields(0) <> "--" Then ' Cleber Sardo - In�cio - INC000004428002 - Data 23/10/2014 - Inibe a sele��o
                cmbUF.AddItem .Fields(0)
               End If                     ' Cleber Sardo - Fim - INC000004428002 - Data 23/10/2014
                .MoveNext
            Loop
        Else
            DtaServidor = Now()
        End If
    End With
    Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss,ms")
End Sub


Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub

Private Sub optPeriodico_Click()
    lblPeriodico.Visible = True
    mskDataOcorrenciaFim.Visible = True
    mskHora.Left = 9360
    lblHora.Left = 9360
End Sub

Private Sub optPontual_Click()
    lblPeriodico.Visible = False
    mskDataOcorrenciaFim.Visible = False
    mskHora.Left = 8040
    lblHora.Left = 8040
End Sub


Private Function mVerificaEventoPreco(Proposta_id As String, _
                                  Evento_id As String) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset
    
    mVerificaEventoPreco = False
    
    SQL = ""
    SQL = SQL & "EXEC SEGS9806_SPS "
    SQL = SQL & Proposta_id
    SQL = SQL & "," & Evento_id
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
    If Rs(0) > 0 Then
        mVerificaEventoPreco = True
    End If
    Rs.Close
    
End Function


Private Sub mskTelefone_LostFocus(Index As Integer)
    '30/05/2012 : Nova - Mauro Vianna : Altera��o de telefone para 9 d�gitos : tratamento de 8 d�gitos
    If Right(mskTelefone(Index).Text, 1) = "_" And Right(mskTelefone(Index).Text, 2) <> "__" Then
        mskTelefone(Index).Text = " " & Left(mskTelefone(Index).Text, 4) + "-" + Mid(mskTelefone(Index).Text, 5, 1) + Mid(mskTelefone(Index).Text, 7, 3)
    End If

End Sub

