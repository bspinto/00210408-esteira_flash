VERSION 5.00
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GridDinamico.ocx"
Begin VB.Form frmAvisoPropostas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0862 - Aviso de Sinistro pela CA"
   ClientHeight    =   7905
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11775
   Icon            =   "frmAvisoPropostas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7905
   ScaleWidth      =   11775
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "Desavisar tudo"
      Height          =   430
      Left            =   6120
      TabIndex        =   9
      Top             =   6840
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   10350
      TabIndex        =   6
      Top             =   7470
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   8955
      TabIndex        =   5
      Top             =   7470
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   7605
      TabIndex        =   4
      Top             =   7470
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Propostas do Cliente"
      Height          =   7350
      Left            =   0
      TabIndex        =   0
      Top             =   45
      Width           =   11760
      Begin GridFrancisco.GridDinamico GridTipo 
         Height          =   1140
         Left            =   180
         TabIndex        =   7
         Top             =   5580
         Width           =   11475
         _ExtentX        =   20241
         _ExtentY        =   2011
         BorderStyle     =   1
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
      Begin VB.CommandButton cmdNAvisar 
         Caption         =   "N�o Avisar"
         Height          =   430
         Left            =   8955
         TabIndex        =   3
         Top             =   6795
         Width           =   1275
      End
      Begin VB.CommandButton cmdReanalise 
         Caption         =   "Reanalise"
         Height          =   430
         Left            =   10305
         TabIndex        =   2
         Top             =   6795
         Width           =   1320
      End
      Begin VB.CommandButton cmdAvisar 
         Caption         =   "Avisar"
         Height          =   430
         Left            =   7560
         TabIndex        =   1
         Top             =   6795
         Width           =   1320
      End
      Begin GridFrancisco.GridDinamico grdResultadoPesquisa 
         Height          =   5175
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   9128
         BorderStyle     =   1
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
   End
End
Attribute VB_Name = "frmAvisoPropostas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public CLIENTE_DOC As String
Public Nome As String
Public LISTA_CLIENTE As String
Public NaoPodeAvisar As Boolean
Public tProposta As cProposta
Public motivo_reanalise As Integer

Private Sub cmdAvisar_Click()
Dim flagReanalise As String
Dim sinistro_id As String
Dim sinistro_bb As String
Dim svalor_anterior As String

   NaoPodeAvisar = False
   svalor_anterior = grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1)
    
    'genjunior - hi agro
    Call VerificaPropostaAgricola(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.RowSel, 2), grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.RowSel, 5), CInt(grdResultadoPesquisa.RowSel))
    If sProdutoAgricola = "ERRO" Then
        Me.MousePointer = vbNormal
        Exit Sub
    End If
    'Fim genjunior

     If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) <> "AVSR" And _
        grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) <> "RNLS" Then
               
           ' GPTI - Raimundo - 13/02/2009 - FLOW : 714343 e 758110
           'Verifica se existe proposta BB e se ela � num�rica
             If IsNumeric(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.RowSel, 3)) Then
                 'GPTI - Osmar - 26/01/09 - FLOW : 708705
                 'Corre��o na passagem  dos par�metros para VerificaPropostaBB
                 'Call VerificaPropostaBB(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.RowSel, 6), IIf(mskDataOcorrencia.Text = "__/__/____", "", "'" & Format(mskDataOcorrencia.Text, "yyyymmdd") & "'"))
                  Call VerificaPropostaBB(CLng("0" & grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.RowSel, 3)), frmEvento.mskDataOcorrencia.Text)
             End If
           'Raimundo - Fim
                         

          
          ' G&P - AFONSO - FLOW : 286647
          'Verifica se o segurado da proposta coincide com o sinistrado informado
          'If Not verificaCoincideSinistradoSegurado(GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 9)) Then
          '     MsgBox "Esta proposta possui um segurado diferente do selecionado nas telas anteriores." & vbCrLf & _
          '            "N�o � poss�vel avisar esta proposta.", vbCritical, "Aviso de Sinistro"
               
          'Else
             
             Set Aviso = CreateObject("SEGL0144.cls00385")
             Aviso.mvarAmbienteId = glAmbiente_id
             Aviso.mvarSiglaSistema = gsSIGLASISTEMA
             Aviso.mvarSiglaRecurso = App.Title
             Aviso.mvarDescricaoRecurso = App.FileDescription

                              
             flagReanalise = Aviso.VerificaReanaliseRE(CDbl(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 2)), _
                             Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd"), _
                             frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex), _
                             Str(frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 2)), _
                             0, _
                             0)

             

             If flagReanalise = "RNLS" Or flagReanalise = "AVSR" Then
                 If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "PSCB" Then
                     If MsgBox("Confirma o registro em proposta sem cobertura?", vbYesNo, "Sinistro sem proposta") = vbNo Then Exit Sub
                 End If
                 If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "*NAVS" Then
                     grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS"
                 Else
                     grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = flagReanalise
                 End If
                 'If InStr(1, cProdutoBBProtecao, grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5), vbTextCompare) Then
                 If VerificarProdutoBB(cProdutoBBProtecao, grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5)) Then
                     For i = 1 To grdResultadoPesquisa.Rows - 1
                         'If InStr(1, cProdutoBBProtecao, grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5), vbTextCompare) Then
                           If VerificarProdutoBB(cProdutoBBProtecao, grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5)) Then
                             grdResultadoPesquisa.TextMatrix(Str(i), 1) = flagReanalise
                         End If
                     Next i
                    'ATUALIZAR PROPOSTAS SEM COBERTURA
                    Call AtualizaPropostasSemCobertura(grdResultadoPesquisa.Row, svalor_anterior)
                 End If

               Else
                   MsgBox "Esta proposta j� possui um aviso de sinistro em aberto" & vbCrLf & _
                        "para os parametros: proposta, evento de sinistro, " & vbCrLf & _
                        "data de ocorr�ncia, CPF ou CNPJ do sinistrado." & vbCrLf & _
                        "N�o � poss�vel avisar esta proposta.", vbCritical, "Aviso de Sinistro"
                   
                   ' GPTI - CAIO C�ZAR LOPES - FLOW : 745690
                   'Ap�s exibir mensagem de que n�o � poss�vel realizar o aviso setamos
                   'a flag para TRUE, pois assim quando clicarmos no bot�o continuar
                   'n�o ser� poss�vel efetuar o aviso de forma manual
                   NaoPodeAvisar = True
               End If
    
          
    End If
    
End Sub

Private Sub ExisteProdutoBB(Proposta_id As Long)
Dim SQL As String
Dim Rs As ADODB.Recordset

SQL = SQL & " SELECT produto_bb "
SQL = SQL & " FROM produto_bb_sinistro_tb b with (nolock) "
SQL = SQL & " INNER JOIN proposta_tb a with (nolock) "
SQL = SQL & " ON a.proposta_id = " & Proposta_id
SQL = SQL & " WHERE b.produto_id = a.produto_id "
SQL = SQL & " AND b.dt_inicio_vigencia <= a.dt_contratacao "
SQL = SQL & " AND (dt_fim_vigencia >= a.dt_contratacao OR dt_fim_vigencia is null)"
'Camila Kume - 19.12.2008 - Inclus�o de query para liberar o aviso para as propostas do ALS
SQL = SQL & " UNION "
SQL = SQL & " SELECT PRODUTO_ID "
SQL = SQL & " FROM PROPOSTA_TB with (nolock) "
SQL = SQL & " WHERE PROPOSTA_ID = " & Proposta_id
SQL = SQL & " AND ORIGEM_PROPOSTA_ID=2"
'Camila Kume - FIM

Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

If Not Rs.EOF Then
    
    produto_id = Rs.Fields(0)
    
    Rs.Close
    
    'Camila Kume - 29.12.2008 - Selecionar os dados de produtos do ALS
    SQL = "set nocount on exec obtem_produto_sps 1," & produto_id
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    If Not Rs.EOF Then
        iCD_PRD = Rs(0)
        iCD_MDLD = Rs(1)
        lCD_ITEM_MDLD = Rs(2)
    Else
        iCD_PRD = 0
        iCD_MDLD = 0
        lCD_ITEM_MDLD = 0
    End If
    Rs.Close
'Camila Kume - 29.12.2008 - Fim
Else
    Rs.Close
End If

End Sub


Public Function VerificaPropostaBB(Proposta_bb As Long, dt_Ocorrencia As String) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset


SQL = "set nocount on exec obtem_Versao_Endosso_sps " & Proposta_bb & ", '" & Format(CDate(dt_Ocorrencia), "yyyymmdd") & "'"

Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

If Not Rs.EOF Then
    VerificaPropostaBB = True
    lNR_VRS_EDS = Rs("NR_VRS_EDS")
    'Alterado por Cleber - Data: 28/11/2006 - Flow 189587
    'Segundo o Junior estava buscando o campo errado.
    'lNR_CTR_SGRO = rs("NR_CTR_SGRO")
    lNR_CTR_SGRO = Rs("NR_CTR_SGRO_BB")
    
   'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : inicio
    iCD_PRD = Rs("CD_PRD")
    iCD_MDLD = Rs("CD_MDLD")
    lCD_ITEM_MDLD = Rs("CD_ITEM_MDLD")
    'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : fim
        

Else
    VerificaPropostaBB = False
    lNR_VRS_EDS = 0
    lNR_CTR_SGRO = 0
   
   'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : inicio
    iCD_PRD = 0
    iCD_MDLD = 0
    lCD_ITEM_MDLD = 0
    'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : fim

End If
Rs.Close

End Function


Private Sub cmdContinuar_Click()
    Dim Ramo, SubRamo As Integer
    Dim Sigla_Dif_SRGL As Boolean

    'Flow 17905311 - IN�CIO
    'Dim Situacao_Real As String
    'Flow 17905311 - FIM
    
    If ObtemPropostasAvisadas = False Or NaoPodeAvisar Then Exit Sub
    bSemProposta = False
    For i = 1 To grdResultadoPesquisa.Rows - 1
        If Mid(grdResultadoPesquisa.TextMatrix(CDbl(i), 1), 1, 1) = "A" Or Mid(grdResultadoPesquisa.TextMatrix(CDbl(i), 1), 1, 1) = "R" Then

            'Flow 17905311 - IN�CIO
'            sSinistro_id = grdResultadoPesquisa.TextMatrix(Str(i), 2)
'
'            If Trim(grdResultadoPesquisa.TextMatrix(CDbl(i), 4)) = "Em Estudo" Then
'
'                MsgBox "Ainda n�o � poss�vel efetuar um aviso de sinistro " & vbCrLf & _
'                       "para uma proposta em estudo ou em aceita��o. " & vbCrLf & _
'                       "Por favor, queira abrir uma manifesta��o." & vbCrLf & _
'                       "Sinistro AB: " & sSinistro_id & vbCrLf & _
'                       "N�o � poss�vel avisar esta proposta.", vbExclamation, "Aviso de Sinistro"
'
'                Me.MousePointer = vbDefault
'                Exit Sub
'
'            ElseIf Trim(grdResultadoPesquisa.TextMatrix(CDbl(i), 4)) = "Emitida" Then
'
'                SQL = ""
'                SQL = SQL & "Select situacao From proposta_tb With (Nolock) Where proposta_id = " & sSinistro_id
'
'                Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
'                                     glAmbiente_id, _
'                                     App.Title, _
'                                     App.FileDescription, _
'                                     SQL, _
'                                     True)
'
'                If Not Rs.EOF Then
'                    Situacao_Real = Rs("situacao")
'                End If
'
'                If UCase(Situacao_Real) = "A" Then
'
'                    MsgBox "Ainda n�o � poss�vel efetuar um aviso de sinistro para uma " & vbCrLf & _
'                           "proposta em estudo ou em aceita��o. " & vbCrLf & _
'                           "Por favor, queira abrir uma manifesta��o." & vbCrLf & _
'                           "Sinistro AB: " & sSinistro_id & vbCrLf & _
'                           "N�o � poss�vel avisar esta proposta.", vbExclamation, "Aviso de Sinistro"
'
'                    Me.MousePointer = vbDefault
'                    Exit Sub
'
'                End If
'            End If
            'Flow 17905311 - FIM

            SQL = ""
            SQL = SQL & "SEGS7366_SPS " & grdResultadoPesquisa.TextMatrix(Str(i), 2)
            SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
            
            Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 True)
            
            If Not Rs.EOF Then
                Ramo = Rs("ramo_id")
                SubRamo = Rs("subramo_id")
            End If

'            'verificar se para as coberturas adicionais deveria seguir processo normal
'            If grdResultadoPesquisa.TextMatrix(Str(i), 5) = 1240 Then
'                sProdutoAgricola = "ABERTURA"
'            Else
            'Reanalise
            'asouza - novo
            Call VerificaPropostaAgricola(grdResultadoPesquisa.TextMatrix(Str(i), 2), grdResultadoPesquisa.TextMatrix(Str(i), 5), CInt(i))
            If sProdutoAgricola = "ERRO" Then
                Me.MousePointer = vbNormal
                Exit Sub
            End If
            
                'verificar se para as coberturas adicionais deveria seguir processo normal
                If grdResultadoPesquisa.TextMatrix(Str(i), 5) = 1240 And sProdutoAgricola <> "REANALISE" Then
                    sProdutoAgricola = "ABERTURA"
                End If
                
'            End If
            
            Set Aviso = CreateObject("SEGL0144.cls00385")
        
            Aviso.mvarAmbienteId = glAmbiente_id
            Aviso.mvarSiglaSistema = gsSIGLASISTEMA
            Aviso.mvarSiglaRecurso = App.Title
            Aviso.mvarDescricaoRecurso = App.FileDescription
                
            If sProdutoAgricola = "INC_OCORRENCIA" Then
                frmAviso.lblTpAviso.Caption = sInclusaoOcorrencia
                frmAviso.txtReanalise.Text = "N�o"
                frmAviso.txtSinistro.Visible = True
                frmAviso.lblSinistro.Visible = True
            ElseIf sProdutoAgricola = "ABERTURA" Then
                frmAviso.lblTpAviso.Caption = sAbertura
                frmAviso.txtReanalise.Text = "N�o"
                frmAviso.txtSinistro.Visible = False 'asouza - novo
                frmAviso.lblSinistro.Visible = False 'asouza - novo
            ElseIf sProdutoAgricola <> "REANALISE" Then
                flagReanalise = Aviso.VerificaReanaliseRE(grdResultadoPesquisa.TextMatrix(Str(i), 2), _
                                            Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd"), _
                                            frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex), _
                                            frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 2), sSinistro_id, sSinistro_bb)
                                            
                                            
                If flagReanalise = "RNLS" Then
                    
                    Call PreencheDadosSinistro(sSinistro_id)
                    Me.MousePointer = vbDefault
                    frmAviso.lblTpAviso.Caption = sReabertura 'asouza
                    
                ElseIf flagReanalise = "NAVS" Then
                    MsgBox "Esta proposta j� possui um aviso de sinistro em aberto " & vbCrLf & _
                                "para os parametros: proposta, CPF/CNPJ do Segurado, evento de sinistro e " & vbCrLf & _
                                "data de ocorr�ncia." & vbCrLf & _
                                "Sinistro AB: " & sSinistro_id & vbCrLf & _
                                "Sinistro BB: " & sSinistro_bb & vbCrLf & _
                                "N�o � poss�vel avisar esta proposta.", vbCritical, "Aviso de Sinistro"
                                
                    Me.MousePointer = vbDefault
                    Exit Sub
                ElseIf flagReanalise = "AVSR" Then
                    frmAviso.lblTpAviso.Caption = sAbertura
                    frmAviso.txtReanalise.Text = "N�o"
                    frmAviso.txtSinistro.Visible = False
                    frmAviso.lblSinistro.Visible = False
                End If
            End If

            Me.Hide
            
            If Not existeProposta(grdResultadoPesquisa.TextMatrix(CDbl(i), 2)) Then
               Set tProposta = New cProposta
               With tProposta
                   .Proposta = Str(grdResultadoPesquisa.TextMatrix(CDbl(i), 2))
                   .Produto = grdResultadoPesquisa.TextMatrix(CDbl(i), 5)
                   .Ramo = Ramo
                   .SubRamo = SubRamo
                   .SituacaoProposta = grdResultadoPesquisa.TextMatrix(CDbl(i), 4)
                   If sProdutoAgricola = "RNLS" Then
                        .TipoAviso = "REANALISE"
                   Else
'                        .TipoAviso = sProdutoAgricola      'AKIO.OKUNO / CVAO - 17860335 - 27/06/2013
                        .TipoAviso = Trim(sProdutoAgricola) 'AKIO.OKUNO / CVAO - 17860335 - 27/06/2013
                   End If
               End With
    
              Propostas.Add tProposta
            End If
        Else
            If existeProposta(grdResultadoPesquisa.TextMatrix(CDbl(i), 2)) Then
                Propostas.Remove (getIndiceProposta(grdResultadoPesquisa.TextMatrix(CDbl(i), 2)))
                'remover do grid obj
                For j = 1 To frmObjSegurado.grdPropostas.Rows - 1
                    If frmObjSegurado.grdPropostas.TextMatrix(CDbl(j), 1) = grdResultadoPesquisa.TextMatrix(CDbl(i), 2) Then
                        If frmObjSegurado.grdPropostas.Rows = 2 Then
                            frmObjSegurado.grdPropostas.AddItem (" ")
                        End If
                        frmObjSegurado.grdPropostas.RemoveItem (j)
                        Exit For
                    End If
                Next j
            End If
        End If
        
    Next
    
  
    '(IN�CIO) -- RSOUZA -- 03/08/2010 ---------------------------------------------------------------------------------------------------------------------------------------------'
    If grdResultadoPesquisa.Rows = 2 Then
      If grdResultadoPesquisa.TextMatrix(1, 1) = "SRGL" Then
         MsgBox ("Sinistro " & Trim(grdResultadoPesquisa.TextMatrix(1, 10)) & " em regula��o (SRGL). Caso queira registrar alguma solicita��o, dever� ser realizada no sistema SIR.")
         Exit Sub
        'If Not MsgBox("Sinistro " & Trim(grdResultadoPesquisa.TextMatrix(1, 10)) & " em regula��o (SRGL). Caso queira registrar alguma solicita��o, dever� ser realizada no sistema SIR. Deseja continuar?", vbYesNo) = vbYes Then
        '       Exit Sub
        'End If
      End If
    End If
    
      
    Sigla_Dif_SRGL = False
    For u = 1 To grdResultadoPesquisa.Rows - 1
      If grdResultadoPesquisa.TextMatrix(CDbl(u), 1) <> "SRGL" Then
          Sigla_Dif_SRGL = True
          Exit For
      End If
    Next u

    If Sigla_Dif_SRGL = False Then
        MsgBox ("Sinistro(s) em regula��o (SRGL). Caso queira registrar alguma solicita��o, dever� ser realizada no sistema SIR.")
        Exit Sub
    End If
    '(FIM)    -- RSOUZA -- 03/08/2010 ---------------------------------------------------------------------------------------------------------------------------------------------'
       
  
    If Propostas.Count = 0 Then
        bSemProposta = True
    End If
    
    Call frmObjSegurado.CarregaPropostas
    Me.MousePointer = vbDefault
    Me.Hide
    
    
    'INICIO - MU00416975 - Reabertura de Sinistro
'    If ( _
'        ((tProposta.Produto = 8 Or tProposta.Produto = 300) And Ramo = 30) _
'        Or ((tProposta.Produto = 1210 Or tProposta.Produto = 1226 Or tProposta.Produto = 1227) And Ramo = 77) _
'        Or ((tProposta.Produto = 155 Or tProposta.Produto = 156) And Ramo = 62) _
'        Or (tProposta.Produto = 1204 And Ramo = 1) _
'        Or ((tProposta.Produto = 227 Or tProposta.Produto = 228 Or tProposta.Produto = 229 Or tProposta.Produto = 1152) And Ramo = 2) _
'        Or (tProposta.Produto = 701 And Ramo = 7) _
'        Or (tProposta.Produto = 1201 And Ramo = 61) _
'        Or (tProposta.Produto = 680 And Ramo = 68)) Then
          frmMotivoReanalise.Status_reanalise = IIf(Trim(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1)) = "", 0, grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1))
        frmMotivoReanalise.Ramo_reanalise = IIf(Trim(Ramo) = "", 0, Ramo)
        frmMotivoReanalise.Produto_reanalise = IIf(Trim(tProposta.Produto) = "", 0, tProposta.Produto)
        frmAviso.Ramo_reanalise = IIf(Trim(Ramo) = "", 0, Ramo)
        frmAviso.Produto_reanalise = IIf(Trim(tProposta.Produto) = "", 0, tProposta.Produto)
'    End If
    'FIM - MU00416975 - Reabertura de Sinistro
    
        frmObjSegurado.Show
     
End Sub

Private Sub Desmarcar_Tudo()
Dim subevento_sinistro_aux As Long

'   If frmEvento.cboSubEvento.ListIndex = -1 Then
'      subevento_sinistro_aux = 0
'   Else
'      subevento_sinistro_aux = frmEvento.cboSubEvento.ItemData(frmEvento.cboSubEvento.ListIndex)
'   End If

   Me.MousePointer = vbHourglass
   
   For i = 1 To grdResultadoPesquisa.Rows - 1
			' CONFITEC - INCIDENTE (IM00850770)- inicio
            If grdResultadoPesquisa.TextMatrix(Str(i), 1) = "RNLS" Or grdResultadoPesquisa.TextMatrix(Str(i), 1) = "*NAVS" Then
                grdResultadoPesquisa.TextMatrix(Str(i), 1) = "*NAVS"
            Else
                grdResultadoPesquisa.TextMatrix(Str(i), 1) = "NAVS"
            End If
			' CONFITEC - INCIDENTE (IM00850770)- Fim
    Next i
   
   Me.MousePointer = vbDefault
   
End Sub

Private Sub cmdNAvisar_Click()
Dim subevento_sinistro_aux As Long

   If frmEvento.cboSubEvento.ListIndex = -1 Then
      subevento_sinistro_aux = 0
   Else
      subevento_sinistro_aux = frmEvento.cboSubEvento.ItemData(frmEvento.cboSubEvento.ListIndex)
   End If

   Me.MousePointer = vbHourglass
   If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 7) = 2 And _
      Left(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1), 1) <> "*" Then
   
        If Not grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "NAVS" Then
            If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS" Then
                grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "*NAVS"
            Else
                grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "NAVS"
         
            End If

            'If InStr(1, cProdutoBBProtecao, Format(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5), "0000"), vbTextCompare) Then
            If VerificarProdutoBB(cProdutoBBProtecao, Format(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5), "0000")) Then

                For i = 1 To grdResultadoPesquisa.Rows - 1
                    'If InStr(1, cProdutoBBProtecao, grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5), vbTextCompare) Then
                      If VerificarProdutoBB(cProdutoBBProtecao, grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5)) Then

                        If grdResultadoPesquisa.TextMatrix(Str(i), 1) = "RNLS" Then
                            grdResultadoPesquisa.TextMatrix(Str(i), 1) = "*NAVS"
                        Else
                            
                            grdResultadoPesquisa.TextMatrix(Str(i), 1) = "NAVS"
            
                        End If
                    End If

                Next i
                'ATUALIZAR PROPOSTAS SEM COBERTURA - n�o passa o indice para n�o atualizar a linha atual
                Call AtualizaPropostasSemCobertura
            End If
            
        End If
   End If
   Me.MousePointer = vbDefault
   
End Sub

Private Sub cmdReanalise_Click()
  
        If Not grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS" And _
           Left(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1), 1) = "*" Then
           
              grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS"
              'frmAviso.GridPropostaAfetada.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS"
            
        Else
              MsgBox "N�o � poss�vel abrir esta proposta para rean�lise de sinistro.", vbCritical, "Aviso de Sinistro"
        End If
   
End Sub

Private Sub Command1_Click()

Call Desmarcar_Tudo

End Sub

Private Sub Form_Load()

    Me.MousePointer = vbHourglass
    'SelecionaPropostas
    CentraFrm Me
    PreencheGridTipo
    montacabecalhoGrid
    CarregaProdutoBBProtecaoVida
    Me.MousePointer = vbDefault
    If grdResultadoPesquisa.Rows = 1 Then
        Call cmdContinuar_Click
    End If
    click_tela = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub cmdVoltar_Click()
    Me.Hide
    frmAgencia.Show
End Sub
Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Public Sub montacabecalhoGrid()
    With grdResultadoPesquisa
        .Cols = 10
        .TextMatrix(0, 1) = "Aviso"
        .TextMatrix(0, 2) = "Proposta AB"
        .TextMatrix(0, 3) = "Proposta BB"
        .TextMatrix(0, 4) = "Situacao"
        .TextMatrix(0, 5) = "Produto"
        .TextMatrix(0, 6) = "Nome do Produto"
        .TextMatrix(0, 7) = "Tipo Ramo"
        .TextMatrix(0, 8) = "Inicio de Vig�ncia"
        .TextMatrix(0, 9) = "Fim de Vig�ncia"
        .AutoFit H�brido
    End With

End Sub

Private Function ObtemPropostasAvisadas() As Boolean
Dim SQL As String
Dim i As Long
Dim propNAVSBBProtecao As String
Dim propAVSRBBProtecao As String

   propostasAvisadas = ""
   propAVSRBBProtecao = ""
   propNAVSBBProtecao = ""
   
   ObtemPropostasAvisadas = True
   
   'Obtem as propostas e os clientes afetados
   For i = 1 To grdResultadoPesquisa.Rows - 1
      If grdResultadoPesquisa.TextMatrix(i, 1) = "AVSR" Then
         
         If propostasAvisadas <> "" Then propostasAvisadas = propostasAvisadas & " , "
         propostasAvisadas = propostasAvisadas & grdResultadoPesquisa.TextMatrix(i, 2)
      
      ElseIf grdResultadoPesquisa.TextMatrix(i, 1) = "RNLS" Then
         
         If propostasReanalise <> "" Then propostasReanalise = propostasReanalise & " , "
         propostasReanalise = propostasReanalise & grdResultadoPesquisa.TextMatrix(i, 2)
      
      End If
   Next i
End Function

Private Function verificaCoincideSinistradoSegurado(Segurado As String) As Boolean

Dim primeiroNomeSinis As String
Dim ultimoNomeSinis As String
Dim primeiroNomeSegu As String
Dim ultimoNomeSegu As String
Dim ultimoEspacoSinis As Long
Dim ultimoEspacoSegu As Long
Dim Nome As String


Dim SQL As String
Dim Rs As ADODB.Recordset

verificaCoincideSinistradoSegurado = False
Nome = frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 1)

'mfujino 24/11/2004
'tratamento para cliente sem sobrenome
If InStr(1, Trim(Nome), " ", vbTextCompare) > 0 Then
    primeiroNomeSinis = Mid(Nome, 1, InStr(1, Nome, " ", vbTextCompare) - 1)
    primeiroNomeSegu = Mid(Segurado, 1, InStr(1, Segurado, " ", vbTextCompare) - 1)
Else
    primeiroNomeSinis = Nome '.Text
    primeiroNomeSegu = Segurado
End If

ultimoEspacoSinis = InStrRev(Trim(Nome), " ", , vbTextCompare)
ultimoNomeSinis = Mid(Trim(Nome), ultimoEspacoSinis + 1, Len(Trim(Nome)) - ultimoEspacoSinis)

ultimoEspacoSegu = InStrRev(Segurado, " ", , vbTextCompare)
ultimoNomeSegu = Mid(Segurado, ultimoEspacoSegu + 1, Len(Segurado) - ultimoEspacoSegu)

SQL = "select difference('" & primeiroNomeSinis & "','" & primeiroNomeSegu & "')"
Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
If Rs(0) >= 2 Then 'G&P - AFONSO - Antigo: If rs(0) > 2 Then
    Rs.Close
    SQL = "select difference('" & ultimoNomeSinis & "','" & ultimoNomeSegu & "')"
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    If Rs(0) > 2 Then
        verificaCoincideSinistradoSegurado = True
    End If
End If

End Function

Private Function ExisteEventoSinistroProduto(evento_sinistro_id As Integer, produto_id As Integer) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset

    'Verifica se o evento de sinistro est� cadastrado para o produto
    ExisteEventoSinistroProduto = False
    
    SQL = ""
    SQL = SQL & " select 1 from produto_estimativa_sinistro_tb with (nolock) "
    SQL = SQL & " where produto_id = " & produto_id
    SQL = SQL & " and evento_sinistro_id = " & evento_sinistro_id
        
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    
    If Not Rs.EOF Then
        ExisteEventoSinistroProduto = True
    End If
    
    Rs.Close
End Function

Private Function VerificaSePodeAvisar(dt_inicio_vigencia As String, dt_fim_vigencia As String) As Boolean
Dim dt_Ocorrencia As String

dt_Ocorrencia = Format(frmEvento.mskDataOcorrencia, "yyyymmdd")
VerificaSePodeAvisar = False

'Verifica vigencia
If Trim(dt_inicio_vigencia) <> "" Then
    dt_inicio_vigencia = Format(dt_inicio_vigencia, "yyyymmdd")
    If Trim(dt_fim_vigencia) <> "" Then
        dt_fim_vigencia = Format(dt_fim_vigencia, "yyyymmdd")
        If dt_inicio_vigencia <= dt_Ocorrencia And _
           dt_fim_vigencia >= dt_Ocorrencia Then
           VerificaSePodeAvisar = True
        End If
    Else
         If dt_inicio_vigencia <= dt_Ocorrencia Then
              VerificaSePodeAvisar = True
        End If
    End If
Else
    If dt_fim_vigencia <> "" Then
        dt_fim_vigencia = Format(dt_fim_vigencia, "yyyymmdd")
        If dt_fim_vigencia >= dt_Ocorrencia Then
           VerificaSePodeAvisar = True
        End If
    End If
End If

End Function

Private Sub PreencheGridTipo()

    With GridTipo
        .Cols = 5
        
        '(IN�CIO) -- RSouza -- 26/07/2010 --------
        '.Rows = 3
        .Rows = 4
        '(FIM)    -- RSouza -- 26/07/2010 --------
      
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "C�digo"
        .TextMatrix(0, 2) = "Descri��o"
        .TextMatrix(1, 1) = "AVSR"
        .TextMatrix(1, 2) = "A Avisar"
        .TextMatrix(2, 1) = "RNLS"
        .TextMatrix(2, 2) = "Solicita��o de Reanalise"
        
        
        '(IN�CIO) -- RSouza -- 26/07/2010 --------
        .TextMatrix(3, 1) = "SRGL"
        .TextMatrix(3, 2) = "Sinistro em regula��o"
        '(FIM)    -- RSouza -- 26/07/2010 --------
        
       
        .TextMatrix(0, 3) = "C�digo"
        .TextMatrix(0, 4) = "Descri��o"
        .TextMatrix(1, 3) = "PSCB"
        .TextMatrix(1, 4) = "N�o Avisar - Proposta Sem Cobertura"
        .TextMatrix(2, 3) = "NAVS"
        .TextMatrix(2, 4) = "N�o Avisar"
        .AutoFit H�brido
    End With
    
End Sub

Private Function ObtemProduto(produto_id As Long) As String

Dim SQL As String
Dim rc As ADODB.Recordset


SQL = " select produto_id, nome produto" & _
      " from produto_tb with (nolock) " & _
      " where produto_id = " & produto_id

Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, _
                            True)
If Not rc.EOF Then
    ObtemProduto = rc!produto_id & " - " & rc!Produto
Else
    ObtemProduto = ""
End If

End Function

Public Function VerificaReanalise(Proposta_id As Long, _
                                   dtOcorrencia As String, _
                                   evento_sinistro_id As Integer, _
                                   CPF As String, _
                                   dt_Nascimento As String, _
                                   ByRef sinistro_id As String, _
                                   ByRef sinistro_bb_id As String) As String

Dim SQL As String
Dim rc As ADODB.Recordset
Dim eventos_equivalentes As String
Dim Situacao As String


SQL = ""

SQL = "set nocount on exec verifica_reanalise_sps " & Proposta_id & ", " & evento_sinistro_id & ",'" & _
    dtOcorrencia & "','" & CPF & "','" & dt_Nascimento & "'"


Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, _
                            True)
                            
If Not rc.EOF Then 'se existe aviso, verifica se est� aberto

    If IsNull(rc!Situacao) Then
        VerificaReanalise = "NAVS"
    Else
        Select Case Trim(rc!Situacao)
            Case "0", "1", ""
                VerificaReanalise = "NAVS"
            Case "2"
                VerificaReanalise = "RNLS"
            Case Else
                VerificaReanalise = "RNLS"
        End Select
    End If
    
    '******************************************************************
    'Alterado por Cleber da Stefanini - data: 14/01/2005
    
    'N�o estava sendo feito o tratamento de null somente para o
        'sinistro_bb e estava era necess�rio o mesmo para o sinistro_id
    
    'sinistro_id = rc!sinistro_id
    
    sinistro_id = IIf(IsNull(rc!sinistro_id), "", rc!sinistro_id)
    '******************************************************************
    sinistro_bb_id = IIf(IsNull(rc!sinistro_bb), "", rc!sinistro_bb)

Else 'if n�o existe aviso
    VerificaReanalise = ""
    sinistro_id = ""
    sinistro_bb_id = ""
End If

End Function

Sub SelecionaPropostas()

        Debug.Print "SelecionaPropostas"
        Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss:ms")
        SQL = ""
        SQL = SQL & "set nocount on exec seguros_db.dbo.SEGS7758_SPS "
        If bSemProposta Then
            SQL = SQL & "0"
        Else
            SQL = SQL & frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 3)
        End If
        SQL = SQL & ", " & frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex)
        SQL = SQL & ", '" & Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd") & "'"
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
        
       frmAvisoPropostas.grdResultadoPesquisa.PopulaGrid (Rs)
       
       Set Aviso = CreateObject("SEGL0144.cls00385")
           
       Aviso.mvarAmbienteId = glAmbiente_id
       Aviso.mvarSiglaSistema = gsSIGLASISTEMA
       Aviso.mvarSiglaRecurso = App.Title
       Aviso.mvarDescricaoRecurso = App.FileDescription
     
        Rs.Close
        
        'GENJUNIOR - HI AGRO - PROPOSTA A N�O AVISAR QUANDO J� FOI AVISADA PELO MOBILE
        
        Set Rs = Nothing
        
        Dim strPropostaID As String

        Dim i As Long
        
        For i = 1 To frmAvisoPropostas.grdResultadoPesquisa.Rows - 1
        
            strPropostaID = frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(i, 2)
            
            SQL = ""
            SQL = SQL & " select 1 from interface_dados_db.dbo.aviso_web_comunicacao_sinistro_tb a with (nolock) " & vbNewLine
            SQL = SQL & "  where a.proposta_id = " & strPropostaID & " " & vbNewLine
            SQL = SQL & "    and not exists(select 1 from seguros_db.dbo.sinistro_tb aux with (nolock) " & vbNewLine
            SQL = SQL & "                    where aux.proposta_id = a.proposta_id " & vbNewLine
            SQL = SQL & "                      and aux.nr_ptc_aviso = a.num_protocolo) " & vbNewLine
        
            Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
            
            If Not Rs.EOF Then
                frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(i, 1) = "NAVS"
            End If
        
            Rs.Close
            Set Rs = Nothing
        
        Next i
        
        'FIM GENJUNIOR
        
        
''        'AKIO.OKUNO - INICIO - 17860335 - 20/06/2013
''        If grdResultadoPesquisa.Rows > 1 Then
''            If LenB(Trim(grdResultadoPesquisa.TextMatrix(1, 1))) = 0 Then
''                grdResultadoPesquisa.Rows = 1
''            End If
''        End If
''        'AKIO.OKUNO - INICIO - 17860335 - 20/06/2013
'
'        For i = 1 To grdResultadoPesquisa.Rows - 1
'
'            'If grdResultadoPesquisa.TextMatrix(Str(i), 5) = "1152" Then
'
'            'DEMANDA: 7979370 - FELIPPE MENDES - 20111020
'            'IMPLEMENTANDO VERIFICA��O PARA O PRODUTO 1204
'
'            If grdResultadoPesquisa.TextMatrix(Str(i), 5) = "1152" Or grdResultadoPesquisa.TextMatrix(Str(i), 5) = "1204" Then
'                a = VerificaPropostaAgricola(grdResultadoPesquisa.TextMatrix(Str(i), 2), grdResultadoPesquisa.TextMatrix(Str(i), 5))
'                If sProdutoAgricola = "REANALISE" Then
'                    grdResultadoPesquisa.TextMatrix(Str(i), 1) = "RNLS"
'                End If
'
'                '-- (IN�CIO) -- RSouza --- 22/07/2010 -------------------------------------------------------------------------------'
'                '-----------------------------------------------------------------
'                'Ricardo Toledo (Confitec) : 13/09/2012 : INC000003713449 : inicio
'                'If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "AVSR" And grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS" Then
'                If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "AVSR" Or grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS" Then
'                'Ricardo Toledo (Confitec) : 13/09/2012 : INC000003713449 : inicio
'                '-----------------------------------------------------------------
'                    If sProdutoAgricola = "INC_OCORRENCIA" Then
'                        grdResultadoPesquisa.TextMatrix(Str(i), 1) = "SRGL"
'                    End If
'                End If
'                '-- (FIM)    -- RSouza --- 22/07/2010 --------------------------------------------------------------------------------'
'
'            Else
'                flagReanalise = Aviso.VerificaReanaliseRE(CDbl(grdResultadoPesquisa.TextMatrix(CDbl(i), 2)), _
'                                Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd"), _
'                                frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex), _
'                                frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 2), _
'                                0, _
'                                0)
'                If flagReanalise = "RNLS" Then
'                    grdResultadoPesquisa.TextMatrix(Str(i), 1) = "RNLS"
'                End If
'
'                '-- (IN�CIO) -- RSouza --- 22/07/2010 -------------------------------------------------------------------------------'
'               If (grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "AVSR" And grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS") Or flagReanalise = "NAVS" Then
'                    If flagReanalise <> "RNLS" And flagReanalise <> "AVSR" Then
'                        grdResultadoPesquisa.TextMatrix(Str(i), 1) = "SRGL"
'                    End If
'                End If
'                '-- (FIM)    -- RSouza --- 22/07/2010 --------------------------------------------------------------------------------'
'            End If
'
'
'           'if grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "AVSR" And grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) = "RNLS"
'
'        Next i
'
        Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss:ms")
        grdResultadoPesquisa_Click
End Sub

Public Sub CarregaProdutoBBProtecaoVida()
   
   cProdutoBBProtecao = ""
   'Alessandra Grig�rio - 28.04.2009 - Seleciona produtos BB Protecao Vida
   SQL = "SELECT PRODUTO_ID FROM MODULO_PLANO_TB with (nolock) "
   Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
   
   Do While Not Rs.EOF
      cProdutoBBProtecao = cProdutoBBProtecao & "," & Rs("PRODUTO_ID")
      Rs.MoveNext
   Loop
   Rs.Close
   
   cProdutoBBProtecao = Mid(cProdutoBBProtecao, 2)
   'Alessandra Grig�rio - 28.04.2009 - Fim

End Sub

Private Sub AtualizaPropostasSemCobertura(Optional indice As Integer, Optional valor As String)

    If IsMissing(indice) Then
        indice = 0
    End If
    
    SQL = ""
    SQL = SQL & "set nocount on exec SEGS7758_SPS "
    If bSemProposta Then
        SQL = SQL & "0"
    Else
        SQL = SQL & frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 3)
    End If
    SQL = SQL & ", " & frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex)
    SQL = SQL & ", '" & Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd") & "'"
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
                         
    While Not Rs.EOF
        For i = 1 To grdResultadoPesquisa.Rows - 1
            If Rs("proposta_id") = grdResultadoPesquisa.TextMatrix(Str(i), 2) Then
                If (Rs("indicador") = "PSCB") And (i <> indice) And (valor <> "PSCB") Then
                    grdResultadoPesquisa.TextMatrix(Str(i), 1) = "PSCB"
                End If
            End If
        Next i
        Rs.MoveNext
    Wend

End Sub

Public Function VerificarProdutoBB(ByVal lista As String, value As String) As Boolean
'********************************************************************
'* AFONSO NOGUEIRA - GPTI                                           *
'* DATA: 04/03/2010                                                 *
'* CRIA��O DO METODO PARA LOCALIZAR O PRODUTO BB NO MEIO DA STRING  *
'********************************************************************

VerificarProdutoBB = False
Dim arr As Variant

On Error GoTo Trata_Erro

arr = Split(lista, ",")

For i = 0 To UBound(arr)
    If Trim(CStr(arr(i))) = Trim(CStr(value)) Then
        VerificarProdutoBB = True
        Exit For
    End If
Next

Exit Function
Trata_Erro:
    VerificarProdutoBB = False
End Function

Private Sub grdResultadoPesquisa_Click()
   
   '-- (IN�CIO) -- RSouza -- 22/07/2010 ---------------------------------------------'
   If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.RowSel, 1) = "SRGL" Then
       cmdAvisar.Enabled = False
       cmdNAvisar.Enabled = False
       cmdReanalise.Enabled = False
   Else
       cmdAvisar.Enabled = True
       cmdNAvisar.Enabled = True
       cmdReanalise.Enabled = True
   End If
   '-- (FIM)    -- RSouza -- 22/07/2010 ---------------------------------------------'

End Sub
