Attribute VB_Name = "FuncoesAmbiente"
Option Explicit
Public db_ServerName As String
Public db_UserName   As String
Public db_UserPass   As String
Public db_Name       As String
Public ArquivoINI    As String

Public Function InicializaVariaveisConexao() As Boolean
  On Local Error GoTo TrataErro
  Dim Resultado       As String
  Dim Posicao         As Long
  Dim ArquivoAmbiente As String
  
  
  InicializaVariaveisConexao = True
  
  'Le arquivo com as informa��es de conex�o
  ArquivoINI = App.Path & "\config.ini"
  If Dir(ArquivoINI) = "" Then
    ArquivoAmbiente = Environ("WINDIR") & "\" & Mid(ArquivoINI, InStrRev("\" & ArquivoINI, "\"))
  Else
    ArquivoAmbiente = ArquivoINI
  End If
  ArquivoINI = ArquivoAmbiente
  
  If Dir(ArquivoAmbiente) = "" Then
    InicializaVariaveisConexao = False
    Exit Function
  End If
  
  Resultado = Decriptar(ArquivoAmbiente)
  Posicao = 1
  db_ServerName = Mid(Resultado, Posicao, InStr(Posicao, Resultado, vbCr) - 1)
  Inc Posicao, Len(db_ServerName) + 1
  db_Name = Mid(Meio(Mid(Resultado, Posicao), vbLf, vbCr), 2)
  Inc Posicao, Len(db_Name) + 2
  db_UserName = Mid(Meio(Mid(Resultado, Posicao), vbLf, vbCr), 2)
  Inc Posicao, Len(db_UserName) + 2
  db_UserPass = Mid(Meio(Mid(Resultado, Posicao), vbLf, vbCr), 2)
      
  Exit Function
  
TrataErro:
  
  InicializaVariaveisConexao = False
  
End Function

Public Function Decriptar(FileName As String) As String
    
'/* Start VB/Format Comment Block-----------------------------------*/
'
'       Copyright � 1998 Sistema Brasil Seguridade All Rights Reserved.
'
'  Project: PROJECT1.VBP
'  Module: CRYPTOFILTERBOX.CLS
'  Procedure: Decrypt
'  Author: Robson
'
'/* Stop VB/Format Comment Block------------------------------------*/

   
    ReDim aPassword(0) As Byte
    ReDim aCryptBuffer(0) As Byte
    Dim lCryptLength As Long
    Dim lCryptBufLen As Long
    Dim lCryptPoint As Long
    Dim lPasswordPoint As Long
    Dim lPasswordCount As Long
    'Dim PROV_RSA_FULL As Long
   ' Dim MS_DEF_PROV As String
    Dim CALG_MD5 As Long
    Dim calg_rc4 As Long
   ' Dim BNULL As Byte
    Dim arquivo1, arquivo2 As Integer
    Dim tempname As String
    Dim outbuffer As String
   
    
    On Error GoTo ErrDecrypt
    'switch Status property
    arquivo1 = FreeFile
    Open FileName For Binary Access Read As arquivo1
  
   
        
    lCryptLength = 360
    lCryptBufLen = lCryptLength + 16
    Dim I  As Integer
    Dim buffer As String * 1
    Dim minhaLocalizacao As Long
    I = 1
    
    minhaLocalizacao = Loc(arquivo1)
    While minhaLocalizacao < LOF(arquivo1)
    ReDim aCryptBuffer(lCryptBufLen)
    lCryptPoint = 1
    
    While lCryptPoint <= lCryptLength And minhaLocalizacao < LOF(arquivo1)
        buffer = Input(1, #arquivo1)
        aCryptBuffer(lCryptPoint - 1) = Asc(buffer)
        lCryptPoint = lCryptPoint + 1
        minhaLocalizacao = Loc(arquivo1)
    Wend 'Decrypt data
    Dim fimArquivo As Long
    If minhaLocalizacao < LOF(arquivo1) Then
       fimArquivo = 1
    Else
       fimArquivo = 0
    End If
    
      ' // Decrypt data
      '        if(!CryptDecrypt(hKey, 0, eof, 0, pbBuffer, &dwCount)) {
      '            printf("Error %x during CryptDecrypt!\n", GetLastError());
      '            goto done;
      '        }
      '
      'mvarOutBuffer = ""
      lCryptPoint = 0
    
      While lCryptPoint <= lCryptBufLen
       ' mvarOutBuffer = mvarOutBuffer & Chr$(aCryptBuffer(lCryptPoint))
         outbuffer = outbuffer & Chr$(8 Xor aCryptBuffer(lCryptPoint))
       ' Put #arquivo2, , Chr$(aCryptBuffer(lCryptPoint))
          
       lCryptPoint = lCryptPoint + 1
      Wend
    Wend
   
Close #arquivo1
Decriptar = outbuffer

    
    Exit Function

ErrDecrypt:
    MsgBox ("ErrDecrypt " & Error$)
   
    
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' faz a express�o aumentar de um determinado
' incremento seja cadeia, num�rica ou l�gica
Public Sub Inc(Expressao As Variant, Optional ByVal Incremento As Variant = 1)
  If Not IsNull(Incremento) Then Expressao = Expressao + Incremento
End Sub

'----------------------------------------------------------------------------------------------
' retorna uma cadeia intermedi�ria entre duas partes que podem ser string ou integer
Public Function Meio(ByVal Cadeia As String, PalavraInicio, Optional PalavraFim) As String
  Dim Inicio As Long, Tamanho As Long, TamInicio As Long
  Meio = ""
  If VarType(PalavraInicio) = vbString Then
    Inicio = InStr(1, Cadeia, PalavraInicio)
    If Inicio = 0 Then Exit Function
    TamInicio = Len(PalavraInicio)
  ElseIf IsNumeric(PalavraInicio) Then
    Inicio = CInt(PalavraInicio)
    If Inicio < 1 Then Inicio = 1
    TamInicio = 0
  Else
    Exit Function
  End If
  If VarType(PalavraFim) = vbString Then
    Tamanho = InStr(Inicio + TamInicio, Cadeia, PalavraFim) - Inicio
  ElseIf IsNumeric(PalavraFim) Then
    Tamanho = CInt(PalavraFim) - Inicio
    If Tamanho > Len(Cadeia) Then
      Tamanho = Len(Cadeia)
    End If
  ElseIf IsMissing(PalavraFim) Then
    Tamanho = Len(Cadeia) - Inicio + 1
  Else
    Exit Function
  End If
  If Tamanho < 1 Then Exit Function
  Meio = Mid(Cadeia, Inicio, Tamanho)
End Function

'----------------------------------------------------------------------------------------------
' retorna os campos com v�rgula sem a �ltima v�rgula
Public Function FormatoSQL(ParamArray OsCampos()) As String
  If IsMissing(OsCampos) Then
    FormatoSQL = ""
    Exit Function
  End If
  Dim Cadeia As String
  Dim OCampo As Variant
  Cadeia = ""
  For Each OCampo In OsCampos
    Inc Cadeia, ", " & FormatoCampoSQL(OCampo)
  Next
  FormatoSQL = Mid(Cadeia, 3)
End Function

Public Function Decifra(Cadeia As String, Optional Chave As String) As String
  Decifra = Criptografia(Cadeia, Chave, -1)
End Function

'----------------------------------------------------------------------------------------------
' retorna a criptografia de uma cadeia
Private Function Criptografia(ByVal Cadeia As String, Optional ByVal Chave As String, _
                              Optional ByVal Passo As Integer = 1) As String
  If Passo = 0 Then
    Passo = 1
  End If
  Dim Inicio As Integer
  Dim Fim As Integer
  Inicio = Se(Passo > 0, 1, Len(Cadeia))
  Fim = Se(Passo > 0, Len(Cadeia), 1)
  Dim Indice As Integer
  If Chave = "" Then
    For Indice = Inicio To Fim Step Passo
      Mid(Cadeia, Indice) = Chr(FazXOR(Cadeia, Indice))
    Next Indice
  Else
    For Indice = Inicio To Fim Step Passo
      Mid(Cadeia, Indice _
                            ) = Chr(FazXOR(Cadeia, Indice) Xor Asc(Mid(Chave, ((Indice - 1) Mod Len(Chave)) + 1, 1)))
    Next Indice
  End If
  Criptografia = Cadeia
End Function

'----------------------------------------------------------------------------------------------
' retorna o campo com delimitador de acordo com o seu tipo
Private Function FormatoCampoSQL(Optional Campo As Variant) As String
  On Local Error GoTo TrataErro
  If IsNull(Campo) Or IsMissing(Campo) Or IsEmpty(Campo) Then
    FormatoCampoSQL = "NULL"
    Exit Function
  End If
  If VarType(Campo) = vbString Or VarType(Campo) = vbDate Then
    Dim CampoStr As String
    Dim Inicio As Byte
    CampoStr = CStr(Campo)
    If UCase(CampoStr) = "NULL" Then
      FormatoCampoSQL = "NULL"
    ElseIf Logico(numero(CampoStr, Inicio)) Then
      CampoStr = Meio(Campo, Inicio)
      FormatoCampoSQL = Str(Regional(CampoStr))
    ElseIf IsDate(CampoStr) Then
      'FormatoCampoSQL = DelimitadorSQL(Format(Regional(CampoStr), "yyyymmdd hh:nn:ss"))
      FormatoCampoSQL = DelimitadorSQL(Format(CampoStr, "yyyymmdd hh:nn:ss"))
    Else
      FormatoCampoSQL = DelimitadorSQL(CampoStr)
    End If
  ElseIf VarType(Campo) = vbBoolean Then
    FormatoCampoSQL = Abs(CInt(Campo))
  ElseIf IsNumeric(Campo) Then
    FormatoCampoSQL = Str(Campo)
  ElseIf VarType(Campo) >= vbArray Then
    Dim Auxiliar As String
    Dim OElemento
    Auxiliar = ""
    For Each OElemento In Campo
      Inc Auxiliar, ", " & FormatoCampoSQL(OElemento)
    Next OElemento
    FormatoCampoSQL = Mid(Auxiliar, 3)
  Else
    FormatoCampoSQL = "NULL"
  End If
  Exit Function
TrataErro:
  FormatoCampoSQL = "0"
  MsgBox "O campo passado ==>" & Se(CampoStr = "", Campo, CampoStr) & _
         "<== n�o p�de ser convertido corretamente.", vbCritical, App.Title
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o segundo par�metro se a condi��o for verdadeira,
' caso contr�rio, o terceiro
Public Function Se(CondicaoVerdadeira As Boolean, _
                   ParametroVerdadeiro As Variant, _
                   ParametroFalso As Variant) As Variant
  Se = IIf(CondicaoVerdadeira, ParametroVerdadeiro, ParametroFalso)
End Function


'----------------------------------------------------------------------------------------------
' retorna o ASCII do XOR entre uma letra e sua anterior na cadeia
Private Function FazXOR(Cadeia As String, Indice As Integer) As Integer
  If Indice > 1 Then
    FazXOR = Asc(Mid(Cadeia, Indice, 1)) Xor Asc(Mid(Cadeia, Indice - 1, 1))
  Else
    FazXOR = Asc(Mid(Cadeia, Indice, 1)) Xor Len(Cadeia)
  End If
End Function

'----------------------------------------------------------------------------------------------
' retorna o valor num�rico acrescentado de um marcador usado
' para ser passado com um par�metro string que � num�rico na fun��o FormatoSQL
' e pode retornar se o valor foi acrescentado de um marcador num�rico
Public Function numero(ByVal ONumero, _
                Optional ByRef NaoPasseEsteParametro As Variant) As String
  Dim Auxiliar As String
  Const Marcador = "#!@#$%^&*()_+|~{#}[#]1234567890#"
  If IsMissing(NaoPasseEsteParametro) Then
    Auxiliar = Marcador & IIf(IsNull(ONumero), 0, ONumero)
  Else
    Auxiliar = Se(Logico(InStr(ONumero, Marcador)), "sim", "n�o")
    NaoPasseEsteParametro = Se(Logico(Auxiliar), Len(Marcador) + 1, 0)
  End If
  numero = Auxiliar
End Function


'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o valor l�gico de uma vari�vel
Public Function Logico(Optional Variavel As Variant) As Boolean
  If IsNull(Variavel) Or IsMissing(Variavel) Or IsEmpty(Variavel) Then
    Logico = False
  ElseIf VarType(Variavel) = vbString Then
    Logico = (LCase(Mid(Variavel, 1, 1)) = "s") Or _
             (LCase(Mid(Variavel, 1, 1)) = "y")
  Else
    Logico = CBool(Variavel)
  End If
End Function


'----------------------------------------------------------------------------------------------
' retorna o valor no formato regional da m�quina para data e n�mero passados em portugu�s
' Portugu�s (Brasil), Espanhol (Tradicional), Alem�o (Padr�o), Franc�s (Padr�o) e Ingl�s (EUA)
Public Function Regional(ByVal Data_Numero As String) As String
  If InStr(Data_Numero, "/") > 1 And _
     IsDate(Data_Numero) Then
    ''If DiaMes Then
    Regional = Format(Data_Numero, "dd/mm/yyyy Hh:Nn:Ss")
    ''Else
      ''Regional = Format(Data_Numero, "mm/dd/yyyy Hh:Nn:Ss")
    ''End If
  Else
    Data_Numero = Replace(Data_Numero, ".", "")
    Data_Numero = Replace(Data_Numero, ",", RegionalDecimal)
    If IsNumeric(Data_Numero) Then
      Regional = Data_Numero
    Else
      Regional = ""
    End If
  End If
End Function


'----------------------------------------------------------------------------------------------
' retorna o campo verificando o delimitador MSSQL
Public Function DelimitadorSQL(ByVal Campo As String) As String
  DelimitadorSQL = "'" & Replace(Campo, "'", "''") & "'"
End Function

'----------------------------------------------------------------------------------------------
' retorna o caracter divisor de decimal
Public Function RegionalDecimal() As String
  On Local Error GoTo TrataErro
  Dim Auxiliar As String
  Auxiliar = ""
  If CDbl("1 1") = 1.1 Then Auxiliar = " "
  If CDbl("1.1") = 1.1 Then Auxiliar = "."
  If CDbl("1,1") = 1.1 Then Auxiliar = ","
  RegionalDecimal = Auxiliar
  Exit Function
TrataErro:
  If Auxiliar = "" Then
    Resume Next
  Else
    RegionalDecimal = Auxiliar
  End If
End Function

