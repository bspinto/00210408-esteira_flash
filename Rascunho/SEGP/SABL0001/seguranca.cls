VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "seguranca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const gsSIGLASISTEMA = "SEGBR"

Private Function LerArquivoIni(ByVal Secao As String, ByVal Item As String, _
                       Optional ByVal ObjAmbiente As Variant) As String
  
  If IsMissing(ObjAmbiente) Then
    Set ObjAmbiente = CreateObject("Ambiente.ClasseAmbiente")
  End If
  LerArquivoIni = ObjAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, Secao, Item, gsCHAVESISTEMA)

End Function

Private Function LerArquivoIni2(ByVal Secao As String, ByVal Item As String, lAmbienteId As Long) As String
  
Dim oSABL0010 As Object

  If IsMissing(oSABL0010) Or oSABL0010 Is Nothing Then
    Set oSABL0010 = CreateObject("SABL0010.cls00009")
  End If
  LerArquivoIni2 = oSABL0010.RetornaValorAmbiente(gsSIGLASISTEMA, Secao, Item, gsCHAVESISTEMA, lAmbienteId)

End Function

Function valida_permissao(dom�nio As String, usuario As String, recurso As String) As Boolean

Dim rs       As New Recordset
Dim sSql     As String
Dim sConexao As String
Dim sServer  As String
Dim sBanco   As String
Dim sUsuario As String
Dim sSenha   As String
Dim Conexao  As New Connection

On Error GoTo Trata_Erro
  

  
  'Le as variaveis de Conex�o
  sServer = LerArquivoIni("Intranet", "Servidor")
  sBanco = LerArquivoIni("Intranet", "Banco")
  sUsuario = LerArquivoIni("Intranet", "Usuario")
  sSenha = LerArquivoIni("Intranet", "Senha")
  
  'Monta a string de Conex�o
  sConexao = "Provider=SQLOLEDB; Data Source=" & sServer & ";Initial Catalog=" & sBanco & "; User Id=" & sUsuario & "; Password=" & sSenha
  sConexao = "UID=" & sUsuario & ";PWD=" & UCase(sSenha) & ";server=" & sServer & ";driver={SQL Server};database=" & sBanco & ";"
  
  Conexao.Open sConexao
  
  'Monta o SELECT para validar o usu�rio e recurso
'  sSql = "           SELECT sec_endereco_tb.endereco"
'  sSql = sSql & "      FROM sec_endereco_tb"
'  sSql = sSql & "      JOIN sec_modulo_funcao_tb"
'  sSql = sSql & "        ON sec_modulo_funcao_tb.modulo_funcao_id = sec_endereco_tb.modulo_funcao_id"
'  sSql = sSql & "      JOIN sec_permissao_tb"
'  sSql = sSql & "        ON sec_permissao_tb.modulo_funcao_id = sec_modulo_funcao_tb.modulo_funcao_id"
'  sSql = sSql & " LEFT JOIN pessoal_tb"
'  sSql = sSql & "        ON sec_permissao_tb.pessoal_id = pessoal_tb.ID"
'  sSql = sSql & " LEFT JOIN sec_grupo_tb"
'  sSql = sSql & "        ON sec_permissao_tb.grupo_id = sec_grupo_tb.grupo_id"
'  sSql = sSql & "     WHERE sec_permissao_tb.Status = 's'"
'  sSql = sSql & "       AND sec_endereco_tb.endereco LIKE '%/" & Trim(recurso) & "/'"
'  sSql = sSql & "       AND (sec_grupo_tb.nome IN ("
'  sSql = sSql & "    SELECT sec_grupo_tb.nome"
'  sSql = sSql & "      FROM sec_grupo_tb"
'  sSql = sSql & "      JOIN sec_membro_tb"
'  sSql = sSql & "        ON sec_membro_tb.grupo_id = sec_grupo_tb.grupo_id"
'  sSql = sSql & "      JOIN pessoal_tb"
'  sSql = sSql & "        ON sec_membro_tb.pessoal_id = pessoal_tb.ID"
'  sSql = sSql & "     WHERE pessoal_tb.username = '" & usuario & "'"
'  sSql = sSql & "       AND pessoal_tb.status = 's')"
'  sSql = sSql & "        OR pessoal_tb.username IN ("
'  sSql = sSql & "    SELECT sec_grupo_tb.nome"
'  sSql = sSql & "      FROM sec_grupo_tb"
'  sSql = sSql & "      JOIN sec_membro_tb"
'  sSql = sSql & "        ON sec_membro_tb.grupo_id = sec_grupo_tb.grupo_id"
'  sSql = sSql & "      JOIN pessoal_tb"
'  sSql = sSql & "        ON sec_membro_tb.pessoal_id = pessoal_tb.ID"
'  sSql = sSql & "     WHERE pessoal_tb.username = '" & usuario & "'"
'  sSql = sSql & "       AND pessoal_tb.status = 's'))"
  
  sSql = "           select c.sigla_recurso, c.referencia"
  sSql = sSql & "      from segab_db..verifica_permissao_tb a,"
  sSql = sSql & "           segab_db..usuario_tb b,"
  sSql = sSql & "           segab_db..recurso_tb c"
  sSql = sSql & "      Where a.usuario_id = b.usuario_id"
  sSql = sSql & "      and a.recurso_id = c.recurso_id"
  sSql = sSql & "      and (c.sigla_recurso = '" & recurso & "' or c.referencia like '%" & recurso & "%')"
  sSql = sSql & "      and b.cpf = '" & usuario & "'"
  Call rs.Open(sSql, Conexao, adOpenStatic)
  If Not rs.EOF Then
    valida_permissao = True
  Else
    valida_permissao = False
  End If
    
  Exit Function
  
Trata_Erro:
  'colocado dessa forma para evitar erros em produ��o
  valida_permissao = False
End Function

Function ValidarPermissaoSegbr2(sDom�nio As String, sCpf As String, lEmpresaId As Long, lAmbienteId As Long, sRecurso As String) As Boolean

Dim rs       As New Recordset
Dim Conexao  As New Connection
Dim sSql     As String
Dim sConexao As String
Dim sServer  As String
Dim sBanco   As String
Dim sUsuario As String
Dim sSenha   As String

On Error GoTo Trata_Erro
    Dim lAmbienteId_anterior As Integer
  lAmbienteId_anterior = lAmbienteId
  
  If lAmbienteId = 6 Then
  lAmbienteId = 2
  ElseIf lAmbienteId = 7 Then
  lAmbienteId = 3
  End If
  
  
  'Le as variaveis de Conex�o
  sServer = LerArquivoIni2("Intranet", "Servidor", lAmbienteId)
  sBanco = LerArquivoIni2("Intranet", "Banco", lAmbienteId)
  sUsuario = LerArquivoIni2("Intranet", "Usuario", lAmbienteId)
  sSenha = LerArquivoIni2("Intranet", "Senha", lAmbienteId)
  
  'Monta a string de Conex�o
  sConexao = "Provider=SQLOLEDB; Data Source=" & sServer & ";Initial Catalog=" & sBanco & "; User Id=" & sUsuario & "; Password=" & sSenha
  sConexao = "UID=" & sUsuario & ";PWD=" & UCase(sSenha) & ";server=" & sServer & ";driver={SQL Server};database=" & sBanco & ";"
  
  Conexao.Open sConexao
  
  
  sSql = "       SELECT segab_db..recurso_tb.sigla_recurso"
  sSql = sSql & "  FROM segab_db..verifica_permissao_tb   "
  sSql = sSql & "  JOIN segab_db..usuario_tb"
  sSql = sSql & "    ON segab_db..verifica_permissao_tb.usuario_id = segab_db..usuario_tb.usuario_id"
  sSql = sSql & "  JOIN segab_db..recurso_tb"
  sSql = sSql & "    ON segab_db..recurso_tb.recurso_id = segab_db..verifica_permissao_tb.recurso_id"
  sSql = sSql & " WHERE segab_db..verifica_permissao_tb.unidade_usuario_id = " & lEmpresaId
  sSql = sSql & "   AND segab_db..recurso_tb.sigla_recurso = '" & sRecurso & "'"
  sSql = sSql & "   AND segab_db..usuario_tb.cpf = '" & sCpf & "'"
  sSql = sSql & "   AND segab_db..verifica_permissao_tb.ambiente_id = " & lAmbienteId
  
  Call rs.Open(sSql, Conexao, adOpenStatic)
  If Not rs.EOF Then
    ValidarPermissaoSegbr2 = True
  Else
    ValidarPermissaoSegbr2 = False
  End If
  lAmbienteId = lAmbienteId_anterior
  Exit Function
  
Trata_Erro:
  'colocado dessa forma para evitar erros em produ��o
  ValidarPermissaoSegbr2 = True
  lAmbienteId = lAmbienteId_anterior
End Function

