VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsXXXX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub IncluirTipoPostagem(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal lAmbienteId As Long, _
                               ByVal iTpPostagemId As Integer, _
                               ByVal sNome As String, _
                               ByVal sContratoPostagem As String, _
                               ByVal sControleRetorno As String, _
                               ByVal sUsuario As String)
                               


Dim sSql As String
Dim rs As Recordset

On Error GoTo Trata_Erro

    sSql = sSql & "EXEC evento_seguros_db.dbo.tp_postagem_spi "
    sSql = sSql & iTpPostagemId
    sSql = sSql & ",'" & sNome & "'"
    sSql = sSql & ",'" & sContratoPostagem & "'"
    sSql = sSql & ",'" & sControleRetorno & "'"
    sSql = sSql & ",'" & sUsuario & "'"
   
   
   
   
    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
    
    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls1111.IncluirTipoPostagem - " & Err.Description)

End Sub

Public Sub AlterarTipoPostagem(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal lAmbienteId As Long, _
                               ByVal iTpPostagemId As Integer, _
                               ByVal sNome As String, _
                               ByVal sContratoPostagem As String, _
                               ByVal sControleRetorno As String, _
                               ByVal sUsuario As String)

Dim sSql As String

On Error GoTo TrataErro

    sSql = "EXEC evento_seguros_db.dbo.Tp_Postagem_spu "
    sSql = sSql & iTpPostagemId & ","
    sSql = sSql & "'" & sNome & "',"
    sSql = sSql & "'" & sContratoPostagem & "',"
    sSql = sSql & "'" & sControleRetorno & "',"
    sSql = sSql & "'" & sUsuario & "'"
    
    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
    
    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls1111.AlterarTipoPostagem - " & Err.Description)

End Sub



Public Function ConsultarTipoPostagem(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      Optional ByVal sConteudo As String, _
                                      Optional ByVal iPesquisa As Integer = -1) As Object


Dim sSql As String

On Error GoTo TrataErro
      
    sSql = ""
    sSql = sSql & "SELECT * " & vbNewLine
    sSql = sSql & "  FROM evento_seguros_db..tp_postagem_tb WITH(nolock) " & vbNewLine
                      
        
    If iPesquisa = 0 Then  'tp_postagem_id
        sSql = sSql & " WHERE tp_postagem_id = " & sConteudo & vbNewLine
        sSql = sSql & " ORDER BY tp_postagem_id"
    ElseIf iPesquisa = 1 Then  'nome
        sSql = sSql & " WHERE nome = '" & sConteudo & "'" & vbNewLine
        sSql = sSql & " ORDER BY nome"
    Else
        sSql = sSql & " ORDER BY nome"
    End If
    
    Set ConsultarTipoPostagem = ExecutarSQL(sSiglaSistema, _
                                      iAmbienteId, _
                                      sSiglaRecurso, _
                                      sDescricaoRecurso, _
                                      sSql, _
                                      True)

    Exit Function
    
TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00XXX.ConsultarTipoPostagem - " & Err.Description)

End Function


Public Sub ExcluirTipoPostagem(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer, _
                              ByVal iTpPostagemId As Integer)

Dim sSql As String

On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC evento_seguros_db.dbo.Tp_Postagem_spd " & iTpPostagemId

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
 
    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00166.ExcluirEvento - " & Err.Description)

End Sub


Public Sub IncluirContratoPostagem(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteId As Long, _
                                   ByVal iFaixaInicial As Integer, _
                                   ByVal iFaixaFinal As Integer, _
                                   ByVal iTpPostagemId As Integer, _
                                   ByVal sUsuario As String)

Dim sSql As String
Dim rs As Recordset

On Error GoTo Trata_Erro

    sSql = "EXEC evento_seguros_db.dbo.Contrato_Postagem_spi "
    sSql = sSql & iFaixaInicial & ","
    sSql = sSql & iFaixaFinal & ","
    sSql = sSql & iTpPostagemId & ","
    sSql = sSql & "'" & sUsuario & "'"
   
    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
    
    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls1111.IncluirTipoPostagem - " & Err.Description)

End Sub

Public Sub AlterarContratoPostagem(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteId As Long, _
                                   ByVal iContratoPostagemId As Integer, _
                                   ByVal iFaixaInicial As Integer, _
                                   ByVal iFaixaFinal As Integer, _
                                   ByVal iTpPostagemId As Integer, _
                                   ByVal sUsuario As String)
                                                     
                                                           

Dim sSql As String

On Error GoTo TrataErro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC evento_seguros_db.dbo.Contrato_Postagem_spu "
    sSql = sSql & iContratoPostagemId & ","
    sSql = sSql & iFaixaInicial & ","
    sSql = sSql & iFaixaFinal & ","
    sSql = sSql & iTpPostagemId & ","
    sSql = sSql & "'" & sUsuario & "'"
        
    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
    
    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls1111.AlterarTipoPostagem - " & Err.Description)

End Sub


Public Function ConsultarContratoPostagem(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal iAmbienteId As Integer, _
                                          ByVal sConteudo As String, _
                                          ByVal iPesquisa As Integer) As Object

Dim sSql As String

On Error GoTo TrataErro
      
    sSql = ""
    sSql = sSql & "SELECT contrato_postagem_tb.contrato_postagem_id, " & vbNewLine
    sSql = sSql & "       contrato_postagem_tb.contrato_postagem_atual, " & vbNewLine
    sSql = sSql & "       contrato_postagem_tb.tp_postagem_id, " & vbNewLine
    sSql = sSql & "       contrato_postagem_tb.faixa_inicial, " & vbNewLine
    sSql = sSql & "       contrato_postagem_tb.faixa_final, " & vbNewLine
    sSql = sSql & "       tp_postagem_tb.nome " & vbNewLine
    sSql = sSql & "  FROM evento_seguros_db..contrato_postagem_tb contrato_postagem_tb WITH(nolock) "
    sSql = sSql & "  JOIN evento_seguros_db..tp_postagem_tb tp_postagem_tb WITH(nolock) "
    sSql = sSql & "    ON tp_postagem_tb.tp_postagem_id = contrato_postagem_tb.tp_postagem_id "
    
    If iPesquisa = 0 Then  'contrato_postagem_id
        sSql = sSql & "WHERE contrato_postagem_tb.contrato_postagem_id = " & sConteudo & vbNewLine
    ElseIf iPesquisa = 1 Then  'tp_postagem_id
        sSql = sSql & "WHERE contrato_postagem_tb.tp_postagem_id = " & sConteudo & vbNewLine
    End If
    
    sSql = sSql & " ORDER BY contrato_postagem_tb.contrato_postagem_id"
    
    Set ConsultarContratoPostagem = ExecutarSQL(sSiglaSistema, _
                                                iAmbienteId, _
                                                sSiglaRecurso, _
                                                sDescricaoRecurso, _
                                                sSql, _
                                                True)

    Exit Function
    
TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00XXX.ConsultarContratoPostagem - " & Err.Description)

End Function


Public Sub ExcluirContratoPostagem(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal iContratoPostagemId As Integer)

Dim sSql As String

On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC evento_seguros_db.dbo.contrato_Postagem_spd " & iContratoPostagemId

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
 
    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00XXX.ExcluirContratoPostagem - " & Err.Description)

End Sub

Public Function ConsultarPostagemDocumento(ByVal sSiglaSistema As String, _
                                           ByVal sSiglaRecurso As String, _
                                           ByVal sDescricaoRecurso As String, _
                                           ByVal iAmbienteId As Integer, _
                                           ByVal sContratoPostagemId As String) As Object

Dim sSql As String

On Error GoTo TrataErro
      
    sSql = ""
    sSql = sSql & "SELECT contrato_postagem_documento_tb.dt_inicio_vigencia, " & vbNewLine
    sSql = sSql & "       documento_tb.documento_id, " & vbNewLine
    sSql = sSql & "       documento_tb.nome , " & vbNewLine
    sSql = sSql & "       documento_tb.origem, " & vbNewLine
    sSql = sSql & "       documento_tb.tp_documento_id, " & vbNewLine
    sSql = sSql & "       tp_documento_tb.nome as tipo_documento " & vbNewLine
    sSql = sSql & "  FROM evento_seguros_db..contrato_postagem_documento_tb contrato_postagem_documento_tb WITH(nolock) " & vbNewLine
    sSql = sSql & "  JOIN evento_seguros_db..documento_tb documento_tb WITH(nolock) " & vbNewLine
    sSql = sSql & "    ON contrato_postagem_documento_tb.documento_id = documento_tb.documento_id " & vbNewLine
    sSql = sSql & "  LEFT JOIN controle_projeto_db..tp_documento_tb tp_documento_tb WITH(nolock) " & vbNewLine
    sSql = sSql & "    ON documento_tb.tp_documento_id = tp_documento_tb.tp_documento_id " & vbNewLine
    sSql = sSql & " WHERE contrato_postagem_documento_tb.contrato_postagem_id = " & sContratoPostagemId & vbNewLine
    sSql = sSql & "   AND contrato_postagem_documento_tb.dt_fim_vigencia IS NULL " & vbNewLine
    sSql = sSql & " ORDER BY contrato_postagem_documento_tb.dt_inicio_vigencia, " & vbNewLine
    sSql = sSql & "       documento_tb.nome "
    
    Set ConsultarPostagemDocumento = ExecutarSQL(sSiglaSistema, _
                                                 iAmbienteId, _
                                                 sSiglaRecurso, _
                                                 sDescricaoRecurso, _
                                                 sSql, _
                                                 True)

    Exit Function
    
TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00XXX.ConsultarPostagemDocumento - " & Err.Description)

End Function

Public Sub ExcluirContratoPostagemDocumento(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal iAmbienteId As Integer, _
                                            ByVal sDocumentoId As String, _
                                            ByVal sDtInicioVigencia As String, _
                                            ByVal sContratoPostagemId As String, _
                                            ByVal sDtFimVigencia As String, _
                                            ByVal sUsuario As String)

Dim sSql As String

On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC evento_seguros_db.dbo.contrato_Postagem_documento_spd "
    sSql = sSql & sDocumentoId
    sSql = sSql & ",'" & Format(sDtInicioVigencia, "yyyymmdd") & "'"
    sSql = sSql & "," & sContratoPostagemId
    sSql = sSql & ",'" & Format(sDtFimVigencia, "yyyymmdd") & "'"
    sSql = sSql & ",'" & sUsuario & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
 
    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00XXX.ExcluirContratoPostagem - " & Err.Description)

End Sub

Public Sub IncluirPropostaPostagem(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteId As Long, _
                                   ByVal iDocumentoId As Integer, _
                                   ByVal lPropostaId As Long, _
                                   ByVal iContratoPostagemId As Integer, _
                                   ByVal iNumeroPostagem As Integer, _
                                   ByVal sUsuario As String)

Dim sSql As String
Dim rsPostagem As Object

On Error GoTo Trata_Erro

    sSql = sSql & "EXEC evento_seguros_db.dbo.proposta_postagem_spi "
    sSql = sSql & iDocumentoId
    sSql = sSql & "," & lPropostaId
    sSql = sSql & "," & iContratoPostagemId
    sSql = sSql & "," & iNumeroPostagem
    sSql = sSql & ",'" & sUsuario & "'"
   
    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
    
    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls1111.IncluirPropostaPostagem - " & Err.Description)

End Sub


Public Function ConsultarLimitePostagem(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal iDocumentoId As Integer) As Object

Dim sSql As String

On Error GoTo TrataErro
      
    sSql = ""
    sSql = sSql & " SELECT TOP 1 ISNULL(contrato_postagem_tb.contrato_postagem_atual + 1, 0) as contrato_postagem_atual, " & vbNewLine
    sSql = sSql & "        contrato_postagem_tb.contrato_postagem_id, " & vbNewLine
    sSql = sSql & "        contrato_postagem_tb.faixa_final " & vbNewLine
    sSql = sSql & "   FROM evento_seguros_db..contrato_postagem_tb contrato_postagem_tb " & vbNewLine
    sSql = sSql & "   JOIN evento_seguros_db..contrato_postagem_documento_tb contrato_postagem_documento_tb " & vbNewLine
    sSql = sSql & "     ON contrato_postagem_tb.contrato_postagem_id = contrato_postagem_documento_tb.contrato_postagem_id " & vbNewLine
    sSql = sSql & "  WHERE contrato_postagem_documento_tb.documento_id = " & iDocumentoId & vbNewLine
    sSql = sSql & "    AND contrato_postagem_documento_tb.dt_fim_vigencia IS NULL " & vbNewLine
    
    Set ConsultarLimitePostagem = ExecutarSQL(sSiglaSistema, _
                                              iAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSql, _
                                              True)

    Exit Function
    
TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00XXX.ConsultarLimitePostagem - " & Err.Description)

End Function

Public Function ConsultarDocumento(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer) As Object

Dim sSql As String

On Error GoTo TrataErro
      
    sSql = ""
    sSql = sSql & "SELECT documento_id, " & vbNewLine
    sSql = sSql & "       nome " & vbNewLine
    sSql = sSql & "  FROM evento_seguros_db..documento_tb WITH(nolock) " & vbNewLine
    sSql = sSql & " ORDER BY nome"
    
    Set ConsultarDocumento = ExecutarSQL(sSiglaSistema, _
                                         iAmbienteId, _
                                         sSiglaRecurso, _
                                         sDescricaoRecurso, _
                                         sSql, _
                                         True)

    Exit Function
    
TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00XXX.ConsultarDocumento - " & Err.Description)

End Function


Public Sub IncluirContratoPostagemDocumento(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal lAmbienteId As Long, _
                                            ByVal iDocumentoId As Integer, _
                                            ByVal iContratoPostagemId As Integer, _
                                            ByVal sDataSistema As String, _
                                            ByVal sUsuario As String)

Dim sSql As String

On Error GoTo Trata_Erro

    sSql = sSql & "EXEC evento_seguros_db.dbo.contrato_postagem_documento_spi "
    sSql = sSql & iDocumentoId
    sSql = sSql & "," & iContratoPostagemId
    sSql = sSql & ",'" & Format(sDataSistema, "yyyymmdd") & "'"
    sSql = sSql & ",'" & sUsuario & "'"
   
    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)
    
    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls1111.IncluirPropostaPostagem - " & Err.Description)

End Sub
