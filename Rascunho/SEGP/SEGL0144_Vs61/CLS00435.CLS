VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00435"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public mvarSiglaSistema As String
Public mvarSiglaRecurso As String
Public mvarDescricaoRecurso As String
Public mvarAmbienteId As Integer
Public mvarUsuario As String
Public mvarAmbiente As String

'asouza
Public Sub CriarTemporariaAvisoSinistro(ByVal lConexao As Long, _
                                        ByVal oSABL0100 As Object, _
                                        ByVal sTabela As String)
Dim sSQL As String
Dim Rs As Recordset

    Set goSABL0100 = oSABL0100
    glConexao = lConexao
    
    sSQL = ""
    sSQL = sSQL & "CREATE TABLE " & sTabela & " (proposta_id NUMERIC(9)," & vbNewLine
    sSQL = sSQL & "                              cod_objeto_segurado INT, " & vbNewLine
    sSQL = sSQL & "                              dt_ocorrencia SMALLDATETIME, " & vbNewLine
    sSQL = sSQL & "                              dt_aviso_sinistro SMALLDATETIME," & vbNewLine
    sSQL = sSQL & "                              tp_aviso VARCHAR(10)," & vbNewLine
    sSQL = sSQL & "                              val_is NUMERIC(15,2))" & vbNewLine
    
    Call goSABL0100.ExecutarSQL(mvarSiglaSistema, mvarAmbiente, mvarSiglaRecurso, mvarDescricaoRecurso, sSQL, lConexao, "0", False)
End Sub

Public Sub CriarTemporariaQuestionario(ByVal lConexao As Long, _
                                       ByVal oSABL0100 As Object, _
                                       ByVal sTabela As String)

Dim sSQL As String
Dim Rs As Recordset

    Set goSABL0100 = oSABL0100

    sSQL = ""
    sSQL = sSQL & "CREATE TABLE " & sTabela & " (pergunta_id INT," & vbNewLine
    sSQL = sSQL & "                              questionario_id INT," & vbNewLine
    sSQL = sSQL & "                              dominio_resposta_id INT," & vbNewLine
    sSQL = sSQL & "                              texto_resposta VARCHAR(100))"
      
    Call goSABL0100.ExecutarSQL(mvarSiglaSistema, mvarAmbiente, mvarSiglaRecurso, mvarDescricaoRecurso, sSQL, lConexao, "0", False)

End Sub


Public Sub IncluirRegistroTemporariaAviso(ByVal sDtInicioOcorrencia As String, _
                                          ByVal sDtAvisoSinistro As String, _
                                          ByVal lTpAvisoId As Long, _
                                          ByVal sUsuario As String, _
                                          ByVal dProposta_id As Double, _
                                          ByVal Cod_Obj_segurado As Integer, _
                                          ByVal sTabela As String, _
                                          ByVal oSABL0100 As Object, _
                                          ByVal lConexao As Long)

Dim sSQL As String
Dim Rs As Recordset

    Set goSABL0100 = oSABL0100
    
    
    sSQL = ""
    sSQL = sSQL & "INSERT INTO " & sTabela & "(proposta_id, " & vbNewLine
    sSQL = sSQL & "                            cod_objeto_segurado , " & vbNewLine
    sSQL = sSQL & "                            dt_ocorrencia , " & vbNewLine
    sSQL = sSQL & "                            dt_aviso_sinistro," & vbNewLine
    sSQL = sSQL & "                            tp_aviso)" & vbNewLine
    sSQL = sSQL & "     VALUES('" & dProposta_id & "'," & vbNewLine
    sSQL = sSQL & "              '" & Cod_Obj_segurado & "'," & vbNewLine
    sSQL = sSQL & "             '" & sDtInicioOcorrencia & "'," & vbNewLine
    sSQL = sSQL & "            '" & sDtAvisoSinistro & "'," & vbNewLine
    sSQL = sSQL & "            " & lTpAvisoId & ")" & vbNewLine

    Call goSABL0100.ExecutarSQL(mvarSiglaSistema, mvarAmbiente, mvarSiglaRecurso, mvarDescricaoRecurso, sSQL, lConexao, "0", False)


End Sub

Public Sub AlterandoValoresTemporariaAviso(ByVal cValIs As Currency, _
                                           ByVal sUsuario As String, _
                                           ByVal sTabela As String, _
                                           ByVal oSABL0100 As Object)
Dim sSQL As String
Dim Rs As Recordset
    On Error GoTo erro
    
    Set goSABL0100 = oSABL0100
    
    sSQL = ""
    sSQL = sSQL & "UPDATE " & sTabela & vbNewLine
    sSQL = sSQL & "    SET val_is = " & Replace(cValIs, ",", ".") & vbNewLine
   
    
    Call goSABL0100.ExecutarSQL(mvarSiglaSistema, mvarAmbiente, mvarSiglaRecurso, mvarDescricaoRecurso, sSQL, glConexao, "0", False)
    
Exit Sub
erro:

    Call Err.Raise(Err.Number, , "SEGL0144.cls00435.AlterandoValoresTemporariaAviso - " & Err.Description & vbNewLine & "�ltimo sql: " & sSQL)
End Sub

'asouza
Public Sub LimparDadosTemporaria(ByVal sTabela As String, _
                                 ByVal sUsuario As String)

Dim sSQL As String

    sSQL = ""
    sSQL = sSQL & "DELETE FROM" & sTabela
    sSQL = sSQL & "WHERE usuario = '" & sUsuario & "'"
    
    Call ExecutarSQL(mvarSiglaSistema, _
                     mvarAmbienteId, _
                     mvarSiglaRecurso, _
                     mvarDescricaoRecurso, _
                     sSQL, _
                     False)
End Sub


'Public Sub RemoverTemporaria(ByVal sTabela As String)
'
'Dim sSQL As String
'
'    sSQL = ""
'    sSQL = sSQL & "DROP TABLE " & sTabela
'
'    Call ExecutarSQL(mvarSiglaSistema, _
'                     mvarAmbienteId, _
'                     mvarSiglaRecurso, _
'                     mvarDescricaoRecurso, _
'                     sSQL, _
'                     False)
'End Sub
