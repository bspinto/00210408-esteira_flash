VERSION 5.00
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GridAlianca.ocx"
Begin VB.Form frmAgencia 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7170
   ClientLeft      =   8565
   ClientTop       =   1125
   ClientWidth     =   9330
   Icon            =   "frmAgencia.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7170
   ScaleWidth      =   9330
   Begin GridAlianca.grdAlianca grdResult 
      Height          =   2655
      Left            =   120
      TabIndex        =   29
      Top             =   1440
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   4683
      BorderStyle     =   1
      AllowUserResizing=   3
      EditData        =   0   'False
      Highlight       =   1
      ShowTip         =   0   'False
      SortOnHeader    =   0   'False
      BackColor       =   -2147483643
      BackColorBkg    =   -2147483633
      BackColorFixed  =   -2147483633
      BackColorSel    =   -2147483635
      FixedCols       =   1
      FixedRows       =   1
      FocusRect       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      ForeColorFixed  =   -2147483630
      ForeColorSel    =   -2147483634
      GridColor       =   -2147483630
      GridColorFixed  =   12632256
      GridLine        =   1
      GridLinesFixed  =   2
      MousePointer    =   0
      Redraw          =   -1  'True
      Rows            =   2
      TextStyle       =   0
      TextStyleFixed  =   0
      Cols            =   2
      RowHeightMin    =   0
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   5220
      TabIndex        =   12
      Top             =   6660
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   8010
      TabIndex        =   14
      Top             =   6660
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   6615
      TabIndex        =   13
      Top             =   6660
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Height          =   1140
      Left            =   0
      TabIndex        =   24
      Top             =   0
      Width           =   9285
      Begin VB.ComboBox cboEstado 
         Height          =   315
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   593
         Width           =   870
      End
      Begin VB.ComboBox cboMunicipio 
         Enabled         =   0   'False
         Height          =   315
         Left            =   3645
         TabIndex        =   3
         Top             =   593
         Width           =   5010
      End
      Begin VB.CommandButton cmdBuscaAgencia 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   8670
         TabIndex        =   4
         Top             =   585
         Width           =   440
      End
      Begin VB.TextBox txtPesquisaAgencia 
         Height          =   330
         Left            =   165
         TabIndex        =   0
         Top             =   585
         Width           =   1500
      End
      Begin VB.CommandButton cmdPesquisarAgencia 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1680
         TabIndex        =   1
         Top             =   585
         Width           =   440
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         Height          =   195
         Left            =   2760
         TabIndex        =   28
         Top             =   360
         Width           =   540
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio"
         Height          =   195
         Left            =   3645
         TabIndex        =   27
         Top             =   360
         Width           =   705
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "C�digo da ag�ncia:"
         Height          =   195
         Left            =   165
         TabIndex        =   26
         Top             =   360
         Width           =   1380
      End
      Begin VB.Label Label3 
         Caption         =   "Pesquisa de Ag�ncia de Contato"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   30
         Width           =   2415
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Agencias"
      Height          =   3030
      Left            =   0
      TabIndex        =   23
      Top             =   1170
      Width           =   9285
   End
   Begin VB.Frame Frame4 
      Caption         =   "Ag�ncia de Contato"
      Enabled         =   0   'False
      Height          =   2355
      Left            =   0
      TabIndex        =   15
      Top             =   4230
      Width           =   9285
      Begin VB.TextBox txtBairro 
         Height          =   330
         Left            =   5130
         TabIndex        =   9
         Top             =   1170
         Width           =   4020
      End
      Begin VB.TextBox txtUF 
         Height          =   330
         Left            =   8595
         TabIndex        =   11
         Top             =   1845
         Width           =   555
      End
      Begin VB.TextBox txtMunicipio 
         Height          =   330
         Left            =   90
         TabIndex        =   10
         Top             =   1845
         Width           =   7620
      End
      Begin VB.TextBox txtEndereco 
         Height          =   330
         Left            =   90
         TabIndex        =   8
         Top             =   1170
         Width           =   4965
      End
      Begin VB.TextBox txtNomeAgencia 
         Height          =   330
         Left            =   3015
         TabIndex        =   7
         Top             =   495
         Width           =   6135
      End
      Begin VB.TextBox txtDV 
         Height          =   330
         Left            =   1620
         TabIndex        =   6
         Top             =   495
         Width           =   510
      End
      Begin VB.TextBox txtCodigoAgencia 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   90
         TabIndex        =   5
         Text            =   "0"
         Top             =   495
         Width           =   1455
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         Height          =   195
         Left            =   5130
         TabIndex        =   22
         Top             =   945
         Width           =   450
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "UF:"
         Height          =   195
         Left            =   8595
         TabIndex        =   21
         Top             =   1575
         Width           =   255
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio"
         Height          =   195
         Left            =   90
         TabIndex        =   20
         Top             =   1620
         Width           =   705
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         Height          =   195
         Left            =   90
         TabIndex        =   19
         Top             =   945
         Width           =   735
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Nome da ag�ncia"
         Height          =   195
         Left            =   3015
         TabIndex        =   18
         Top             =   270
         Width           =   1260
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "DV:"
         Height          =   195
         Left            =   1620
         TabIndex        =   17
         Top             =   270
         Width           =   270
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "C�digo da ag�ncia:"
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   270
         Width           =   1380
      End
   End
End
Attribute VB_Name = "frmAgencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'AKIO.OKUNO - cboMunicipio_Click - 17860335 - 10/06/2013
Private Sub cboMunicipio_Click()
    If cboMunicipio.ListIndex <> -1 And cboEstado.ListIndex = -1 Then
        cboEstado.Text = obtemEstado(cboMunicipio.ItemData(cboMunicipio.ListIndex), cboMunicipio.Text)
    End If

End Sub

Private Sub cmdContinuar_Click()

  With frmAviso
  Me.MousePointer = vbHourglass
  
      If Val(Me.txtCodigoAgencia.Text) <> 0 Then
           .txtCodigoAgencia.Text = txtCodigoAgencia.Text
           .txtDV.Text = txtDV.Text
           .txtNomeAgencia.Text = txtNomeAgencia.Text
           .txtEnderecoAgencia.Text = txtEndereco.Text
           .txtBairroAgencia.Text = txtBairro.Text
           .txtMunicipioAgencia.Text = txtMunicipio.Text
           .txtUFAgencia.Text = txtUF.Text
      Else
          Me.MousePointer = vbDefault
          MsgBox "� preciso especificar a agencia de contato!", vbInformation, "Aviso de Sinistro"
          txtPesquisaAgencia.SetFocus
          Exit Sub
      End If

  
  Me.MousePointer = vbDefault

  .SSTab1.Tab = 0
  End With
 
    Me.Hide
    If frmConsulta.grdResultadoPesquisa.Rows > 1 And bSemProposta = False Then
        Me.Hide
        frmAvisoPropostas.SelecionaPropostas
        If frmAvisoPropostas.grdResultadoPesquisa.Rows = 1 Then
            bSemProposta = True
            frmAvisoPropostas.Hide
            frmObjSegurado.Show
        Else
            frmAvisoPropostas.Show
        End If
    Else
        Me.Hide
        bSemProposta = True
        frmObjSegurado.Show
    End If
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call Sair(Me)
End Sub

Private Sub cmdVoltar_Click()
    Me.Hide
    frmSolicitante.Show
End Sub

Private Sub Form_Load()

'AKIO.OKUNO - INICIO - 17860335 - 26/06/2013
'    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2, "Produ��o", "Qualidade")
    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2 Or glAmbiente_id = 6, "Produ��o", "Qualidade")
'AKIO.OKUNO - FIM - 17860335 - 26/06/2013

    CentraFrm Me
    
    Dim SQL As String
    Dim Rs As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & "Select Estado                                        " & vbNewLine
    SQL = SQL & "  From Seguros_Db.Dbo.UF_Tb      UF_Tb with (NoLock) " & vbNewLine
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    With cboEstado
        .Clear
        If Not Rs.EOF Then
           While Not Rs.EOF
                 .AddItem Trim(Rs(0))
                 Rs.MoveNext
           Wend
        End If
        Rs.Close
    End With
'    Monta_cboEstado        'AKIO.OKUNO - 17860335 - 10/06/2013
'    Monta_GridAgencia
    
End Sub

Private Sub cboEstado_Click()
    If cboEstado.ListIndex = -1 Then Exit Sub
    Monta_cboMunicipio
End Sub


Private Sub cmdBuscaAgencia_Click()
    If cboMunicipio.ListIndex = -1 Then
        MsgBox "Escolha um munic�pio."
        cboEstado.SetFocus
        Exit Sub
    End If
    Monta_GridAgencia
End Sub

Private Sub montaGrid()
    With grdResult
        .Cols = 6
        
        .TextMatrix(0, 0) = ""
        .TextMatrix(0, 1) = "Agencia"
        .TextMatrix(0, 2) = "DV"
        .TextMatrix(0, 3) = "Nome"
        .TextMatrix(0, 4) = "Endere�o"
        .TextMatrix(0, 5) = "Bairro"
        
        .AutoFit H�brido
    End With
End Sub

Public Sub cmdPesquisarAgencia_Click()
    Dim Rst  As ADODB.Recordset
    Dim SQL As String
    
    Debug.Print "cmdPesquisarAgencia_Click"
    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss:ms")
    
    Screen.MousePointer = 11
    If txtPesquisaAgencia.Text <> "" Then
        
        SQL = ""
'AKIO.OKUNO - INICIO - 17860335 - 10/06/2013
'        SQL = " SELECT distinct a.agencia_id, nome = isnull(a.nome,''), estado = isnull(a.estado,''), a.prefixo, " _
'            & " dv_prefixo = isnull(a.dv_prefixo,0), endereco = isnull(a.endereco,''), " _
'            & " bairro = isnull(a.bairro,''),  'municipio' = m.nome " _
'            & " FROM agencia_tb a with (nolock), municipio_tb m (nolock)" _
'            & " WHERE a.municipio_id = m.municipio_id" _
'            & " AND  a.estado = m.estado" _
'            & " AND prefixo = " & txtPesquisaAgencia.Text
        montaGrid
        
        SQL = SQL & "Set NoCount On                                                                       " & vbNewLine
        SQL = SQL & "Declare @Agencia             Table                                                   " & vbNewLine
        SQL = SQL & "      ( Agencia_ID           Numeric(4)                                              " & vbNewLine
        SQL = SQL & "      , Nome                 VarChar(60)                                             " & vbNewLine
        SQL = SQL & "      , Estado               Char(2)                                                 " & vbNewLine
        SQL = SQL & "      , Prefixo              Char(4)                                                 " & vbNewLine
        SQL = SQL & "      , Dv_Prefixo           Char(1)                                                 " & vbNewLine
        SQL = SQL & "      , Endereco             VarChar(85)                                             " & vbNewLine
        SQL = SQL & "      , Bairro               VarChar(30)                                             " & vbNewLine
        SQL = SQL & "      , Municipio            VarChar(60)                                             " & vbNewLine
        SQL = SQL & "      , Municipio_ID         Numeric(3)                                              " & vbNewLine
        SQL = SQL & "   )                                                                                 " & vbNewLine
        SQL = SQL & "                                                                                     " & vbNewLine
        SQL = SQL & "Insert into @Agencia                                                                 " & vbNewLine
        SQL = SQL & "      ( Agencia_ID                                                                   " & vbNewLine
        SQL = SQL & "      , Nome                                                                         " & vbNewLine
        SQL = SQL & "      , Estado                                                                       " & vbNewLine
        SQL = SQL & "      , Prefixo                                                                      " & vbNewLine
        SQL = SQL & "      , Dv_Prefixo                                                                   " & vbNewLine
        SQL = SQL & "      , Endereco                                                                     " & vbNewLine
        SQL = SQL & "      , Bairro                                                                       " & vbNewLine
        SQL = SQL & "      , Municipio                                                                    " & vbNewLine
        SQL = SQL & "      , Municipio_ID                                                                 " & vbNewLine
        SQL = SQL & "   )                                                                                 " & vbNewLine
        SQL = SQL & "Select Agencia_ID                                                                    " & vbNewLine
        SQL = SQL & "      , IsNull(Nome, '')                                                             " & vbNewLine
        SQL = SQL & "      , IsNull(Estado, '')                                                           " & vbNewLine
        SQL = SQL & "      , Prefixo                                                                      " & vbNewLine
        SQL = SQL & "      , IsNull(Dv_Prefixo, '')                                                       " & vbNewLine
        SQL = SQL & "      , IsNull(Endereco, '')                                                         " & vbNewLine
        SQL = SQL & "      , IsNull(Bairro, '')                                                           " & vbNewLine
        SQL = SQL & "      , ''                                                                           " & vbNewLine
        SQL = SQL & "      , Municipio_ID                                                                 " & vbNewLine
        SQL = SQL & "  From Seguros_Db.Dbo.Agencia_Tb Agencia_Tb With (NoLock)                            " & vbNewLine
        SQL = SQL & " Where Prefixo                       = " & txtPesquisaAgencia.Text & "               " & vbNewLine
        SQL = SQL & "                                                                                     " & vbNewLine
        SQL = SQL & "Update @Agencia                                                                      " & vbNewLine
        SQL = SQL & "   Set Municipio                     = Municipio_Tb.Nome                             " & vbNewLine
        SQL = SQL & "  From Seguros_Db.Dbo.Municipio_Tb   Municipio_Tb With (NoLock)                      " & vbNewLine
        SQL = SQL & "  Join @Agencia                      Agencia                                         " & vbNewLine
        SQL = SQL & "    on Agencia.Municipio_ID          = Municipio_Tb.Municipio_ID                     " & vbNewLine
        SQL = SQL & "                                                                                     " & vbNewLine
        SQL = SQL & "                                                                                     " & vbNewLine
        SQL = SQL & "Select *                                                                             " & vbNewLine
        SQL = SQL & "  From @Agencia                                                                      " & vbNewLine
        'AKIO.OKUNO - FIM - 17860335 - 10/06/2013
            
        Set Rst = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        
        If Rst.EOF Then
            cboEstado.ListIndex = -1
            cboMunicipio.ListIndex = -1
            txtCodigoAgencia.Text = ""
            txtDV.Text = ""
            txtNomeAgencia.Text = ""
            txtEndereco.Text = ""
            txtBairro.Text = ""
            txtMunicipio.Text = ""
            txtUF.Text = ""
            Screen.MousePointer = 0
            Exit Sub
        Else
            cboEstado.ListIndex = -1
            cboMunicipio.ListIndex = -1
            txtCodigoAgencia.Text = Rst!prefixo
            txtDV.Text = Rst!dv_prefixo
            txtNomeAgencia.Text = UCase(Rst!Nome)
            txtEndereco.Text = UCase(Rst!Endereco)
            txtBairro.Text = UCase(Rst!Bairro)
            txtMunicipio.Text = UCase(Rst!Municipio)
            txtUF.Text = Rst!estado
        End If
        
    End If
    Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss:ms")
    Screen.MousePointer = 0
End Sub

'AKIO.OKUNO - Monta_cboEstado - 17860335 - 10/06/2013
'Private Sub Monta_cboEstado()
'Dim i As Integer
'
'    On Error GoTo TratarErro
'
'    Dim Rst  As ADODB.Recordset
'    Dim SQL As String
'    Screen.MousePointer = 11
'    SQL = "SELECT distinct estado " _
'        & "FROM seguros_db..municipio_tb with (nolock) " _
'        & "ORDER BY estado"
'    Set Rst = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
'
'    i = 1
'    With cboEstado
'        .Clear
'        If Not Rst.EOF Then
'           While Not Rst.EOF
'                 .AddItem Rst(0)
'                 .ItemData(.NewIndex) = i
'                 Rst.MoveNext
'                 i = i + 1
'           Wend
'        End If
'        Rst.Close
'    End With
'
'    Screen.MousePointer = 0
'    Exit Sub
'TratarErro:
'End Sub

Private Sub Monta_cboMunicipio()
    On Error GoTo TratarErro
    Dim Rst  As ADODB.Recordset
    Dim SQL As String
    
    Screen.MousePointer = 11
    SQL = "SELECT distinct municipio_id, nome " _
        & "FROM seguros_db..municipio_tb with (nolock) " _
        & "WHERE estado = '" & cboEstado.Text & "' " _
        & "ORDER BY nome"
    Set Rst = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    With cboMunicipio
        .Clear
        If Not Rst.EOF Then
           While Not Rst.EOF
                 .AddItem Rst(1)
                 .ItemData(.NewIndex) = Rst(0)
                 Rst.MoveNext
           Wend
           .Enabled = True
        Else
            .Enabled = False
        End If
        
        Rst.Close
        
    End With
    Screen.MousePointer = 0
    Exit Sub
TratarErro:
End Sub

'AKIO.OKUNO - Monta_GridAgencia - 17860335 - 10/06/2013
'Private Sub Monta_GridAgencia()
'    On Error GoTo TratarErro
'    Dim slinha As String
'    Dim Rst  As ADODB.Recordset
'    Dim SQL As String
'    Dim GridResposta As Boolean
'
'    Screen.MousePointer = 11
'    SQL = ""
'    If cboMunicipio.ListIndex <> -1 Then
'
'        SQL = " select distinct  prefixo Agencia, dv_prefixo DV, nome Nome,   endereco Endereco, bairro Bairro " _
'            & " from seguros_db..agencia_tb with (nolock) " _
'            & " WHERE municipio_id = " & cboMunicipio.ItemData(cboMunicipio.ListIndex) _
'            & " ORDER BY nome"
'    Else
'        SQL = " select 'Agencia' = '', " & vbCrLf
'        SQL = SQL & " 'DV' = '', "
'        SQL = SQL & " 'Nome      ' = '', " & vbCrLf
'        SQL = SQL & " 'Endereco' = '', " & vbCrLf
'        SQL = SQL & " 'Bairro' = '' " & vbCrLf
'
'    End If
'    Set Rst = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
'
'
'    GridResposta = grdResult.PopulaGrid(Rst)
'
'    Rst.Close
'    Screen.MousePointer = 0
'
'    Exit Sub
'TratarErro:
'End Sub

Private Sub Monta_GridAgencia()
    On Error GoTo TratarErro
    Dim rsRecordSet             As ADODB.Recordset
    Dim sSQL                    As String
    Dim GridResposta            As Boolean
    
    Debug.Print "SEGP0862 - Monta_GridAgencia"
    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss:ms")
    Screen.MousePointer = 11
    
    If cboMunicipio.ListIndex <> -1 Then
        
        'Raimundo - GPTI - 27/02/2009
        'Flow 812068
                
        sSQL = ""
        sSQL = sSQL & "Set NoCount On                                                                                       " & vbNewLine
        sSQL = sSQL & "Select Agencia                               = Prefixo                                               " & vbNewLine
        sSQL = sSQL & "     , Dv                                    = DV_Prefixo                                            " & vbNewLine
        sSQL = sSQL & "     , Nome                                  = Nome                                                  " & vbNewLine
        sSQL = sSQL & "     , Endereco                              = Endereco                                              " & vbNewLine
        sSQL = sSQL & "     , Bairro                                = Bairro                                                " & vbNewLine
        sSQL = sSQL & "  From Seguros_Db.Dbo.Agencia_Tb             Agencia_Tb with (NoLock)                                " & vbNewLine
        If cboMunicipio.ListIndex <> -1 Then
            sSQL = sSQL & " Where Municipio_ID                      = " & cboMunicipio.ItemData(cboMunicipio.ListIndex) & " " & vbNewLine
        End If
        
        sSQL = sSQL & " Order By Nome                                                                                       " & vbNewLine
                
        Set rsRecordSet = ExecutarSQL(gsSIGLASISTEMA _
                                    , glAmbiente_id _
                                    , App.Title _
                                    , App.FileDescription _
                                    , sSQL _
                                    , True)
        
        GridResposta = grdResult.PopulaGrid(rsRecordSet)
    
        rsRecordSet.Close
        Set rsRecordSet = Nothing
    End If
    
    Screen.MousePointer = 0
    Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss:ms")
    
    Exit Sub
TratarErro:
End Sub


Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub
Private Sub grdResult_Click()
    If grdResult.Rows >= 1 Then                             'AKIO.OKUNO - 17860335 - 11/06/2013
        If Trim(grdResult.TextMatrix(1, 1)) <> "" Then      'AKIO.OKUNO - 17860335 - 11/06/2013
            txtCodigoAgencia.Text = grdResult.TextMatrix(grdResult.Row, 1)
            txtDV.Text = grdResult.TextMatrix(grdResult.Row, 2)
            txtNomeAgencia.Text = grdResult.TextMatrix(grdResult.Row, 3)
            txtEndereco.Text = grdResult.TextMatrix(grdResult.Row, 4)
            txtBairro.Text = grdResult.TextMatrix(grdResult.Row, 5)
            txtMunicipio.Text = cboMunicipio.Text
            txtUF.Text = cboEstado.Text
        End If                                              'AKIO.OKUNO - 17860335 - 11/06/2013
    End If                                                  'AKIO.OKUNO - 17860335 - 11/06/2013
End Sub

Private Sub cmdLimparCampos_Click()
    txtCodigoAgencia.Text = ""
    txtDV.Text = ""
    txtNomeAgencia.Text = ""
    txtEndereco.Text = ""
    txtBairro.Text = ""
    txtMunicipio.Text = ""
    txtUF.Text = ""
    cboEstado.ListIndex = -1
    cboMunicipio.ListIndex = -1
    txtPesquisaAgencia.Text = ""
End Sub

Private Sub txtPesquisaAgencia_KeyPress(KeyAscii As Integer)
    On Error Resume Next        'AKIO.OKUNO - 17860335 - 20/06/2013
    If KeyAscii = 13 And txtPesquisaAgencia.Text <> "" Then
        SendKeys "{TAB}"
    End If
End Sub

