VERSION 5.00
Begin VB.Form frmSair 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0862 - Aviso de Sinistro pela CA"
   ClientHeight    =   1605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4785
   Icon            =   "frmSair.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1605
   ScaleWidth      =   4785
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSim 
      Caption         =   "Sim"
      Height          =   420
      Left            =   900
      TabIndex        =   3
      Top             =   1080
      Width           =   1275
   End
   Begin VB.CommandButton cmdNao 
      Caption         =   "N�o"
      Height          =   420
      Left            =   2295
      TabIndex        =   2
      Top             =   1080
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Height          =   870
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   4605
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "Deseja encerrar o programa sem gravar?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   1
         Top             =   360
         Width           =   4335
      End
   End
End
Attribute VB_Name = "frmSair"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Formulario As Form

Private Sub Form_Load()
    CentraFrm Me
End Sub

Private Sub cmdNao_Click()
    bRespostaSaida = False
    Unload Me
End Sub

Private Sub cmdSim_Click()
    bRespostaSaida = True
    Unload Me
End Sub
