VERSION 5.00
Begin VB.Form frmMotivoReanalise 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0862 "
   ClientHeight    =   3480
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9090
   Icon            =   "frmMotivoReanalise.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3480
   ScaleWidth      =   9090
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   4800
      TabIndex        =   5
      Top             =   2880
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   7590
      TabIndex        =   7
      Top             =   2880
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   6195
      TabIndex        =   6
      Top             =   2880
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Motivo de Rean�lise"
      Height          =   2535
      Left            =   0
      TabIndex        =   8
      Top             =   120
      Width           =   9045
      Begin VB.ComboBox cboMotivoReanalise 
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Text            =   "Combo1"
         Top             =   360
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   120
         MaxLength       =   70
         TabIndex        =   4
         Top             =   1950
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   120
         MaxLength       =   70
         TabIndex        =   3
         Top             =   1680
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   120
         MaxLength       =   70
         TabIndex        =   2
         Top             =   1395
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   120
         MaxLength       =   70
         TabIndex        =   1
         Top             =   1110
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   120
         MaxLength       =   70
         TabIndex        =   0
         Top             =   840
         Width           =   8775
      End
   End
End
Attribute VB_Name = "frmMotivoReanalise"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Motivo_reanalise_id As Integer
'INICIO - MU00416975 - Reabertura de Sinistro
    Public reanalise_id As String
    Public caption_original As String
    Public Ramo_reanalise As String
    Public Produto_reanalise As String
    Public Status_reanalise As String
    Public produtosreanaliseok As Boolean
'FIM  - MU00416975 - Reabertura de Sinistro - confitec - jose.viana
'

Private Sub cboMotivoReanalise_Click()
   'INICIO - MU00416975 - Reabertura de Sinistro
        txtMotivo(0).Text = ""
        txtMotivo(1).Text = ""
        txtMotivo(2).Text = ""
        txtMotivo(3).Text = ""
        txtMotivo(4).Text = ""
        txtMotivo(0).Enabled = False
        txtMotivo(1).Enabled = False
        txtMotivo(2).Enabled = False
        txtMotivo(3).Enabled = False
        txtMotivo(4).Enabled = False
        reanalise_id = 0
       ProdutoMotivoReanalise
    'FIM - MU00416975 - Reabertura de Sinistro
   If cboMotivoReanalise.ItemData(cboMotivoReanalise.ListIndex) = 3 Then
        txtMotivo(0).Enabled = True
        txtMotivo(1).Enabled = True
        txtMotivo(2).Enabled = True
        txtMotivo(3).Enabled = True
        txtMotivo(4).Enabled = True

    ElseIf produtosreanaliseok Then
    'INICIO - MU00416975 - Reabertura de Sinistro
            'Me.Caption = caption_original
        If (Trim(cboMotivoReanalise.Text) = "Reanalise devido a discord�ncia do indeferimento") Or (Trim(cboMotivoReanalise.Text) = "Reanalise devido a discord�ncia do pagamento de indeniza��o") Then
            SQL = ""
            SQL = SQL & "SELECT motivo_reanalise_sinistro_id "
            SQL = SQL & "FROM SEGUROS_DB.DBO.motivo_reanalise_sinistro_tb  with (nolock) "
            SQL = SQL & "WHERE descricao like '" & Trim(cboMotivoReanalise.Text) & "'"
            SQL = SQL & "and (tp_ramo_id = " & tipo_ramo & " or tp_ramo_id is null)"

            Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     True)
            If Not Rs.EOF Then
                reanalise_id = Rs(0)
                Rs.Close
           End If
        End If
    If cboMotivoReanalise.ItemData(cboMotivoReanalise.ListIndex) = IIf(IsNull(reanalise_id), 0, reanalise_id) Then
        Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro"
            If MsgBox("Existe argumenta��o para a discord�ncia?", vbExclamation + vbYesNo, "Informa��o") = vbYes Then
                txtMotivo(0).Enabled = True
                txtMotivo(1).Enabled = True
                txtMotivo(2).Enabled = True
                txtMotivo(3).Enabled = True
                txtMotivo(4).Enabled = True
           End If
    End If
' FIM - MU00416975 - Reabertura de Sinistro

    Else
        txtMotivo(0).Enabled = False
        txtMotivo(1).Enabled = False
        txtMotivo(2).Enabled = False
        txtMotivo(3).Enabled = False
        txtMotivo(4).Enabled = False
        
    End If
End Sub

Private Sub cmdContinuar_Click()
    Dim indice As Integer
    Dim texto As String
    If cboMotivoReanalise.ListIndex = -1 Then
   
        MsgBox "� preciso especificar o motivo de rean�lise", vbCritical, "Aviso de Sinistro"
        cboMotivoReanalise.SetFocus
        Exit Sub
    End If
    
    If cboMotivoReanalise.ItemData(cboMotivoReanalise.ListIndex) = 3 And _
        Trim(txtMotivo(0).Text) = "" And _
        Trim(txtMotivo(1).Text) = "" And _
        Trim(txtMotivo(2).Text) = "" And _
        Trim(txtMotivo(3).Text) = "" And _
        Trim(txtMotivo(4).Text) = "" Then
            MsgBox "� preciso descrever o motivo de reanalise, caso seja escolhido a op��o Outros.", vbCritical, "Aviso de Sinistro"
            txtMotivo(0).SetFocus
            Exit Sub
    End If
    
    Motivo_reanalise_id = cboMotivoReanalise.ItemData(cboMotivoReanalise.ListIndex)
    Me.Hide

    If cboMotivoReanalise.ListIndex <> -1 And cboMotivoReanalise.ListIndex <> 3 Then
        Tamanho = Int(Len(Trim(cboMotivoReanalise.Text)) / 70)
        Dim posic As Integer
        Dim i As Integer
        posic = 1
        For i = 0 To Tamanho
            frmAviso.txtExigencia(i).Text = Mid(Trim(cboMotivoReanalise.Text), posic, 70)
            posic = posic + 70
            texto = texto & frmAviso.txtExigencia(i).Text & " "
        Next
    Else
        frmAviso.txtExigencia(0).Text = Trim(txtMotivo(0).Text)
        frmAviso.txtExigencia(1).Text = Trim(txtMotivo(1).Text)
        frmAviso.txtExigencia(2).Text = Trim(txtMotivo(2).Text)
        frmAviso.txtExigencia(3).Text = Trim(txtMotivo(3).Text)
        frmAviso.txtExigencia(4).Text = Trim(txtMotivo(4).Text)
    End If
    
'INICIO - MU00416975 - Reabertura de Sinistro - inc 02/04/2018
    If produtosreanaliseok Then
        frmAviso.txtExigencia(0).Text = Trim(txtMotivo(0).Text)
        frmAviso.txtExigencia(1).Text = Trim(txtMotivo(1).Text)
        frmAviso.txtExigencia(2).Text = Trim(txtMotivo(2).Text)
        frmAviso.txtExigencia(3).Text = Trim(txtMotivo(3).Text)
        frmAviso.txtExigencia(4).Text = Trim(txtMotivo(4).Text)
    End If
    'FIM - MU00416975 - Reabertura de Sinistro
    
    indice = getIndiceProposta(frmObjSegurado.grdPropostas.TextMatrix(frmObjSegurado.grdPropostas.RowSel, 1))
    Propostas.Item(indice).MotivoReanalise = Motivo_reanalise_id
    Propostas.Item(indice).TextoReanalise = texto
    'frmObjSegurado.grdDescrObjSegurado.SetFocus
    frmObjSegurado.cmdAdicionar.Enabled = True
    frmObjSegurado.Show
    
    
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call Sair(Me)
End Sub

Private Sub cmdVoltar_Click()

    frmObjSegurado.Show
    
    Me.Hide
    cboMotivoReanalise.ListIndex = 0
        txtMotivo(0).Text = ""
        txtMotivo(1).Text = ""
        txtMotivo(2).Text = ""
        txtMotivo(3).Text = ""
        txtMotivo(4).Text = ""
        reanalise_id = 0
End Sub

Private Sub Form_Load()
'AKIO.OKUNO - INICIO - 17860335 - 26/06/2013
'    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2, "Produ��o", "Qualidade")
    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2 Or glAmbiente_id = 6, "Produ��o", "Qualidade")
'AKIO.OKUNO - FIM - 17860335 - 26/06/2013
  
    CentraFrm Me
    
    Call CarregaCombo
End Sub

Private Sub CarregaCombo()
Dim Rs As Recordset

    Set Rs = ObtemMotivosReanalise(tipo_ramo)
    
    With cboMotivoReanalise
        .Clear
        If Not Rs.EOF Then
           While Not Rs.EOF
                 .AddItem Trim(Rs(1))
                 .ItemData(.NewIndex) = Rs(0)
                 Rs.MoveNext
           Wend
        End If
        Rs.Close
    End With
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub

'INICIO - MU00416975 - Reabertura de Sinistro

Public Sub ProdutoMotivoReanalise()

        txtMotivo(0).Text = ""
        txtMotivo(1).Text = ""
        txtMotivo(2).Text = ""
        txtMotivo(3).Text = ""
        txtMotivo(4).Text = ""
        txtMotivo(0).Enabled = False
        txtMotivo(1).Enabled = False
        txtMotivo(2).Enabled = False
        txtMotivo(3).Enabled = False
        txtMotivo(4).Enabled = False
        reanalise_id = 0
'00421597 - Inclu�do produto 1240 ramo 3 - Seguro Agr�cula Pecu�rio - Evaldo Porto
'RAFAEL MARTINS C00149269 - EMISS�O RURAL NA ABS
'ADICIONADO PRODUTOS 230
If ( _
        ((Produto_reanalise = 8 Or Produto_reanalise = 300) And Ramo_reanalise = 30) _
        Or ((Produto_reanalise = 1210 Or Produto_reanalise = 1226 Or Produto_reanalise = 1227) And Ramo_reanalise = 77) _
        Or ((Produto_reanalise = 155 Or Produto_reanalise = 156) And Ramo_reanalise = 62) _
        Or (Produto_reanalise = 1204 And Ramo_reanalise = 1) _
        Or ((Produto_reanalise = 227 Or Produto_reanalise = 228 Or Produto_reanalise = 229 Or Produto_reanalise = 230 Or Produto_reanalise = 1152) And Ramo_reanalise = 2) _
        Or (Produto_reanalise = 701 And Ramo_reanalise = 7) _
        Or (Produto_reanalise = 1201 And Ramo_reanalise = 61) _
        Or (Produto_reanalise = 680 And Ramo_reanalise = 68) _
        Or (Produto_reanalise = 1240 And Ramo_reanalise = 3) _
) Then
    produtosreanaliseok = True
    frmAviso.produtosreanaliseok = True
Else
    produtosreanaliseok = False
End If
End Sub

'FIM - MU00416975 - Reabertura de Sinistro




