VERSION 5.00
Begin VB.Form frmMsgPgtoImediato 
   Caption         =   "Resultado da Classifica��o"
   ClientHeight    =   3480
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4665
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3480
   ScaleWidth      =   4665
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnDefault 
      Default         =   -1  'True
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   3600
      Width           =   135
   End
   Begin VB.CommandButton btnSim 
      Caption         =   "Sim"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Top             =   2760
      Width           =   1200
   End
   Begin VB.CommandButton btnNao 
      Caption         =   "N�o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3120
      TabIndex        =   1
      Top             =   2760
      Width           =   1200
   End
   Begin VB.Label lblVlIndenizar 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Valor a Indenizar: R$ 0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Width           =   4455
   End
   Begin VB.Label lblAceite 
      Alignment       =   2  'Center
      Caption         =   "Cliente aceitou o valor a indenizar?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      TabIndex        =   4
      Top             =   1800
      Width           =   3855
   End
   Begin VB.Label lblAviso 
      Alignment       =   2  'Center
      BackColor       =   &H0000FFFF&
      Caption         =   "O preju�zo apurado ficou abaixo da franquia estipulada na ap�lice. N�o haver� indeniza��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   720
      Left            =   150
      TabIndex        =   3
      Top             =   360
      Width           =   4395
   End
End
Attribute VB_Name = "frmMsgPgtoImediato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'20/05/2020 (ntedencia) 00210408-esteira_flash (inicio)
Private Sub btnNao_Click()
    Unload Me
    MsgPgtoImediato = 1
    iAceitaPgtoImediato = 0
End Sub

Private Sub btnSim_Click()
    Unload Me
    MsgPgtoImediato = 1
    iAceitaPgtoImediato = 1
End Sub
'20/05/2020 (ntedencia) 00210408-esteira_flash (Fim)

'20/05/2020 (ntedencia) 00210408-esteira_flash (fix - inicio)
Private Sub Form_Activate()
    btnDefault.SetFocus
End Sub
'20/05/2020 (ntedencia) 00210408-esteira_flash (fix - fim)
