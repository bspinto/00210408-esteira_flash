VERSION 5.00
Begin VB.Form frmClienteNaoEncontrado 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5715
   Icon            =   "frmClienteNaoEncontrado.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   5715
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAvisarSemProposta 
      Caption         =   "Avisar SEM Proposta"
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   990
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Height          =   870
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   5685
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "Proposta n�o localizada na base."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1095
         TabIndex        =   4
         Top             =   360
         Width           =   3525
      End
   End
   Begin VB.CommandButton cmdNovaPesquisa 
      Caption         =   "Nova Pesquisa"
      Height          =   420
      Left            =   1935
      TabIndex        =   1
      Top             =   990
      Width           =   1815
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   3870
      TabIndex        =   2
      Top             =   990
      Width           =   1815
   End
End
Attribute VB_Name = "frmClienteNaoEncontrado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAvisarSemProposta_Click()
    With frmDadosCliente
        Me.MousePointer = vbHourglass
        .txtPropostaBB = ""
        .mskCPF.Text = ""
        .txtNome.Text = ""
        bSemProposta = True
        .txtEndRisco.Text = ""
        .txtBairroRisco.Text = ""
        .txtCEP.Text = "_____-___"
        .cboUFRisco.ListIndex = -1
        .cmbMunicipio.Text = ""
        
        .Show
        Me.Hide
        Me.MousePointer = vbDefault
    End With
End Sub

Private Sub cmdNovaPesquisa_Click()
    frmConsulta.Show
    Me.Hide
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call Sair(Me)
End Sub

Private Sub Form_Load()

'AKIO.OKUNO - INICIO - 17860335 - 26/06/2013
'    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2, "Produ��o", "Qualidade")
    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2 Or glAmbiente_id = 6, "Produ��o", "Qualidade")
'AKIO.OKUNO - FIM - 17860335 - 26/06/2013

    CentraFrm Me

End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub

