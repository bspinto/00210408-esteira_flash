VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cProposta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private proposta_id     As String
Private produto_id      As Integer
Private ramo_id         As Integer
Private subramo_id      As Integer
Private situacao_prop   As String
Private tipo_aviso      As String
Private motivo_reanalise As Integer
Private texto_reanalise As String
Public Property Let Proposta(ByVal vData As String)
    proposta_id = vData
End Property
Public Property Get Proposta() As String
    Proposta = proposta_id
End Property
Public Property Let Produto(ByVal vData As Integer)
    produto_id = vData
End Property
Public Property Get Produto() As Integer
    Produto = produto_id
End Property
Public Property Let Ramo(ByVal vData As Integer)
    ramo_id = vData
End Property
Public Property Get Ramo() As Integer
    Ramo = ramo_id
End Property
Public Property Let SubRamo(ByVal vData As Integer)
    subramo_id = vData
End Property
Public Property Get SubRamo() As Integer
    SubRamo = subramo_id
End Property
Public Property Let SituacaoProposta(ByVal vData As String)
    situacao_prop = vData
End Property
Public Property Get SituacaoProposta() As String
    SituacaoProposta = situacao_prop
End Property
Public Property Let TipoAviso(ByVal vData As String)
    tipo_aviso = vData
End Property
Public Property Get TipoAviso() As String
    TipoAviso = tipo_aviso
End Property
Public Property Let MotivoReanalise(ByVal vData As Integer)
    motivo_reanalise = vData
End Property
Public Property Get MotivoReanalise() As Integer
    MotivoReanalise = motivo_reanalise
End Property
Public Property Let TextoReanalise(ByVal vData As String)
    texto_reanalise = vData
End Property
Public Property Get TextoReanalise() As String
    TextoReanalise = texto_reanalise
End Property
