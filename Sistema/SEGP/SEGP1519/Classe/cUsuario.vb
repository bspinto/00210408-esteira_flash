﻿Public Class cUsuario
    Private URL As String
    Private IP As String
    Private PASSWORD As String
    Private CPF As String
    Private USUARIO_ID As String
    Private LOGON_WEB As String
    Private LOGIN_REDE As String
    Private UNIDADE_ID As String
    Private NOME As String
    Private LOGIN As String
    Private AMBIENTE As String
    Public Property pURL() As String
        Get
            Return URL
        End Get
        Set(ByVal Value As String)
            URL = Value
        End Set
    End Property
    Public Property pIP() As String
        Get
            Return IP
        End Get
        Set(ByVal Value As String)
            IP = Value
        End Set
    End Property
    Public Property pPASSWORD() As String
        Get
            Return PASSWORD
        End Get
        Set(ByVal Value As String)
            PASSWORD = Value
        End Set
    End Property
    Public Property pCPF() As String
        Get
            Return CPF
        End Get
        Set(ByVal Value As String)
            CPF = Value
        End Set
    End Property
    Public Property pUSUARIO_ID() As String
        Get
            Return USUARIO_ID
        End Get
        Set(ByVal Value As String)
            USUARIO_ID = Value
        End Set
    End Property
    Public Property pLOGON_WEB() As String
        Get
            Return LOGON_WEB
        End Get
        Set(ByVal Value As String)
            LOGON_WEB = Value
        End Set
    End Property
    Public Property pLOGIN_REDE() As String
        Get
            Return LOGIN_REDE
        End Get
        Set(ByVal Value As String)
            LOGIN_REDE = Value
        End Set
    End Property
    Public Property pUNIDADE_ID() As String
        Get
            Return UNIDADE_ID
        End Get
        Set(ByVal Value As String)
            UNIDADE_ID = Value
        End Set
    End Property
    Public Property pNOME() As String
        Get
            Return NOME
        End Get
        Set(ByVal Value As String)
            NOME = Value
        End Set
    End Property
    Public Property pLOGIN() As String
        Get
            Return LOGIN
        End Get
        Set(ByVal Value As String)
            LOGIN = Value
        End Set
    End Property
    Public Property pAMBIENTE() As String
        Get
            Return AMBIENTE
        End Get
        Set(ByVal Value As String)
            AMBIENTE = Value
        End Set
    End Property
End Class
