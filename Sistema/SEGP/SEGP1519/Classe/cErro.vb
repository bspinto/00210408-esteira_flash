Public Class cErro

    Private _exiteErro As Boolean
    Public Property ErExisteErro() As Boolean
        Get
            Return _exiteErro
        End Get
        Set(ByVal value As Boolean)
            _exiteErro = value
        End Set
    End Property

    Private _metodo As String
    Public Property ErMetodo() As String
        Get
            Return _metodo
        End Get
        Set(ByVal value As String)
            _metodo = value
        End Set
    End Property


    Private _procedure As String
    Public Property ErProcedure() As String
        Get
            Return _procedure
        End Get
        Set(ByVal value As String)
            _procedure = value
        End Set
    End Property


    Private _descricao As String
    Public Property ErDescricao() As String
        Get
            Return _descricao
        End Get
        Set(ByVal value As String)
            _descricao = value
        End Set
    End Property


    Private _erroMsg As String
    Public Property ErErroMsg() As String
        Get
            Return _erroMsg
        End Get
        Set(ByVal value As String)
            _erroMsg = value
        End Set
    End Property

    Public Sub PopularErro(ByVal pMetodo As String, ByVal pProcedure As String, ByVal pErroMsg As String, ByVal pDescricao As String)
        If ErExisteErro = False Then
            ErExisteErro = True
            ErMetodo = pMetodo
            ErProcedure = pProcedure
            ErDescricao = pDescricao
            ErErroMsg = pErroMsg
        End If
    End Sub

    Public Function getErroMensagem() As String
        Dim str As String = "Erro! "
        str = str & ErDescricao
        If Not ErMetodo.ToString = "" Then
            str = str & "; Metodo: " & ErMetodo
        End If
        If Not ErProcedure.ToString = "" Then
            str = str & "; Procedure: " & ErProcedure
        End If
        If Not ErErroMsg.ToString = "" Then
            str = str & "; Erro: " & ErErroMsg
        End If

        Return str
    End Function

    ''' <summary>
    ''' Metodo para retornar o metodo atual em execucao do sistema
    ''' </summary>
    ''' <returns>uma string contendo o nome do metodo</returns>
    ''' <remarks></remarks>
    Function GetCurrentMethodName() As String
        Dim stack As New System.Diagnostics.StackFrame(1)
        Return stack.GetMethod().Name
    End Function

    Public Sub LimparErro()
        ErExisteErro = False
    End Sub

    Public Sub GravarArquivoDeLog(ByVal LocalDoExecutavel As String, ByVal sNomeSistema As String, Optional ByVal sErroMsg As String = "")
        Dim oFile As System.IO.StreamWriter
        Dim sArquivo As String = LocalDoExecutavel & "\Log_" & sNomeSistema & ".txt"
        sErroMsg = Format(Now, "dd-MM-yyyy HH:mm:ss") & " - " & sErroMsg

        If IO.File.Exists(sArquivo) Then
            oFile = My.Computer.FileSystem.OpenTextFileWriter(sArquivo, True)
        Else
            oFile = New IO.StreamWriter(sArquivo, True)
        End If

        oFile.WriteLine(sErroMsg)
        oFile.Close()
    End Sub

End Class
