Imports System.Data.SqlClient

Public Class cConect
    Public oNegocios As New cNegociosDAO

#Region "Configuração"

    Public Function GetEquipamentoAtivo() As DataTable
        Return oNegocios.GetEquipamentoAtivo()
    End Function


    Public Function GravarNovaVersao(ByVal pSql As String) As Boolean
        Return oNegocios.GravarNovaVersao(pSql)
    End Function

#End Region

End Class
