Attribute VB_Name = "AvisoSinistro1"
Public Type CoberturaAfetada
    tp_cobertura_id As String
    Descricao As String
    val_estimado As String
End Type

Public Type Aviso_Sinistro
    Sinistro As Sinistro
    cobertura_afetada() As CoberturaAfetada
    cobertura_atingida As Boolean
End Type

Public Type estimativa
 val_inicial As Double
 perc_estimativa As Double
 Descricao As String
 atinge As Boolean
End Type

Public Type Proposta_Avisada
   proposta_id As String
   sinistro_id As String
   produto As String
   Produto_id As Integer
   ramo_id As Integer
   subramo_id As Integer
End Type

Public RespostaSaida As Boolean
Public RsProposta As ADODB.Recordset

Public TIPO_RAMO As Integer
Public Tabelao(100) As String

Public propostasAvisadas As String 'para as propostas avisadas
Public propostasReanalise As String 'para as propostas preanalise
Public clientesPropostas As String 'para o cliente da proposta

Public numCon As Long 'para a conex�o com transacao

Public Avisos_Sinistros() As Aviso_Sinistro
Public Proposta_Avisada() As Proposta_Avisada 'para as informa��es finais das propostas avisadas

Public Evento_Segbr_id As Long
Public Localizacao As String

Public Const Evento_bb_id = 1100 'evento da CA

Public Const entidade_id = 12
Public Const banco_id = 1
Public Const canal_comunicacao = 3
Public Const processa_reintegracao_is = "N"
Public Const situacao_aviso = "N"

Public Sub Sair(Formulario As Form)
    SEGP0794_sair.Show vbModal
    If RespostaSaida = True Then
        End
    Else
        Formulario.Show
    End If
End Sub

Public Sub Main()
  Dim rs As ADODB.Recordset
  Dim sql As String
  
  If Not Trata_Parametros(Command) Then
    Call FinalizarAplicacao
  End If
    
    sql = "Select dt_operacional, "
    sql = sql & "status_sistema,  "
    sql = sql & "dt_contabilizacao "
    sql = sql & "from parametro_geral_tb "
    
     Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sql, _
                         True)
    If Not rs.EOF Then
        Data_Sistema = Format(rs(0), "dd/mm/yyyy")
    End If
    
    SEGP0794_1.Show
  
End Sub

Public Sub LimpaTudo()
Dim i As Long

 With SEGP0794_2
      .cboTipoPesquisa.ListIndex = -1
      .txtValorPesquisa.Text = ""
      .GrdResultadoPesquisa.Rows = 1
      .montacabecalhoGrid
      .GrdResultadoPesquisa.Rows = 2
      .GrdResultadoPesquisa.FixedRows = 1
 End With

 With SEGP0794_3
      .txtNomeSinistrado.Text = ""
      .mskCPF.Text = "___.___.___-__"
      .mskDtNascimento.Text = "__/__/____"
      .cboSexo.ListIndex = -1
      .mskDataOcorrencia.Text = "__/__/____"
      .cboEvento.ListIndex = -1
      For i = 0 To 9
        .txtExigencia(i).Text = ""
      Next
      .mskHora = "__:__"
      .txtLocal = ""
 End With
  
 With SEGP0794_4_1
      For i = 0 To 4
        .txtMotivo(i).Text = ""
      Next i
 End With
 
 With SEGP0794_5
      .txtNomeSolicitante.Text = ""
      .txtEnderecoSolicitante.Text = ""
      .txtBairroSolicitante.Text = ""
      .txtCEPSolicitante.Text = ""
      .txtEmailSolicitante.Text = ""
      .cmbGrauParentesco.ListIndex = -1
      .cmbMunicipio.ListIndex = -1
      .cmbUF.ListIndex = -1
      .municipio_id = 0
      For i = 0 To 4
        .mskDDD(i).Text = "__"
        .mskTelefone(i).Text = "____-____"
        .txtRamal(i).Text = ""
        .cmbTipoTelefone(i).ListIndex = -1
      Next i
 End With
 
 With SEGP0794_6
      .txtPesquisaAgencia.Text = ""
      .cmdPesquisarAgencia_Click
 End With

With SEGP0794_9
    .txtNomeSegurado.Text = ""
    .mskCPF.Text = "___.___.___-__"
    .mskDtNascimento.Text = "__/__/____"
    .cboSexo.ListIndex = -1
    .txtPropostaBB.Text = ""
End With

End Sub

Public Function getEstimativa(proposta As Long, _
                                tp_cobertura_id As Long, _
                                Evento_sinistro_id As Long, _
                                tp_componente_id As Integer, _
                                dtOcorrenciaSinistro As String) As estimativa
Dim sql As String
Dim rs As ADODB.Recordset
Dim ramo_id As Integer
Dim subramo_id As Long
Dim Produto_id As Integer

    'Seleciona as informa��es da proposta
    sql = ""
    sql = sql & " select a.ramo_id, b.subramo_id, b.produto_id "
    sql = sql & " from apolice_tb a with(nolock), proposta_tb b with(nolock)"
    sql = sql & " where a.proposta_id = b.proposta_id "
    sql = sql & " and b.proposta_id = " & proposta
    sql = sql & " union "
    sql = sql & " select a.ramo_id, b.subramo_id, b.produto_id"
    sql = sql & " from proposta_adesao_tb a with(nolock), proposta_tb b with(nolock)"
    sql = sql & " where a.proposta_id = b.proposta_id"
    sql = sql & " and b.proposta_id = " & proposta
                                  
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sql, _
                         True)
    If Not rs.EOF Then
        ramo_id = rs!ramo_id
        subramo_id = IIf(IsNull(rs!subramo_id), 0, rs!subramo_id)
        Produto_id = rs!Produto_id
    End If
    
    'Seleciona as informa��es da estimativa da cobertura
    sql = ""
    sql = sql & " select val_inicial, perc_estimativa, nome "
    sql = sql & " from produto_estimativa_sinistro_tb with(nolock), "
    sql = sql & " tp_cobertura_tb  "
    sql = sql & " where produto_id = " & Produto_id
    sql = sql & " and produto_estimativa_sinistro_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id"
    sql = sql & " and ramo_id = " & ramo_id
    sql = sql & " and subramo_id = " & subramo_id
    sql = sql & " and tp_componente_id = " & tp_componente_id
    sql = sql & " and evento_sinistro_id = " & Evento_sinistro_id
    sql = sql & " and produto_estimativa_sinistro_tb.tp_cobertura_id = " & tp_cobertura_id
    sql = sql & " and convert(char(8),dt_inicio_vigencia, 112) <= '" & dtOcorrenciaSinistro & "'"
    sql = sql & " and (convert(char(8), dt_fim_vigencia, 112) >= '" & dtOcorrenciaSinistro & "'"
    sql = sql & " or dt_fim_vigencia is null)"
    
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sql, _
                         True)
    
    If Not rs.EOF Then
         If Not IsNull(rs!val_inicial) Then
             getEstimativa.val_inicial = CDbl(rs!val_inicial)
         End If
         If Not IsNull(rs!perc_estimativa) Then
             getEstimativa.perc_estimativa = CDbl(rs!perc_estimativa)
         End If
         getEstimativa.Descricao = rs!NOME
         getEstimativa.atinge = True
    Else
         getEstimativa.val_inicial = CDbl("0.00")
         getEstimativa.perc_estimativa = CDbl("0.00")
         getEstimativa.Descricao = ""
         getEstimativa.atinge = False
    End If
    
End Function

Public Sub GravarCoberturas(evento_id As String, coberturas() As CoberturaAfetada, Evento_sinistro_id As Integer)
Dim i As Long
Dim sql As String
Dim rc As ADODB.Recordset

    For i = 1 To UBound(coberturas)
        sql = "exec evento_SEGBR_sinistro_cobertura_spi "
        sql = sql & evento_id & ","
        sql = sql & coberturas(i).tp_cobertura_id & ","
        sql = sql & "'" & IIf(Not IsNull(coberturas(i).val_estimado), TrocaVirgulaPorPonto(coberturas(i).val_estimado), "") & "',"
        sql = sql & Evento_sinistro_id & ","
        sql = sql & "'" & cUserName & "'"
                
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            sql, _
                            numCon, _
                            False)
    Next i
End Sub

Public Function descricao_tp_telefone(tp_telefone As String) As String

    Select Case tp_telefone
        Case "1"
            descricao_tp_telefone = "Residencial"
        Case "2"
            descricao_tp_telefone = "Comercial"
        Case "3"
            descricao_tp_telefone = "Celular"
        Case "4"
            descricao_tp_telefone = "Fax"
        Case "5"
            descricao_tp_telefone = "Recado"
        Case Else
            descricao_tp_telefone = ""
        End Select

End Function

Public Sub GravarTabelao(sin As Sinistro, coberturas() As CoberturaAfetada, cob_atingida As Boolean)
Dim i As Long
Dim sql As String
Dim qtd_cob As Long
Dim maxTabelao As Integer
Dim rc As ADODB.Recordset

    If sin.Ind_reanalise = "S" Then
    
        Tabelao(0) = "AVISO DE SINISTRO"
        Tabelao(1) = "-----------------"
        Tabelao(2) = ""
        Tabelao(3) = "SOLICITA��O DE REAN�LISE"
        Tabelao(4) = "--------------"
        Tabelao(5) = ""
        Tabelao(6) = "Motivo(s) apresentado(s):"
        For i = 7 To 11
            Tabelao(i) = SEGP0794_4_1.txtMotivo(i - 7).Text
        Next i
        
        Tabelao(12) = "Observa��es:"
        For i = 13 To 22
            Tabelao(i) = SEGP0794_3.txtExigencia(i - 13).Text
        Next i

        maxTabelao = 22
        
        
    Else
    
        Tabelao(0) = "AVISO DE SINISTRO"
        Tabelao(1) = "-----------------"
        Tabelao(2) = ""
        Tabelao(3) = "DADOS DO AVISO"
        Tabelao(4) = "--------------"
        Tabelao(5) = ""
        Tabelao(6) = "N�mero do Aviso Alian�a: " & sin.sinistro_id
        Tabelao(7) = "Data da comunica��o: " & Data_Sistema
        Tabelao(8) = "Aviso pela Central de Atendimento"
        Tabelao(9) = ""
        
        Tabelao(10) = "DADOS DO SEGURADO"
        Tabelao(11) = "Nome do Sinistrado: " & sin.SinistradoNomeCliente
        Tabelao(12) = "Tipo Sinistrado: " & IIf(sin.SeguradoComponente = 1, "Titular", "Conjuge")
        Tabelao(13) = "CPF/CGC: " & sin.SeguradoCPF_CNPJ
        Tabelao(14) = "C�digo do Produto AB: " & sin.Produto_id
        Tabelao(15) = "Nome do Produto AB: " & sin.NomeProduto
        Tabelao(16) = "Proposta BB: " & sin.PropostaBB
        Tabelao(17) = "Ramo: " & sin.ramo_id & " Ap�lice: " & sin.Apolice_id
        Tabelao(18) = ""
        
        Tabelao(19) = "OCORR�NCIA"
        Tabelao(20) = "----------"
        Tabelao(21) = ""
        Tabelao(22) = "Data da ocorr�ncia: " & Right(sin.dt_ocorrencia, 2) & "/" & Mid(sin.dt_ocorrencia, 5, 2) & "/" & Left(sin.dt_ocorrencia, 4)
        Tabelao(23) = "Evento causador do sinistro: " & sin.Causa
        Tabelao(24) = "Observa��o da ocorr�ncia:"
        
        For i = 25 To 34
            Tabelao(i) = SEGP0794_3.txtExigencia(i - 25).Text
        Next i
        
        Tabelao(35) = ""
        If Not cob_atingida Then
            i = 35
        Else
            qtd_cob = UBound(coberturas)
            If qtd_cob > 0 Then
                Tabelao(36) = "Cobertura(s) Afetada(s):"
            
                For i = 37 To 36 + qtd_cob
                    Tabelao(i) = coberturas(i - 36).tp_cobertura_id & " - " & coberturas(i - 36).Descricao
                Next i
            End If
        End If
        
        Tabelao(i) = ""
        Tabelao(i + 1) = "DADOS DO SOLICITANTE"
        Tabelao(i + 2) = "--------------------"
        Tabelao(i + 3) = ""
        Tabelao(i + 4) = "Nome: " & sin.SolicitanteNome
        Tabelao(i + 5) = "Endere�o: " & sin.SolicitanteEndereco
        Tabelao(i + 6) = "Bairro: " & sin.SolicitanteBairro
        Tabelao(i + 7) = "Munic�pio: " & sin.SolicitanteMunicipio
        Tabelao(i + 8) = "U.F.: " & sin.SolicitanteEstado
        
        Tabelao(i + 9) = "DDD: " & sin.ddd1 & _
                 " Telefone: " & sin.telefone1 & _
                 " Ramal: " & sin.ramal1 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone1)
        Tabelao(i + 10) = "DDD: " & sin.ddd2 & _
                 " Telefone: " & sin.telefone2 & _
                 " Ramal: " & sin.ramal2 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone2)
        Tabelao(i + 11) = "DDD: " & sin.ddd3 & _
                 " Telefone: " & sin.telefone3 & _
                 " Ramal: " & sin.ramal3 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone3)
        Tabelao(i + 12) = "DDD: " & sin.ddd4 & _
                 " Telefone: " & sin.telefone4 & _
                 " Ramal: " & sin.ramal4 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone4)
        Tabelao(i + 13) = "DDD: " & sin.ddd5 & _
                 " Telefone: " & sin.telefone5 & _
                 " Ramal: " & sin.ramal5 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone5)
        Tabelao(i + 14) = ""
        Tabelao(i + 15) = "E-mail: " & sin.SolicitanteEmail
        Tabelao(i + 16) = ""
        
        Tabelao(i + 17) = "AG�NCIA DE CONTATO"
        Tabelao(i + 18) = "------------------"
        Tabelao(i + 19) = ""
        Tabelao(i + 20) = "C�digo da ag�ncia: " & sin.AgenciaAviso & " DV: " & sin.AgenciaDvAviso
        Tabelao(i + 21) = "Nome: " & sin.AgenciaNome
        Tabelao(i + 22) = "------------------"
        
        maxTabelao = i + 22
    End If
    
    'Grava na evento_SEGBR_sinistro_detalhe_tb
    For i = 0 To maxTabelao
        sql = "exec evento_SEGBR_sinistro_detalhe_spi "
        sql = sql & sin.evento_id & ","
        sql = sql & "'" & "ANOTACAO" & "',"
        sql = sql & i + 1 & ","
        sql = sql & "'" & Tabelao(i) & "',"
        sql = sql & "'" & cUserName & "'"
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            sql, numCon, _
                            False)
    Next i
    
End Sub

Public Sub GravarDescricaoOcorrencia(sin As Sinistro)
Dim i As Long
Dim sql As String
Dim rc As ADODB.Recordset
Dim Linha As Long
    
    '----Grava na evento_SEGBR_sinistro_descri��o_tb
    l = 0
    
    '---Se for reanalise, gravar o motivo da reabertura
    If sin.Ind_reanalise = "S" Then
        For i = 0 To 4
        
            If Trim(SEGP0794_4_1.txtMotivo(i).Text) <> "" Then
                sql = "exec evento_SEGBR_sinistro_descricao_spi "
                sql = sql & sin.evento_id & ","
                sql = sql & l + 1 & ","
                sql = sql & "'" & SEGP0794_4_1.txtMotivo(i).Text & "',"
                sql = sql & "'" & cUserName & "'"
                
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sql, numCon, _
                                    False)
                
               
                             
                l = l + 1
           End If
        Next i
    Else
    
    '---Se n�o for reanalise, gravar as observa��es
        For i = 0 To 9
        
            's� insere se tiver algo
            If Trim(SEGP0794_3.txtExigencia(i).Text) <> "" Then
                sql = "exec evento_SEGBR_sinistro_descricao_spi "
                sql = sql & sin.evento_id & ","
                sql = sql & l + 1 & ","
                sql = sql & "'" & SEGP0794_3.txtExigencia(i).Text & "',"
                sql = sql & "'" & cUserName & "'"
                
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sql, numCon, _
                                    False)
                l = l + 1
            End If
        Next i
        
        'Se n�o tem observa��es, colocar a descri��o do evento de sinsitro
        If l = 0 Then
            sql = "exec evento_SEGBR_sinistro_descricao_spi "
            sql = sql & sin.evento_id & ","
            sql = sql & l + 1 & ","
            sql = sql & "'" & SEGP0794_3.cboEvento.Text & "',"
            sql = sql & "'" & cUserName & "'"
            
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                sql, numCon, _
                                False)
        End If
    End If
End Sub

Public Sub GravaGTREmissaoLayout(sin As Sinistro)

Dim sql As String

    sql = " exec interface_db..gtr_emissao_layout_CA_spi "
    sql = sql & sin.evento_id & ","
    sql = sql & sin.Evento_Segbr_id & ","
    sql = sql & "'" & cUserName & "'"
        
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                sql, numCon, _
                                False)
    
End Sub


Public Function Busca_Numero_Sinistro(ByRef sin As Sinistro) As Boolean
Dim rc As ADODB.Recordset
Dim vSql As String

    Busca_Numero_Sinistro = False
    On Error GoTo Trata_Erro
    
    vSql = "SET nocount on "
    vSql = vSql & "EXEC seguros_db..chave_sinistro_spu '" & Format(Trim(Str(sin.ramo_id)), "00") & "'," & _
                        sin.Sucursal_id & "," & sin.Seguradora_id & ",'" & cUserName & "'"
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    If Not rc.EOF Then
        sin.sinistro_id = rc("sinistro_id")
    End If
    Busca_Numero_Sinistro = True
    rc.Close
    Exit Function

Trata_Erro:
    TrataErroGeral "Busca N�mero Sinistro"
    Busca_Numero_Sinistro = False
    Exit Function
End Function

Public Function Busca_Numero_Remessa(ByRef sin As Sinistro) As Boolean
Dim rc As ADODB.Recordset
Dim vSql As String
Dim Chave As String
Dim num_remessa As Long

    Busca_Numero_Remessa = False
    On Error GoTo Trata_Erro
    
    Chave = "NUMERO REMESSA MQ"

    vSql = "SET nocount on "
    vSql = vSql & "EXEC seguros_db..chave_spu '" & Chave & "'," & _
                        "'MQ_USER','" & num_remessa & "'"
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    If Not rc.EOF Then
        sin.NumRemessa = Format(rc(0), "0000000000000000")
    End If
    Busca_Numero_Remessa = True
    rc.Close
    Exit Function

Trata_Erro:
    TrataErroGeral "Busca Numero Remessa"
    Busca_Numero_Remessa = False
End Function

Public Sub Obtem_Evento_SEGBR()
Dim rc As ADODB.Recordset
Dim vSql As String

    
    On Error GoTo Trata_Erro
    
    'Preenche as vari�veis para o aviso de sinistro
    vSql = " select evento_SEGBR_id, prox_localizacao_processo " & _
           " from evento_segbr_tb with(nolock)" & _
           " where evento_bb_id = " & Evento_bb_id
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    If Not rc.EOF Then
         Evento_Segbr_id = rc(0)
         Localizacao = rc(1)
    End If
    rc.Close
    
    Exit Sub

Trata_Erro:
    TrataErroGeral "Obtem_Evento_SEGBR", "Aviso Sinistro"
    Exit Sub
End Sub
